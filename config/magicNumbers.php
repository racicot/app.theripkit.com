<?php

return [
    'MEETINGLISTUSERLIMIT' => 4,
    'MEETINGMAILACTION' => 1,
    'ONE' => 1,
    'SUBONEMONTH' => 1,
    'REDUCEMONTH' => 30,
    'TODOUSERLIMIT' => 1,
    'TASKOVERVIEWUSERLIMIT' => 1,
    'QUEUEATTEMPTS' => 3
];