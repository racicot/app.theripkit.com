<?php

return [
    'UPDATEUSER' => 'User has been Updated Successfully.',
    'COMPANYREQUIRED' => 'Please select a company.',
    'PWDUPDATE' => 'Password has been updated successfully.',
    'PROFUPDATE' => 'Profile has been updated successfully.',
    'USERCREAT' => 'User has been created successfully.',
    'EMAILPWD' => 'Email with new password is successfully sent on user email.',
    'LOGINBGMTUPDATE' => 'Login background and motto updated successfully.',
    'USERDEL' => 'User has been deleted successfully.',
    'NOPERMISSIONTOCREATEUSER' => 'You do not have the permission to create user in this company.',
    'ROLECREATED' => 'Role has been created successfully.',
    'ROLEUPDATED' => 'Role has been updated successfully.',
    'ROLEDELETED' => 'Role has been deleted successfully.',
    'COMPANYSWITCH' => 'You have switched to "<company_name>" company.',
    'ACCESSDENIED'=> 'Access Denied.',

    'CATEGORYCREATED' => 'Category has been created successfully.',
    'CATEGORYUPDATED' => 'Category has been updated successfully.',

    'COMPANYCREATED' => 'Company has been created successfully.',
    'COMPANYUPDATED' => 'Company has been updated successfully.',
    'COMPANYDELETED' => 'Company has been deleted successfully.',
    'COMPANY-NOPERMISSION' => 'Dear user. You do not have the permission for this action.',
    'CANADDCOMPANYPARENT' => 'Add As Company Parent',
    'CANADDCOMPANYOWNER' => 'Add As Company Owner',
    'VALIDCSV' => 'The file you have uploaded is not a csv file. Please upload a valid csv file.',
    'CSVUPLOAD' => 'CSV has been uploaded successfully.',
    
    'RIPCREATE' => 'Rip Period has been created successfully.',
    'RIPUPDATE' => '',
    'RIPDELETE' => 'Your RIP Period has been successsfully deleted.',
    'DUPLICATERIP' => 'Same Rip Period already exists!',
    'EXPIREDRIP' => 'Your RIP Period is successsfully added to expired RIP Period section',
    'REVERTRIP' => 'Your RIP Period is successsfully reverted to current RIP Period section',
    'CURRENTRIP' => 'Your RIP Period is successsfully added to current RIP Period section',
    'RIPNOTASSOCIATED' => 'You are not associated with the selected RIP Period',
    'COMPANYREMOVED' => 'The selected company has been removed by the admin',
    'ASSOCIATIONREMOVED' => 'You are not associated with the selected company',
    
    'ASSOCIATEDWITH' => "You are not associated with '",
    'COMPANY' => "' company",
    'THECOMPANY' => "The company '",
    'HASBEENDEACTIVATED' => "' has been deactivated by admin",
    

];