<?php 

return [

    'id' => [

            'super_admin' => 3,
            'admin' => ''

    ],

    'email' =>[

            'super_admin' => 'noreply@theripkit.com',
            'announcement' => '',
    ],


    'csv_mime_type' => [
        'application/vnd.ms-excel',
        'text/csv',
    ],
    'csv_actions' => [
        'csv_deliminator' => env('CSV_COMPANY_DELIMINATOR', '/')
    ],

    'notification' => [
        'assign_rip_to_user' => 'Assign RIP To A User',
        'rip_due_date_update' => 'RIP Due Date Update',
        'rip_percentage_update' => 'RIP Percentage Update',
        'rip_name_update' => 'RIP Name Update',
        'delete_a_rip' => 'Delete A RIP',
        'comment_on_assign_rip' => 'Comment On Assign RIP',
        'mention_on_comments' => 'Mention On Comments ( @ )',
        'company_assigned_to_user' => 'Company Assigned To User',
        'edit_comment' => 'Edit Comment',
        'undo_task_status' => 'Undo Task Status',
        'delete_task' => 'Delete A Task',
        'update_task_name' => 'Update A Task',
        
        /* Meeting Schedule Text Localization */
        'meeting_schedule' => 'is inviting you to a',
        'meeting_cancel' => 'has cancelled the following',
        'meeting_schedule_subject' => 'RIPKIT: Zoom meeting scheduled for Rip',
        'meeting_cancel_subject' => 'RIPKIT: Zoom meeting cancelled for Rip',
        'scheduled_meeting' => ' is inviting you to a scheduled Zoom meeting.',
        'join_meeting' => 'Join Zoom Meeting',
        'topic' => 'Topic: ',
        'time' => 'Time: ',
        
        /* RIP Text Localization */
        'removed_user_rip' => 'Removed from RIP',
        'removed_from_rip' => 'You have been removed from the RIP ',
        'assigned_to_rip' => 'You have been assigned to the RIP ',
        'related_company' => ' of the company ',
        'rip' => 'The RIP ',
        'rip_deleted' => ' has been deleted.',
        'rip_progress' => 'The progress for the RIP ',
        'updated_to' => ' has been updated to ',
        'rip_due_date' => 'The due date of RIP ',
        'updated_from' => ' has been updated from ',
        'rip_name' => 'The RIP name for ',
        
        
        /* Task Text Localization */
        'assign_task_to_user' => 'Task Assigned',
        'assigned_to_task' => 'You have been assigned to the Task ',
        'under_rip' => ' under the RIP ',
        'removed_from_task' => 'You have been removed from the Task ',
        'removed_user' => 'Removed from Task',
        'mentioned_in_comment' => "You were mentioned in a comment:",
        'task_completed' => "Task Completed",
        'assigned_task' => "Your assigned task ",
        'mark_completed' => " has been marked as complete.",
        'comment_added' => "The comment: '",
        'comment_edited' => "' has been edited to '",
        'on_rip' => " on the RIP",
        'added_on_rip' => "' added on RIP:" ,
        'comment_on_rip' => "Comment Added against the RIP",
        'comment_updated' => "Comment Updated",
        'task_status' => "The status for the task ",
        'mark_incomplete' => "', has been changed to Incomplete.",
        'added_on_rip' => "' has been added on the RIP",
        /* Company Notifications Localization */
        'company_assigned' => "You have been associated with the company "
    ],
    
    'tour' => [
        'company_dropdown' => "Company Details",
        'company_details' => "From here, you may switch between different companies that you belong to.",
        'dashboard' => "Dashboard",
        'dashboard_text' => "From here, you can have a view of different tasks and rip periods regarding the company that you are logged in with.",
        'rip_period' => "Rip Period(s)",
        'rip_period_text' => "From here, you may access various rip periods - active and expired. You may even download a copy of these.",
        'portfolio' => "Snapshot",
        'portfolio_text' => "You may use this section to provide/view a snapshot of the organization.",
        'users' => "Users",
        'users_text' => "From here, you may add new users or manage the existing ones.",
        'company' => "Companies",
        'companies_text' => "From here, you may add new companies or manage the existing ones.",
        'roles' => "Manage Roles",
        'roles_text' => "From here, you may add new roles or manage the existing ones.",
        'csv_upload' => "CSV Upload",
        'csv_upload_text' => "From here, you may add/modify users in bulk.",
        'account' => 'Account Details',
        'account_text' => 'From here, you may manage your profile or log out from the application.',
        'to_do_list' => 'TO-DO List',
        'to_do_list_text' => 'From here, you may keep a track of your TO-DOs for the day.',
        'overview' => "Overview",
        'overview_text' => "From here, you can have a view of different Tasks and RIP(s) regarding all the companies that you are associated with.",
        'notification' => "Notifications",
        'notification_text' => "From here, you can check all the recent activities done on the companies associated with you.",
    ],
    
    'password' => [
        'update_password' => "Your Password has been updated by RIPKIT Admin.",
        'wrong_password' => "You are receiving this email because we suspected some suspicious activity. Someone might be trying to log into
                            your system. We suggest you to reset your password immediately.",
        'reset_password' => "You are receiving this email because we received a password reset request for your account."
    ],
    
    'roles' => [
        'one' => 'Super Admin',
        'superadmin' => 1
    ]


];
?>
