<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});
Route::get('/checkWelcomePopup', 'Auth\LoginController@checkWelcomePopup')->name('Check Welcome Popup');
Route::get('/updateWelcomePopup', 'Auth\LoginController@updateWelcomePopup')->name('Update Welcome Popup');

Auth::routes();

Route::get('/checkTourStatus', 'HomeController@checkTourValue')->name('Check Guided Tour Status');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/todo/{id}', 'HomeController@showTodo')->name('Show User Todo Items');
Route::get('/changeCompany/{id}', 'HomeController@changeCompany')->name('Change Company');

/* Routes for User Account Section */
Route::get('account', 'UserController@showAccount')->name('Access User Account Section');
Route::post('/profile/edit', 'UserController@editProfile')->name('Edit Profile');
Route::post('/account/{id}', 'UserController@updatePassword')->name('Update Password');
Route::post('/updateUserImage/{id}', 'UserController@updateUserImage')->name('Update Profile Image');
Route::post('/updateCompanyLogo/{id}', 'UserController@updateCompanyLogo')->name('Update Company Logo');
Route::post('loginview','UserController@updateLoginView')->name('Update Login Background and Quote');
Route::get('/user/resetPassword/{id}', 'UserController@userPasswordReset')->name('Update user password from user listing');
Route::get('/user/doneTask', 'UserController@getDoneTask')->name('Edit Profile');
Route::get('/user/notDoneTask', 'UserController@getNotDoneTask')->name('Edit Profile');
Route::post('/user/createTask', 'UserController@addTask')->name('Edit Profile');;
Route::put('/user/taskStatus/{taskId}', 'UserController@taskStatus')->name('Edit Profile');
Route::get('/user/clearDoneTask', 'UserController@clearDoneTask')->name('Edit Profile');

/* Routes for User Section */
Route::get('user/ajaxData', 'UserController@ajaxData')->name('View Users In User Section');
Route::get('userToken/{emailToken}', 'Auth\RegisterController@checkEmailToken');
Route::resource('user', 'UserController', ['names' => [
    'index' => 'Access User Section',
    'store' => 'Add New User',
    'edit' => 'Show Edit User Page',
    'update' => 'Update User Details',
    'destroy' => 'Delete A User'
]]);

/*Routes for Roles section */
Route::get('ajaxData', 'RoleController@ajaxData')->name('View Roles In Roles Section');
Route::resource('roles', 'RoleController', [ 'names' => [
    'index' => 'Access Roles Section',
    'store' => 'Add New Role',
    'edit' => 'Show Edit Permissions Page For A Role',
    'update' => 'Update Permissions For A Role',
    'destroy' => 'Delete A Role'
]]);


/*Routes for login */
Route::get('/verifyemail/{token}', 'Auth\RegisterController@verifyEmail');
Route::get('userLogin/{token}', 'Auth\RegisterController@userLogin')->name('Show Update Password');
Route::post('ajaxValidateLogin', 'Auth\LoginController@ajaxValidateLogin');
Route::post('ajaxLogin', 'Auth\LoginController@ajaxLogin');
Route::get('forgotPassword/{email}', 'Auth\LoginController@forgotPassword');
/* Route added for reset password with email & token */
Route::get('/password/reset/{token}/{email}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');

/* Route added for handling Wrong Password/Abusive Usage */
Route::post('bad_credentials', 'Auth\LoginController@wrongPassword');

/*Routes for social media login */
Route::get('/facebookRedirect', 'SocialAuthController@facebookRedirect');
Route::get('/facebookCallback', 'SocialAuthController@facebookCallback');
Route::get('/googleRedirect', 'SocialAuthController@googleRedirect');
Route::get('/googleCallback', 'SocialAuthController@googleCallback');

/*Routes for company */
Route::get('company/ajaxData', 'CompanyController@ajaxData')->name('Company AjaxData');

Route::get('company/showAssignedUsers', 'CompanyController@showAssignedUsers')->name('View Users Assigned To Comapny');
Route::get('company/companyUserList', 'CompanyController@companyUserList')->name('View Users Assigned To A Company');
Route::get('company/assignCompanyUser', 'CompanyController@assignCompanyUser')->name('Assign Users To A Company');

Route::resource('company', 'CompanyController',['names' => [
    'index' => 'Access Company Section',
    'store' => 'Add New Company',
    'edit' => 'Show Edit Company Page',
    'update' => 'Update Company Details',
    'destroy' => 'Delete A Company'
]]);

/*Routes for RIP */
Route::get('rip/{id}', 'RipController@index')->name('Access RIP Section');
Route::post('addRipGoal', 'RipController@addRipGoal')->name('Add A Goal In RIP');
Route::post('addCategory', 'RipController@addCategory')->name('Add A Category In RIP');
Route::get('deleteCategory/{id}', 'CategoryController@deleteCategory')->name('Delete A Category From RIP');
Route::get('task/user', 'RipController@taskUser')->name('View Users Assigned To A Task');
Route::get('goal/user', 'RipController@goalUser')->name('View Users Assigned To A Goal');
Route::post('ripAddTask/{id}', 'RipController@ripAddTask')->name('Add A Task In RIP');
Route::get('users/assignUserToAddedGoal', 'RipController@assignUserToAddedGoal')->name('Assign User To A RIP');
Route::get('task/assignUserToAddedtask', 'RipController@assignUserToAddedtask')->name('Assign User To A Task');
Route::get('goal/user/delete/{id}/{goalId}', 'RipController@goalUserDelete')->name('Delete User from A RIP');
Route::get('task/user/delete/{id}/{taskId}', 'RipController@taskUserDelete')->name('Delete User from A Task');
Route::post('goalPercentageUpdate/{goalId}', 'RipController@goalPercentageUpdate')->name('Update Goal Percentage');
Route::get('editGoal/{goalId}', 'RipController@editGoal')->name('Edit Goal Percentage');
Route::get('ripTabHistory', 'RipController@ripTabHistory')->name('Show RIP History');
Route::get('currentRip', 'RipController@currentRip')->name('Show Current RIP');
Route::get('deleteGoal/{id}', 'RipController@deleteGoal')->name('Delete Goal From RIP');
Route::get('deleteTask/{id}', 'RipController@deleteTask')->name('Delete Task From RIP');
Route::get('incompleteTask/{id}', 'RipController@incompleteTask')->name('Show Incomplete Tasks Of Goal');
Route::get('completeTask/{id}', 'RipController@completeTask')->name('Show Complete Tasks Of Goal');
Route::get('ajaxCommentSection/{goalId}', 'RipController@ajaxCommentSection')->name('Show Comment Section');
Route::get('taskComplete', 'RipController@taskComplete')->name('Complete A Task');
Route::get('taskUndo', 'RipController@taskUndo')->name('Undo A Completed Task');
Route::post('addRip', 'RipController@addRip')->name('Add New RIP');
Route::post('editRip', 'RipController@editRip')->name('Edit A RIP');
Route::get('ripTab/{id}', 'RipController@ripTab')->name('Switch Between Rips');
Route::get('viewHistoryRip/{id}', 'RipController@viewHistoryRip')->name('View History Rip');
Route::get('rip/deleteTab/{id}', 'RipController@deleteTab')->name('Delete A Rip');
Route::get('rip/markExpired/{id}', 'RipController@markExpiredRip')->name('Mark Expired');
Route::get('editCategory/{id}','CategoryController@edit')->name('Edit Category Form');
Route::get('editRipPeriod/{id}','RipController@editRipPeriod')->name('Show Edit RIP Period Page');
Route::get('editRipTab/{id}','RipController@editRipTabName')->name('Show Edit RIP Page');
Route::get('editTask/{taskId}', 'RipController@editTask')->name('Edit Task');
Route::get('goal/userList', 'RipController@goalUserList')->name('View Users Assigned To A Goal');
Route::post('createMeeting','ScheduleMeeting@createMeeting')->name('Show Edit RIP Period Page');
Route::get('getCompanyUser', 'ScheduleMeeting@getCompanyUserList')->name('Show Edit RIP Period Page');
Route::get('getMeetings/{ripId}', 'ScheduleMeeting@getMeetings')->name('Show Edit RIP Period Page');
Route::get('getParticipants/{meetingId}', 'ScheduleMeeting@getParticipants')->name('Show Edit RIP Period Page');
Route::get('deleteMeeting/{meetingId}', 'ScheduleMeeting@deleteMeeting')->name('Show Edit RIP Period Page');
Route::get('task/userList', 'RipController@taskUserList')->name('View Users Assigned To A Task');

Route::get('goal/showAssignedUser', 'RipController@showAssignedUsers')->name('View Users Assigned To A Goal');
Route::get('task/showAssignedUser', 'RipController@showAssignedTaskUsers')->name('View Users Assigned To A Goal');
Route::get('overview-task/{id?}', 'RipController@taskOverview')->name('Add A Task In RIP');
Route::get('assigned/user/jsonData', 'CommentController@jsonData')->name('Show User Suggestions In Comment Section');

Route::get('overview-rip/{id?}', 'RipController@ripOverview')->name('View overview By Rips');


Route::get('/goalDueDate', 'RipController@goalDueDate')->name('Update The Due Date Of A Goal');
Route::post('/goalName', 'RipController@goalName')->name('Update The Name Of A Goal');
Route::get('/taskName', 'RipController@taskName')->name('Update The Name Of A Task');
Route::post('/categoryName', 'CategoryController@categoryName')->name('Update The Name Of A Category');
Route::get('/allTasks/{id}', 'RipController@allTasks')->name('Show All Tasks');
Route::get('showGoalTask/{id}', 'RipController@showGoalTask')->name('Show All Tasks Of Goal');
Route::get('/rip/duplicateRip/{id}', 'RipController@duplicateRip')->name('Duplicate Rip');
Route::get('/rip/revertExpiredRip/{id}', 'RipController@revertExpiredRip')->name('Revert Expired Rip');


Route::get('/readNotification/{id}', 'RipController@readNotification')->name('Mark Notification As Read');
Route::get('notificationData', 'RipController@notificationData')->name('Get Notification Data');



/* Pusher routes*/

Route::post('pusher/showTask', 'RipController@showTask')->name('Broadcast Task data');
Route::post('pusher/updateTask', 'RipController@updateTask')->name('Broadcast updated data in Task');
Route::post('pusher/updateGoal', 'RipController@updateGoal')->name('Broadcast updated data in Goal');
Route::post('pusher/showGoal', 'RipController@showGoal')->name('Broadcast Goal data');
Route::post('pusher/showName', 'RipController@showName')->name('Broadcast Goal Name');
Route::post('pusher/showTaskName', 'RipController@showTaskName')->name('Broadcast Task Name');
Route::post('pusher/showCategoryName', 'CategoryController@showCategoryName')->name('Broadcast Category Name');
Route::post('pusher/showDueDate', 'RipController@showDueDate')->name('Broadcast Goal Due Date');
Route::post('pusher/showPercentage', 'RipController@showPercentage')->name('Broadcast Goal Percentage');
Route::post('pusher/assignUser', 'RipController@assignUser')->name('Broadcast Assigned user to goal');
Route::post('pusher/removeUser', 'RipController@removeUser')->name('Broadcast Removed user from goal');
Route::post('pusher/addComment', 'CommentController@addComment')->name('Broadcast New Comment');
Route::post('pusher/editComment', 'CommentController@editComment')->name('Broadcast Updated Comment');
Route::post('pusher/broadcastCategory', 'RipController@broadcastCategory')->name('Broadcast New Category');
Route::post('pusher/editRipTab', 'RipController@editRipTab')->name('Broadcast Rip Edit');
Route::post('pusher/addRipTab', 'RipController@addRipTab')->name('Broadcast Newly Added Rip');
Route::post('pusher/portfolioUpdate', 'PortfolioController@portfolioUpdate')->name('Broadcast Updated Portfolio');
Route::post('pusher/showCompletedTask', 'RipController@showCompletedTask')->name('Broadcast Completed Task Data');
Route::post('pusher/showUndoTask', 'RipController@showUndoTask')->name('Broadcast Mark As Incomplete task Data');

//Route::get('rip', 'RipController@index')->name('Rip index page');

/*Routes for Team Member */
Route::get('team/ajaxData', 'TeamMemberController@ajaxData')->name('Team Member Ajaxdata');
Route::resource('team', 'TeamMemberController');

/* Routes for Comments */
Route::get('comment/commentTaskUser/{id}', 'CommentController@commentTaskUser')->name('Add User To A Comment');
Route::get('comments/deleteComment/{id}', 'CommentController@deleteComment')->name('Delete A Comment');
Route::get('rip/assigned/user/jsonData', 'CommentController@jsonData')->name('Show User Suggestions In Comment Section');
Route::get('deleteAttachment/{id}', 'CommentController@deleteAttachment')->name('Delete Attachment from comment');
Route::resource('comments', 'CommentController', ['names' => [
    'store' => 'Create New Comment',
    'update' => 'Edit A Comment',
]]);
Route::get('editComment/{CommentId}', 'CommentController@edit')->name('Edit Comment');
Route::post('comments/update/{CommentId}', 'CommentController@update')->name('Update Comment');

/* Routes for Portfolio */
Route::get('portfolio', 'PortfolioController@index')->name('Access Portfolio Section');
Route::get('portfolio/save', 'PortfolioController@save')->name('Update Portfolio Content');
Route::get('portfolio/download', 'PortfolioController@download')->name('Download Portfolio');



/* Routes for CSV upload */
Route::get('massUpload', 'CsvUploadController@massUpload')->name('Access CSV Upload Section');
Route::get('sampleCsv', 'CsvUploadController@sampleCsv')->name('Download Sample CSV');
Route::get('csvData', 'CsvUploadController@csvData')->name('List All CSV Files');
Route::post('updateCsvData', 'CsvUploadController@updateCsvData')->name('Add User Data from CSV');
Route::post('validateCsv', 'CsvUploadController@validateCsv')->name('Validate CSV File');


/* Routes for Rip Period excel export*/
Route::get('ripPeriodExport', 'RipExportController@downloadExcel')->name('Export Rip Period');
/* Routes for laravel logs*/
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

//Wordpress related url
Route::get('/getRipkitRipdetails', 'CheckLoginController@getRipkitRipdetails');


