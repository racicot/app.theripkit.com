# BuildApp (Laravel 5 App)

### Prerequisites 
* php v7.1.x, [see](https://laravel.com/docs/5.4/installation) Laravel specific requirements
* Apache  with ```mod_rewrite```
* MySql v5.6.x
* [Composer](https://getcomposer.org) v1.0.0
* [node-js](https://github.com/creationix/nvm) >=6.10.2 and npm >=5.3.0

### Quick setup ###
* Clone this repo, checkout to ```dev``` branch
* Install dependencies
```
composer install
npm install
```
* Write permissions on ```storage``` and ```bootstrap/cache``` folders
* Create config (copy from ```.env.example```), and update environment variables in ```.env``` file
```
cp .env.example .env
php artisan key:generate
```
* Migrate and Seed database
```
php artisan migrate
php artisan db:seed
```
* Create the symbolic link for local file uploads
```
php artisan storage:link
```
* Point your web server to **public** folder of this project
* Additionally you can run these commands on production server
```
php artisan route:cache
php artisan config:cache
```
* You should rebuild these cache on each new deployment