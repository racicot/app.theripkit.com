<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedule_meetings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('agenda', 256);
            $table->date('meeting_date');
            $table->time('meeting_time');
            $table->integer('rips_id');
            $table->integer('companies_id');
            $table->string('duration', 100);
            $table->string('participants', 100);
            $table->string('meeting_uuid', 100);
            $table->integer('meeting_id');
            $table->text('start_url');
            $table->string('join_url', 256);
            $table->string('host_id', 256);
            $table->integer('created_by');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedule_meetings');
    }
}
