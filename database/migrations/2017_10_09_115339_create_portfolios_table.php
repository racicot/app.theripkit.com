<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfoliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('portfolios', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('company_id')->unsigned();
            $table->foreign('company_id')->references('id')->on('companies');
            $table->longText('what')->nullable();
            $table->longText('how')->nullable();
            $table->longText('elevator')->nullable();
            $table->longText('customer_problem')->nullable();
            $table->longText('why')->nullable();
            $table->longText('strategic')->nullable();
            $table->longText('customer_solution')->nullable();
            $table->longText('financial')->nullable();
            $table->longText('behavioural')->nullable();
            $table->longText('key_activities_resource')->nullable();
            $table->longText('key_partner')->nullable();
            $table->longText('value_proposition')->nullable();
            $table->longText('key_customer_segments')->nullable();
            $table->longText('revenue_stream')->nullable();
            $table->longText('key_cost')->nullable();
            $table->longText('sales_channel')->nullable();
            $table->longText('economic')->nullable();
            $table->longText('emotional')->nullable();
            $table->longText('functional')->nullable();
            $table->string('promise')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('portfolios');

    }
}
