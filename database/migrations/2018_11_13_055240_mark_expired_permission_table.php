<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class MarkExpiredPermissionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {   

        $exists = Permission::where('name', 'Mark Expired')->first();
        if($exists){
            Permission::where('name', 'Mark Expired')->first()->delete();//...remove previous row created from seed.
        }    

        Permission::create([  'name' => 'Mark Expired', 'guard_name' => 'web', 'group_name' => 'RIP Period']);
        $superAdmin = Role::where('name', 'Super Admin')->first();                
        $superAdmin->givePermissionTo('Mark Expired');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
