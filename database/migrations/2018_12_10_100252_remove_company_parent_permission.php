<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;

class RemoveCompanyParentPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // delete permission for adding as Company Parent
        $permission = Permission::where('name', 'Add As Company Parent')->first();
        if($permission) {
            Permission::where('name',"Add As Company Parent")->first()->delete();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // rollback migration by creating permission for adding as company Parent
        $permission = Permission::where('name', 'Add As Company Parent')->first();
        if(!$permission) {
            Permission::create([  'name' => 'Add As Company Parent', 'guard_name' => 'web', 'group_name' => 'Users']);  
        }
    }
}
