<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AddAssignUserTocompanyPermissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Permission::create([  'name' => 'Assign Users To Company From Company Section', 'guard_name' => 'web', 'group_name' => 'Company']);  

        $superAdmin = Role::where('name', 'Super Admin')->first();                
        $superAdmin->givePermissionTo('Assign Users To Company From Company Section');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            //
        });
    }
}
