<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateNotificationTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $notification_types = [
            ['name' => 'Assign RIP to a user', 'action' => '0'],
            ['name' => 'Comment On Assign RIP', 'action' => '0'],
            ['name' => 'Company Assigned To User', 'action' => '0'],
            ['name' => 'Delete A RIP', 'action' => '0'],
            ['name' => 'Mention On Comments ( @ )', 'action' => '0'],
            ['name' => 'RIP Due Date Update', 'action' => '0'],
            ['name' => 'RIP Name Update', 'action' => '0'],
            ['name' => 'RIP Percentage Update', 'action' => '0'],
        ];
        DB::table('notification_types')->insert($notification_types);      
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
