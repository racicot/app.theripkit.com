<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Carbon\Carbon;

class UpdateExpireToRipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('rips', function($table) {
            $table->integer('expired')->default(0);
        });

        //$results = DB::table('rips')->select('id','name')->get();

        $currentdate = Carbon::now()->format('Y/m/d');
        DB::table('rips')
            ->where('end_date', '<', $currentdate)
            ->update([
                "expired" => "1"
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::table('rips', function($table) {
             $table->dropColumn('expired');
          });
    }
}
