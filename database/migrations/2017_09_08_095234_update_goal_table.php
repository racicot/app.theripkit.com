<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateGoalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          
         Schema::table('goals', function(Blueprint $table) {
            
            $table->integer('percentage_complete')->nullable()->default(0);
            $table->integer('status')->nullable()->default(0);
            $table->string('assign_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // Schema::table('goals', function(Blueprint $table){
           
      //      $table->dropColumn('percentage_complete');
      //      $table->dropColumn('status');
      //      $table->dropColumn('assign_to');
      //   });
    }
}
