<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class UpdatePermissionsForSuperAdmin extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $superAdmin = Role::where('name', 'Super Admin')->first();
        
        if(!($superAdmin->hasPermissionTo('Assign Companies To User From User Section'))) {
            $superAdmin->givePermissionTo('Assign Companies To User From User Section');
        }
        if(!($superAdmin->hasPermissionTo('Update The Name Of A Category'))) {
            $superAdmin->givePermissionTo('Update The Name Of A Category');
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
