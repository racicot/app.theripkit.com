<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $roles = Role::all();
        DB::table('role_has_permissions')->delete();
        $roles->each(function ($item) {
            $item->delete();
        });
        Role::create(['id' => 1, 'name' => 'Super Admin']);
        Role::create(['id' => 2, 'name' => 'Admin']);
        Role::create(['id' => 3, 'name' => 'Account Manager']);
        Role::create(['id' => 4, 'name' => 'Company Owner']);

        DB::table('model_has_roles')->insert([
            ['role_id' => 1, 'model_id' => 1, 'model_type' => 'App\\User'],
        ]);
        app()['cache']->forget('spatie.permission.cache');
    }
}
