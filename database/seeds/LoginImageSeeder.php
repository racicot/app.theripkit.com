<?php

use Illuminate\Database\Seeder;

class LoginImageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\LoginImage::firstOrCreate(
            [
                'name' => 'login-bg-1.jpg'
            ],
            [
                'name' => 'login-bg-1.jpg',
                'motto' => 'Soon is not as good as now'
            ]
        );
    }
}
