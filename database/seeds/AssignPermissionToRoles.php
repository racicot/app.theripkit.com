<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class AssignPermissionToRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $superAdmin = Role::where('name', 'Super Admin')->first();
        $admin = Role::where('name', 'Admin')->first();
        $manager = Role::where('name', 'Account Manager')->first();
        $owner = Role::where('name', 'Company Owner')->first();
        app()['cache']->forget('spatie.permission.cache');
        /* Assign Permissions to SUPER ADMIN */
        /* Company */
        $superAdmin->givePermissionTo('Access Company Section');
        $superAdmin->givePermissionTo('Add New Company');
        $superAdmin->givePermissionTo('Show Edit Company Page');
        $superAdmin->givePermissionTo('Update Company Details');
        $superAdmin->givePermissionTo('Delete A Company');
        $superAdmin->givePermissionTo('Update Company Parents');

        /* User */
        $superAdmin->givePermissionTo('Access User Section');
        $superAdmin->givePermissionTo('Add New User');
        $superAdmin->givePermissionTo('Show Edit User Page');
        $superAdmin->givePermissionTo('Update User Details');
        $superAdmin->givePermissionTo('Delete A User');

        /* CSV Upload */
        $superAdmin->givePermissionTo('Access CSV Upload Section');

        /* User Account */
        $superAdmin->givePermissionTo('Access User Account Section');
        $superAdmin->givePermissionTo('Edit Profile');
        $superAdmin->givePermissionTo('Update Password');
        $superAdmin->givePermissionTo('Update Profile Image');
        $superAdmin->givePermissionTo('Update Company Logo');
        $superAdmin->givePermissionTo('Update Login Background and Quote');

        /* RIP Section */
        $superAdmin->givePermissionTo('Access RIP Section');
        $superAdmin->givePermissionTo('Add A Goal In RIP');
        $superAdmin->givePermissionTo('Add A Category In RIP');
        $superAdmin->givePermissionTo('Update The Name Of A Category');
        $superAdmin->givePermissionTo('Delete A Category From RIP');
        $superAdmin->givePermissionTo('View Users Assigned To A Task');
        $superAdmin->givePermissionTo('View Users Assigned To A Goal');
        $superAdmin->givePermissionTo('Add A Task In RIP');
        $superAdmin->givePermissionTo('Assign User To A RIP');
        $superAdmin->givePermissionTo('Assign User To A Task');
        $superAdmin->givePermissionTo('Delete User from A RIP');
        $superAdmin->givePermissionTo('Delete User from A Task');
        $superAdmin->givePermissionTo('Update Goal Percentage');
        $superAdmin->givePermissionTo('Show RIP History');
        $superAdmin->givePermissionTo('Show Current RIP');
        $superAdmin->givePermissionTo('Delete Goal From RIP');
        $superAdmin->givePermissionTo('Delete Task From RIP');
        $superAdmin->givePermissionTo('Show Incomplete Tasks Of Goal');
        $superAdmin->givePermissionTo('Show Complete Tasks Of Goal');
        $superAdmin->givePermissionTo('Show Comment Section');
        $superAdmin->givePermissionTo('Complete A Task');
        $superAdmin->givePermissionTo('Undo A Completed Task');
        $superAdmin->givePermissionTo('Add New RIP');
        $superAdmin->givePermissionTo('Edit A RIP');
        $superAdmin->givePermissionTo('Switch Between Rips');
        $superAdmin->givePermissionTo('Delete A Rip');
        $superAdmin->givePermissionTo('Update The Due Date Of A Goal');
        $superAdmin->givePermissionTo('Update The Name Of A Goal');
        $superAdmin->givePermissionTo('Mark Expired');

        /* Comment Section */
        $superAdmin->givePermissionTo('Add New Comment');
        $superAdmin->givePermissionTo('Edit A Comment');
        $superAdmin->givePermissionTo('Add User To Comment');
        $superAdmin->givePermissionTo('Show User Suggestions In Comment Section');
        $superAdmin->givePermissionTo('Delete Comment');

        /* Portfolio Section */
        $superAdmin->givePermissionTo('Access Portfolio Section');
        $superAdmin->givePermissionTo('Update Portfolio Content');
        $superAdmin->givePermissionTo('Download Portfolio');

        /* Roles Section */
        $superAdmin->givePermissionTo('Access Roles Section');
        $superAdmin->givePermissionTo('Add New Role');
        $superAdmin->givePermissionTo('Show Edit Permissions Page For A Role');
        $superAdmin->givePermissionTo('Update Permissions For A Role');
        $superAdmin->givePermissionTo('Delete A Role');
        $superAdmin->givePermissionTo('Create Admin');
        $superAdmin->givePermissionTo('Create Account Manager');
        $superAdmin->givePermissionTo('Create Company Owner');

        /* Assign Permissions to ADMIN */
        /* Company */
        $admin->givePermissionTo('Access Company Section');
        $admin->givePermissionTo('Add New Company');
        $admin->givePermissionTo('Show Edit Company Page');
        $admin->givePermissionTo('Update Company Details');
        $admin->givePermissionTo('Delete A Company');
        $admin->givePermissionTo('Update Company Parents');

        /* User */
        $admin->givePermissionTo('Access User Section');
        $admin->givePermissionTo('Add New User');
        $admin->givePermissionTo('Show Edit User Page');
        $admin->givePermissionTo('Update User Details');
        $admin->givePermissionTo('Delete A User');

        /* User Account */
        $admin->givePermissionTo('Access User Account Section');
        $admin->givePermissionTo('Edit Profile');
        $admin->givePermissionTo('Update Password');
        $admin->givePermissionTo('Update Profile Image');
        $admin->givePermissionTo('Update Company Logo');
        $admin->givePermissionTo('Update Login Background and Quote');

        /* RIP Section*/
        $admin->givePermissionTo('Access RIP Section');
        $admin->givePermissionTo('Add A Goal In RIP');
        $admin->givePermissionTo('Add A Category In RIP');
        $admin->givePermissionTo('Delete A Category From RIP');
        $admin->givePermissionTo('View Users Assigned To A Task');
        $admin->givePermissionTo('View Users Assigned To A Goal');
        $admin->givePermissionTo('Add A Task In RIP');
        $admin->givePermissionTo('Assign User To A RIP');
        $admin->givePermissionTo('Assign User To A Task');
        $admin->givePermissionTo('Delete User from A RIP');
        $admin->givePermissionTo('Delete User from A Task');
        $admin->givePermissionTo('Update Goal Percentage');
        $admin->givePermissionTo('Show RIP History');
        $admin->givePermissionTo('Show Current RIP');
        $admin->givePermissionTo('Delete Goal From RIP');
        $admin->givePermissionTo('Delete Task From RIP');
        $admin->givePermissionTo('Show Incomplete Tasks Of Goal');
        $admin->givePermissionTo('Show Complete Tasks Of Goal');
        $admin->givePermissionTo('Show Comment Section');
        $admin->givePermissionTo('Complete A Task');
        $admin->givePermissionTo('Add New RIP');
        $admin->givePermissionTo('Edit A RIP');
        $admin->givePermissionTo('Switch Between Rips');
        $admin->givePermissionTo('Delete A Rip');
        $admin->givePermissionTo('Update The Due Date Of A Goal');
        $admin->givePermissionTo('Update The Name Of A Goal');

        /* Comment Section */
        $admin->givePermissionTo('Add New Comment');
        $admin->givePermissionTo('Edit A Comment');
        $admin->givePermissionTo('Add User To Comment');
        $admin->givePermissionTo('Show User Suggestions In Comment Section');
        $admin->givePermissionTo('Delete Comment');

        /* Portfolio Section */
        $admin->givePermissionTo('Access Portfolio Section');
        $admin->givePermissionTo('Update Portfolio Content');
        $admin->givePermissionTo('Download Portfolio');

        /* Roles Section */
        $admin->givePermissionTo('Access Roles Section');
        $admin->givePermissionTo('Add New Role');
        $admin->givePermissionTo('Show Edit Permissions Page For A Role');
        $admin->givePermissionTo('Update Permissions For A Role');
        $admin->givePermissionTo('Delete A Role');
        $admin->givePermissionTo('Create Account Manager');
        $admin->givePermissionTo('Create Company Owner');

        /* Assign Permissions to 'ACCOUNT MANAGER */
        /* Company */
        $manager->givePermissionTo('Access Company Section');
        $manager->givePermissionTo('Show Edit Company Page');
        $manager->givePermissionTo('Update Company Details');

        /* User */
        $manager->givePermissionTo('Access User Section');
        $manager->givePermissionTo('Show Edit User Page');
        $manager->givePermissionTo('Update User Details');
        $manager->givePermissionTo('Add As Company Parent');

        /* User Account */
        $manager->givePermissionTo('Access User Account Section');
        $manager->givePermissionTo('Edit Profile');
        $manager->givePermissionTo('Update Password');
        $manager->givePermissionTo('Update Profile Image');
        $manager->givePermissionTo('Update Company Logo');
        $manager->givePermissionTo('Update Login Background and Quote');

        /* RIP Section */
        $manager->givePermissionTo('Access RIP Section');
        $manager->givePermissionTo('Add A Goal In RIP');
        $manager->givePermissionTo('Add A Category In RIP');
        $manager->givePermissionTo('Delete A Category From RIP');
        $manager->givePermissionTo('View Users Assigned To A Task');
        $manager->givePermissionTo('View Users Assigned To A Goal');
        $manager->givePermissionTo('Add A Task In RIP');
        $manager->givePermissionTo('Assign User To A RIP');
        $manager->givePermissionTo('Assign User To A Task');
        $manager->givePermissionTo('Delete User from A RIP');
        $manager->givePermissionTo('Delete User from A Task');
        $manager->givePermissionTo('Update Goal Percentage');
        $manager->givePermissionTo('Show RIP History');
        $manager->givePermissionTo('Show Current RIP');
        $manager->givePermissionTo('Delete Goal From RIP');
        $manager->givePermissionTo('Delete Task From RIP');
        $manager->givePermissionTo('Show Incomplete Tasks Of Goal');
        $manager->givePermissionTo('Show Complete Tasks Of Goal');
        $manager->givePermissionTo('Show Comment Section');
        $manager->givePermissionTo('Complete A Task');
        $manager->givePermissionTo('Add New RIP');
        $manager->givePermissionTo('Edit A RIP');
        $manager->givePermissionTo('Switch Between Rips');
        $manager->givePermissionTo('Delete A Rip');
        $manager->givePermissionTo('Update The Due Date Of A Goal');
        $manager->givePermissionTo('Update The Name Of A Goal');

        /* Comment Section */
        $manager->givePermissionTo('Add New Comment');
        $manager->givePermissionTo('Edit A Comment');
        $manager->givePermissionTo('Add User To Comment');
        $manager->givePermissionTo('Show User Suggestions In Comment Section');
        $manager->givePermissionTo('Delete Comment');

        /* Portfolio Section */
        $manager->givePermissionTo('Access Portfolio Section');
        $manager->givePermissionTo('Update Portfolio Content');
        $manager->givePermissionTo('Download Portfolio');

        /* Assign Permissions to 'COMPANY OWNER */
        /* Company */
        $owner->givePermissionTo('Access Company Section');
        $owner->givePermissionTo('Show Edit Company Page');
        $owner->givePermissionTo('Update Company Details');

        /* User */
        $owner->givePermissionTo('Access User Section');
        /*$manager->givePermissionTo('Show Edit User Page');
        $manager->givePermissionTo('Update User Details');*/
        $owner->givePermissionTo('Add As Company Owner');

        /* User Account */
        $owner->givePermissionTo('Access User Account Section');
        $owner->givePermissionTo('Edit Profile');
        $owner->givePermissionTo('Update Password');
        $owner->givePermissionTo('Update Profile Image');
        $owner->givePermissionTo('Update Company Logo');
        $owner->givePermissionTo('Update Login Background and Quote');

        /* RIP Section*/
        $owner->givePermissionTo('Access RIP Section');
        $owner->givePermissionTo('Add A Goal In RIP');
        $owner->givePermissionTo('Add A Category In RIP');
        $owner->givePermissionTo('Delete A Category From RIP');
        $owner->givePermissionTo('View Users Assigned To A Task');
        $owner->givePermissionTo('View Users Assigned To A Goal');
        $owner->givePermissionTo('Add A Task In RIP');
        $owner->givePermissionTo('Assign User To A RIP');
        $owner->givePermissionTo('Assign User To A Task');
        $owner->givePermissionTo('Delete User from A RIP');
        $owner->givePermissionTo('Delete User from A Task');
        $owner->givePermissionTo('Update Goal Percentage');
        $owner->givePermissionTo('Show RIP History');
        $owner->givePermissionTo('Show Current RIP');
        $owner->givePermissionTo('Delete Goal From RIP');
        $owner->givePermissionTo('Delete Task From RIP');
        $owner->givePermissionTo('Show Incomplete Tasks Of Goal');
        $owner->givePermissionTo('Show Complete Tasks Of Goal');
        $owner->givePermissionTo('Show Comment Section');
        $owner->givePermissionTo('Complete A Task');
        $owner->givePermissionTo('Add New RIP');
        $owner->givePermissionTo('Edit A RIP');
        $owner->givePermissionTo('Switch Between Rips');
        $owner->givePermissionTo('Delete A Rip');
        $owner->givePermissionTo('Update The Due Date Of A Goal');
        $owner->givePermissionTo('Update The Name Of A Goal');

        /* Comment Section */
        $owner->givePermissionTo('Add New Comment');
        $owner->givePermissionTo('Edit A Comment');
        $owner->givePermissionTo('Add User To Comment');
        $owner->givePermissionTo('Show User Suggestions In Comment Section');
        $owner->givePermissionTo('Delete Comment');

        /* Portfolio Section */
        $owner->givePermissionTo('Access Portfolio Section');
        $owner->givePermissionTo('Update Portfolio Content');
        $owner->givePermissionTo('Download Portfolio');

        app()['cache']->forget('spatie.permission.cache');

    }
}
