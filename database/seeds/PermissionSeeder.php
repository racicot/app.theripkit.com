<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\DB;

class PermissionSeeder extends Seeder
{
    private $counter = 0;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $permissions = Permission::all();
        $permissions->each(function ($item) {
            $item->delete();
        });

        app()['cache']->forget('spatie.permission.cache');

        /* Permissions for Company Section*/
        Permission::create([  'name' => 'Access Company Section', 'guard_name' => 'web', 'group_name' => 'Company']);
        Permission::create([  'name' => 'Add New Company', 'guard_name' => 'web', 'group_name' => 'Company']);
        Permission::create([  'name' => 'Show Edit Company Page', 'guard_name' => 'web', 'group_name' => 'Company']);
        Permission::create([  'name' => 'Update Company Details', 'guard_name' => 'web', 'group_name' => 'Company']);
        Permission::create([  'name' => 'Delete A Company', 'guard_name' => 'web', 'group_name' => 'Company']);
        Permission::create([  'name' => 'Update Company Parents', 'guard_name' => 'web', 'group_name' => 'Company']);

        /* Permisisions for User Section*/
        Permission::create([  'name' => 'Access User Section', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Add New User', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Show Edit User Page', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([ 'name' => 'Update User Details', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Delete A User', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Add As Company Parent', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Add As Company Owner', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Create Admin', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Create Account Manager', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Create Company Owner', 'guard_name' => 'web', 'group_name' => 'Users']);
        Permission::create([  'name' => 'Assign Companies To User From User Section', 'guard_name' => 'web', 'group_name' => 'Users']);

        /* Permissions for User Account Section */
        Permission::create([  'name' => 'Access User Account Section', 'guard_name' => 'web', 'group_name' => 'User Account']);
        Permission::create([  'name' => 'Edit Profile', 'guard_name' => 'web', 'group_name' => 'User Account']);
        Permission::create([  'name' => 'Update Password', 'guard_name' => 'web', 'group_name' => 'User Account']);
        Permission::create([  'name' => 'Update Profile Image', 'guard_name' => 'web', 'group_name' => 'User Account']);
        Permission::create([  'name' => 'Update Company Logo', 'guard_name' => 'web', 'group_name' => 'User Account']);
        Permission::create([  'name' => 'Update Login Background and Quote', 'guard_name' => 'web', 'group_name' => 'User Account']);

        /* Permission for Roles Section */
        Permission::create([  'name' => 'Access Roles Section', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Add New Role', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Show Edit Permissions Page For A Role', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Update Permissions For A Role', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Delete A Role', 'guard_name' => 'web', 'group_name' => null]);

        /* Permissions for RIP Period Section */
        Permission::create([  'name' => 'Access RIP Section', 'guard_name' => 'web', 'group_name' => 'RIP Period']);
        Permission::create([  'name' => 'Show RIP History', 'guard_name' => 'web', 'group_name' => 'RIP Period']);
        Permission::create([  'name' => 'Show Current RIP', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Add New RIP', 'guard_name' => 'web', 'group_name' => 'RIP Period']);
        Permission::create([  'name' => 'Edit A RIP', 'guard_name' => 'web', 'group_name' => 'RIP Period']);
        Permission::create([  'name' => 'Switch Between Rips', 'guard_name' => 'web', 'group_name' => null]);
        Permission::create([  'name' => 'Delete A Rip', 'guard_name' => 'web', 'group_name' => 'RIP Period']);

        /* Permissions for RIP Section */
        Permission::create([  'name' => 'Add A Goal In RIP', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'View Users Assigned To A Goal', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Assign User To A RIP', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Delete User from A RIP', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Update Goal Percentage', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Delete Goal From RIP', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Update The Due Date Of A Goal', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Update The Name Of A Goal', 'guard_name' => 'web', 'group_name' => 'RIP']);
        Permission::create([  'name' => 'Mark Expired', 'guard_name' => 'web', 'group_name' => 'RIP']);        


        /* Permissions for Task Section */
        Permission::create([  'name' => 'View Users Assigned To A Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Add A Task In RIP', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Assign User To A Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Delete User from A Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Delete Task From RIP', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Complete A Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Undo A Completed Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Update The Name Of A Task', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Show All Tasks', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Show Incomplete Tasks Of Goal', 'guard_name' => 'web', 'group_name' => 'Task']);
        Permission::create([  'name' => 'Show Complete Tasks Of Goal', 'guard_name' => 'web', 'group_name' => 'Task']);

        /* Permissions for Category Section */
        Permission::create([  'name' => 'Add A Category In RIP', 'guard_name' => 'web', 'group_name' => 'Category']);
        Permission::create([  'name' => 'Delete A Category From RIP', 'guard_name' => 'web', 'group_name' => 'Category']);
        Permission::create([  'name' => 'Update The Name Of A Category', 'guard_name' => 'web', 'group_name' => 'Category']);



        /* Permissions for Comment Section */
        Permission::create([  'name' => 'Add New Comment', 'guard_name' => 'web', 'group_name' => 'Comment']);
        Permission::create([  'name' => 'Edit A Comment', 'guard_name' => 'web', 'group_name' => 'Comment']);
        Permission::create([  'name' => 'Add User To Comment', 'guard_name' => 'web', 'group_name' => 'Comment']);
        Permission::create([  'name' => 'Show User Suggestions In Comment Section', 'guard_name' => 'web', 'group_name' => 'Comment']);
        Permission::create([  'name' => 'Delete Comment', 'guard_name' => 'web', 'group_name' => 'Comment']);
        Permission::create([  'name' => 'Show Comment Section', 'guard_name' => 'web', 'group_name' => 'Comment']);


        /* Permissions for Portfolio Section */
        Permission::create([  'name' => 'Access Portfolio Section', 'guard_name' => 'web', 'group_name' => 'Portfolio']);
        Permission::create([  'name' => 'Update Portfolio Content', 'guard_name' => 'web', 'group_name' => 'Portfolio']);
        
        Permission::create([  'name' => 'Download Portfolio', 'guard_name' => 'web', 'group_name' => 'Portfolio']);


        /* Permissions for SCV Upload */
        Permission::create([  'name' => 'Access CSV Upload Section', 'guard_name' => 'web', 'group_name' => 'CSV']);
    }
}
