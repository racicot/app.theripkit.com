<?php

use Illuminate\Database\Seeder;

class CompanySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Model\Company::firstOrCreate(
            [
                'id' => 1
            ],
            [

                'id' => 1,
                'name' => 'Build',
                'plan' => 'paid',
                'phone' => '1523652545',
                'status' => 1
            ]
        );
    }
}
