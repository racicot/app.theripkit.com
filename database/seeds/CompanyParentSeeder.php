<?php

use Illuminate\Database\Seeder;

class CompanyParentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('company_parents')->insert([
           ['parent_id' => 2, 'company_id' => 1],
           ['parent_id' => 3, 'company_id' => 1],
           ['parent_id' => 4, 'company_id' => 1],
           ['parent_id' => 5, 'company_id' => 1],
           ['parent_id' => 6, 'company_id' => 1],
           ['parent_id' => 7, 'company_id' => 1],
           ['parent_id' => 8, 'company_id' => 1],

           ['parent_id' => 2, 'company_id' => 2],
           ['parent_id' => 3, 'company_id' => 2],
           ['parent_id' => 4, 'company_id' => 2],
           ['parent_id' => 5, 'company_id' => 2],
           ['parent_id' => 6, 'company_id' => 2],
           ['parent_id' => 7, 'company_id' => 2],
           ['parent_id' => 8, 'company_id' => 2],

           ['parent_id' => 2, 'company_id' => 3],
           ['parent_id' => 3, 'company_id' => 3],
           ['parent_id' => 4, 'company_id' => 3],
           ['parent_id' => 5, 'company_id' => 3],
           ['parent_id' => 6, 'company_id' => 3],
           ['parent_id' => 7, 'company_id' => 3],
           ['parent_id' => 8, 'company_id' => 3],

        ]);
    }
}
