<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\User::firstOrCreate(
            [
                'id' => 1
            ],
            [
                'id' => 1,
                'name' => 'Super Admin',
                'email' => 'admin@businessinstincts.com',
                'password' => bcrypt('bigRipkit')
            ]
        );
    }
}
