<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'reset' => 'Your password has been reset!',
    'sent' => 'If you don\'t receive the password resend link in next 1 minute, please try again. If you still do not receive it, please get in touch with our support team.',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that e-mail address.",

];
