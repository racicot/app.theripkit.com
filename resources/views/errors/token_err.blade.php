@extends('layouts.token-error-layout')

<div class="d-flex align-items-center justify-content-center expired-page">
    <div class="expired-section">
        <div class="expired-logo-image">
            <img src="{{ asset('images/ripkit-logo.png')}}">
            </div>    
        <section class="text-center expired-box-section">

            <h1 class="error-heading text-primary">302</h1>
            <h2>Token Expired</h2>
            <p>The token you are accessing is either expired or already used. Please reset your password.</p>
            <p> <a class="btn btn-theme expired-btn text-uppercase" href="{{url('/password/reset')}}">Click Here</a> </p>
        </section>
    </div>    
</div>    
