@extends('layouts.404-layout')
@section('title')
    404 Not Found
@endsection
<section class="text-center">
    <h1 class="error-heading text-primary">404</h1>
    <h2>Page not found</h2>
    <p>The page you are looking for can't be found.</p>
    <p><a href="{{url('/')}}">Back to home</a></p>
</section>
