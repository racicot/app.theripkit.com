<link href="{{ asset('css/mui.min.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/AdminLTE.min.css') }}" rel="stylesheet" type="text/css">
<link href="https://fonts.googleapis.com/css?family=Lato|Open+Sans:300,400,400i,600i,600,700" rel="stylesheet">
<link href="{{ asset('css/app.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('css/introjs.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/vedio.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.mentionsInput.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/jquery.mCustomScrollbar.css') }}">
