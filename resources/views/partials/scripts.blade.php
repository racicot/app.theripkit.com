<script>
    window.Laravel = {!! json_encode([
            'csrfToken' => csrf_token(),
        ]) !!};
</script>
<script type="text/javascript"> var APP_URL = {!! json_encode(url('/')) !!} </script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.0.7/js/swiper.min.js"></script>
<script src="{{ asset('js/intro.js') }}"></script>
@if(auth()->check())
<script src="{{ asset('js/app.js') }}"></script>
<script src="https://vjs.zencdn.net/7.3.0/video.js"></script>
<!-- <script src="{{ asset('js/jquery-mobile.js') }}"></script> -->
@endif
@if(!auth()->check())
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/login.js') }}"></script>
@endif
<script src="{{ asset('js/clipboard.min.js') }}"></script>







