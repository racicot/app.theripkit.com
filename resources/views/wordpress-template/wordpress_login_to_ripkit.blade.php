<link href="//db.onlinewebfonts.com/c/1572b251b0319fdd12219a000aad6196?family=Sofia+Pro+Black" rel="stylesheet" type="text/css"/>
<style>
a:hover{
    background-color: #f26738 !important;
}

#loading-iframe{
    height: 40px;
}

.no-rip-div{
    padding-bottom: 20px;
    text-align: center;
    font-weight: 600;
    color: #ff6800;
}   
</style>
@if ($message != '')
<div class="no-rip-div">
    <p>{{ $message }}</p>
</div>
@endif
<a style="background-color: #8FD5DF;
    display: block;
    color: #fff;
    text-align: center;
    padding: 7px 10px;
    border-radius: 8px;
    text-transform:uppercase;
    text-decoration:none;
    font-size: 14px;
    font-family: 'Sofia Pro Black',sans-serif;
    -webkit-transition: all 0.6s ease-in-out;
    -o-transition: all 0.6s ease-in-out;
    transition: all 0.6s ease-in-out;" target = "_blank" href="{{ url('getRipkitRipdetails?company='.$company.'&email='.$email.'&user_name='.$user_name.'&action=dologin&return_url='.urlencode($return_url)) }}">ACCESS YOUR RIPKIT</a>
