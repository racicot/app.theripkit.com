<link rel="stylesheet" type="text/css" href="{{ asset('css/style.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/new-theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/responsive.css') }}">

<link href="//db.onlinewebfonts.com/c/1572b251b0319fdd12219a000aad6196?family=Sofia+Pro+Black" rel="stylesheet" type="text/css"/>
<style>
a:hover{
    background-color: #f26738 !important;
}

.secondrow .contentmeet .activatebtn {
    margin-top: 0px;
}

.rip-period-progress{
    justify-content: flex-end !important;
}
.content-header .progress-radial--md {
     margin-right: 0px; 
}

.rip-header-wrap span {
   white-space: nowrap;
}
.no-rip-div{
    padding-bottom: 20px;
    text-align: center;
    font-weight: 600;
    color: #ff6800;
}    
</style>
@if($currentRip)
<section class="content-header content-header--lg rip-header-wrap">

<div >
    <h1 class="rip-heading" id="rip_content">{{ $currentRip->name }}</h1>
    <div class="rip-date-header">
        <span id="date-header">{{ date('M d, Y', strtotime($currentRip->start_date)).' - '.date('M d, Y', strtotime($currentRip->end_date)) }}</span>

        @if ($currentRip->expired == 0)
            <span id="left-days" @if(\Carbon\Carbon::now()->format('Y-m-d') > $currentRip->end_date)class="red-bg-button" @endif> {{ $left }}  </span>

        @else
            <span class="red-bg-button">Expired</span>
        @endif
    </div>
</div>
<div class="rip-period-progress align-items-end">
     <div class="progress-radial progress-radial--md progress-{{ $currentRip->getRipPercentage() }}">
        <div class="overlay ripPer_{{ $currentRip->id }}">
        <span>{{ $currentRip->getRipPercentage() }}</span>
        <span>%</span>
        </div>
    </div>
</div>    
</section>
@else

<div class="no-rip-div">
    <p>You haven’t created any RIP(s) yet!</p>
</div>

@endif

<a style="background-color: #8FD5DF;
    display: block;
    color: #fff;
    text-align: center;
    padding: 7px 10px;
    border-radius: 8px;
    text-transform:uppercase;
    text-decoration:none;
    font-size: 14px;
    font-family: 'Sofia Pro Black',sans-serif;
    -webkit-transition: all 0.6s ease-in-out;
    -o-transition: all 0.6s ease-in-out;
    transition: all 0.6s ease-in-out;" target = "_blank" href="{{ url('overview-rip') }}">GO TO RIPKIT DASHBOARD</a>
