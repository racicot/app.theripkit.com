<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>

<head>
    <meta content="text/html;charset=UTF-8" http-equiv="content-type"/>
    <title></title>
</head>

<body bgcolor="#ffffff" style="font-family: Montserrat, Helvetica, Arial, sans-serif; font-size: 16px;"
      vlink="#000000">
<div style="width: 800px; margin: 0px auto;">
    <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
        <tbody>
        <tr>
            <td width="800" align="center" valign="top" style="word-wrap:break-word;"><a href="#"><img
                            src="http://beta.buildimpossible.com/images/build-impossible-logo.png" width="150"
                            height="89" border="0"/></a></td>
        </tr>
        <tr>
            <td width="800" align="left" valign="top" style="word-wrap:break-word;padding:10px 0;">
                <div style="font-weight: bold;">Hola John Doe</div>
            </td>
        </tr>
        <tr>
            <td width="800" align="left" valign="top" style="word-wrap:break-word;padding:10px 0;">Thanks for joining
                Build Impossible! We really appreciate it. Please click on the link below to login with the following
                credentials.
            </td>
        </tr>
        <tr>
            <td width="800" align="left" valign="top" style="word-wrap:break-word;padding:10px 0;">
                <strong>Username:</strong> {{ $email }}</td>
        </tr>
        <tr>
            <td width="800" align="left" valign="top" style="word-wrap:break-word;padding:10px 0;">
                <strong>Password:</strong> {{ $password }}</td>
        </tr>
        <tr>
            <td style="word-wrap:break-word;font-size:0px;padding:0;;" align="center">
                <table role="presentation" cellpadding="0" cellspacing="0" style="border-collapse:separate;"
                       align="left" border="0">
                    <tbody>
                    <tr>
                        <td height="40" style="border:none;border-radius:30px;color:#FFFFFF;" align="left"
                            valign="middle" bgcolor="#5FA91D"><a href="{{ $url }}"
                                                                 style="text-decoration:none;background:#db4d5d;border-radius:30px;color:#FFFFFF;font-family:Montserrat, Helvetica, Arial, sans-serif;font-size:16px;font-weight:normal;line-height:100%;text-transform:none;margin:0px;padding:11px 25px;"
                                                                 target="_blank">Login</a></td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
</div>
</body>

</html>