<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
</head>
<body style=' margin:0px; padding:0px;' >
    
<!--<div style=' background:#ededed; margin:0px; padding:0px;line-height: 1.231;font-family:arial; font-size:18px; '>-->
    <div class='invoice_widget' style='max-width: 1000px; margin: 0 auto;padding:30px 0;'>
        <div style='padding: 40px 5%;color: #222; background: #fff; border-radius:10px; margin:0 10px;'>
            <div style='text-align:center;'> <img src='{{ asset('images/ripkit-logo-new.svg') }}' style='max-width:100%;' /></div>
            <div style='color:#f36d0e; font-weight:bold; margin:60px 0 0 0; font-size:20px;'>Hi <span>{{ $name }},</span> </div>
            <div style='color:#000; margin:25px 0 36px 0;font-size: 16px;'> 2018 has been a BIG year. Our team has seen growth across all our projects, and we’re energized by what we’ve done this year and what’s on the horizon for 2019. <br></br><br></br>

            But before the new year starts, we’re squeezing a little more out of 2018 by launching version 3 of RIPKIT, complete with several new features designed to improve your RIPKIT and offsite experience. The rollout of RIPKIT V3 will launch:<br></br>

            <p><span style='color:#f36d0e; margin:35px 0 0 0; line-height:28px;'>New look and feel:</span> Enjoy a cleaner design that’s easier to navigate and use. </p>

            <p><span style='color:#f36d0e; margin:35px 0 0 0; line-height:28px;'>Overview:</span> See all your RIPs or Tasks – from one project or several – in a single, organized view. </p>

            <p><span style='color:#f36d0e; margin:35px 0 0 0; line-height:28px;'>New Snapshot:</span> Enjoy the redesigned Snapshot and built-in download function, which makes it easy for you to print, share and display your BIG vision proudly. </p>
            <br></br>

            This is just the beginning! We have even more plans for RIPKIT, so be on the lookout for new and improved functionalities in 2019. </div>
            
            <div style='margin:37px 0 0 0;color:#505050;line-height:28px;'>
                With love, 
                <div style='color:#000;font-weight:bold;'>The BIG Team</div>
                <br>
                <img src='{{ asset('images/build-impossible-logo.png') }}' style='max-width:50%;' />
            </div>
            <p style='color:#505050; margin:35px 0 0 0; line-height:28px;font-size:16px'>P.S. We’d love your feedback on how we can improve RIPKIT in V4 and beyond. If you have thoughts or questions, please <a href='mailto:it@businessinstincts.com' style='text-decoration:underline;color:#505050;'>reach out</a>.</p>
        </div>
        <p style='max-width:493px; text-align:center; color:#989898; font-size:13px; margin:0 auto; margin-top:25px; padding: 0 20px;'>This is automated message from The RIPKIT. Please do not reply to this message.<br/><br/> Privacy and Unsubscribe Policy.<br/> Your privacy is important to us. You can read more about The RIPKIT's Privacy Policy on the RIPKIT website. © {{ date('Y') }} <a href="https://www.theripkit.com" style='text-decoration:underline;color:#989898;'>theripkit.com</a> </p>
    </div>
<!--</div>-->
</body>
</html>
