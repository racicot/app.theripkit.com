<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
</head>
<body style=' margin:0px; padding:0px;' >
<div style=' background:#ededed; margin:0px; padding:0px;line-height: 1.231;font-family:arial; font-size:18px; '>
    <div class='invoice_widget' style='max-width: 650px; margin: 0 auto;padding:30px 0;'>
        <div style='padding: 50px 5%;color: #222; background: #fff; border-radius:10px; margin:0 15px;'>
            <div style='text-align:center;'> <img src='{{ asset('images/ripkit-logo-new_orange.png') }}' style='max-width:100%;height: 44px;' /></div>
            <div style='color:#f36d0e; font-weight:bold; margin:60px 0 0 0; font-size:18px;'>Dear {{ $name }},  </div>
            <div style='color:#000; font-weight:normal; margin:25px 0 42px 0;font-size: 16px;'>
                {{ config('setting.password.reset_password') }}
            </div>
            <div style='font-style:italic; margin:0 0 7px 0;font-size:15px;'><span style='color:#999999;'>URL: </span><span style='color:#535353; margin:0 0 0 5px;'><a href='{{ url('password/reset').'/'.$data.'/'.$email }}' style='color:#535353; text-decoration:underline;'>{{ url('password/reset').'/'.$data.'/'.$email }}</a></span></div>
            <p style='color:#505050; margin:35px 0 0 0; line-height:28px;font-size:16px'>To personalize your password: <br/>
                1. Login and go to VIEW ACCOUNT <br/>
                2. Scroll down to CHANGE PASSWORD and update your password <br/> <br/>

                Please feel free to reach out to our support team (<a href='mailto:support@theripkit.com' style='text-decoration:underline;color:#505050;'>support@theripkit.com</a>) if you need any help with navigating the system.</p>
            <div style='margin:37px 0 0 0;color:#505050;line-height:28px;'>
                Sincerely,
                <div style='color:#000;font-weight:bold;'>The RIPKIT Team</div>
                <br>
                <img src='{{ asset('images/build-impossible-logo.png') }}' style='max-width:50%;' /> 
            </div>
        </div>
        <p style='max-width:493px; text-align:center; color:#989898; font-size:12px; margin:0 auto; margin-top:25px; padding: 0 20px;'>This is automated message from The RIPKIT. Please do not reply to this message.<br/><br/> Privacy and Unsubscribe Policy.<br/> Your privacy is important to us. You can read more about The RIPKIT's Privacy Policy on the RIPKIT website. © {{ date('Y') }} <a href="https://www.theripkit.com" style='text-decoration:underline;color:#989898;'>theripkit.com</a> </p>
    </div>
</div>
</body>
</html>