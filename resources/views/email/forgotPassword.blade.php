Your password has been reset. Please login with this new password
<br>
<b>New Password:</b> {{ $password }}
<br>
You can update your password later in <b>"Account"</b> section.