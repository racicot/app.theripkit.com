@extends('layouts.master')
@section('body-content')

@if(session('success') != '') 
    <div class="alert-msg alert-success">
        <img src="{{ asset('images/toast-success-icon.svg') }}">        
        {{ session('success') }}
        {{ Session::forget('success') }}        
    </div>    
@elseif (session('fail') != '')
    <div class="alert-msg alert-danger">
        <img src="{{ asset('images/toast-error-icon.svg') }}">
        {{ session('fail') }}
        {{ Session::forget('fail') }}
    </div>
@endif

<!-- alert ajax-->
<div id="displayAjaxMessage" class="alert-none">
    <div class="alert-msg-ajax"> </div> 
</div>    


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    @if (session('flashMessage'))
        <div class="alert {{ session('class') }}">
            {{ session('flashMessage') }}
        </div>

    @endif
    @yield('content')      

<?php if(isset(auth()->user()->watched_guided_tour)) { ?>
    <input type="hidden" id="watched_tour" val="{{ auth()->user()->watched_guided_tour }}">
<?php } ?>
    
</div>
@include('sidebar.notification-sidebar')

    @yield('modal')
    <div class="modal fade modal--theme" id="discoveryComplete" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <h4 class="modal-title" id="myModalLabel">Hurray! You have successfully completed descovery
                        phase</h4>

                    <p>Please hit ok and it will take you to the next step, that is 30 days RIP for yout goal
                        Planning.</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-theme">Ok</button>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="editUserModal">

    </div><!-- /.modal -->
    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="userPermissionModal">
    </div>
    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="editModal">

    </div><!-- /.modal -->
    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="editCategoryModal">
        
    </div><!-- /.modal -->
    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="editRipTabModal">
        
    </div><!-- /.modal -->
    
    
    <!-- Modal -->
    <div class="modal fade new-modal" id="confirmPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Delete RIP Period</h4>
                </div>
                <div class="modal-body">
                    <div class="add-user">
                        <div class="form-group"> <span class="module_msg">Are you sure you want to delete</span> <span class="font-weight600">“90 Days”</span> <span class="module_name">RIP Period?</span></div>
                        <div class="modal-footer">
                            <button type="button" onclick="$('#confirmPopup').modal('hide');" class="btn btn-link pull-left" id="cancelPopModel" data-dismiss="modal">Cancel</button>
                            <button type="button" value="1" id="confirm" class="btn btn-delete btn-theme pull-right">Yes, Delete</button>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- Modal -->

        <!--Cancel Meeting Modal -->
        <div class="modal fade new-modal" id="cancelMeetingModel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Cancel Meeting</h4>
                    </div>
                    <div class="modal-body">
                        <div class="add-user">
                            <div class="form-group"> <span class="module_msg">Are you sure you want to cancel</span><span class="font-weight600">“90 Days”</span> <span class="module_name">meeting?</span></div>
                            <div class="modal-footer">
                                <button type="button" onclick="$('#cancelMeetingModel').modal('hide');" class="btn btn-link pull-left" id="cancelPopModel" data-dismiss="modal">Cancel</button>
                                <button type="button" value="1" id="confirmCancel" class="btn btn-delete pull-right">Yes, Cancel</button>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
        <!-- Cancel Meeting Modal End -->
    <div class="modal fade confirm-popup" id="duplicateRipModal" tabindex="-1" role="dialog" aria-labelledby="duplicateRipModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-theme">Save changes</button>
                </div>
            </div>
        </div>
    </div>


    <!-- Add User Modal -->
    <div class="modal new-modal fade" id="addNewUser" tabindex="-1" role="dialog" aria-labelledby="addNewUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add New User</h4>
                </div>
                <div class="modal-body">

                <?php if(isset($errors->has)) {?>
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('email'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('role'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('role') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('company'))
                        <span class="help-block">
                        <strong class="text-danger">{{ $errors->first('company') }}</strong>
                    </span>
                    @endif
                   <?php }?> 
                    @if(auth()->user()->can('Add New User'))
                        <div class="add-user">
                            <form method="POST" id="addUser" action="{{ url('/user') }}" novalidate autocomplete="off">
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <label for="inputName">Name</label>
                                    <input id="inputName" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                        placeholder="Name">
                                    <span class="error-msg" id="name-err"></span>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail">Email Address</label>
                                    <input id="inputEmail" type="text" class="form-control" name="email" placeholder="Email"
                                        value="{{ old('email') }}">
                                    <span class="error-msg" id="email-err"></span>
                                </div>
                                <div class="form-group">
                                    <label>Select Role</label>
                                    <select class="form-control common-chosen" name="role" id="selectRole" required data-placeholder="Role">
                                        @if(isset($roles[1]))
                                        <?php if(isset($roles) && $roles[1]->id > 0){ ?>

                                        @foreach($roles as $role)
                                        <option value="{{ $role->name }}">{{ $role->name }}</option>
                                        @endforeach
                                        <?php } ?>
                                        @endif
                                    </select>
                                    <span class="error-msg" id="role-err"></span>
                                </div>
                                <div class="form-group add-user-company" id="add-user-company">
                                    <label>Select Company</label>

                                    <select class="form-control common-chosen selectCompany" name="company[]" id="selectCompany" required data-placeholder="Company" multiple>
                                        @if(isset($companies[0]))
                                        <?php if(isset($companies) && $companies[0]->id > 0){ ?>
                                        @foreach($companies as $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @endforeach
                                        <?php } ?>
                                        @endif
                                    </select>
                                    <span class="error-msg" id="company-err"></span>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-theme pull-right">Add new user</button>
                                </div>
                            </form>
                        </div><!-- /.add-user -->
                    @endif
                </div>
            </div>
        </div>
    </div>

     <!-- Schedule Meeting -->
     <div class="modal new-modal fade" id="scheduleMeeting" tabindex="-1" role="dialog" aria-labelledby="addNewUser">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Schedule Meeting</h4>
                </div>
                <div class="modal-body">
                        <div class="add-user">
                            <form id="addMeeting"  autocomplete="off" novalidate="novalidate">
                                <div class="form-group">
                                    <label for="agenda">Meeting Title</label>
                                    <div class="textarea-wrap">
                                        <textarea id="agenda" type="text" class="form-control meet-title" maxlength="100" name="agenda" placeholder="Meeting Title"></textarea>
                                    </div>
                                    <span class="error-msg" id="agenda_err"></span>
                                </div>
                                <div class="form-group">
                                    <label for="meetingDate">Meeting Date (Timezone: Pacific)</label>
                                    <input type="text" class="form-control date-picker meetingDate" name="meetingDate" placeholder="Meeting Date">
                                    <span class="error-msg" id="meetingDate_err"></span>
                                </div>
                                <div class="form-group">
                                    <label for="meetingTime">Meeting Time<span style="font-size:12px;"> (24 Hrs)</span></label>
                                    <input type="text" class="form-control date-picker time-picker meetingTime" name="meetingTime" placeholder="Meeting Time">
                                    <span class="error-msg" id="meetingTime_err"></span>
                                </div>
                                <div class="form-group">
                                    <label for="duration">Duration (In minutes)</label>
                                    <input type="number" min="1" max="600" id="duration" class="form-control duration" name="duration" placeholder="Duration" maxlength="3" oninput="this.value=this.value.slice(0,this.maxLength)">
                                    <span class="error-msg" id="duration_err"></span>
                                    <span class="highlight custom-note">
                                        <b>Note: </b>Maximum duration for a meeting cannot exceed 600 minutes
                                    </span>
                                </div>
                                <div class="form-group addParticipants">
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                    <button type="submit"  class="btn btn-theme pull-right">SUBMIT</button>
                                </div>
                            </form>
                        </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Add Company Modal -->
    <div class="modal new-modal fade" id="addNewCompany" tabindex="-1" role="dialog" aria-labelledby="addNewCompany">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add New Company</h4>
                </div>
                <div class="modal-body">

                 <?php if(isset($errors->has)) {?>
                    @if ($errors->has('image'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('image') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('company_name'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('company_name') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('phone'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('parent'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('parent') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('assigned_parent'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('assigned_parent') }}</strong>
                        </span>

                    @endif
                    @if ($errors->has('owner'))
                        <span class="help-block">
                            <strong class="text-danger">{{ $errors->first('owner') }}</strong>
                        </span>
                    @endif

                    <?php }?>

                    @if(auth()->user()->can('Add New Company'))
                        <div class="add-user">
                            <form id="addCompany" method="post" enctype="multipart/form-data"
                                  action="{{ url('company') }}" autocomplete="off">
                                {{ csrf_field() }}
                                <!-- Max Length added for Name -->
                                <div class="form-group">
                                    <label for="inputCompanyName">Company Name</label>
                                    <input type="text" class="form-control" id="inputCompanyName" value="{{ old('company_name') }}"
                                        name="company_name" 
                                        placeholder="Company Name" maxlength="26">
                                    <span id="name-err" class="help-block error-msg margin-0"></span>
                                </div>
                                <input type="hidden" value="1" name="status">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-theme pull-right">Add new Company</button>
                                </div>
                            </form>
                        </div><!-- /.add-user -->
                    @endif
                </div>
            </div>
        </div>
    </div>

    <!-- Rip Period : Mark Expired Model -->
    <div class="modal new-modal  fade confirm-popup" id="markExpiredConfirmPopup" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Mark as Expired</h4>
                </div>
                <div class="modal-body">
                <div class="add-user">
                    <div class="form-group">
                        <span class="module_msg">
                            Do you want to mark
                        </span>
                        <span class="font-weight600"> this </span>
                        <span>rip as expired?</span>
                    </div>
                </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Close</button>
                    <button type="button" value="1" id="expiredConfirm" class="btn btn-theme">Confirm</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Add task Modal -->
    <div class="modal new-modal fade" id="addtask" tabindex="-1" role="dialog" aria-labelledby="addtask">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add Task</h4>
                </div>
                <div class="modal-body">
                        <div class="add-user">
                            <form id="" method="post" autocomplete="off">
                               
                                <!-- Max Length added for Name -->
                                <div class="form-group">
                                    <label for="inputCompanyName">Task Name</label>
                                    <input type="text" class="form-control" placeholder="Development Department">
                                    <span class="help-block"></span>
                                </div>
                                <input type="hidden" value="1" name="status">
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-theme pull-right">Update</button>
                                </div>
                            </form>
                    </div><!-- /.add-user -->
                </div>
            </div>
        </div>
    </div>
    <!-- Add comment Modal -->
    <div class="modal new-modal fade" id="addcomment" tabindex="-1" role="dialog" aria-labelledby="addcomment">
        <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Add Comment</h4>
            </div>
            <div class="modal-body">
                    <div class="add-user">
                        <form id="" method="post" autocomplete="off">
                            
                            <!-- Max Length added for Name -->
                            <div class="form-group">
                                <label for="inputCompanyName">Add Comment</label>
                                <textarea rows="5" class="form-control"></textarea>
                            </div>
                            <div class="form-group attach-group mgb-0">
                                <div class="attach-row">
                                <label for="fusk">Attach file</label>
                                <input type="file" class="form-control ajaxAddTask" id="fusk">
                                </div>
                            </div>
                            <div class="modal-footer mgt-0">
                                <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-theme pull-right">Save</button>
                            </div>
                        </form>
                </div><!-- /.add-user -->
            </div>
        </div>
        </div>
    </div>
    <!-- Update progress Modal -->
    <div class="modal new-modal fade" id="updateripprogess" tabindex="-1" role="dialog" aria-labelledby="updateripprogess">
        
    </div>
    <!-- Add Rip Modal -->
    <div class="modal new-modal fade" id="addripPeriod" tabindex="-1" role="dialog" aria-labelledby="addripPeriod">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add RIP Period</h4>
                </div>    
                <div class="modal-body">
                    <div class="add-user">
                        <form id="addRip" method="post" autocomplete="off" action="{{ url('addRip') }}">
                            {{ csrf_field() }}
                            <!-- Max Length added for Name -->
                            <div class="form-group">
                                <label for="inputRipName">RIP Period</label>
                                            <input type="text" class="form-control" value="" maxlength="30" id="inputRipName" name="name"
                                placeholder="RIP Period">
                                <span class="error-msg err_rip"></span>

                            </div>
                            <div class="form-group">
                                <label for="inputDateStart">Start Date</label>
                                <input type="text" class="form-control date-picker inputDateStart" name="start_date" placeholder="Start Date">
                                <span class="error-msg err_start"></span>
                            </div>
                            <div class="form-group">
                                <label for="inputDateEnd">End Date</label>
                                <input type="text" class="form-control date-picker inputDateEnd" name="end_date"readonly placeholder="End Date">
                                <span class="error-msg err_end"></span>
                            </div>
                            <input type="hidden" value="1" name="status">
                            <div class="modal-footer">
                                <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                <button id="ripModalButton" type="submit" class="btn btn-theme pull-right">Create</button>
                            </div>
                        </form>
                    </div><!-- /.add-user -->
                </div>
            </div>
        </div>    
    </div>
    
    <!-- Add Rip Modal -->
    <div class="modal new-modal fade" id="editRipModal" tabindex="-1" role="dialog" aria-labelledby="editRipModal">
        
    </div>
    
    <!-- Add Users Modal in RIP -->
    <div class="modal new-modal fade" id="addusers" tabindex="-1" role="dialog" aria-labelledby="addusers">
    
    </div>    
    
     <!-- Show User List Modal--> 
    <div class="modal new-modal fade" id="showUserListModal" tabindex="-1" role="dialog" aria-labelledby="showUserListModal">

    </div>

    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="csvupload">
                <div class="modal-dialog modal-add-company" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title">Upload CSV</h4>
                        </div>
                        <div class="modal-body">
                        <form id="csv-form" method="POST" action="{{ url('validateCsv') }}" enctype="multipart/form-data" class="form-horizontal csv_form">
                            {{ csrf_field() }}
                            <div>

                                <div>
                                    @if(!session()->has('isValidated'))
                                        <div class="form-group select-style">
                                            <label for="name">Action</label>
                                            <select name="action" class="add-edit-csv form-control mr10">
                                                <label for="name">Role Name</label>
                                                <option value="add">Add</option>
                                                <option value="edit">Update</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="name">Upload FIle</label>
                                            <div class="file-uploader">
                                                <input type="file" name="csv" id="csv-upload" />
                                                <span class="title">choose file</span>
                                                <i class="upload-icon">upload</i>
                                            </div>
                                            <!--<span class="csv-err help-block margin-0" id="csv-err">**Max Upload Size: 2MB</span>-->
                                        </div>
                                        <div class="col-sm-12">
                                            <span id="csv-upload-status"></span>
                                        </div>
                                        <div class="form-group">
                                            <a href="#" class="text-link" id="sample-csv"> Download Sample CSV</a>
                                        </div>
                                    @endif
                                    @if(session()->has('isValidated'))
                                        <div class="file-input-wrap">

                                            <input type="text" readonly value="{{ session('name') }}">
                                            <input type="hidden" readonly value="{{ session('file_name') }}" name="fileName">
                                        </div>
                                    @endif
                                    <div class="modal-footer">
                                        <div class="modal-footer-left">
                                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                                        @if(session()->has('isValidated'))
                                            <button type="submit" id="submit-csv" class="btn btn-theme btn-block btn-uppercase">
                                                Done
                                            </button>
                                        @else
                                            
                                            <input type="button" id="validate-csv" value="Validate & Upload"
                                                    class="btn btn-theme btn-uppercase">
                                                
                                            
                                        
                                        @endif
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>

                        </form> 
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div>
        
@endsection

