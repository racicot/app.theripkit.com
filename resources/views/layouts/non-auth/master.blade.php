



<!DOCTYPE html>
<html lang="en">
<head>
    <title>
        @yield('title')
    </title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/fav_icon.png') }}">
    @include('partials.styles')

</head>
<body>

@if (session('flashMessage'))
    <div class="alert alert-success">
        {{ session('flashMessage') }}
    </div>
@endif
@yield('body-content')
@include('partials.scripts')
@yield('modal')
<!--
   <div class="modal fade modal--theme new-modal" id="welcome-popup" tabindex="-1" role="dialog"
     aria-labelledby="myModalLabel" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="pop-header">
                        <h4 class="modal-title" id="myModalLabel">Welcome <small>to</small></h4><figure class="welcome-logo"></figure>
                        <p class="welcome-msg">We are glad to bring you the enhanced version of your application.</p>
                    </div>

                    <ul class="feature-list">
                        <li>
                            <i class="icon look">new look</i>
                            <div class="content-wrap">
                               <strong>New look and feel</strong>
                                Enjoy a cleaner design that's easier to navigate and use.
                            </div>
                        </li>
                        <li>
                            <i class="icon overview">overview</i>
                            <div class="content-wrap">
                                <strong>Overview</strong>
                                See all your RIPs or Tasks - from one project or several - in a single, organized view.
                            </div>
                        </li>
                        <li>
                            <i class="icon snapshot">new snapshot</i>
                            <div class="content-wrap">
                                <strong>New Snapshot</strong>
                                Enjoy the redesigned Snapshot and built-in download function, which makes it easy for you to print, share and display your BIG vision proudly.
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-theme" data-dismiss="modal" id="experience_change">Experience the Change!</button>
                </div>
            </div>
        </div>
    </div>
    -->
</body>
</html>






