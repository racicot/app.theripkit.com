<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>
        @yield('title')
    </title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="">
    <meta name="robots" content="noindex, nofollow">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/png" href="{{ asset('images/fav_icon.png') }}">

    @include('partials.styles')
</head>

<body class="hold-transition sidebar-mini sidebar-collapse skin-dark">
<div class="wrapper-overlay"></div>
<div id='app'></div>
<div class="loader loading"><div class="loader-ring"><div></div><div></div><div></div><div></div></div></div>
<div class="wrapper">
    @include('header.auth.header')
    @include('sidebar.sidebar')
    @include('auth.rip.rip_period_menu')
    @include('auth.taskForToday.list')
    @include('auth.scheduleMeeting.meetingSidebar')
    @yield('body-content')
    @include('partials.scripts')
    <script>
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
    </script>
</div>

</body>
</html>