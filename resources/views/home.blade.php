
@extends('layouts.auth.master')
@section('title')
    Dashboard
@endsection
@section('content')
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <section class="content-header content-header--inner flex-row justify-content-between dashboard-choose">
            <h1 href="javascript:void(0);" class="mr10"><span class="text-uppercase">To-Do</span> List</h1>
            <select id="todo" class="todo-select common-chosen">

                @foreach($users as $user)
                    <option value="{{ $user->id }}"
                            @if($current_id == $user->id) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>
        </section>
        <div class="row">
            <div class="col-sm-12 col-lg-12">
                @include('auth.todo.todo')
            </div>
        </div>

        <div class="goals-block">
            <section class="content-header content-header--inner">
                <h1>RIP Period</h1>
            </section>
            @if($rips->isEmpty())
                <div class="no-rip">
                    <img src="../images/new-site/empty_icon.svg" alt="">
                    <h3>No active RIP period(s)</h3>
                    <p>Click on the RIP period section link from the sidebar to create a new Period.</p>
                </div>
            @else
                <div class="row">
                <!-- <div class="grid-sizer col-sm-6 col-lg-4"></div> -->
                @foreach($rips as $rip)
                    <div class="grid-item col-sm-6 col-lg-4 col-xs-12">
                        <div class="card">
                            <div class="card-head">
                                <div class="card-head__left">
                                    <h2 class="card__title">{{ $rip->name }}</h2>
                                    {{--<a href="{{ url('ripTab/'.$rip->id) }}" class="card__link">View <i--}}
                                    {{--class="fa fa-angle-right" aria-hidden="true"></i></a>--}}
                                    <a href="{{ url('rip').'/'.$rip->id }}" class="card__link">View <i
                                                class="fa fa-angle-right" aria-hidden="true"></i></a>
                                </div>
                                <div class="card-head__right v-center">
                                    <div class="progress-radial progress-radial--md progress-{{ $rip->getRipPercentage() }}">
                                        <div class="overlay ripPer_{{ $rip->id }}">
                                            <span>{{ $rip->getRipPercentage() }}</span>
                                            <span>%</span>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- /.card-head -->
                            @if($rip->category->isNotEmpty())
                                <div class="card-body">
                                <div class="card-catgory"> {{ count($rip->category) }} Categories</div>
                                    @foreach($rip->category as $category)
                                        <div class="card-item">
                                            <h3 class="card-item__title">{{ $category->name }}</h3>
                                            @foreach($category->goals as $goal)
                                                <p class="card-item__desc">{{ $goal->goal }}</p>
                                            @endforeach
                                        </div><!-- /.card-item -->
                                    @endforeach
                                </div><!-- /.card-body -->
                            @endif
                        </div><!-- /.card -->
                    </div>
                @endforeach
            </div>
            @endif

        </div>

        
    </section>
    <!-- /.content -->

    <footer class="page-footer">© All rights reserved 2018-<?php echo date('Y'); ?>. RIPKIT Inc.</footer>
@endsection



