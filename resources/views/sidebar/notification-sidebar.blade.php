<aside class="notification-sidebar-full">
    <div class="heading-box">
            <h3>Notifications</h3>
            <span class="close-btn"></span>
        </div>
    <div class="content-box">
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="read-notification">
                <img src="{{ url('images/google-plus-jingle.gif') }}" alt="">
                <h3>All caught up!</h3>
            </div>
            <!-- Home tab content -->
            <div class="tab-pane active" id="control-sidebar-home-tab">

            </div>
            <!-- /.tab-pane -->
        </div>
    </div>
</aside>
