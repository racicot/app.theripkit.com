<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->

        <ul class="sidebar-menu" data-widget="tree">
            <div class="treeview">
                <a href="{{ url('/home') }}" class="logo" title="Company Logo">
                    <!-- <div class="logo-mini">{{ session('initials') }} <i class="fa fa-angle-down" aria-hidden="true"></i>
                    </div> -->
                    <img src="/images/bitmap@3x.png" alt="Logo" class="logo-small">
                    <img src="/images/ripkit-logo-new_orange.png" alt="Logo" class="logo-large">
                    <!-- <span class="logo-lg">{{ session('company_name') }} <i class="fa fa-angle-down"
                                                                           aria-hidden="true"></i></span> -->
                </a>
            </div>


        <li id="data-step-2" class=" menuLength {{ ((Request::is('overview-rip')) || (Request::is('overview-task')))? 'active' : '' }} treeview menu-open" data-step="2" data-position="right" data-intro="<b>{{ config('setting.tour.overview') }}</b></br>{{ config('setting.tour.overview_text') }}">
            <a <?php /*href="{{ url('overview-rip') }}*/ ?> role="button" aria-expanded="false" aria-controls="collapseExample"  data-toggle="collapse" href="#collapseExample" aria-expanded="false" aria-controls="collapseExample">
           <!-- <svg class="nav-icon overview" xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                <path fill-rule="nonzero" d="M19.241 4.172V1H1v19.431A3.573 3.573 0 0 0 4.569 24h15.862A3.573 3.573 0 0 0 24 20.431V4.172h-4.759zM16.07 18.448H4.172V16.07H16.07v2.38zm0-4.758H4.172v-2.38H16.07v2.38zm0-7.138V8.93H4.172V4.172H16.07v2.38zm5.552 13.879a1.191 1.191 0 0 1-2.38 0V6.551h2.38v13.88z"/>
                            <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                    <clippath id="nav-icon-overview">
                                    <path d="M19.241 4.172V1H1v19.431A3.573 3.573 0 0 0 4.569 24h15.862A3.573 3.573 0 0 0 24 20.431V4.172h-4.759zM16.07 18.448H4.172V16.07H16.07v2.38zm0-4.758H4.172v-2.38H16.07v2.38zm0-7.138V8.93H4.172V4.172H16.07v2.38zm5.552 13.879a1.191 1.191 0 0 1-2.38 0V6.551h2.38v13.88z"/>
                                </clippath>
                            <g clip-path="url(#nav-icon-overview)"><rect height="100%" width="100%"></rect></g>
                            </g>
            
            </svg>-->
            <svg width="45" height="25" class="nav-icon">

                        <path d="m7.5,21l12,0c0.825,0 1.5,-0.675 1.5,-1.5l0,-12l-13.5,0l0,13.5zm12,-21l-18,0c-0.825,0 -1.5,0.675 -1.5,1.5l0,4.5l21,0l0,-4.5c0,-0.825 -0.675,-1.5 -1.5,-1.5zm-19.5,19.5c0,0.825 0.675,1.5 1.5,1.5l4.5,0l0,-13.5l-6,0l0,12z" id="Shape"/>
                            
                        <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                            <clippath id="nav-icon-dashboard">
                                <path d="m7.5,21l12,0c0.825,0 1.5,-0.675 1.5,-1.5l0,-12l-13.5,0l0,13.5zm12,-21l-18,0c-0.825,0 -1.5,0.675 -1.5,1.5l0,4.5l21,0l0,-4.5c0,-0.825 -0.675,-1.5 -1.5,-1.5zm-19.5,19.5c0,0.825 0.675,1.5 1.5,1.5l4.5,0l0,-13.5l-6,0l0,12z" id="Shape"/>
                            </clippath>
                            <g clip-path="url(#nav-icon-dashboard)"><rect height="100%" width="100%"></rect></g>
                        </g>
                    </svg>
                <div class="overview">Overview</div>    
            </a>  
            <div class="submenu collapse" id="collapseExample">
                <ul>
                    <li>
                        <a href="{{ url('/overview-rip') }}">
                            <i class="icon rip">rip</i>
                            <div>RIP(s)</div>    
                        </a>           
                    </li>
                    <li>
                        <a href="{{ url('/overview-task') }}">
                            <i class="icon task">task</i>
                            <div>Task</div>    
                        </a>            
                    </li>

                </ul>
            </div>           
        </li>    

<!--        <li class="{{ Request::is('home') ? 'active' : '' }} treeview menu-open" data-step="2" data-position="right" data-intro= "<b>{{ config('setting.tour.dashboard') }}</b></br>{{ config('setting.tour.dashboard_text') }}">

                <a href="{{ url('home') }}">
                    <svg width="45" height="25" class="nav-icon">

                        <path d="m7.5,21l12,0c0.825,0 1.5,-0.675 1.5,-1.5l0,-12l-13.5,0l0,13.5zm12,-21l-18,0c-0.825,0 -1.5,0.675 -1.5,1.5l0,4.5l21,0l0,-4.5c0,-0.825 -0.675,-1.5 -1.5,-1.5zm-19.5,19.5c0,0.825 0.675,1.5 1.5,1.5l4.5,0l0,-13.5l-6,0l0,12z" id="Shape"/>
                            
                        <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                            <clippath id="nav-icon-dashboard">
                                <path d="m7.5,21l12,0c0.825,0 1.5,-0.675 1.5,-1.5l0,-12l-13.5,0l0,13.5zm12,-21l-18,0c-0.825,0 -1.5,0.675 -1.5,1.5l0,4.5l21,0l0,-4.5c0,-0.825 -0.675,-1.5 -1.5,-1.5zm-19.5,19.5c0,0.825 0.675,1.5 1.5,1.5l4.5,0l0,-13.5l-6,0l0,12z" id="Shape"/>
                            </clippath>
                            <g clip-path="url(#nav-icon-dashboard)"><rect height="100%" width="100%"></rect></g>
                        </g>
                    </svg>
                    <div>Dashboard</div>    
                </a>
                <span>Dashboard</span>
            </li>-->
            @if(auth()->user()->can('Access RIP Section'))
                <li id="data-step-3" class="menuLength {{ Request::is('url', 'rip/*') ? 'active' : '' }} {{ Request::is('url', 'viewHistoryRip/*') ? 'active' : '' }} treeview" data-step="3" data-position="right" data-intro= "<b>{{ config('setting.tour.rip_period') }}</b></br>{{ config('setting.tour.rip_period_text') }}">
                    <a href="javascript:void(0)" id="rip_menu">
                        <svg width="45" height="25" class="nav-icon">

                            <path d="m2,0l19,0c1.10457,0 2,0.89543 2,2l0,19c0,1.10457 -0.89543,2 -2,2l-19,0c-1.10457,0 -2,-0.89543 -2,-2l0,-19c0,-1.10457 0.89543,-2 2,-2zm7,6c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm-4,-10c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1z" id="Combined-Shape"/>

                                <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                    <clippath id="nav-icon-rip">
                                    <path d="m2,0l19,0c1.10457,0 2,0.89543 2,2l0,19c0,1.10457 -0.89543,2 -2,2l-19,0c-1.10457,0 -2,-0.89543 -2,-2l0,-19c0,-1.10457 0.89543,-2 2,-2zm7,6c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1l9,0c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1l-9,0zm-4,-10c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1zm0,5c-0.55228,0 -1,0.44772 -1,1c0,0.55228 0.44772,1 1,1c0.55228,0 1,-0.44772 1,-1c0,-0.55228 -0.44772,-1 -1,-1z" id="Combined-Shape"/>
                                </clippath>
                            <g clip-path="url(#nav-icon-rip)"><rect height="100%" width="100%"></rect></g>
                            </g>
                        </svg>
                        <div>RIP Period(s)</div>
                    </a>
                    <span>RIP Period(s)</span>
                </li>
            @endif
            @if(auth()->user()->can('Access Portfolio Section'))
                <li id="data-step-4" class="menuLength {{ Request::is('portfolio') ? 'active' : '' }} treeview" data-step="4" data-position="right" data-intro= "<b>{{ config('setting.tour.portfolio') }}</b></br>{{ config('setting.tour.portfolio_text') }}">
                    <a href="{{ url('portfolio') }}">
                        <svg width="45" height="25" class="nav-icon">

                            <path d="m1.49153,9.67851l0,-8.35052c0,-0.53542 0.29569,-0.97134 0.65925,-0.97189l5.14464,0c0.23044,0 0.44709,0.18086 0.56529,0.4722l0.92661,2.27636l11.43478,0c0.36356,0 0.65926,0.43592 0.65926,0.97188l0,5.60197l0.9844,0c0.13871,0 0.26997,0.06041 0.35983,0.16518c0.09024,0.10516 0.12977,0.24387 0.10851,0.38109l-1.83346,11.35573c-0.03617,0.23417 -0.23305,0.40308 -0.46871,0.40308l-17.72454,0c-0.23529,0 -0.43217,-0.16891 -0.46834,-0.40196l-1.83383,-11.35834c-0.02088,-0.1376 0.01939,-0.27593 0.10963,-0.38034c0.08986,-0.10441 0.22112,-0.16444 0.35908,-0.16444l1.0176,0zm1.49152,0l16.40678,0l0,-1.62591l-16.40678,0l0,1.62591z" id="Combined-Shape"/>

                                <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                    <clippath id="nav-icon-portfolio">
                                    <path d="m1.49153,9.67851l0,-8.35052c0,-0.53542 0.29569,-0.97134 0.65925,-0.97189l5.14464,0c0.23044,0 0.44709,0.18086 0.56529,0.4722l0.92661,2.27636l11.43478,0c0.36356,0 0.65926,0.43592 0.65926,0.97188l0,5.60197l0.9844,0c0.13871,0 0.26997,0.06041 0.35983,0.16518c0.09024,0.10516 0.12977,0.24387 0.10851,0.38109l-1.83346,11.35573c-0.03617,0.23417 -0.23305,0.40308 -0.46871,0.40308l-17.72454,0c-0.23529,0 -0.43217,-0.16891 -0.46834,-0.40196l-1.83383,-11.35834c-0.02088,-0.1376 0.01939,-0.27593 0.10963,-0.38034c0.08986,-0.10441 0.22112,-0.16444 0.35908,-0.16444l1.0176,0zm1.49152,0l16.40678,0l0,-1.62591l-16.40678,0l0,1.62591z" id="Combined-Shape"/>
                                </clippath>
                            <g clip-path="url(#nav-icon-portfolio)"><rect height="100%" width="100%"></rect></g>
                            </g>
                        </svg>
                        <div>Snapshot</div>
                    </a>
                    <span>Snapshot</span>
                </li>
            @endif
            @if(auth()->user()->can('Access User Section'))
                <li id="data-step-5" class=" menuLength {{ Request::is('user') ? 'active' : '' }} treeview" data-step="5" data-position="right" data-intro= "<b>{{ config('setting.tour.users') }}</b></br>{{ config('setting.tour.users_text') }}">
                    <a href="{{ url('user') }}">
                        <svg width="45" height="25" class="nav-icon">

                            <path d="m24.72727,20.93182l-17.18182,0l0,-0.91555c0,-1.05177 0.57355,-2.01845 1.49687,-2.52163l3.6495,-1.99064c0.61077,-0.33341 0.99,-0.97282 0.99,-1.66786l0,-1.64373l0,-0.00041l-0.07937,-0.09491l-0.01554,-0.01841c-0.00082,-0.00123 -0.02618,-0.03191 -0.0675,-0.08591c-0.00246,-0.00327 -0.00491,-0.00654 -0.00777,-0.00982c-0.02169,-0.02822 -0.04705,-0.06218 -0.07609,-0.10268c-0.00041,-0.00082 -0.00082,-0.00122 -0.00123,-0.00204c-0.06096,-0.08468 -0.13746,-0.19473 -0.22255,-0.32728c-0.00204,-0.00286 -0.00368,-0.00613 -0.00572,-0.009c-0.0401,-0.06259 -0.08264,-0.1309 -0.126,-0.20331c-0.00328,-0.00532 -0.00655,-0.01064 -0.00982,-0.01637c-0.09246,-0.15504 -0.19064,-0.33054 -0.28841,-0.52486c0,0 -0.00041,-0.00041 -0.00041,-0.00082c-0.05196,-0.10432 -0.10391,-0.21395 -0.15464,-0.32809c-0.00695,-0.01595 -0.01432,-0.0315 -0.02127,-0.04745c-0.0225,-0.05114 -0.045,-0.10473 -0.06791,-0.15996c-0.00818,-0.02004 -0.01636,-0.04091 -0.02454,-0.06136c-0.02128,-0.05359 -0.04296,-0.10759 -0.06587,-0.16937c-0.04173,-0.11127 -0.081,-0.22745 -0.11863,-0.34731l-0.0225,-0.07282c-0.00246,-0.00818 -0.00532,-0.01637 -0.00778,-0.02496c-0.03845,-0.12927 -0.07527,-0.26141 -0.10636,-0.39722l-0.03723,-0.162l-0.1395,-0.09c-0.23727,-0.153 -0.37882,-0.4095 -0.37882,-0.68605l0,-1.63636c0,-0.2295 0.09737,-0.44346 0.27409,-0.60341l0.135,-0.1215l0,-2.54782l0,-0.14482l-0.01104,-0.00859c-0.01391,-0.29536 0.00368,-1.20068 0.66395,-1.95382c0.67787,-0.77359 1.83478,-1.1655 3.438,-1.1655c1.5975,0 2.75196,0.38905 3.43064,1.15691c0.7965,0.90041 0.66477,2.05241 0.66395,2.06223l-0.00368,2.60141l0.135,0.12191c0.17673,0.15954 0.27409,0.3735 0.27409,0.603l0,1.63636c0,0.35714 -0.234,0.66968 -0.58172,0.77686l-0.20373,0.06259l-0.06546,0.2025c-0.27368,0.85132 -0.66354,1.6376 -1.15936,2.33714c-0.1215,0.17223 -0.23973,0.32482 -0.34241,0.44141l-0.10186,0.11618l0,1.6875c0,0.72409 0.40213,1.37496 1.04973,1.69896l3.90804,1.95381c0.97936,0.48969 1.58768,1.47396 1.58768,2.5691l0,0.83577zm-19,-0.91555l0,0.91555l-4.90909,0l0,-0.73514c0,-0.88773 0.48437,-1.70345 1.28496,-2.14077l2.72127,-1.70714c0.54532,-0.29741 0.88404,-0.86768 0.88404,-1.4895l0,-1.46904l-0.13009,-0.1215c-0.01063,-0.00982 -1.09759,-1.03664 -1.41872,-2.43614l-0.03723,-0.162l-0.13991,-0.09c-0.18818,-0.12191 -0.30068,-0.32604 -0.30068,-0.54573l0,-1.413c0,-0.14727 0.10023,-0.32236 0.2745,-0.48027l0.13459,-0.1215l-0.00082,-2.20091c0.00736,-0.11741 0.2205,-2.88736 3.68264,-2.88736c0.97895,0 1.80327,0.22623 2.45454,0.67254l0,1.93541c-0.26182,0.29823 -0.40909,0.67991 -0.40909,1.07387l0,1.63636c0,0.12436 0.01432,0.24668 0.04132,0.36532c0.01105,0.04745 0.03314,0.09082 0.04827,0.13663c0.0225,0.06873 0.0405,0.1395 0.072,0.20455c0.00041,0.00082 0.00082,0.00123 0.00123,0.00205c0.10473,0.216 0.25732,0.40909 0.44959,0.56331c0.00205,0.00778 0.0045,0.01473 0.00655,0.02209c0.02454,0.09369 0.05031,0.18696 0.07813,0.27819l0.03314,0.10677c0.00573,0.01882 0.01268,0.03804 0.01882,0.05686c0.01432,0.04418 0.02822,0.08796 0.04295,0.13132c0.02455,0.07159 0.05032,0.14564 0.08018,0.22623c0.01269,0.03354 0.02659,0.06382 0.03969,0.09695c0.03354,0.0855 0.06709,0.16814 0.10227,0.24996c0.00859,0.01963 0.01595,0.04091 0.02454,0.06013l0.02291,0.05155c0.01064,0.02373 0.02168,0.045 0.03232,0.06832c0.04009,0.08754 0.07936,0.17222 0.12027,0.25404c0.00655,0.01309 0.01269,0.02741 0.01923,0.0405c0.02577,0.05114 0.05155,0.09941 0.07732,0.1485c0.04418,0.08428 0.08754,0.16364 0.13091,0.24055c0.02127,0.03763 0.04213,0.07445 0.063,0.11004c0.05891,0.10064 0.11495,0.19309 0.16936,0.279c0.01187,0.01841 0.02332,0.03764 0.03477,0.05523c0.099,0.15341 0.18491,0.27777 0.2561,0.37473c0.01881,0.02577 0.03518,0.04786 0.05113,0.06954c0.009,0.01187 0.02127,0.02905 0.02905,0.03969l0,1.35368c0,0.396 -0.216,0.75927 -0.56332,0.94909l-1.08246,0.59032l-0.18859,-0.01678l-0.07691,0.16159l-2.30154,1.2555c-1.18636,0.648 -1.92314,1.88919 -1.92314,3.24082z" id="Combined-Shape"/>

                                <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                    <clippath id="nav-icon-users">
                                    <path d="m24.72727,20.93182l-17.18182,0l0,-0.91555c0,-1.05177 0.57355,-2.01845 1.49687,-2.52163l3.6495,-1.99064c0.61077,-0.33341 0.99,-0.97282 0.99,-1.66786l0,-1.64373l0,-0.00041l-0.07937,-0.09491l-0.01554,-0.01841c-0.00082,-0.00123 -0.02618,-0.03191 -0.0675,-0.08591c-0.00246,-0.00327 -0.00491,-0.00654 -0.00777,-0.00982c-0.02169,-0.02822 -0.04705,-0.06218 -0.07609,-0.10268c-0.00041,-0.00082 -0.00082,-0.00122 -0.00123,-0.00204c-0.06096,-0.08468 -0.13746,-0.19473 -0.22255,-0.32728c-0.00204,-0.00286 -0.00368,-0.00613 -0.00572,-0.009c-0.0401,-0.06259 -0.08264,-0.1309 -0.126,-0.20331c-0.00328,-0.00532 -0.00655,-0.01064 -0.00982,-0.01637c-0.09246,-0.15504 -0.19064,-0.33054 -0.28841,-0.52486c0,0 -0.00041,-0.00041 -0.00041,-0.00082c-0.05196,-0.10432 -0.10391,-0.21395 -0.15464,-0.32809c-0.00695,-0.01595 -0.01432,-0.0315 -0.02127,-0.04745c-0.0225,-0.05114 -0.045,-0.10473 -0.06791,-0.15996c-0.00818,-0.02004 -0.01636,-0.04091 -0.02454,-0.06136c-0.02128,-0.05359 -0.04296,-0.10759 -0.06587,-0.16937c-0.04173,-0.11127 -0.081,-0.22745 -0.11863,-0.34731l-0.0225,-0.07282c-0.00246,-0.00818 -0.00532,-0.01637 -0.00778,-0.02496c-0.03845,-0.12927 -0.07527,-0.26141 -0.10636,-0.39722l-0.03723,-0.162l-0.1395,-0.09c-0.23727,-0.153 -0.37882,-0.4095 -0.37882,-0.68605l0,-1.63636c0,-0.2295 0.09737,-0.44346 0.27409,-0.60341l0.135,-0.1215l0,-2.54782l0,-0.14482l-0.01104,-0.00859c-0.01391,-0.29536 0.00368,-1.20068 0.66395,-1.95382c0.67787,-0.77359 1.83478,-1.1655 3.438,-1.1655c1.5975,0 2.75196,0.38905 3.43064,1.15691c0.7965,0.90041 0.66477,2.05241 0.66395,2.06223l-0.00368,2.60141l0.135,0.12191c0.17673,0.15954 0.27409,0.3735 0.27409,0.603l0,1.63636c0,0.35714 -0.234,0.66968 -0.58172,0.77686l-0.20373,0.06259l-0.06546,0.2025c-0.27368,0.85132 -0.66354,1.6376 -1.15936,2.33714c-0.1215,0.17223 -0.23973,0.32482 -0.34241,0.44141l-0.10186,0.11618l0,1.6875c0,0.72409 0.40213,1.37496 1.04973,1.69896l3.90804,1.95381c0.97936,0.48969 1.58768,1.47396 1.58768,2.5691l0,0.83577zm-19,-0.91555l0,0.91555l-4.90909,0l0,-0.73514c0,-0.88773 0.48437,-1.70345 1.28496,-2.14077l2.72127,-1.70714c0.54532,-0.29741 0.88404,-0.86768 0.88404,-1.4895l0,-1.46904l-0.13009,-0.1215c-0.01063,-0.00982 -1.09759,-1.03664 -1.41872,-2.43614l-0.03723,-0.162l-0.13991,-0.09c-0.18818,-0.12191 -0.30068,-0.32604 -0.30068,-0.54573l0,-1.413c0,-0.14727 0.10023,-0.32236 0.2745,-0.48027l0.13459,-0.1215l-0.00082,-2.20091c0.00736,-0.11741 0.2205,-2.88736 3.68264,-2.88736c0.97895,0 1.80327,0.22623 2.45454,0.67254l0,1.93541c-0.26182,0.29823 -0.40909,0.67991 -0.40909,1.07387l0,1.63636c0,0.12436 0.01432,0.24668 0.04132,0.36532c0.01105,0.04745 0.03314,0.09082 0.04827,0.13663c0.0225,0.06873 0.0405,0.1395 0.072,0.20455c0.00041,0.00082 0.00082,0.00123 0.00123,0.00205c0.10473,0.216 0.25732,0.40909 0.44959,0.56331c0.00205,0.00778 0.0045,0.01473 0.00655,0.02209c0.02454,0.09369 0.05031,0.18696 0.07813,0.27819l0.03314,0.10677c0.00573,0.01882 0.01268,0.03804 0.01882,0.05686c0.01432,0.04418 0.02822,0.08796 0.04295,0.13132c0.02455,0.07159 0.05032,0.14564 0.08018,0.22623c0.01269,0.03354 0.02659,0.06382 0.03969,0.09695c0.03354,0.0855 0.06709,0.16814 0.10227,0.24996c0.00859,0.01963 0.01595,0.04091 0.02454,0.06013l0.02291,0.05155c0.01064,0.02373 0.02168,0.045 0.03232,0.06832c0.04009,0.08754 0.07936,0.17222 0.12027,0.25404c0.00655,0.01309 0.01269,0.02741 0.01923,0.0405c0.02577,0.05114 0.05155,0.09941 0.07732,0.1485c0.04418,0.08428 0.08754,0.16364 0.13091,0.24055c0.02127,0.03763 0.04213,0.07445 0.063,0.11004c0.05891,0.10064 0.11495,0.19309 0.16936,0.279c0.01187,0.01841 0.02332,0.03764 0.03477,0.05523c0.099,0.15341 0.18491,0.27777 0.2561,0.37473c0.01881,0.02577 0.03518,0.04786 0.05113,0.06954c0.009,0.01187 0.02127,0.02905 0.02905,0.03969l0,1.35368c0,0.396 -0.216,0.75927 -0.56332,0.94909l-1.08246,0.59032l-0.18859,-0.01678l-0.07691,0.16159l-2.30154,1.2555c-1.18636,0.648 -1.92314,1.88919 -1.92314,3.24082z" id="Combined-Shape"/>
                                </clippath>
                            <g clip-path="url(#nav-icon-users)"><rect height="100%" width="100%"></rect></g>
                            </g>
                        </svg>
                        <div>Users</div>
                    </a>
                    <span>Users</span>
                </li>
            @endif
            @if(auth()->user()->can('Access Company Section') || auth()->user()->can('Access Roles Section'))
                @if(auth()->user()->can('Access Company Section'))
                    <li id="data-step-6" class=" menuLength {{ Request::is('company') ? 'active' : '' }} treeview" data-step="6" data-position="right" data-intro= "<b>{{ config('setting.tour.company') }}</b></br>{{ config('setting.tour.companies_text') }}">
                        <a href="{{ url('company') }}">
                            <svg width="45" height="25" class="nav-icon">

                                <path d="m20.83488,2.87073l-4.73518,0l0,-0.94707c0,-1.0446 -0.84945,-1.89409 -1.89409,-1.89409l-5.68225,0c-1.0446,0 -1.89409,0.84945 -1.89409,1.89409l0,0.94703l-4.73517,0c-1.04465,0.00004 -1.8941,0.84949 -1.8941,1.89409l0,2.84112c0,1.04465 0.84945,1.8941 1.8941,1.8941l7.57633,0l0,-0.47354c0,-0.26173 0.2118,-0.47353 0.47354,-0.47353l2.84112,0c0.26174,0 0.47354,0.2118 0.47354,0.47353l0,0.47354l7.57634,0c1.04455,0 1.894,-0.84945 1.894,-1.8941l0,-2.84112c0,-1.0446 -0.84945,-1.89405 -1.89409,-1.89405zm-6.62927,0l-5.68225,0l0,-0.94707l5.68225,0l0,0.94707zm8.26069,7.14023c0.16092,0.08004 0.26267,0.24416 0.26272,0.42408l0,6.6413c0,1.04464 -0.8495,1.89409 -1.8941,1.89409l-18.94082,0c-1.04465,0 -1.8941,-0.84949 -1.8941,-1.89409l0,-6.6413c0,-0.17992 0.10175,-0.34404 0.26263,-0.42408c0.16141,-0.08093 0.35376,-0.06242 0.49666,0.0467c0.33667,0.25481 0.72879,0.38937 1.13481,0.38937l7.57629,0l0,1.42056c0,0.26173 0.2118,0.47353 0.47354,0.47353l2.84112,0c0.26173,0 0.47353,-0.2118 0.47353,-0.47353l0,-1.42056l7.57625,0c0.40602,0 0.79818,-0.13456 1.13481,-0.38937c0.14245,-0.10819 0.33529,-0.1267 0.49666,-0.0467z" id="Combined-Shape"/>

                                    <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                        <clippath id="nav-icon-companies">
                                        <path d="m20.83488,2.87073l-4.73518,0l0,-0.94707c0,-1.0446 -0.84945,-1.89409 -1.89409,-1.89409l-5.68225,0c-1.0446,0 -1.89409,0.84945 -1.89409,1.89409l0,0.94703l-4.73517,0c-1.04465,0.00004 -1.8941,0.84949 -1.8941,1.89409l0,2.84112c0,1.04465 0.84945,1.8941 1.8941,1.8941l7.57633,0l0,-0.47354c0,-0.26173 0.2118,-0.47353 0.47354,-0.47353l2.84112,0c0.26174,0 0.47354,0.2118 0.47354,0.47353l0,0.47354l7.57634,0c1.04455,0 1.894,-0.84945 1.894,-1.8941l0,-2.84112c0,-1.0446 -0.84945,-1.89405 -1.89409,-1.89405zm-6.62927,0l-5.68225,0l0,-0.94707l5.68225,0l0,0.94707zm8.26069,7.14023c0.16092,0.08004 0.26267,0.24416 0.26272,0.42408l0,6.6413c0,1.04464 -0.8495,1.89409 -1.8941,1.89409l-18.94082,0c-1.04465,0 -1.8941,-0.84949 -1.8941,-1.89409l0,-6.6413c0,-0.17992 0.10175,-0.34404 0.26263,-0.42408c0.16141,-0.08093 0.35376,-0.06242 0.49666,0.0467c0.33667,0.25481 0.72879,0.38937 1.13481,0.38937l7.57629,0l0,1.42056c0,0.26173 0.2118,0.47353 0.47354,0.47353l2.84112,0c0.26173,0 0.47353,-0.2118 0.47353,-0.47353l0,-1.42056l7.57625,0c0.40602,0 0.79818,-0.13456 1.13481,-0.38937c0.14245,-0.10819 0.33529,-0.1267 0.49666,-0.0467z" id="Combined-Shape"/>
                                    </clippath>
                                <g clip-path="url(#nav-icon-companies)"><rect height="100%" width="100%"></rect></g>
                                </g>
                            </svg>
                            <div>Companies</div>
                        </a>
                        <span>Companies</span>
                    </li>
                @endif
                @if(auth()->user()->can('Access Roles Section'))
                    <li id="data-step-7" class=" menuLength {{ Request::is('roles') ? 'active' : '' }} treeview" data-step="7" data-position="right" data-intro= "<b>{{ config('setting.tour.roles') }}</b></br>{{ config('setting.tour.roles_text') }}">
                        <a href="{{ url('roles') }}">
                            <svg width="45" height="25" class="nav-icon">

                               <path d="M22 20.5V23H2v-2.5c0-2.31 3.136-4.248 7.392-4.822.193-.359.304-.76.304-1.184 0-.381-.113-.737-.276-1.073C7.975 12.435 7 10.665 7 8.625 7 5.518 9.239 3 12 3s5 2.518 5 5.625c0 2.04-.975 3.81-2.42 4.796-.163.336-.276.692-.276 1.073 0 .424.11.825.304 1.184C18.864 16.252 22 18.19 22 20.5z"/>
                               <path stroke="#2C3542" d="M14.8 6.77V5.9a4.4 4.4 0 1 1 8.8 0v.87a1.8 1.8 0 0 1 1.3 1.73v5.2a1.8 1.8 0 0 1-1.8 1.8h-7.8a1.8 1.8 0 0 1-1.8-1.8V8.5a1.8 1.8 0 0 1 1.3-1.73zm4.4 3.53a.8.8 0 1 0 0 1.6.8.8 0 0 0 0-1.6zm-2.1-3.6h4.2v-.8c0-1.158-.942-2.1-2.1-2.1-1.157 0-2.1.943-2.1 2.1v.8z"/>
                                    <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                        <clippath id="nav-icon-manageroles">
                                        <path d="M22 20.5V23H2v-2.5c0-2.31 3.136-4.248 7.392-4.822.193-.359.304-.76.304-1.184 0-.381-.113-.737-.276-1.073C7.975 12.435 7 10.665 7 8.625 7 5.518 9.239 3 12 3s5 2.518 5 5.625c0 2.04-.975 3.81-2.42 4.796-.163.336-.276.692-.276 1.073 0 .424.11.825.304 1.184C18.864 16.252 22 18.19 22 20.5z"/>
                                        <path stroke="#2C3542" d="M14.8 6.77V5.9a4.4 4.4 0 1 1 8.8 0v.87a1.8 1.8 0 0 1 1.3 1.73v5.2a1.8 1.8 0 0 1-1.8 1.8h-7.8a1.8 1.8 0 0 1-1.8-1.8V8.5a1.8 1.8 0 0 1 1.3-1.73zm4.4 3.53a.8.8 0 1 0 0 1.6.8.8 0 0 0 0-1.6zm-2.1-3.6h4.2v-.8c0-1.158-.942-2.1-2.1-2.1-1.157 0-2.1.943-2.1 2.1v.8z"/>
                                    </clippath>
                                <g clip-path="url(#nav-icon-manageroles)"><rect height="100%" width="100%"></rect></g>
                                </g>
                            </svg>
                            <div>Manage Roles</div>
                        </a>
                        <span>Manage Roles</span>
                    </li>
                @endif
                @if(auth()->user()->can('Access CSV Upload Section'))
                    <li id="data-step-8" class=" menuLength {{ Request::is('massUpload') ? 'active' : '' }} treeview" data-step="8" data-position="right" data-intro= "<b>{{ config('setting.tour.csv_upload') }}</b></br>{{ config('setting.tour.csv_upload_text') }}">
                        <a href="{{ url('massUpload') }}">
                            <svg width="45" height="25" class="nav-icon">

                                <path d="m12.08698,4.13965l0,-4.13965l-9.75239,0c-1.26361,0 -2.31115,1.04132 -2.31115,2.30475l0,19.3905c0,1.26361 1.04754,2.30475 2.31115,2.30475l13.91749,0c1.26342,0 2.31097,-1.04132 2.31097,-2.30475l0,-14.90167l-3.81592,0c-1.4729,0 -2.66015,-1.18726 -2.66015,-2.65393zm-6.88257,7.73968l3.7207,-3.99353c0.09521,-0.10162 0.22852,-0.15875 0.37445,-0.15875c0.14612,0 0.27301,0.05713 0.37463,0.15875l3.72071,3.99353c0.19043,0.20325 0.17779,0.52698 -0.02545,0.71759c-0.09522,0.08881 -0.22229,0.1333 -0.34278,0.1333c-0.13348,0 -0.27301,-0.05712 -0.37463,-0.15875l-2.85077,-3.04761l0,7.25702c0,0.27942 -0.22852,0.50794 -0.50794,0.50794c-0.27941,0 -0.50793,-0.22852 -0.50793,-0.50794l0,-7.25702l-2.83813,3.04761c-0.19043,0.20307 -0.51435,0.21588 -0.71741,0.02545c-0.20325,-0.19061 -0.21588,-0.50793 -0.02545,-0.71759zm9.82214,8.24762c0,0.27942 -0.22852,0.50794 -0.50793,0.50794l-10.45075,0c-0.27942,0 -0.50793,-0.22852 -0.50793,-0.50794c0,-0.27942 0.22851,-0.50793 0.50793,-0.50793l10.45715,0c0.27942,0 0.50153,0.2287 0.50153,0.50793zm-0.27924,-14.34924c-0.91442,0 -1.64447,-0.73004 -1.64447,-1.63806l0,-3.35871l4.76185,4.99677l-3.11738,0z" id="Combined-Shape"/>

                                    <g fill="#606771" fill-rule="nonzero" id="nav-icon" transform="translate(0.000000, 0.000000)">
                                        <clippath id="nav-icon-csvupload">
                                        <path d="m12.08698,4.13965l0,-4.13965l-9.75239,0c-1.26361,0 -2.31115,1.04132 -2.31115,2.30475l0,19.3905c0,1.26361 1.04754,2.30475 2.31115,2.30475l13.91749,0c1.26342,0 2.31097,-1.04132 2.31097,-2.30475l0,-14.90167l-3.81592,0c-1.4729,0 -2.66015,-1.18726 -2.66015,-2.65393zm-6.88257,7.73968l3.7207,-3.99353c0.09521,-0.10162 0.22852,-0.15875 0.37445,-0.15875c0.14612,0 0.27301,0.05713 0.37463,0.15875l3.72071,3.99353c0.19043,0.20325 0.17779,0.52698 -0.02545,0.71759c-0.09522,0.08881 -0.22229,0.1333 -0.34278,0.1333c-0.13348,0 -0.27301,-0.05712 -0.37463,-0.15875l-2.85077,-3.04761l0,7.25702c0,0.27942 -0.22852,0.50794 -0.50794,0.50794c-0.27941,0 -0.50793,-0.22852 -0.50793,-0.50794l0,-7.25702l-2.83813,3.04761c-0.19043,0.20307 -0.51435,0.21588 -0.71741,0.02545c-0.20325,-0.19061 -0.21588,-0.50793 -0.02545,-0.71759zm9.82214,8.24762c0,0.27942 -0.22852,0.50794 -0.50793,0.50794l-10.45075,0c-0.27942,0 -0.50793,-0.22852 -0.50793,-0.50794c0,-0.27942 0.22851,-0.50793 0.50793,-0.50793l10.45715,0c0.27942,0 0.50153,0.2287 0.50153,0.50793zm-0.27924,-14.34924c-0.91442,0 -1.64447,-0.73004 -1.64447,-1.63806l0,-3.35871l4.76185,4.99677l-3.11738,0z" id="Combined-Shape"/>
                                    </clippath>
                                <g clip-path="url(#nav-icon-csvupload)"><rect height="100%" width="100%"></rect></g>
                                </g>
                            </svg>
                            <div>CSV Upload</div>
                        </a>
                        <span>CSV Upload</span>
                    </li>
                @endif
            @endif
            <!-- <li class="treeview">
                <a href="#" id="c-sidebar-toggle">
                    <i class="fa fa-lightbulb-o" aria-hidden="true"></i>
                </a>
            </li> -->
            <!-- <li class="treeview">
                <a href="#" data-toggle="offcanvas" role="button">
                    <i class="fa fa-angle-double-right" aria-hidden="true"></i>
                </a>
            </li> -->
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
