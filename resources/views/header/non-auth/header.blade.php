<header class="login-header container-fluid">
    <h1 class="login__logo">
        <a href="{{ url('/login') }}"><img src="{{ url('images/ripkit-logo-new.png') }}" class="logo__icon" alt="Site-logo"></a>
    </h1><!-- /.login__logo -->
</header>
