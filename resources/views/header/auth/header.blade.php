
<header class="main-header">
    <!-- Logo -->
    <!-- <span class="logo" class="sidebar-toggle" data-toggle="offcanvas" role="button">

      <span class="logo-mini"><b>BI</b><i class="fa fa-angle-down pull-right"></i></span>

      <span class="logo-lg"><b>Build</b> Impossible</span>
    </span> -->

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top navbar-fixed-top">
        <div>
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <svg width="20" height="17" viewBox="0 0 20 17">
            <path fill="#ff6800" fill-rule="nonzero" d="M1.16 2.312h17.68a1.157 1.157 0 1 0 0-2.312H1.16a1.157 1.157 0 0 0 0 2.312zm17.68 4.767H1.16a1.157 1.157 0 0 0 0 2.312h17.68a1.157 1.157 0 1 0 0-2.312zm0 7.08H1.16c-.641 0-1.16.518-1.16 1.155 0 .64.519 1.157 1.16 1.157h17.68c.641 0 1.16-.518 1.16-1.157 0-.637-.519-1.156-1.16-1.156z"/>
        </svg>

            <span class="sr-only">Toggle navigation</span>
        </a>
        
        <!--  Logo -->

        <div class=" headerMenuLength treeview pull-left" id="data-step-1" data-step="1" data-position="bottom-left-aligned" data-intro= "<b>{{ config('setting.tour.company_dropdown') }}</b></br>{{ config('setting.tour.company_details') }}">
            <a href="#" class="company-logo-link">
                <div class="logo-container">
                    <div class="logo-img">
                    <?php if (!empty(\App\Model\Company::find(session('company_id'))->companyImage()) && (\App\Model\Company::find(session('company_id'))->companyImage() != asset('images/defaultCompany.jpg'))) { ?>  
                        <img src="{{ \App\Model\Company::find(session('company_id'))->companyImage() }}" alt="Company Logo" />
                    <?php } else {  ?>
                        <img src="../images/company-placeholder.png" alt="Company logo">
                    <?php } ?>
                    </div>
                    <div class="logo-detail hidden-xs">
                        <div class="logo-detail-txt">
                            <div class="logo-mini">Company Name</div>
                            <span>{{ session('company_name') }}</span>

                        </div>
                        <i class="fa fa-caret-down"></i>
                    </div>
                </div>
            </a>
            @if((\App\Model\Company::ifLoginDeviceMobile()) == 1)
            
            <div class="visible-xs company-drop">
            <select id="companyselectmobile">
                            @foreach(session('companies') as $company)
                                @if(empty($company->companies))
                                    <option class="companyselectMobile" value="{{ url('changeCompany').'/'.$company->id.'-'.$company->status.'-'.$company->name }}"
                                            @if($company->name == session('company_name')) selected @endif>{{ $company->name }}</option>
                                @else
                                    <option class="companyselectMobile" value="{{ url('changeCompany').'/'.$company->companies->id.'-'.$company->companies->status.'-'.$company->companies->name }}"
                                            @if($company->companies->name == session('company_name')) selected @endif>{{ $company->companies->name }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>
            @else     
                <ul class="treeview-menu company-sidebar-dropdown hidden-xs">

                    @if(!empty(session('companies')))
                        @if(count(session('companies')) <= 3)
                            <div class="chosen-container">
                                <ul class="company-dropdown-three chosen-results">
                                    @foreach(session('companies') as $company)
                                        @if(empty($company->companies))
                                            <li class="li-company active-result @if($company->name == session('company_name')) highlighted @endif"
                                                data-value="{{ url('changeCompany').'/'.$company->id.'-'.$company->status.'-'.$company->name }}"><a
                                                        href="#"> {{ $company->name }}</a>
                                            </li>
                                        @else
                                            <li class="li-company active-result @if($company->companies->name == session('company_name')) highlighted @endif"
                                                data-value="{{ url('changeCompany').'/'.$company->companies->id.'-'.$company->companies->status.'-'.$company->companies->name }}"><a
                                                        href="#">{{ $company->companies->name }}</a>
                                            </li>
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        @else
                            <select id="companyselect" class="scrollable-menu">
                                <option value=""></option>
                                @foreach(session('companies') as $company)
                                    @if(empty($company->companies))
                                        <option value="{{ url('changeCompany').'/'.$company->id.'-'.$company->status.'-'.$company->name }}"
                                                @if($company->name == session('company_name')) selected @endif>{{ $company->name }}</option>
                                    @else
                                        <option value="{{ url('changeCompany').'/'.$company->companies->id.'-'.$company->companies->status.'-'.$company->companies->name }}"
                                                @if($company->companies->name == session('company_name')) selected @endif>{{ $company->companies->name }}</option>
                                    @endif
                                @endforeach
                            </select>
                        @endif
                    @endif
                </ul>
           @endif
           
        </div>
        </div>
        <!--<span class="company-logo"> <img src="{{ \App\Model\Company::find(session('company_id'))->companyImage() }}"></span>-->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            
            <span class="to-do-icon tour-icon overlay-on">
               guided tour
            </span>
            <!--@if((\App\Model\Company::ifLoginDeviceMobile()) != 1)-->
            <!--@endif-->
            <span class=" headerMenuLength to-do-icon to-do-list" id="data-step-9" data-step="9" data-position="left" data-intro= "<b>{{ config('setting.tour.to_do_list') }}</b></br>{{ config('setting.tour.to_do_list_text') }}">
                to do icon
            </span>
            <ul class="nav navbar-nav">
            @if(!auth()->user()->hasRole('Super Admin'))
                <li class="headerMenuLength" data-step="10" id="data-step-10" data-position="left" data-intro= "<b>{{ config('setting.tour.notification') }}</b></br>{{ config('setting.tour.notification_text') }}">
                    <a href="#" data-toggle="control-sidebar" class="notify-bell" id="notification-bell">
                        <div class="notification-icon">
                            <svg width="25" height="25">
                                <g class="layer">
                                <g fill="none" fill-rule="evenodd" id="">
                                <g fill="#FF6800" fill-rule="nonzero" id="notifications-icon" transform="translate(2.000000, 0.000000)">
                                <path d="m10.27644,25c1.41665,0 2.56412,-1.14742 2.56412,-2.56412l-5.12818,0c-0.00006,1.4167 1.14742,2.56412 2.56406,2.56412z" id="notification-bottom-shape"/>
                                <path d="m17.96875,17.30769l0,-6.41027c0,-3.94231 -2.09615,-7.23077 -5.76923,-8.10259l0,-0.87175c0,-1.06413 -0.85896,-1.92308 -1.92308,-1.92308c-1.06412,0 -1.92307,0.85895 -1.92307,1.92308l0,0.87181c-3.67308,0.87182 -5.76924,4.16028 -5.76924,8.10259l0,6.41021l-2.56412,2.56412l0,1.28204l20.5128,0l0,-1.28204l-2.56406,-2.56412z" id="notification-top-shape"/>
                                </g>
                                </g>
                                </g>
                            </svg>
                        </div>
                        <span class="label label-theme notification-count"
                            style="@if(auth()->user()->unreadNotifications->count() == 0 )
                                    display: none;
                                    @else
                                    display: block;
                            @endif">
                        </span>
                    </a>
                </li>
                @endif
                <li class=" headerMenuLength dropdown user user-menu"  id="data-step-11" data-step="11" data-position="left" data-intro= "<b>{{ config('setting.tour.account') }}</b></br>{{ config('setting.tour.account_text') }}">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::user()->userImage() }} " class="user-image" alt="User Image">
                        <span class="v-center"><span class="author hidden-xs">{{ \Auth::user()->name }}</span> <i
                                    class="fa fa-caret-down pull-right"></i></span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <!-- Menu Body -->


                        <li class="user-body">
                            <ul class="user-blody__list">
                                <li class="user-blody__list-item">
                                    <a href="{{ url('account') }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#2C3542" fill-rule="nonzero" d="M19.176 21H6v-.702c0-.807.44-1.548 1.148-1.934l2.799-1.527c.468-.255.759-.746.759-1.279v-1.26l-.061-.073-.012-.014-.052-.066a2.982 2.982 0 0 1-.064-.086v-.002a5.7 5.7 0 0 1-.172-.251l-.004-.007-.097-.156-.007-.012a6.349 6.349 0 0 1-.34-.655l-.017-.037a6.314 6.314 0 0 1-.052-.122l-.018-.047a5.18 5.18 0 0 1-.141-.396l-.018-.056-.006-.02c-.03-.099-.058-.2-.082-.304l-.028-.124-.107-.07a.625.625 0 0 1-.29-.525V10.02a.62.62 0 0 1 .21-.463l.103-.093V7.399l-.008-.007c-.011-.226.002-.92.509-1.498C10.472 5.3 11.359 5 12.588 5c1.225 0 2.11.298 2.631.887.611.69.51 1.574.51 1.582l-.004 1.995.104.093c.136.123.21.287.21.463v1.255c0 .273-.18.513-.446.595l-.156.048-.05.156a6.568 6.568 0 0 1-.89 1.792c-.093.132-.183.249-.262.338l-.078.09v1.294c0 .555.308 1.054.805 1.303l2.997 1.498a2.19 2.19 0 0 1 1.217 1.97V21z"/>
                                    </svg>
                                        <span>View Account</span>
                                    </a>
                                </li>
                                <li class="user-blody__list-item">
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                        <svg width="25" height="25" viewBox="0 0 25 25">
                                            <g fill="#F14343" fill-rule="nonzero">
                                                <path d="M18.538 10.055a.839.839 0 0 1 .002-1.182.833.833 0 0 1 1.18 0l3.03 3.031a.834.834 0 0 1 .005 1.186l-3.035 3.035a.833.833 0 0 1-1.182 0 .84.84 0 0 1 0-1.182l1.61-1.609h-3.802v-1.67h3.802l-1.61-1.609z"/>
                                                <path d="M6.049 5.023h7.514a2.786 2.786 0 0 1 2.783 2.784v3.857H9.047a.837.837 0 0 0-.836.836c0 .46.375.835.836.835h7.3V17.196a2.786 2.786 0 0 1-2.784 2.784H6.05a2.786 2.786 0 0 1-2.783-2.784v-9.39a2.786 2.786 0 0 1 2.783-2.783z"/>
                                            </g>
                                        </svg>
                                        <span class="logout">Log Out</span>
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}</form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </li>
                
                
            </ul>
        </div>

    </nav>

</header>

<!-- Modal -->
<div class="modal fade quqicksetup" id="quicksetup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-header">
   
        <figure class="site-logo"><img src="../images/new-site/site-logo-white.svg" alt="Site Logo" /></figure>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        </button>
  </div>
  <div class="modal-dialog" role="document">
    <div class="modal-content">
 
      <div class="modal-body">
      <div id="setup-slider" class="carousel slide" data-ride="carousel" data-interval="false">
    
           

            <!-- Wrapper for slides -->
            <div class="carousel-inner">
                <div class="item active">
                    <div class="text-box">
                        <h3>RIP Period(s) List</h3>
                        <p>You may access the list containing the active RIP Periods by clicking on the highlighted button in the left bar. <br />
                        You may add a new RIP period by clicking the highlighted ‘ADD RIP PERIOD’ button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide18.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>RIP Period Detail</h3>
                        <p>Once you click on one of the RIP Periods from the list, the detail of that particular RIP Period would be displayed. 
The highlighted progress icon on the right side is auto modified based on the progress of assiociated RIP(s).</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide22.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Edit/Delete Rip Period</h3>
                        <p>You may use the highlighted buttons to edit and/or delete a RIP.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide8.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Schedule Meeting</h3>
                        <p>You may schedule a meeting in regard to a RIP period through the highlighted button.</p>
                        <div class="image-box" style="position:relative;">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide6.png') }}" alt="Site Logo" />   
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Meeting Calendar</h3>
                        <p>You may find a list of various meetings - scheduled & passed, through the highlighted button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide5.png') }}" alt="Site Logo" />
                        </div>
                   
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Mark as Expired</h3>
                        <p>You may use the highlighted button to mark a RIP period as expired and, therefore, send it to the Expired RIP Period(s) list.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide2.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Export Rip Period(s)</h3>
                        <p>You can download complete details of all the active RIP periods by clicking on the highlighted buttons.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide1.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Expired RIP Period(s) List</h3>
                        <p>A list of all the expired RIP periods could be accessed by clicking the highlighted icon.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide4.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Expired RIP Period Detail</h3>
                        <p>Once we click on an expired RIP period from the list, the detail of that particular RIP period is displayed. We may perform a number of actions here, accessible through the three dots. One such action is duplicating an expired RIP period. You may do so through the highlighted button. 
                          
                            Note that the start date of the duplicate period would be the present date.
                        </p>
                        
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide9.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Undo Expired Status</h3>
                        <p>You may revert your 'Mark as Expired' action and make an expired RIP period active again, by clicking the highlighted button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide7.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Delete an Expired RIP Period</h3>
                        <p>You may use the highlighted buttons to edit and delete a RIP period.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide3.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Add a Category</h3>
                        <p>In order to add a category under a RIP period, you may add the description in the highlighted area and click on the ‘ADD CATEGORY’ button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide19.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Edit/Delete a Category</h3>
                        <p>You may use the highlighted buttons to edit and/or delete a category.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide21.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Add a RIP</h3>
                        <p>In order to add a RIP under a category, you need to add the description in the highlighted area and click on the ‘ADD RIP’ button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide20.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Edit/Delete a RIP</h3>
                        <p>You may use the highlighted buttons to edit and/or delete a RIP.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide14.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Actions on a RIP</h3>
                        <p>Once a RIP has been created, you may edit the due date by using the highlighted dropdown. You may assign the RIP to a user by clicking the highlighted green button. You may also update the progress by clicking the ’x%’ text (x denoting numeric value).</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide17.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Update Progress</h3>
                        <p>You may also upadate the progress of a RIP through the highlighted option accessible from the three dots.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide10.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Add a Task</h3>
                        <p>In order to add a task under a RIP, you need to add the description in the highlighted area and click on the ‘ADD TASK’ button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide16.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Actions on a Task</h3>
                        <p>Once a task has been created, you may mark it as complete by clicking on the highlighted check icon. You may assign the task to specific users by clicking the highlighted green button. You may also view a list of concerned users by clicking on ‘+x’ button (x denoting number of users).</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide12.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Edit/Delete a Task</h3>
                        <p>You may use the highlighted buttons to edit and/or delete a task.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide15.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Add a Comment</h3>
                        <p>In order to add a comment under a RIP, you need to add the description in the highlighted area and click on the ‘ADD COMMENT’ button.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide13.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="text-box">
                        <h3>Edit/Delete a Comment</h3>
                        <p>You may use the highlighted buttons to edit and/or delete a comment.</p>
                        <div class="image-box">
                            <img class="img-responsive" height="calc(100% / 19*8)" src="{{ asset('images/new-site/slide11.png') }}" alt="Site Logo" />
                        </div>
                    </div>
                </div>

                <div class="item">
                    <div class="text-box">
                        <h3>Complete Walkthrough</h3>
                        <p>Watch this video for a complete walkthrough of the entire RIP period section.</p>
                        <div class="image-box" style="position:relative;">
                        <video id="my-video" class="video-js" controls preload="auto" width="960" height="540"
                            poster="{{ asset('images/new-site/slide1.png') }}" data-setup="{}">
                          
                    <source src="{{ asset('vedio/ripkit-video.mp4') }}" type="video/mp4" />

                    <p class="vjs-no-js">
                    To view this video please enable JavaScript, and consider upgrading to a web browser that
                    <a href="https://videojs.com/html5-video-support/" target="_blank">supports HTML5 video</a>
                    </p>
                </video>
                        </div>
                    </div>
                </div>
            </div>
             <!-- Indicators -->
             <ol class="carousel-indicators">
                <li data-target="#setup-slider" data-slide-to="0" class="active"></li>
                <li data-target="#setup-slider" data-slide-to="1"></li>
                <li data-target="#setup-slider" data-slide-to="2"></li>
                <li data-target="#setup-slider" data-slide-to="3"></li>
                <li data-target="#setup-slider" data-slide-to="4"></li>
                <li data-target="#setup-slider" data-slide-to="5"></li>
                <li data-target="#setup-slider" data-slide-to="6"></li>
                <li data-target="#setup-slider" data-slide-to="7"></li>
                <li data-target="#setup-slider" data-slide-to="8"></li>
                <li data-target="#setup-slider" data-slide-to="9"></li>
                <li data-target="#setup-slider" data-slide-to="10"></li>
                <li data-target="#setup-slider" data-slide-to="11"></li>
                <li data-target="#setup-slider" data-slide-to="12"></li>
                <li data-target="#setup-slider" data-slide-to="13"></li>
                <li data-target="#setup-slider" data-slide-to="14"></li>
                <li data-target="#setup-slider" data-slide-to="15"></li>
                <li data-target="#setup-slider" data-slide-to="16"></li>
                <li data-target="#setup-slider" data-slide-to="17"></li>
                <li data-target="#setup-slider" data-slide-to="18"></li>
                <li data-target="#setup-slider" data-slide-to="19"></li>
                <li data-target="#setup-slider" data-slide-to="20"></li>
                <li data-target="#setup-slider" data-slide-to="21"></li>
                <li data-target="#setup-slider" data-slide-to="22"></li>
            </ol>
              <!-- Left and right controls -->
            <a class="left carousel-control" href="#setup-slider" data-slide="prev">
                <span class="glyphicon prev">
                    <img src="../images/new-site/arrow-next-white.svg" alt="previous slide" />
                </span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#setup-slider" data-slide="next">
                <span class="glyphicon next">
                    <img src="../images/new-site/arrow-next-white.svg" alt="next slide" />
                </span>
                <span class="sr-only">next</span>
            </a>
 
        </div>
      </div>
  
    </div>
  </div>
</div>


