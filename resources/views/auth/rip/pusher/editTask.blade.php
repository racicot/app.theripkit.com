
<div class="modal-dialog modal-add-company" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit Task</h4>
        </div>
        <div class="modal-body">
            <form id="editTask" method="POST" action="{{ url('taskName') }}">
                {{ csrf_field() }}
                
                <input type="hidden" name="task_id" value="{{ $tasks->id }}">
                <div class="form-group">
                    <label for="name">Task Name</label>
                    <textarea class="form-control taskName" id="name" name="name"
                        placeholder="Task Name" autofocus>{{ $tasks->name }}</textarea>
                    <span class="error-msg" id="name-err"></span>
                </div>
                

                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-theme pull-right editTaskButton">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

