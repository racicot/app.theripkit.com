<div class="modal-dialog modal-add-company modal-edit-comment" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit Comment</h4>

        </div>
        <div class="modal-body">
            <form action="{{ url('comments/update') . '/' . $comment->id }}"
                          commentId="{{ $comment->id }}"
                          id="editCommentForm_{{ $comment->id }}"
                          method="post"
                          class="editCommentForm"
                          enctype="multipart/form-data">
                {{ csrf_field() }}
                
                <input type="hidden" name="commentId" id="commentId" value="{{ $comment->id }}" class="commentId">
                 <input type="hidden" name="mention_comment" id="mention_comment" value="{{ $comment->mention_comment }}">
                <div>
                    <div class="form-group">
                        <label for="name">Comment</label>
                        <div class="textarea-comment-group">                        
                            <textarea name="comment" id="edit-comment" data-gramm_editor="false" cols="30" rows="3" class="textarea--full addCommentSection mention-example2" rows="4" cols="20"><?php echo stripcslashes(htmlspecialchars_decode($comment->message));?></textarea>  
                             <span class="error-msg" id="edit-comment-error"></span> 
                        </div>  
                    </div>
                    <div class="form-group comment-field-option">
                        <label for="name">Attach File</label>
                        <div class="d-flex comment-field-row">
                            <div class="file-upload-name"></div>
                                <div class="comment-box-options-item">
                                    <label for="upload-file_{{ $comment->id }}" class="comment-link">
                                        <svg width="25" height="25" viewBox="0 0 25 20">
                                            <path fill="#2C3542" fill-rule="nonzero" d="M11.6 12.03a.631.631 0 0 0-.892-.892l-3.339 3.339a2.943 2.943 0 0 0-.877 2.108c0 .8.308 1.538.877 2.107a2.971 2.971 0 0 0 4.216 0l7.538-7.538a4.131 4.131 0 0 0 1.215-2.939c0-1.107-.43-2.153-1.215-2.938a4.16 4.16 0 0 0-5.892 0L5.708 12.8a5.306 5.306 0 0 0-1.57 3.785 5.28 5.28 0 0 0 1.57 3.784 5.306 5.306 0 0 0 3.784 1.57 5.28 5.28 0 0 0 3.785-1.57l3.338-3.338a.631.631 0 0 0-.892-.893l-3.338 3.339c-.77.77-1.8 1.2-2.893 1.2a4.074 4.074 0 0 1-2.892-1.2c-.77-.77-1.2-1.8-1.2-2.892 0-1.093.43-2.123 1.2-2.893l7.523-7.523a2.9 2.9 0 0 1 4.092 0 2.9 2.9 0 0 1 0 4.092L10.677 17.8c-.662.662-1.754.662-2.43 0a1.711 1.711 0 0 1 0-2.43l3.353-3.34z"></path>
                                        </svg></label>
                                    <input type="file" id="upload-file_{{ $comment->id }}" class="custom-input-file upload-file" name="attachment[]" value="" multiple="multiple">
                            </div>
                        </div>
                        <span class="error-msg max-file-error" id="edit-attach-error"></span>
                    </div>
                </div>

                @if(count($comment->attachment) > 0)
                <div class="form-group mgb-0">
                    <div class="media-attachment attachmentList_{{ $comment->id }}">
                        <!-- <span class="attachment-heading">{{ count($comment->attachment) }}
                            attachments</span> -->

                        @foreach($comment->attachment as $attachment)
                            <div class="commentAttachment_{{ $attachment->id }}">
                                
                                    <div class="comment-modal-attachement comments-links-items commentAttachment_7">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                            <path fill="#2C3542" fill-rule="nonzero" d="M11.6 12.03a.631.631 0 0 0-.892-.892l-3.339 3.339a2.943 2.943 0 0 0-.877 2.108c0 .8.308 1.538.877 2.107a2.971 2.971 0 0 0 4.216 0l7.538-7.538a4.131 4.131 0 0 0 1.215-2.939c0-1.107-.43-2.153-1.215-2.938a4.16 4.16 0 0 0-5.892 0L5.708 12.8a5.306 5.306 0 0 0-1.57 3.785 5.28 5.28 0 0 0 1.57 3.784 5.306 5.306 0 0 0 3.784 1.57 5.28 5.28 0 0 0 3.785-1.57l3.338-3.338a.631.631 0 0 0-.892-.893l-3.338 3.339c-.77.77-1.8 1.2-2.893 1.2a4.074 4.074 0 0 1-2.892-1.2c-.77-.77-1.2-1.8-1.2-2.892 0-1.093.43-2.123 1.2-2.893l7.523-7.523a2.9 2.9 0 0 1 4.092 0 2.9 2.9 0 0 1 0 4.092L10.677 17.8c-.662.662-1.754.662-2.43 0a1.711 1.711 0 0 1 0-2.43l3.353-3.34z"></path>
                                        </svg>
                                        <div class="comments-show-file">
                                            <a class="mr10" href="{{ $attachment->attachmentUrl() }}" target="_blank">
                                                <i class="fa fa-picture-o" aria-hidden="true"></i>
                                                {{ $attachment->attachment }}
                                            </a>
                                        </div>
                                        <div class="delete-btn">
                                            <a href="{{ url('deleteAttachment').'/'.$attachment->id }}" aria-hidden="true" class="delete-attachment" fileName="{{$attachment->attachment}}">
                                                <svg width="25" height="25" viewBox="0 0 25 25">
                                                    <path fill="#2C3542" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"></path>
                                                </svg>
                                            </a>    
                                        </div>
                                    </div>
                               
                            </div>
                        @endforeach

                    </div><!-- /.media-attachment -->
                </div>
                @endif

                <div class="modal-footer mt-15">
                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" class="btn btn-theme pull-right editCommentButton">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
