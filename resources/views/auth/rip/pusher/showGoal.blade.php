<div id="goalStart_{{ $goal->id }}">
    <div class="panel-item ">

        <div class="n-panel-body-top n-rip-card ">
            <div class="accordion-header">
            <svg width="25" height="25" viewBox="0 0 25 25">
                <g fill="none" fill-rule="nonzero" transform="translate(3 3)">
                    <rect width="19" height="19" fill="#2C3542" opacity=".5" rx="2"/>
                    <g fill="#FFF" transform="translate(3 6)">
                        <rect width="9" height="1" x="3" rx=".5"/>
                        <rect width="9" height="1" x="3" y="3" rx=".5"/>
                        <rect width="9" height="1" x="3" y="6" rx=".5"/>
                        <rect width="1" height="1" rx=".5"/>
                        <rect width="1" height="1" y="3" rx=".5"/>
                        <rect width="1" height="1" y="6" rx=".5"/>
                    </g>
                </g>
            </svg>

            </div>
            <div class="n-rip-card-left">
                <div class="rip-card-in-left">

                    <h3 class="card-list__heading accordion-header goalName" goal_id="{{ $goal->id }}"
                        @if(auth()->user()->can('Update The Name Of A Goal'))
                        contenteditable="true" @endif>{{ $goal->goal }}</h3>

                </div>
                <div class="rip-card-in-right">
                    <p class="rip-card-subhead accordion-header taskCount_{{ $goal->id }}">{{ $goal->task->count() }}
                        Tasks </p>
                        <p class="margin-0">|</p>
                    <p class="rip-card-subhead show-goal-comments" goalId= {{ $goal->id }}>
                        <span class="commentCount_{{ $goal->id }}">{{ $goal->comment->count() }}</span>
                        <i class="fa fa-comment-o" aria-hidden="true"></i>

                    </p>
                </div>
            </div>
            <div class="n-rip-card-right goal-right due-date-wrap">
                <div class="rip-card-in-right">
                    <form action="{{ url("/goalDueDate")}}">
                        <div class="form-group">

                            <input type="text"
                                   class="form-control inputDateStart_{{ $goal->id }} dueDate dueDateUpdate_{{ $goal->id }}"
                                   data-toggle="tooltip" title="Due Date" name="due_date"
                                   value="{{ \Carbon\Carbon::parse($goal->due_date)->format('F d Y') }}"
                                   @if(!auth()->user()->can('Update The Due Date Of A Goal')) disabled
                                   @endif placeholder="Due Date">

                            <input type="hidden" name="goal_id" value="{{ $goal->id }}">
                        </div>
                    </form>
                </div>
                <div class="n-avatar-wrap">
                    <div class="goalUserList goal-avatar-wrap goalUserTabList_{{ $goal->id}}">
                        @foreach($goalusers as $goaluser)
                            @if(in_array($goaluser->user->id, explode(',', $goal->assign_to)))
                                <div class="card-list-avatar">
                                    <img data-toggle="tooltip" src="{{ $goaluser->user->userImage() }}"
                                         title="{{ $goaluser->user->name }}">
                                    {{ $goaluser->user->name }}
                                </div>
                            @endif
                        @endforeach
                    </div>
                </div><!-- /.avatar-wrap -->
                <div class="v-bottom">
                    <div class="v-bottom">
                        <div class="progress-group">
                            <span class="progress-text">Progress</span>
                            <span class="progress-number innerGoalAvgPer_{{ $goal->id }}">{{ round($goal->percentage_complete).'%' }}</span>

                            <div class="progress sm">
                                <div class="progress-bar progress-bar-aqua innerGoalAvgWidth_{{ $goal->id }}"
                                    style="width: {{ round($goal->percentage_complete)}}%"></div>
                            </div>
                        </div>
                        @if(auth()->user()->can('Update Goal Percentage'))
                            <div class="dropdown">
                                <a href="#" class="link-gray dropdown-toggle edit-progress"><i
                                            class="fa fa-2x fa-pencil" aria-hidden="true"></i></a>
                                <div class="dropdown-menu dropdown-menu-right edit-progress-popup"
                                     aria-labelledby="addUser">
                                    <form method="post" class="goalPercentageUpdate taskPercentageUpdate"
                                          action="{{ url('goalPercentageUpdate').'/'.$goal->id }}">
                                        <div class="edit-input">
                                            <input type="number" min="0" max="100" step="1" class="form-control"
                                                   name="percentage" aria-label="...">
                                        </div>
                                        <div class="edit-buttons">
                                            <a href="#" class="btn btn-gray close-btn">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                            </a>
                                            <button type="submit" goalId="{{ $goal->id }}"
                                                    class="btn btn-theme save-btn">
                                                <i class="fa fa-check" aria-hidden="true"></i>
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>
                @if((auth()->user()->can('Delete Goal From RIP')) || auth()->user()->can('Show Incomplete Tasks Of Goal') || auth()->user()->can('Show Complete Tasks Of Goal'))
                    <div class="dropdown menu-top-right">
                        <div class="kebab-menu dropdown-toggle" id="kebab-menu" data-toggle="dropdown"
                             role="button" aria-haspopup="true" aria-expanded="false">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul class="dropdown-menu" id="kebab-menu" aria-labelledby="kebab-menu">
                            @if(auth()->user()->can('Delete Goal From RIP'))
                                <li><a class="deleteGoalItem" href="{{ url('/').'/deleteGoal/'.$goal->id }}">Delete
                                        Goal</a></li>@endif
                            @if(auth()->user()->can('Show Incomplete Tasks Of Goal'))
                                <li><a class="taskStatus" href="{{ url('/').'/incompleteTask/'.$goal->id }}">Incomplete
                                        Task</a></li>@endif
                            @if(auth()->user()->can('Show Complete Tasks Of Goal'))
                                <li><a class="taskStatus" href="{{ url('/').'/completeTask/'.$goal->id }}">Complete
                                        Task</a></li>@endif
                        </ul>
                    </div>
                @endif
                <div class="show-comments-sidebar js-show-comments" goalId= {{ $goal->id }}>
                    <i class="fa fa-angle-double-right " aria-hidden="true"></i>
                </div>
            </div><!-- /.rip-card-right -->
        </div>
        <div id="collapseIn" class="panel-body panel-w-100 n-task-panel panel-collapse collapse" role="tabpanel"
             aria-labelledby="headingIn">
            <div id="ajaxTask_{{ $goal->id }}">

            </div>
            @if(auth()->user()->can('Add A Task In RIP'))
                <div class="panel-item add-panel">
                    <div class="add-goal-wrap">
                        <form method="post" id="taskForm_{{ $goal->id  }}" class="taskForm"
                              action="{{ url('ripAddTask').'/'.base64_encode($goal->id) }}">
                            {{ csrf_field() }}

                            <input type="text" name="task" placeholder="Add Task" class="form-control form-bg-control ajaxAddTask">
                        </form>
                    </div>
                </div><!-- /.panel-item -->
            @endif
        </div><!-- /.panel-body -->
    </div><!-- /.panel-item -->
</div>

