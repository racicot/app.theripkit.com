
<div class="modal-dialog modal-add-company" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit RIP</h4>
        </div>
        <div class="modal-body">

            <form id="editRipTab"  method="POST" action="{{ url('goalName') }}">
                {{ csrf_field() }}
                
                <input type="hidden" name="goal_id" value="{{ $currentRip->id }}">
                <div class="form-group">
                    <label for="name">RIP Name</label>
                    <input type="text" class="form-control ajaxAddGoal" id="name" name="name"
                        placeholder="RIP Name"  value="{{ $currentRip->goal }}" maxlength="256" autofocus>
                    <span class="error-msg" id="name-err"></span>
                </div>
                

                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" id="editRipTabButton" class="btn btn-theme pull-right">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->

