<div style="margin-right: auto;">
    <h1 class="rip-heading" id="rip_content">{{ $currentRip->name }}</h1>
    <div class="rip-date-header">
        <span id="date-header">{{ date('M d, Y', strtotime($currentRip->start_date)).' - '.date('M d, Y', strtotime($currentRip->end_date)) }}</span>

        @if ($currentRip->expired == 0)
            <span id="left-days" @if(\Carbon\Carbon::now()->format('Y-m-d') > $currentRip->end_date)class="red-bg-button" @endif> {{ $left }}  </span>

        @else
            <span class="red-bg-button">Expired</span>
        @endif
    </div>
</div>
<div class="rip-period-progress align-items-end">
     <div class="progress-radial progress-radial--md progress-{{ $currentRip->getRipPercentage() }}">
        <div class="overlay ripPer_{{ $currentRip->id }}">
        <span>{{ $currentRip->getRipPercentage() }}</span>
        <span>%</span>
        </div>
    </div>

    <div class="dropdown menu-top-right">
       <div class="kebab-menu dropdown-toggle" id="kebab-menu" data-toggle="dropdown"
                role="button" aria-haspopup="true" aria-expanded="false">
           <span></span>
            <span></span>
           <span></span>
       </div>
        <ul class="dropdown-menu" id="kebab-menu" aria-labelledby="kebab-menu">
            @if ($currentRip->expired == 0)
                @if(auth()->user()->can('Edit A RIP'))
                    <li id="editRip">
                        <a class="editRipLink" data-toggle="modal" data-target="#editRipModal" href="{{ url('/').'/editRipPeriod/'.$currentRip->id }}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                            <path fill="#2C3542" fill-rule="nonzero" d="M6 17.084V20h2.916l8.601-8.601-2.916-2.916L6 17.083zm13.773-7.94a.774.774 0 0 0 0-1.097l-1.82-1.82a.774.774 0 0 0-1.097 0l-1.423 1.424 2.916 2.916 1.424-1.423z"/>
                        </svg>
                        Edit RIP Period
                        </a>
                   </li>
                   <li id="scheduleForRip" value="{{$currentRip->id}}">
                        <a class="scheduleMeetingPopUp" data-toggle="modal" data-target="#scheduleMeeting" href="#">
                        <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                            <g fill="#2C3542" fill-rule="nonzero">
                                <path d="M16 2c-3.308 0-6 2.72-6 6.018C10 11.317 12.692 14 16 14s6-2.683 6-5.982C22 4.72 19.308 2 16 2zm3 7h-4V4h1.25v3.75H19V9zM3 21.463V23h9v-1.537C12 19.002 9.981 17 7.5 17S3 19.002 3 21.463zM10 13.5a2.5 2.5 0 1 1-5 0 2.5 2.5 0 0 1 5 0z"/>
                            </g>
                        </svg>
                        Schedule Meeting
                        </a>
                   </li>
                   <li id="meetingCalenderForRip" value="{{$currentRip->id}}">
                        <a class="meetingCalenderForRipList" href="#">
                            <svg xmlns="http://www.w3.org/2000/svg" width="23" height="23" viewBox="0 0 23 23">
                                <path fill="#2C3542" fill-rule="nonzero" d="M2.875 3.594V.719a.718.718 0 1 1 1.437 0v2.875a.718.718 0 1 1-1.437 0zm12.219.719a.718.718 0 0 0 .719-.72V.72a.718.718 0 1 0-1.438 0v2.875c0 .397.322.719.719.719zM23 17.25a5.75 5.75 0 1 1-11.5 0 5.75 5.75 0 0 1 11.5 0zm-1.437 0a4.317 4.317 0 0 0-4.313-4.313 4.317 4.317 0 0 0-4.312 4.313 4.317 4.317 0 0 0 4.312 4.312 4.317 4.317 0 0 0 4.313-4.312zM5.75 8.625H2.875V11.5H5.75V8.625zm-2.875 7.188H5.75v-2.876H2.875v2.876zM7.187 11.5h2.876V8.625H7.187V11.5zm0 4.313h2.876v-2.876H7.187v2.876zm-5.75 1.294v-9.92H17.25v2.876h1.438V4.456c0-.873-.697-1.581-1.557-1.581h-.6v.719a1.44 1.44 0 0 1-1.437 1.437 1.44 1.44 0 0 1-1.438-1.437v-.719H5.031v.719A1.44 1.44 0 0 1 3.594 5.03a1.44 1.44 0 0 1-1.438-1.437v-.719h-.598A1.57 1.57 0 0 0 0 4.455v12.652c0 .87.697 1.58 1.558 1.58h8.505V17.25H1.558c-.066 0-.12-.067-.12-.143zM14.376 11.5V8.625H11.5V11.5h2.875zm5.031 5.75H17.25v-2.156a.718.718 0 1 0-1.437 0v2.875c0 .397.32.719.718.719h2.875a.718.718 0 1 0 0-1.438z"/>
                            </svg>
                            Meeting Calender
                        </a>
                   </li>
                @endif
                @if(auth()->user()->can('Mark Expired'))
                    <li>
                    <a id="markExpired" href="{{ url('rip/markExpired').'/'.$currentRip->id }}">
                    <svg width="25" height="25" viewBox="0 0 25 25">
                        <g fill="none" fill-rule="evenodd">
                            <path fill="#2C3542" fill-rule="nonzero" d="M15.029 12.016c.152-.134.327-.28.513-.436 1.366-1.149 3.237-2.722 3.237-4.375v-.994a1.928 1.928 0 0 0-1.925-1.926H8.189a1.928 1.928 0 0 0-1.925 1.926v.994c0 1.653 1.871 3.226 3.237 4.375.186.155.36.302.513.436a.646.646 0 0 1 .23.484.647.647 0 0 1-.23.484c-.152.134-.327.28-.513.436-1.366 1.149-3.237 2.722-3.237 4.375v.994c0 1.062.864 1.926 1.925 1.926h8.665a1.928 1.928 0 0 0 1.925-1.926.642.642 0 1 0-1.284 0 .643.643 0 0 1-.641.642H8.189a.643.643 0 0 1-.641-.642v-.994c0-1.056 1.74-2.518 2.78-3.392.189-.16.368-.31.529-.45.426-.37.67-.9.67-1.453a1.93 1.93 0 0 0-.67-1.453c-.161-.14-.34-.29-.53-.45-1.04-.874-2.78-2.336-2.78-3.392v-.994c0-.354.289-.642.642-.642h8.665c.353 0 .641.288.641.642v.994c0 1.056-1.74 2.518-2.78 3.392-.189.16-.368.31-.529.45-.426.37-.67.9-.67 1.453s.244 1.082.67 1.453c.168.146.355.303.553.469.625.522 1.332 1.114 1.891 1.745a.642.642 0 0 0 .96-.852c-.621-.701-1.4-1.354-2.027-1.878-.193-.162-.376-.314-.534-.453a.646.646 0 0 1-.23-.484c0-.181.084-.358.23-.484z"/>
                            <path fill="#2C3542" fill-rule="nonzero" d="M13.186 15.024a1.02 1.02 0 0 0-1.33 0c-.023.02-1.86 1.571-2.348 2.093l-.016.017a.546.546 0 0 0 .4.917h5.258a.546.546 0 0 0 .401-.917l-.016-.017c-.488-.522-2.325-2.072-2.349-2.093z"/>
                            <path stroke="#2C3542" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.3" d="M16.555 15.077c1.472 1.472 1.963 2.944 1.472 4.417"/>
                        </g>
                    </svg>

                    Mark as Expired
                    </a>
                    </li>
                @endif

            @else
                <li>
                <a href="{{ url('rip/duplicateRip').'/'.$currentRip->id }}">
                    <svg width="25" height="25" viewBox="0 0 25 25">
                        <g fill="#2C3542" fill-rule="nonzero">
                            <path d="M16.316 4H4.684A.684.684 0 0 0 4 4.684v11.632c0 .377.307.684.684.684h11.632a.684.684 0 0 0 .684-.684V4.684A.684.684 0 0 0 16.316 4zm-.684 11.632H5.368V5.368h10.264v10.264zM19.667 18v1.667H18V21h2.333a.667.667 0 0 0 .667-.667V18h-1.333zM12.5 19.7h4V21h-4zM9.333 19.667V18H8v2.333c0 .368.299.667.667.667H11v-1.333H9.333zM20.333 8H18v1.333h1.667V11H21V8.667A.667.667 0 0 0 20.333 8zM19.7 12.5H21v4h-1.3z"/>
                        </g>
                    </svg>
                        Duplicate
                   </a>
                </li>

                <li>
                    <a href="{{ url('rip/revertExpiredRip').'/'.$currentRip->id }}">
                    <svg width="25" height="25" viewBox="0 0 25 25">
                        <path fill="#2C3542" fill-rule="nonzero" d="M20.006 15.693c1.307-4.275-1.108-8.817-5.383-10.124a8.11 8.11 0 0 0-6.01.51l-.866-.753c-.478-.416-.978-.18-1.116.527l-.806 4.14c-.138.707.302 1.09.983.857l3.99-1.372c.681-.235.9-.714.49-1.07l-.743-.647a5.82 5.82 0 0 1 3.41-.01 5.832 5.832 0 0 1 3.869 7.275 5.832 5.832 0 0 1-7.275 3.868 5.819 5.819 0 0 1-2.533-1.57 1.14 1.14 0 1 0-1.66 1.565 8.106 8.106 0 0 0 3.526 2.188c4.275 1.307 8.817-1.108 10.124-5.384z"/>
                    </svg>
                        Undo Expired Status
                    </a>
               </li>
            @endif

            @if(auth()->user()->can('Delete A Rip'))
                <li>
                    <a href="{{ url('rip/deleteTab').'/'.$currentRip->id }}" id="deleteTab">
                        <svg width="25" height="25" viewBox="0 0 25 25">
                            <path fill="#F14343" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"/>
                        </svg>
                        Delete RIP period
                    </a>
                </li>
            @endif
        </ul>
    </div>
</div>

