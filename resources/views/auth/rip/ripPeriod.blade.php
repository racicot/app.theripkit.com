<?php use App\Http\Controllers\RipController;?>

@if((new App\Model\Company())->getRips()->isNotEmpty())
     <li role="presentation" id="rip-listing_{{ session('company_id') }}" class="rip-column rip-listing-sidebar" data-mcs-theme="dark">
    @foreach((new App\Model\Company())->getRips() as $key => $rip)
        <div class="rip-box">
        <a class="ripTab1 ripTab_{{ $rip->id }}" href="{{ url('rip/'.$rip->id) }}">    
            <div class="w-100">
                <div class="d-flex align-items-center expired-title">
                    <div class="exp-days text-ellipsis">{{  $rip->name }}</div>
                    @if(\Carbon\Carbon::now()->format('Y-m-d') > $rip->end_date)
                    
                        <svg enable-background="new 0 0 16 16" height="16px" id="Layer_1" version="1.1" viewBox="0 0 48 48" width="16px" xml:space="preserve"><g id="Layer_3"><path d="M24,0C10.745,0,0,10.745,0,24s10.745,24,24,24s24-10.745,24-24S37.255,0,24,0z M24,44   C12.954,44,4,35.046,4,24S12.954,4,24,4s20,8.954,20,20S35.046,44,24,44z" fill="#e2574c"></path><g><circle cx="24" cy="33" fill="#e2574c" r="2"></circle><rect fill="#e2574c" height="15.031" width="3.997" x="22.001" y="12.969"></rect>
                        </g></g></svg>
                    @endif    
                </div>

                <div class="rip-date-header">
                    <span id="date-header-menu">{{ date('M d, Y', strtotime($rip->start_date)).' - '.date('M d, Y', strtotime($rip->end_date)) }}</span>
                    <span id="left-days-menu" @if(\Carbon\Carbon::now()->format('Y-m-d') > $rip->end_date)class="red-bg-button" @endif> {{ with(new RipController())->datePeriod($rip) }} </span>           
                </div>
            </div>
            <div>
                <svg width="25" height="25" viewBox="0 0 25 25">
                    <path fill="#2C3542" fill-rule="nonzero" d="M16.121 6.19a.636.636 0 0 0-.907 0 .64.64 0 0 0 0 .898l4.6 4.6H4.635a.632.632 0 0 0-.635.636c0 .354.281.644.635.644h15.179l-4.6 4.591a.651.651 0 0 0 0 .907.636.636 0 0 0 .907 0l5.689-5.688a.625.625 0 0 0 0-.898L16.12 6.19z"/>
                </svg>
            </div>
        </a>
        </div>
    @endforeach
    </li>
@endif    


@if((new App\Model\Company())->getExpiredRips()->isNotEmpty())
    <li role="presentation" id="expired-rip-listing" class="rip-column" data-mcs-theme="dark">
    @foreach((new App\Model\Company())->getExpiredRips() as $key => $rip)
        <div class="rip-box">
            <a class="ripTab1 ripTab_{{ $rip->id }}" href="{{ url('rip/'.$rip->id) }}">    
            <div class="w-100">
            <div class="text-ellipsis">
                {{  $rip->name }}
            </div>    
                <div class="rip-date-header">
                    <span>{{ date('M d, Y', strtotime($rip->start_date)).' - '.date('M d, Y', strtotime($rip->end_date)) }}</span>                
                     <span class="red-bg-button">Expired</span> 
                </div>
            </div>
            <div>
                <svg width="25" height="25" viewBox="0 0 25 25">
                    <path fill="#2C3542" fill-rule="nonzero" d="M16.121 6.19a.636.636 0 0 0-.907 0 .64.64 0 0 0 0 .898l4.6 4.6H4.635a.632.632 0 0 0-.635.636c0 .354.281.644.635.644h15.179l-4.6 4.591a.651.651 0 0 0 0 .907.636.636 0 0 0 .907 0l5.689-5.688a.625.625 0 0 0 0-.898L16.12 6.19z"/>
                </svg>
            </div>
            </a>
        </div>
    @endforeach
    </li>
 @endif   

