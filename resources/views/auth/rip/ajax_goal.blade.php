<div id="ajaxGoal_{{ $category->id }}">
<div class="rip-divider">RIP(s)</div>
	@if(count($category->goals) > 0)
	    @foreach($category->goals as $goal)
	        <div id="goalStart_{{ $goal->id }}">
	             @include('auth.rip.ajax.addGoal')
	        </div>
	    @endforeach
	@else
		<div class="comment-msg no-content no-active-rip">
			<div class="no-rip">
			  <img src="../images/new-site/empty_icon.svg" alt="">
			  <h3>Looks a little empty here.</h3>
			  <p>You haven’t created any RIP yet!</p>
			  <p>Start creating RIP(s) from the bottom bar.</p>
		 	</div>
		</div>	
	@endif  
    
</div>

