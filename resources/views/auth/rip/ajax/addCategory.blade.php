<div id="categoryStart_{{ $category->id }}" class="checkForRecord">
    <div class="panel-group " id="accordion" role="tablist"
         aria-multiselectable="true">
        <div class="panel panel-default panel-theme rip-tabs">
            <div class="n-panel-heading" role="tab" id="headingOne">
                <div class="n-rip-card">
                    <div class="n-rip-card-left flex-column align-items-start flex-2 n-rip-card-flex">
                        <div id="accordionDiva" class="n-rip-card-in-left rip-panel-heading-wrap">
                        <a class="mysvg accordionDiv" data-toggle="collapse" data-parent="#accordion"
                               href="#collapse{{ $category->id }}"
                               aria-expanded="true" role="button" aria-hidden="true">
                                <svg width="25" height="25" viewBox="0 0 20 25">
                                    <path fill="#2C3542" fill-rule="nonzero" d="M8.949 19.782a.513.513 0 0 1-.372.154.526.526 0 0 1-.372-.897l6.539-6.539-6.539-6.538a.526.526 0 0 1 .744-.744l6.91 6.91a.526.526 0 0 1 0 .744l-6.91 6.91z"/>
                                </svg>
                                </a>
                               <div class="rip-card-tl">
                               <span class="error-msg cust-msg"> </span>
                                <h3 class="n-rip-card-heading categoryName categoryName_{{ $category->id }}"
                                category_id="{{ $category->id }}"
                                data-gramm_editor="false"
                                aria-controls="collapseTop"  @if(auth()->user()->can('Update The Name Of A Category'))
                                contenteditable="false" @endif>
                                {{ $category->name }}</h3>
                                <p class="rip-card-subhead goalCount_{{ $category->id }}">{{ $category->goals->count() }}
                                RIP(s)</p>

                                
                                </div>
                        </div>
                    </div>
                    <div class="n-rip-card-right">

                        <div class="rip-period-progress">                      
                            <div class="progress-group">
                                <span class="progress-text">Progress</span>
                                <span class="progress-number categoryAvgPer_{{ $category->id }}">{{ $category->getAvgPer($currentRip->id) }}%</span>

                                <div class="progress sm">
                                    <div class="progress-bar progress-bar-aqua categoryAvgWidth_{{ $category->id }}"
                                        style="width: {{ $category->getAvgPer($currentRip->id) }}%"></div>
                                </div>
                            </div>
                        </div>
            
                    </div><!-- /.avatar-wrap -->
                </div><!-- /.rip-card-right -->
                
                    <div class="dropdown menu-top-right">
                        <div class="kebab-menu dropdown-toggle" id="kebab-menu"
                             data-toggle="dropdown" role="button" aria-haspopup="true"
                             aria-expanded="false">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul class="dropdown-menu" id="kebab-menu"
                            aria-labelledby="kebab-menu">
                        @if(auth()->user()->can('Update The Name Of A Category'))                                
                            <li><a class="editCategoryLink" data-toggle="modal" data-target="#editCategoryModal" href="{{ url('/').'/editCategory/'.$category->id }}">
                                   <svg width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#2C3542" fill-rule="nonzero" d="M6 17.084V20h2.916l8.601-8.601-2.916-2.916L6 17.083zm13.773-7.94a.774.774 0 0 0 0-1.097l-1.82-1.82a.774.774 0 0 0-1.097 0l-1.423 1.424 2.916 2.916 1.424-1.423z"/>
                                    </svg>
                                   Edit Category
                                </a>
                            </li>
                        @endif    
                        @if(auth()->user()->can('Delete A Category From RIP'))
                            <li><a catName="{{ $category->name }}" class="deleteCategoryItem" id="deleteCategory"
                                   href="{{ url('/').'/deleteCategory/'.$category->id }}">
                                   <svg width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#F14343" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"/>
                                    </svg>
                                   Delete Category
                                </a>
                            </li>
                        @endif    
                        </ul>
                    </div>
                
            </div><!-- /.panel-heading -->

            <div id="collapse{{ $category->id }}" class="panel-collapse collapse "
                 role="tabpanel" aria-labelledby="headingOne">
                @include('auth.rip.ajax_goal')
                @if(auth()->user()->can('Add A Goal In RIP'))
                    <div class="panel panel-default panel-theme">
                        <div class="panel-item add-panel add-goal">
                            <div class="panel-body-top">
                                
                                    <form method="post"
                                      id="goalForm_{{ $category->id  }}"
                                      class="goalForm"
                                      action="{{ url('addRipGoal') }}">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <input type="hidden" name="category"
                                               value="{{ $category->id }}">
                                    </div>
                                    <div class="form-group flex-group">
                                        <div class="inline-form">
                                            <input type="text" name="goal" id="" data-gramm_editor="false" cols="30" rows="1" placeholder="Add a RIP" class="form-control form-bg-control ajaxAddGoal" maxlength="256">                                        
                                            <span class="error-msg margin-0"> <span>
                                        </div>                                      
                                        <input type="hidden" name="goal_type_id"
                                            value="{{ $currentRip->id }}">
                                        <button class="btn add-rip-button">ADD RIP</button>
                                        
                                    </div>
                                    </form>
                            </div>
                            <div id="collapseIn"
                                 class="panel-body panel-collapse collapse in"
                                 role="tabpanel" aria-labelledby="headingIn">
                    
                            </div><!-- /.panel-body -->
                        </div><!-- /.panel-item -->
                    </div>
                @endif
            </div><!-- /.panel-collapse -->
        </div>
    </div><!-- /.panel-group -->
</div>
