@foreach($users as $user)
    <div class="card-list-avatar">
        <img data-toggle="tooltip" @if($user->image != '' && $user->image != 'default.jpg') src="{{ $user->userImage() }}"  @endif title="{{ $user->name }}">
    </div>
@endforeach
