<div class="rip-content">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs tab-wrapper nav-tabs--custom nav-tabs-rip" role="tablist">

        <!--@include('auth.rip.ripPeriod')-->

        @if(auth()->user()->can('Show RIP History'))
            <li role="presentation">
                <a class="dropdown-toggle rip-history" data-toggle="dropdown" href="{{ url('ripTabHistory') }}"
                   role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="add-icon">
                              <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </span>
                </a>
            </li>
        @endif

        @if($rips->isNotEmpty())
            <li role="presentation">
                <a href="{{ url('ripPeriodExport')}}">
                   <i class="fa fa-download" aria-hidden="true"></i>
                </a>
            </li>
        @endif   
    </ul>
    <!-- Tab panes -->
    <div class="tab-content n-tab-content" id="tabContent">

        @if($rips->isNotEmpty())
            <div role="tabpanel" class="tab-pane active" id="monthly">
                <section class="content-header content-header--lg rip-header-wrap">

                    @include('auth.rip.pusher.editRip')

                </section>
                <div class="tab-main">
                    <div id="ajaxCategory_{{ $rips->first()->id }}">
                        @foreach($categories as $category)
                            <div class="panel-group " id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default panel-theme">
                                    <div class="n-panel-heading" role="tab" id="headingOne">
                                        <div class="n-rip-card">
                                            <div class="n-rip-card-left">
                                                <div class="n-rip-card-in-left ">
                                                    <div class="n-rip-card-in-left rip-panel-heading-wrap">
                                                        <a class="mysvg" data-toggle="collapse" data-parent="#accordion"
                                                           href="#collapse{{ $category->id }}"
                                                           aria-expanded="true" role="button" aria-hidden="true">
                                                            <svg width="25" height="25" viewBox="0 0 20 25">
                                                                <path fill="#2C3542" fill-rule="nonzero" d="M8.949 19.782a.513.513 0 0 1-.372.154.526.526 0 0 1-.372-.897l6.539-6.539-6.539-6.538a.526.526 0 0 1 .744-.744l6.91 6.91a.526.526 0 0 1 0 .744l-6.91 6.91z"/>
                                                            </svg>
                                                        </a>
                                                        <h3 class="n-rip-card-heading categoryName categoryName_{{ $category->id }}"
                                                            category_id="{{ $category->id }}"
                                                            data-gramm_editor="false"
                                                            aria-controls="collapseTop" @if(auth()->user()->can('Update The Name Of A Category'))
                                                            contenteditable="false" @endif>
                                                            {{ $category->name }}</h3>
                                                    </div>
                                                </div>
                                                <div class="rip-card-in-right">
                                                    <p class="rip-card-subhead goalCount_{{ $category->id }}">{{ $goals->where('category_id', $category->id)->count() }}
                                                        RIP(s)</p>
                                                </div>
                                            </div>
                                            <div class="n-rip-card-right">
                                                <div class="rip-period-progress">                      
                                                    <div class="progress-group">
                                                        <span class="progress-text">Progress</span>
                                                        <span class="progress-number categoryAvgPer_{{ $category->id }}">{{ $category->getAvgPer($rips->first()->id) }}%</span>

                                                        <div class="progress sm">
                                                            <div class="progress-bar progress-bar-aqua categoryAvgWidth_{{ $category->id }}"
                                                                style="width: {{ $category->getAvgPer($rips->first()->id) }}%"></div>
                                                        </div>
                                                    </div>
                                                </div>                                         
                                            </div><!-- /.avatar-wrap -->
                                        </div><!-- /.rip-card-right -->
                                        @if(auth()->user()->can('Delete A Category From RIP'))
                                            <div class="dropdown menu-top-right">
                                                <div class="kebab-menu dropdown-toggle" id="kebab-menu"
                                                     data-toggle="dropdown" role="button" aria-haspopup="true"
                                                     aria-expanded="false">
                                                    <span></span>
                                                    <span></span>
                                                    <span></span>
                                                </div>
                                                <ul class="dropdown-menu" id="kebab-menu" aria-labelledby="kebab-menu">
                                                    <li><a class="deleteCategoryItem"
                                                           href="{{ url('/').'/deleteCategory/'.$category->id }}">Delete
                                                            Category</a></li>
                                                </ul>
                                            </div>
                                        @endif
                                    </div><!-- /.panel-heading -->

                                    <div id="collapse{{ $category->id }}" class="panel-collapse collapse "
                                         role="tabpanel" aria-labelledby="headingOne">
                                        @include('auth.rip.ajax_goal')
                                        @if(auth()->user()->can('Add A Goal In RIP'))
                                            <div class="panel panel-default panel-theme">
                                                <div class="panel-item add-panel add-goal">
                                                    <div class="panel-body-top">
                                                        <a href="#" class="add-card-link accordion-header">+ Add a
                                                            RIP...</a>
                                                    </div>
                                                    <div id="collapseIn" class="panel-body panel-collapse collapse in"
                                                         role="tabpanel" aria-labelledby="headingIn">
                                                        <form method="post" id="goalForm_{{ $category->id  }}"
                                                              class="goalForm" action="{{ url('addRipGoal') }}">
                                                            {{ csrf_field() }}
                                                            <div class="form-group">
                                                                <input type="hidden" name="category"
                                                                       value="{{ $category->id }}">
                                                            </div>
                                                            <div class="form-group">
                                                                <textarea name="goal" id="" data-gramm_editor="false"
                                                                          cols="30" rows="2" placeholder="Add New RIP"
                                                                          class="textarea--full ajaxAddGoal"  maxlength="256"></textarea>
                                                            </div>
                                                            <input type="hidden" name="goal_type_id"
                                                                   value="{{ $rips->first()->id }}">

                                                        </form>
                                                    </div><!-- /.panel-body -->
                                                </div><!-- /.panel-item -->
                                            </div>
                                        @endif
                                    </div><!-- /.panel-collapse -->
                                </div>
                            </div><!-- /.panel-group -->
                        @endforeach
                    </div>

                    @if(auth()->user()->can('Add A Category In RIP'))
                        <div class="panel-group add-goal-panel fixed-footer" id="accordion" role="tablist"
                             aria-multiselectable="true">
                            <div class="panel panel-default panel-theme">
                                <div class="panel-item add-panel add-goal">
                                    <div class="panel-body-top">
                                        <form method="post" id="categoryForm" class="categoryForm"
                                              action="{{ url('addCategory') }}">
                                            {{ csrf_field() }}
                                            <textarea name="category" id="" data-gramm_editor="false" cols="30" rows="1"
                                                      placeholder="Add New Category"
                                                      class="textarea--full ajaxAddCategory form-bg-control" maxlength="256"></textarea>
                                            <input type="hidden" name="goal_type" value="{{ $rips->first()->id }}">
                                            <span class="error-msg cust-msg"> <span>
                                        </form>
                                    </div>
                                </div><!-- /.panel-item -->
                            </div>
                        </div><!-- /.panel-group -->
                    @endif
                </div><!-- /.tab-main -->

                <div class="tab-comments">
                </div>
            </div><!-- /.tab-pane -->
         @else

                    <div class="no-content">
                        <div class="no-rip">
                            <img src="../images/new-site/empty_icon.svg" alt="">
                            <h3>Looks a little empty here.</h3>
                            <p>You haven’t created any RIP(s) yet!</p>
                            <p>Start creating RIP(s) from the top bar.</p>
                        </div>
                    </div>
                @endif
    </div>
</div>


