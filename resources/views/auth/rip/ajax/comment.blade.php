<div class="panel panel-default panel-theme rip-tabs mediaComment_{{ $comment->id }}">
                               
    <div class="n-panel-heading mb-20">
        <div class="n-rip-card">
            <div class="n-rip-card-left flex-column align-items-start">
                <div class="n-rip-card-in-left rip-panel-heading-wrap">
                    <div class="rip-usercomment-pics" title=" {{ $comment->userName() }}">

                        <img class="responsive-img img-circle"
                             src="{{ $comment->userImage() }}" alt="">

                    </div>
                    <div class="rip-card-tl" id="commentData_{{$comment->id}}">
                        <div class="rip-comments-msg">
                            <?php echo stripcslashes(htmlspecialchars_decode($comment->message));?>
                        </div>
                        <p class="rip-card-subhead">{{ date_format($comment->created_at, 'M d, Y') }} at {{ date_format($comment->created_at, 'H:i') }}</p>
                         <div class="d-flex align-items-start flex-wrap media-attachment-edit attachmentList_{{ $comment->id }}">

                        @if(count($comment->attachment) > 0)
                       
                            @foreach($comment->attachment as $attachment)
                                <div class="comments-links-items commentAttachment_{{ $attachment->id }}">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#2C3542" fill-rule="nonzero" d="M11.6 12.03a.631.631 0 0 0-.892-.892l-3.339 3.339a2.943 2.943 0 0 0-.877 2.108c0 .8.308 1.538.877 2.107a2.971 2.971 0 0 0 4.216 0l7.538-7.538a4.131 4.131 0 0 0 1.215-2.939c0-1.107-.43-2.153-1.215-2.938a4.16 4.16 0 0 0-5.892 0L5.708 12.8a5.306 5.306 0 0 0-1.57 3.785 5.28 5.28 0 0 0 1.57 3.784 5.306 5.306 0 0 0 3.784 1.57 5.28 5.28 0 0 0 3.785-1.57l3.338-3.338a.631.631 0 0 0-.892-.893l-3.338 3.339c-.77.77-1.8 1.2-2.893 1.2a4.074 4.074 0 0 1-2.892-1.2c-.77-.77-1.2-1.8-1.2-2.892 0-1.093.43-2.123 1.2-2.893l7.523-7.523a2.9 2.9 0 0 1 4.092 0 2.9 2.9 0 0 1 0 4.092L10.677 17.8c-.662.662-1.754.662-2.43 0a1.711 1.711 0 0 1 0-2.43l3.353-3.34z"></path>
                                    </svg>
                                    <div class="comments-show-file">
                                        <a class="mr10" href="{{ $attachment->attachmentUrl() }}"
                                            target="_blank">
                                            <i class="fa fa-picture-o"
                                               aria-hidden="true"></i>
                                            {{ $attachment->attachment }}
                                        </a>
                                    </div>
                                    <div class="delete-btn">
                                        <a href="{{ url('deleteAttachment').'/'.$attachment->id }}"
                                           aria-hidden="true" class="delete-attachment" fileName="{{$attachment->attachment}}" >
                                            <svg width="25" height="25" viewBox="0 0 25 25">
                                                <path fill="#2C3542" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"></path>
                                            </svg>
                                        </a>    
                                    </div>
                                </div>
                            @endforeach   
                        @endif
                         </div>
                    </div>

                    <div class="current-comment currentCommentData_{{ $comment->id }}">
                        <input type="hidden"
                               value="{{ $comment->message }}" id="commentVal_{{ $comment->id }}">
                    </div><!-- /.current-comment -->
                </div>
            </div>
        </div>

        @if($comment->isAllowed() && ((auth()->user()->can('Edit A Comment')) || auth()->user()->can('Delete Comment')))
            <div class="dropdown menu-top-right">
                <div class="kebab-menu dropdown-toggle" id="kebab-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <ul class="dropdown-menu" id="kebab-menu" aria-labelledby="kebab-menu" style="">
                @if(auth()->user()->can('Edit A Comment'))
                    @if($comment->isAllowed())
                    <li>
                        <a class="js-edit-comment" data-toggle="modal" data-target="#editModal" href="{{ url('/').'/editComment/'.$comment->id }}" companyId = "{{ $goal->company_info->id }}"><svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
                              <path fill="#2C3542" fill-rule="nonzero" d="M6 17.084V20h2.916l8.601-8.601-2.916-2.916L6 17.083zm13.773-7.94a.774.774 0 0 0 0-1.097l-1.82-1.82a.774.774 0 0 0-1.097 0l-1.423 1.424 2.916 2.916 1.424-1.423z"></path>
                              </svg>
                            Edit Comment
                        </a>
                    </li>
                    @endif
                @endif
                @if(auth()->user()->can('Delete Comment'))
                    @if($comment->isAllowed())
                    <li><a class="js-delete-comment" href="{{ url('comments').'/deleteComment/'.$comment->id }}">
                            <svg width="25" height="25" viewBox="0 0 25 25">
                                <path fill="#F14343" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"></path>
                            </svg>
                            <span class="red-text">Delete Comment</span>
                        </a>
                    </li>
                    @endif
                @endif
                </ul>
            </div> 
        @endif       
    </div>   
    <!-- repeat div's end-->
</div>




