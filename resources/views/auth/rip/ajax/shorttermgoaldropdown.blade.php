@foreach ($users as $user)
    <div class="media media--small">
        <div class="media-left media-middle">
            <img class="media-object img-circle" src="{{ $user->userImage() }}" alt="User">
        </div>
        <div class="media-body">
            <div class="media-body-in">
                <h4 class="media-heading">{{ $user->name }}</h4>
                <h5 class="media-subhead">{{ $user->email }}</h5>
            </div>
            @if(auth()->user()->can('Delete User from A RIP'))
                <div class="media-delete">
                    <a href="{{ url('goal/user/delete') . '/' . $user->id . '/' . $goal_id }}"><i class="fa fa-trash-o"
                                                                                                  aria-hidden="true"></i></a>
                </div>
            @endif
        </div>
    </div>
@endforeach