@extends('layouts.auth.master')
@section('title')
    RIP
@endsection
@section('content')
    <section class="content">
        <div class="rip-content"> 
            <!-- Tab panes -->
            <div class="tab-content n-tab-content" id="tabContent">
                <div role="tabpanel" class="tab-pane active" id="monthly">
                    <section class="content-header content-header--lg rip-header-wrap editRip_{{ $currentRip->id }}">
                        @include('auth.rip.pusher.editRip')
                    </section>

                    <div class="tab-main">

                        <div id="ajaxCategory_{{ $currentRip->id }}">
                            @foreach($categories as $category)
                                @include('auth.rip.ajax.addCategory')
                            @endforeach
                        </div>
                        @if(auth()->user()->can('Add A Category In RIP'))
                            <div class="panel-group add-goal-panel fixed-footer" id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="panel panel-default panel-theme">
                                    <div class="panel-item add-panel add-goal">
                                        <div class="panel-body-top">
                                            <form method="post" id="categoryForm" class="categoryForm"
                                                  action="{{ url('addCategory') }}">
                                                {{ csrf_field() }}
                                                <textarea name="category" id="" data-gramm_editor="false" cols="30" rows="1"
                                                          placeholder="Add New Category"
                                                          class="textarea--full ajaxAddCategory form-bg-control" maxlength="256"></textarea>
                                                <input type="hidden" name="goal_type" value="{{ $currentRip->id }}">
                                                <span class="error-msg cust-msg"> <span>
                                            </form>
                                        </div>
                                    </div><!-- /.panel-item -->
                                </div>
                            </div><!-- /.panel-group -->
                        @endif
                    </div><!-- /.tab-main -->
                    <div class="tab-comments">
                    </div>
                </div><!-- /.tab-pane -->
            </div>
    </section>
@endsection    
