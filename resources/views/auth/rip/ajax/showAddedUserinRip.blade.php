    <div class="modal-dialog user-modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="modal-title" id="myModalLabel">Users(s) List</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <svg enable-background="new 0 0 30 28" height="20px" id="Слой_1" version="1.1" viewBox="0 0 30 28" width="20px" xml:space="preserve">
                            <path d="M17.459,16.014l8.239-8.194c0.395-0.391,0.395-1.024,0-1.414c-0.394-0.391-1.034-0.391-1.428,0  l-8.232,8.187L7.73,6.284c-0.394-0.395-1.034-0.395-1.428,0c-0.394,0.396-0.394,1.037,0,1.432l8.302,8.303l-8.332,8.286  c-0.394,0.391-0.394,1.024,0,1.414c0.394,0.391,1.034,0.391,1.428,0l8.325-8.279l8.275,8.276c0.394,0.395,1.034,0.395,1.428,0  c0.394-0.396,0.394-1.037,0-1.432L17.459,16.014z" fill="#000" id="Close"></path>
                        </svg>
                    </button>
                </div>
            </div>
            <div class="modal-body">
                @if(count($selectedUsers) > 0)            
                    <div class="add-user">
                        
                        <form id="showUserList" method="post" autocomplete="off">

                            <div class="form-group" data-mcs-theme="dark">
                                <!-- repeat items -->
                                
                                @foreach($selectedUsers as $key=>$goaluser)
                                <div class="user-lists addedGoalUserSelect_{{ $goal_id }}  @if(count($selectedUsers) == $key+1) last @endif">
                                    <label class="show-user-list">
                                        
                                        <span class="checkbox-items">
                                            <div class="user-pics">
                                                <img class="responsive-img" @if($goaluser->image != '' && $goaluser->image != 'default.jpg') src="{{ $goaluser->userImage() }}" @endif
                                                    title="{{ $goaluser->name }}">
                                            </div>
                                            <div class="user-data">
                                                <div class="user-title">{{ $goaluser->name}}</div>
                                                <div class="user-text">{{ $goaluser->email }}</div>
                                            </div>
                                        </span>
                                    </label>
                                </div>
                                @endforeach
                            <input type="hidden" value="{{ $goal_id }}" name="goal_id">                        
                        </form>
                    </div><!-- /.add-user -->
                @else
                    <div class="no-content">
                        <div class="no-rip">
                            <img src="../images/new-site/empty_icon.svg" alt="">
                            <h3>Looks a little empty here.</h3>
                            <p>There is no user mapped with this RIP</p>
                        </div>
                    </div>                
                @endif    
            </div>
        </div>
    </div>
</div>