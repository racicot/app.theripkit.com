<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Update Progress</h4>
        </div>   
        <div class="modal-body">
            <div class="add-user" id="update_progress">                 
                     
                         {{ csrf_field() }}
                         <input type="hidden"  name="goal_id" value="{{$goal->id}}">
                        <div class="form-group">
                            <label for="inputCompanyName">Progress</label>
                             <input type="number"
                               min="0"
                               max="100"
                               step="1"
                               class="form-control goal-tab-percentage"
                               class="form-control"
                               name="percentage"
                               value="{{ $goal->percentage_complete }}"
                               aria-label="...">
                               <span class="error-msg cust-msg"></span>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link pull-left close_modal" data-dismiss="modal">Cancel</button>

                            <button type="button" class="btn btn-theme pull-right save-btn" value="Create">ADD</button>
                        </div>
                    
            </div><!-- /.add-user -->
        </div>
    </div>
</div>