<a class="goal-user-list d-flex" data-toggle="modal" data-target="#showUserListModal">
@if($count <= 1 ) 
    @foreach($users as $user)
        <?php 
            $userName = explode(' ',$user->name);
            $newUser_name = $userName[0];
            if(isset($userName[1])){
                $newUser_name.= ' '.strtoupper(substr($userName[1], 0, 1)).'.';
            }                                     
        ?>
        <div class="card-list-avatar">
            <img data-toggle="tooltip" @if($user->image != '' && $user->image != 'default.jpg') src="{{ $user->userImage() }}" @endif title="{{ $user->name }}">           
        </div>
         <span class="username">{{ $newUser_name }}</span>
    @endforeach
@else 
    @for ($i = 0; $i < 1; $i++)
        @if(isset($users[$i]))
            <?php 
                $userName = explode(' ',$users[$i]->name);
                $newUser_name = $userName[0];
                if(isset($userName[1])){
                   $newUser_name.= ' '.strtoupper(substr($userName[1], 0, 1)).'.';
                }                                     
            ?>
            <div class="card-list-avatar">
                <img data-toggle="tooltip" @if($users[$i]->image != '' && $users[$i]->image != 'default.jpg') src="{{ $users[$i]->userImage() }}" @endif title="{{ $users[$i]->name }}">
            </div>
             <span class="username">{{ $newUser_name }}</span>
        @endif    
    @endfor 
    <div class="card-list-avatar lists-count">
        <span class="goal-user-list"> +{{ $count-1 }}</span>
    </div>  
@endif    
</a>