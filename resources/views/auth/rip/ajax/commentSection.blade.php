<div class="comments-header">
    <div class="n-rip-card-right">
        <span class="activity-heading goalName_{{ $goal->id }}">{{ $goal->goal }}</span>
        <div class="n-avatar-wrap">

            <div class="goalUserList goal-avatar-wrap goalUserList_{{ $goal->id}}">
                @foreach($goalusers as $goaluser)
                    @if(in_array($goaluser->user->id, explode(',', $goal->assign_to)))
                        <div class="card-list-avatar">
                            <img data-toggle="tooltip" src="{{ $goaluser->user->userImage() }}"
                                 title="{{ $goaluser->user->name }}">
                        </div>
                    @endif
                @endforeach
            </div>

            @if(auth()->user()->can('Assign User To A RIP'))
                <div class="dropdown">
                    <a href="#" id="assignUserToGoal_{{ $goal->id }}"
                       goalId="{{ $goal->id }}"
                       class="card-list-avatar dropdown-toggle addedGoalUser"><i
                                class="fa fa-user-plus" aria-hidden="true"></i></a>
                    <div class="add-user-dropdown dropdown-menu dropdown-menu-left"
                         id=""
                         aria-labelledby="addUser">
                        <h4>Add users</h4>
                        <div class="form-group flex-row">
                            <select data-placeholder="Add Users" name="user[]"
                                    class="select addedGoalUserSelect addedGoalUserSelect_{{ $goal->id }}">
                                @foreach($goalusers as $goaluser)
                                    <option value="{{ $goaluser->user->id }}">{{ $goaluser->user->name }}</option>
                                @endforeach

                            </select>
                            <button type="button" class="btn btn-theme invite-goal "
                                    goalId="{{ $goal->id }}">Add
                            </button>
                        </div>
                        <div class="add-user-dropdown-list addedUserGoalList_{{ $goal->id }}"
                             data-goalId={{ $goal->id }}>

                        </div><!-- /.add-user-dropdown-list -->
                    </div><!-- /.add-user-dropdown -->
                </div>
            @endif
        </div><!-- /.avatar-wrap -->
        <div class="v-bottom">
            <div class="progress-radial progress-radial--small progress-radial--white progress-radial--editable progress-green-sec-{{ round($goal->percentage_complete) }}">
                <div class="overlay innerGoalpercentage_{{ $goal->id }}">
                    {{ round($goal->percentage_complete) }}%
                </div>
            </div>
            @if(auth()->user()->can('Update Goal Percentage'))
                <div class="dropdown">
                    <a href="#" class="link-gray dropdown-toggle edit-progress"><i class="fa fa-2x fa-pencil"
                                                                                   aria-hidden="true"></i></a>
                    <div class="dropdown-menu dropdown-menu-right edit-progress-popup" aria-labelledby="addUser">
                        <form method="post" class="goalPercentageUpdate taskPercentageUpdate"
                              action="{{ url('goalPercentageUpdate').'/'.$goal->id }}">
                            <div class="edit-input">
                                <input type="number" min="0" max="100" id="goal-per-input" value="{{ round($goal->percentage_complete) }}" step="1" class="form-control"
                                       name="percentage" aria-label="...">
                            </div>
                            <div class="edit-buttons">
                                <a href="#" class="btn btn-gray close-btn">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </a>
                                <button type="submit" goalId="{{ $goal->id }}" class="btn btn-theme save-btn">
                                    <i class="fa fa-check" aria-hidden="true"></i>
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            @endif
        </div>
    </div><!-- /.rip-card-right -->

    @if(auth()->user()->can('Add New Comment'))
        <div class="activity-head flex-row">
            <a href="#add-comment" class="btn btn-theme">Add New Comment</a>
        </div>
    @endif

    <div class="close-comments" id="close-comments">
        <i class="fa fa-times" aria-hidden="true"></i>
    </div>

    <div class="comment_{{ $goal->id }}">
        @foreach($goal->comment as $comment)
            <div class="commentStart_{{ $comment->id }}">
                <div class="media media-theme media-comment mediaComment_{{$comment->id}}">
                    <div class="media-left">
                        <a href="#">
                            <img class="media-object img-circle"
                                 src="{{ $comment->userImage() }}" alt="">
                        </a>
                    </div><!-- /.media-left -->
                    <div class="media-body">
                        <h4 class="media-heading">{{ $comment->userName() }}</h4>
                        <div class="media-comment">
                            <div class="current-comment currentCommentData_{{ $comment->id }}">
                                <input type="hidden"
                                       value="{{ $comment->message }}" id="commentVal_{{ $comment->id }}">
                                <div id="commentData_{{$comment->id}}">
                                    <strong>{{ $comment->assignUserHtml() }}</strong> {{ $comment->message }}</div>
                            </div><!-- /.current-comment -->
                            @if(count($comment->attachment) > 0)
                                <div class="media-attachment attachmentList_{{ $comment->id }}">
                                    <span class="attachment-heading">{{ count($comment->attachment) }}
                                        attachments</span>

                                    @foreach($comment->attachment as $attachment)
                                        <div class="commentAttachment_{{ $attachment->id }}">
                                            <ul class="attachment-list list-inline">
                                                <li><a class="mr10" href="{{ $attachment->attachmentUrl() }}"
                                                       target="_blank">
                                                        <i class="fa fa-picture-o"
                                                           aria-hidden="true"></i>
                                                        {{ $attachment->attachment }}
                                                    </a><i class="fa fa-trash delete-attachment"
                                                           href="{{ url('deleteAttachment').'/'.$attachment->id }}"
                                                           aria-hidden="true"></i>
                                                </li>
                                            </ul>
                                        </div>
                                    @endforeach

                                </div><!-- /.media-attachment -->
                            @endif
                        </div>
                        <!---start---->
                        @if(auth()->user()->can('Edit A Comment'))
                            <div class="comment-box edit-comment-box">
                                <form action="{{ url('comments').'/'.$comment->id }}"
                                      goalId="{{ $goal->id }}" commentId="{{ $comment->id }}"
                                      id="editCommentForm_{{ $comment->id }}"
                                      method="post"
                                      class="editCommentForm"
                                      enctype="multipart/form-data">
                                    {{ csrf_field() }}
                                    {{ method_field('PUT') }}

                                    <div class="comment-box-area">
                                        <textarea name="comment" id="" rows="5"
                                                  data-gramm_editor="false"
                                                  class="commentBox comment-box-input mention-example2"
                                                  placeholder="User @ to add user to comment"></textarea>
                                        <div class="comment-box-options">
                                            <!-- /.dropdown -->
                                            <div class="dropdown">
                                                <div class="comment-box-options-item">
                                                    <label for="upload-file_{{ $comment->id }}"><i
                                                                class="fa fa-paperclip" aria-hidden="true"></i></label>
                                                    <input type="file" id="upload-file_{{ $comment->id }}"
                                                           class="custom-input-file" name="attachment[]" value=""
                                                           multiple="">
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <input type="hidden" id="goal_id"
                                           value="{{ $goal->id }}"
                                           name="taskId">
                                    @if(count($comment->attachment) > 0)
                                        <div class="media-attachment-edit attachmentList_{{ $comment->id }}">
                                            <span class="attachment-heading">{{ count($comment->attachment) }}
                                                attachments</span>

                                            @foreach($comment->attachment as $attachment)
                                                <div class="commentAttachment_{{ $attachment->id }}">
                                                    <ul class="attachment-list list-inline">
                                                        <li><a class="mr10" href="{{ $attachment->attachmentUrl() }}"
                                                               target="_blank">
                                                                <i class="fa fa-picture-o"
                                                                   aria-hidden="true"></i>
                                                                {{ $attachment->attachment }}
                                                            </a><i class="fa fa-trash delete-attachment"
                                                                   href="{{ url('deleteAttachment').'/'.$attachment->id }}"
                                                                   aria-hidden="true"></i>
                                                        </li>
                                                    </ul>
                                                </div>
                                            @endforeach

                                        </div><!-- /.media-attachment -->
                                    @endif
                                    <button type="submit" class="btn btn-theme">
                                        Save
                                    </button>
                                </form>
                            </div><!-- /.comment-box edit-comment-box -->
                    @endif
                    <!-----end--->
                        <div class="media-meta">
                            <time>{{ date('F d Y', strtotime($comment->created_at)) }}
                                at {{ date('H:m', strtotime($comment->created_at)) }}</time>
                            @if(auth()->user()->can('Edit A Comment'))
                                @if($comment->isAllowed())
                                    - <a href="#" class="js-edit-comment">Edit</a>
                                @endif
                            @endif
                            @if(auth()->user()->can('Delete Comment'))
                                @if($comment->isAllowed())
                                    - <a href="{{ url('comments').'/deleteComment/'.$comment->id }}"
                                         class="js-delete-comment">Delete</a>
                                @endif
                            @endif
                        </div><!-- /.media-meta -->

                    </div><!-- /.media-body-->
                </div><!-- /.media media-theme media-comment-->
            </div>
        @endforeach

    </div><!-- /.comment -->
</div>

@if(auth()->user()->can('Add New Comment'))
    <div class="comment-box border-top">
        <span class="activity-heading succes-comment-message">Add Comment</span>
        <form action="{{ url('comments') }}" goalId="{{ $goal->id }}"
              id="addCommentForm" method="post"
              class="addCommentForm" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="comment-box-area">
                                                                            <textarea name="comment" id="add-comment"
                                                                                      rows="5" data-gramm_editor="false"
                                                                                      class="addCommentSection commentBox comment-box-input mention-example2"
                                                                                      placeholder="Write a comment..."></textarea>
                <div class="comment-box-options">
                    <!-- /.dropdown -->
                    <div class="dropdown">
                        <div class="comment-box-options-item">
                            <label for="upload-file"><i class="fa fa-paperclip" aria-hidden="true"></i></label>
                            <input type="file" id="upload-file" class="custom-input-file " name="attachment[]" value=""
                                   multiple="">
                        </div>
                    </div>
                </div><!-- /.comment-box-options -->
            </div><!-- /.comment-box-area -->
            <input type="hidden" id="goal_id" value="{{ $goal->id }}"
                   name="goalId">
            <input type="hidden" name="mention_id" id="mention_id" value="">
            <button type="submit" class="btn btn-theme">Save</button>
        </form><!-- /.form -->
    </div><!-- /.comment-box border-top -->
@endif
