@if($count <= 3 ) 
    @foreach($userArray as $userArray)

    <div class="card-list-avatar goalTaskUser_{{ $userArray->find($taskNewUser)->id }}">
        <img data-toggle="tooltip" @if($userArray->find($taskNewUser)->image != '' && $userArray->find($taskNewUser)->image != 'default.jpg') src="{{ $userArray->find($taskNewUser)->userImage() }}" @endif
             title="{{ $userArray->find($taskNewUser)->name }}">
    </div>
    @endforeach
@else 
    @for ($i = 0; $i < 3; $i++)
        @if(isset($userArray[$i]))
            <div class="card-list-avatar goalTaskUser_{{ $userArray[$i]->find($taskNewUser)->id }}">
                <img data-toggle="tooltip" @if($userArray[$i]->find($taskNewUser)->image 
                != '' && $userArray[$i]->find($taskNewUser)->image != 'default.jpg') src="{{ $userArray[$i]->find($taskNewUser)->userImage() }}" @endif
                     title="{{ $userArray[$i]->find($taskNewUser)->name }}">
            </div>
        @endif    
    @endfor 
    <div class="card-list-avatar lists-count">
        <span class="avtar-pics-count black-pics-count">+{{ $count-3 }}</span>
    </div>  
@endif    