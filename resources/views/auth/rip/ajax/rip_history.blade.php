<div class="rip-content">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs tab-wrapper nav-tabs--custom nav-tabs-rip" role="tablist">
        @if(auth()->user()->can('Show RIP History'))
            <li role="presentation">
                <a class="dropdown-toggle rip-history" data-toggle="dropdown" href="{{ url('currentRip') }}"
                   role="button" aria-haspopup="true" aria-expanded="false">
                            <span class="add-icon">
                              <i class="fa fa-clock-o" aria-hidden="true"></i>
                            </span>
                </a>

            </li>
        @endif
    </ul>
    <!-- Tab panes -->
    <div class="tab-content n-tab-content" id="tabContent">
     @if($rips->isNotEmpty())
        <div class="history-wrap">
            <div class="row">
                @foreach($rips as $rip)
                <div class="col-sm-3">
                    <div class="card-history">
                        <h2>{{ $rip->name }}</h2>
                        <div class="rip-period-progress">
                            <div class="progress-radial progress-radial--md progress-{{ $rip->getRipPercentage() }}">
                            <div class="overlay ripPer_{{ $rip->id }}"><span>{{ $rip->getRipPercentage() }}</span><span>%</span>
                            </div>
                            </div>
                        </div>
                        <span>{{ date('F d Y', strtotime($rip->start_date)).' - '.date('F d Y', strtotime($rip->end_date)) }}</span>
                        <div class="card-history-btns">
                            <a href="{{ url('viewHistoryRip').'/'.$rip->id }}" class="btn btn-default btn-view-rip viewHistoryRip">View RIP</a>                     
                            <a href="{{ url('rip/duplicateRip').'/'.$rip->id }}" class="btn btn-default btn-view-rip" >Duplicate</a>

                             <a href="{{ url('rip/revertExpiredRip').'/'.$rip->id }}" class="btn btn-default revertExpiredRip">Revert RIP</a>
                        </div>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
        @else
            <div class="no-content">
                <div class="no-rip">
                    <img src="../images/new-site/empty_icon.svg" alt="">
                    <h3>There is no expired RIP available.</h3>
                </div>
            </div>
        @endif
    </div>
</div>

