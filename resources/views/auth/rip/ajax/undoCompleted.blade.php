<div id="taskStart_{{ $task->id }}">
    <div class="n-panel-body-top n-rip-card task-row-wrap " taskid="{{ $task->id }}">
        <div class="n-rip-card-left">
            <div class="rip-card-in-left task-row">
                @if($task->percentage_complete != 100)
                    <span class="task-status icon-wrap js-task-comp">
                        <img src="{{ asset('images/icon-success.png') }}" class="success-icon">
                    </span>
                @else
                    <div class="task-status icon-wrap js-task-undo">
                        <img src="{{ asset('images/icon-undo.png') }}" class="success-icon">
                    </div>
                @endif               

                @if(isset($current_page) && $current_page == '/home')
                  <div>
                      <h3 class="card-list__heading">{{ $task->name }}</h3>
                      
                      <div class="rip-status incomplete-status">
                              INCOMPLETE
                          </div>
                      <p class="card-list__meta">
                          <span><label>RIP :</label> {{ $task->goal->rip->name }}</span>
                          <span><label>RIP Period :</label> {{ $task->goal->goal }}</span>
                          <span><label>Created on :</label> <date>{{ date('F d Y', strtotime($task->created_at)) }}</date></span>
                      </p>
                  </div>
                @else
                    <div>
                        <h3 class="card-list__heading">{{ $task->name }}</h3>
                        <div class="rip-status incomplete-status">
                              INCOMPLETE
                          </div>
                        <p class="card-list__meta">                      
                            <span><label>Created on :</label> <date>{{ date('F d Y', strtotime($task->created_at)) }}</date></span>
                        </p>                   
                    </div>

                    @if(auth()->user()->can('Assign User To A Task'))
                        <div class="n-avatar-wrap">
                            <div class="goalTaskUserList goal-avatar-wrap goalTaskUserList_{{ $task->goal_id }} taskUserList_{{ $task->id}}">
                            @foreach($task->getTaskUser() as $taskuser)

                                <div class="card-list-avatar goalTaskUser_{{ $taskuser->id }}">
                                    <img data-toggle="tooltip" src="{{ $taskuser->userImage() }}"
                                title="{{ $taskuser->name }}">
                                </div>

                            @endforeach
                            </div>
                            <div class="dropdown dropup">
                                <a href="#" id="addUser_{{ $task->id }}"
                                taskId="{{ $task->id }}"
                                class="dropdown-toggle addedTaskUser"><i
                                class="fa fa-user-plus" aria-hidden="true"></i></a>

                                <div class="add-user-dropdown dropdown-menu dropdown-menu-right"
                                id="addUser_{{ $task->id }}" aria-labelledby="addUser">
                                    <h4>Add users</h4>
                                    <div class="form-group flex-row">
                                        <select data-placeholder="Add Users" name="user[]"
                                        class="select addedTaskUserSelect addedTaskUserSelect_{{ $task->id }}">
                                            <option value="">Assign User</option>
                                            @foreach($goalusers as $goaluser)
                                            <option value="{{ $goaluser->user->id }}">{{ $goaluser->user->name }}</option>
                                            @endforeach

                                        </select>
                                        <button type="button" class="btn btn-theme invite-task "
                                        taskId="{{ $task->id }}" goalId="{{ $task->goal_id }}">Add
                                        </button>
                                    </div>
                                    <div class="add-user-dropdown-list addedUserTaskList_{{ $task->id }}"
                                    data-goalId="{{ $task->goal_id }}">

                                    </div><!-- /.add-user-dropdown-list -->
                                </div><!-- /.add-user-dropdown -->

                            </div>

                        </div><!-- /.avatar-wrap -->
                        @endif
                        @if(auth()->user()->can('Delete Task From RIP'))
                        <div class="del-task">
                        <i class="fa fa-trash deleteTaskItem" href="{{ url('/').'/deleteTask/'.$task->id }}"
                        aria-hidden="true"></i>
                        </div>
                        @endif
                        </div>
                        @if(auth()->user()->can('Complete A Task'))
                        <div class="task-confirm">
                        <span>Mark Complete</span>
                        <div>
                        <span class="icon-wrap js-task-confirm">
                        <svg class="check-icon" viewBox="0 0 32 32">
                        <polygon
                        points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon>
                        </svg>
                        </span>
                        <span class="icon-wrap js-cancel-task-confirm">
                        <svg class="close-icon" viewBox="0 0 32 32">
                        <polygon
                        points="24.485,27.314 27.314,24.485 18.828,16 27.314,7.515 24.485,4.686 16,13.172 7.515,4.686 4.686,7.515 13.172,16 4.686,24.485 7.515,27.314 16,18.828 "></polygon>
                        </svg>
                        </span>
                        </div>
                        </div>
                        @endif
                @endif
            </div>
            
        <div class="task-undo-in">
            <span>Mark Incomplete</span>
            <div>
<span class="icon-wrap js-task-undo js-task-undo-icon">
<svg class="check-icon"
viewBox="0 0 32 32">
<polygon
points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon>
</svg>
</span>
<span class="icon-wrap js-cancel-task-undo">
<svg class="close-icon"
viewBox="0 0 32 32">
<polygon
points="24.485,27.314 27.314,24.485 18.828,16 27.314,7.515 24.485,4.686 16,13.172 7.515,4.686 4.686,7.515 13.172,16 4.686,24.485 7.515,27.314 16,18.828 "></polygon>
</svg>
</span>
</div>
        </div>
    </div><!-- /.n-panel-body-top -->
</div>
