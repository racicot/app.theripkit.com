@extends('layouts.auth.master')
@section('title')
    RIP
@endsection
@section('content')
    <section class="content">
        <div class="rip-content"> 
          
            {{ session('status') }}
            {{ Session::forget('status') }}
            
            <!-- Tab panes -->
            <div class="tab-content n-tab-content" id="tabContent">
                @if($rips->isNotEmpty())
                    <div role="tabpanel" class="tab-pane active" id="monthly">
                        <section
                                class="content-header content-header--lg rip-header-wrap editRip_{{ $currentRip->id }}">
                            @include('auth.rip.pusher.editRip')
                        </section>

                        <div class="tab-main">
                            <div id="ajaxCategory_{{ $currentRip->id }}">
                                @foreach($categories->where('goal_type_id', $currentRip->id) as $category)
                                    @include('auth.rip.ajax.addCategory')
                                @endforeach
                            </div>
                            
                            <div class="no-content" id="checkForRecordGoal">
                                <div class="no-rip">
                                    <img src="../images/new-site/empty_icon.svg" alt="">
                                    <h3>Looks a little empty here.</h3>
                                    <p>You haven’t created any categories yet!</p>
                                    <p>Start creating categories from the bottom bar.</p>
                                </div>
                            </div>
                      
                            @if(auth()->user()->can('Add A Category In RIP'))
                                <div class="panel-group add-goal-panel fixed-footer" id="accordion" role="tablist"
                                     aria-multiselectable="true">
                                    <div class="panel panel-default panel-theme">
                                        <div class="panel-item add-panel add-goal container">
                                            <div class="panel-body-top">
                                                <form method="post" id="categoryForm" class="categoryForm"
                                                      action="{{ url('addCategory') }}">
                                                    {{ csrf_field() }}
                                                    <div class="inline-form">
                                                        <input type="text" name="category" id="" data-gramm_editor="false" cols="30"
                                                                  rows="1" placeholder="Add New Category"
                                                                  class="form-control ajaxAddCategory form-bg-control add-category-textarea" maxlength="256"></textarea>
                                                        <input type="hidden" name="goal_type"
                                                               value="{{ $currentRip->id }}">
                                                        <span class="error-msg margin-0"> </span>
                                                    </div>    
                                                    <button class="add-cta-button">Add Category</button>
                                                </form>
                                            </div>
                                        </div><!-- /.panel-item -->
                                    </div>
                                </div><!-- /.panel-group -->
                            @endif
                        </div><!-- /.tab-main -->
                    </div><!-- /.tab-pane -->
                @else

                    <div class="no-content">
                        <div class="no-rip">
                            <img src="../images/new-site/empty_icon.svg" alt="">
                            <h3>Looks a little empty here.</h3>
                            <p>You haven’t created any RIP(s) yet!</p>
                            <p>Start creating RIP(s) from the top bar.</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>
    
@endsection



