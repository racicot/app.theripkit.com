      <div class="rip-content rip-col" id="ripMenu_list">
       <div class="tab-wrapper rip-tabs-outer">
            <ul class="nav nav-tabs nav-tabs--custom nav-tabs-rip" role="tablist">
                <li> 
                    <div class="rip-sidebar-title">
                        <div class="back-to-rip">
                            <a href= "javascript:" id="back_ro_rip"> 
                                <svg width="25" height="25" viewBox="0 0 25 25">
                                    <path fill="#2C3542" fill-rule="nonzero" d="M9.879 6.19a.636.636 0 0 1 .907 0 .64.64 0 0 1 0 .898l-4.6 4.6h15.179c.354 0 .635.282.635.636a.639.639 0 0 1-.635.644H6.186l4.6 4.591a.651.651 0 0 1 0 .907.636.636 0 0 1-.907 0L4.19 12.778a.625.625 0 0 1 0-.898L9.88 6.19z"></path>
                                </svg> 
                            </a>
                        </div>  
                        <div class="rip-period">RIP Period(s)</div>
                        <div class="rip-download-links">
                            <a href="javascript:" role="button" class="rip-setup" data-toggle="modal" data-target="#quicksetup">
                                rip setup
                            </a>
                         @if(auth()->user()->can('Show RIP History'))
                            <a href="javascript:" role="button" class="rip-history"
                                aria-haspopup="true" aria-expanded="false">
                                <svg width="25" height="25" viewBox="0 0 25 25">
                                    <g fill="none" fill-rule="evenodd">
                                        <path fill="#FF6800" fill-rule="nonzero" d="M15.927 11.842c.208-.18.445-.38.697-.592 1.856-1.56 4.397-3.697 4.397-5.942V3.956a2.618 2.618 0 0 0-2.615-2.615H6.636a2.618 2.618 0 0 0-2.615 2.615v1.352c0 2.245 2.542 4.382 4.398 5.942.252.211.49.411.697.592.197.172.31.412.31.658a.878.878 0 0 1-.31.658c-.208.18-.445.38-.697.592-1.856 1.56-4.398 3.697-4.398 5.942v1.352a2.618 2.618 0 0 0 2.616 2.615h11.77a2.618 2.618 0 0 0 2.614-2.615.872.872 0 1 0-1.743 0c0 .48-.391.871-.872.871H6.636a.873.873 0 0 1-.87-.871v-1.352c0-1.433 2.363-3.42 3.775-4.607.257-.217.5-.421.72-.612a2.62 2.62 0 0 0 .91-1.973 2.62 2.62 0 0 0-.91-1.974c-.22-.19-.463-.394-.72-.61C8.129 8.727 5.765 6.74 5.765 5.307V3.956c0-.48.391-.871.872-.871h11.77c.48 0 .87.39.87.871v1.352c0 1.433-2.363 3.42-3.775 4.607-.257.217-.5.421-.72.611a2.62 2.62 0 0 0-.91 1.974c0 .751.332 1.47.91 1.973.229.199.483.412.752.637.848.71 1.81 1.515 2.568 2.37a.872.872 0 0 0 1.305-1.156c-.845-.953-1.903-1.839-2.754-2.55-.262-.22-.51-.428-.726-.616a.878.878 0 0 1-.31-.658c0-.246.113-.486.31-.658z"/>
                                        <path fill="#FF6800" fill-rule="nonzero" d="M13.424 15.93a1.387 1.387 0 0 0-1.805 0c-.033.027-2.528 2.133-3.192 2.842l-.021.023a.742.742 0 0 0 .545 1.246h7.141a.742.742 0 0 0 .545-1.246l-.021-.023c-.664-.71-3.16-2.815-3.192-2.843z"/>
                                        <path stroke="#FF6800" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.7" d="M18 16c2 2 2.667 4 2 6"/>
                                    </g>
                                </svg>
                            </a>
                        @endif
                        @if((new App\Model\Company())->getRips()->isNotEmpty())
                            <a href="{{ url('ripPeriodExport')}}">
                            <svg width="25" height="25" viewBox="0 0 25 25">
                                <g fill="#FF6800" fill-rule="nonzero" stroke="#FFF" stroke-width=".8">
                                    <path d="M22.687 15c.73 0 1.313.537 1.313 1.209v6.582c0 .672-.584 1.209-1.313 1.209H2.362C1.632 24 1 23.463 1 22.791v-6.582C1 15.537 1.632 15 2.362 15c.729 0 1.312.537 1.312 1.209v5.328h17.7V16.21c0-.672.584-1.209 1.313-1.209z"/>
                                    <path d="M12.031 17.616l-5.61-5.283c-.561-.528-.561-1.296 0-1.825a1.383 1.383 0 0 1 1.938 0l3.264 3.026V2.297C11.623 1.577 12.235 1 13 1s1.377.576 1.377 1.297v11.237l3.213-3.026a1.446 1.446 0 0 1 1.99 0c.56.529.56 1.297 0 1.825l-5.611 5.283c-.255.24-.612.384-.969.384s-.714-.144-.969-.384z"/>
                                </g>
                            </svg>
                            </a>
                        @endif
                            <a href="javascript:" class="close_rip" >
                                <svg enable-background="new 0 0 30 28" height="30px" id="Слой_1" version="1.1" viewBox="0 0 30 28" width="30px" xml:space="preserve">
                                    <path d="M17.459,16.014l8.239-8.194c0.395-0.391,0.395-1.024,0-1.414c-0.394-0.391-1.034-0.391-1.428,0  l-8.232,8.187L7.73,6.284c-0.394-0.395-1.034-0.395-1.428,0c-0.394,0.396-0.394,1.037,0,1.432l8.302,8.303l-8.332,8.286  c-0.394,0.391-0.394,1.024,0,1.414c0.394,0.391,1.034,0.391,1.428,0l8.325-8.279l8.275,8.276c0.394,0.395,1.034,0.395,1.428,0  c0.394-0.396,0.394-1.037,0-1.432L17.459,16.014z" fill="#ff6800" id="Close"></path>
                                </svg>
                            </a>
                        </div>
                    </div>
                </li>
                <li class="search-items">
                    <div class="rip-search">
                    <svg width="25" height="25" viewBox="0 0 20 25">
                        <path fill="#2C3542" fill-rule="nonzero" stroke="#2C3542" stroke-width=".4" d="M19.876 19.286l-3.402-3.402a5.945 5.945 0 0 0 1.465-3.915A5.976 5.976 0 0 0 11.969 6 5.976 5.976 0 0 0 6 11.97c0 3.29 2.679 5.969 5.97 5.969a5.945 5.945 0 0 0 3.914-1.465l3.402 3.402c.08.08.19.124.295.124a.42.42 0 0 0 .295-.714zM6.835 11.969a5.137 5.137 0 0 1 5.131-5.13 5.137 5.137 0 0 1 5.132 5.13 5.14 5.14 0 0 1-5.132 5.135 5.14 5.14 0 0 1-5.131-5.135z"/>
                    </svg>

                        <input type="text" name="keyword" id="serach_ripperiod" placeholder="Search RIP Period" autocomplete="off">
                    </div>
                </li>                                     
                @include('auth.rip.ripPeriod')   

                @if((new App\Model\Company())->getRips()->isEmpty())
                    <li class="no-content no-active-rip">
                    <div >
                    <div class="no-rip">
                        <img src="../images/new-site/empty_icon.svg" alt="">
                        <h3>Looks a little empty here.</h3>
                        <p>You haven’t created any RIP(s) yet!</p>
                        <p>Start creating RIP(s) from the top bar.</p>
                         </div>
                     </div>  
                    </li>    
                @endif  

                @if((new App\Model\Company())->getExpiredRips()->isEmpty())
                     <li class="no-content no-expired-rip">
                    <div >
                    <div class="no-rip">
                    <img src="../images/new-site/empty_icon.svg" alt="">
                    <h3>There is no expired RIP available.</h3>
                     </div>   
                    </div> 
                     </li>    
                @endif               
               
            </ul>  
            <div class="footer-rip-button">
                <button class="add-rip text-uppercase" data-toggle="modal" data-target="#addripPeriod">Add RIP Period</button>
            </div>
        </div>
    </div>
