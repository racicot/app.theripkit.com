<div id="taskStart_{{ $task->id }}" class="company_{{$task->goal->company_info->id}}">
 @if($task->percentage_complete != 100)
   <div class="n-panel-body-top n-rip-card task-row-wrap " taskid="{{ $task->id }}"
         current_id="{{ $task->id }}">
  @else
    <div class="n-panel-body-top n-rip-card task-row-wrap task-completing" taskid="{{ $task->id }}"
         current_id="{{ $task->id }}">
  @endif
        <div class="n-rip-card-left">
            <div class="rip-card-in-left task-row">
                <div class="task-status icon-wrap">
                @if(auth()->user()->hasTask($task->id))

                    @if($task->percentage_complete != 100)
                        @if(auth()->user()->can('Complete A Task'))
                            <span class="task-status icon-wrap js-task-comp">
                            <img src="{{ asset('images/icon-success.png') }}" class="success-icon">
                            </span>
                        @endif
                    @else
                        @if(auth()->user()->can('Undo A Completed Task'))
                            <div class="task-status icon-wrap js-task-undo">
                                <img src="{{ asset('images/icon-undo.png') }}" class="success-icon">
                            </div>
                        @endif
                    @endif

                @endif
                </div>
                @if(isset($current_page) && $current_page != ''  && $current_page != 'rip' && $current_page != 'overview-rip' && $current_page != 'overview-task')
                  <div class="dashboard-users">
                      <h3 class="card-list__heading taskName_{{ $task->id }}" >{{ $task->name }}</h3>

                      @if($task->percentage_complete == 100)
                       <span class="label label-success complete-btn">COMPLETED</span>
                      @else
                        <span class="label label-success incomplete-btn">
                          INCOMPLETE
                        </span>
                      @endif
                      <p class="card-list__meta">
                          <span><label>RIP:</label> {{ $task->goal->goal }}</span>
                          <span><label>RIP Period:</label> {{ $task->goal->rip->name }}</span>
                          <span><label>Created on:</label> <date>{{ date('M d, Y', strtotime($task->created_at)) }}</date></span>
                      </p>
                  </div>

                   @if(auth()->user()->can('Assign User To A Task'))
                      <div class="avtar-users">
                        <div class="goalTaskUserList goalTaskUserList_{{ $task->goal_id }} taskUserList_{{ $task->id}}" taskId="{{ $task->id }}">
                        <a class="task-user-list d-flex" data-toggle="modal" data-target="#showUserListModal">
                          @foreach($task->getTaskUser() as $key=>$taskuser) 
                            @if($key < 1)
                              <?php 
                                $userName = explode(' ',$taskuser->name);
                                $newUser_name = $userName[0];
                                if(isset($userName[1])){
                                   $newUser_name.= ' '.strtoupper(substr($userName[1], 0, 1)).'.';
                                }                                     
                              ?>
                              <div class="card-list-avatar goalTaskUser_{{ $taskuser->id }}">
                                <img data-toggle="tooltip"
                                @if($taskuser->image != '' && $taskuser->image != 'default.jpg') src="{{ $taskuser->userImage() }}" @endif
                               title="{{ $taskuser->name }}"> 
                              </div>
                              <span class="username">{{ $newUser_name }}</span>
                            @endif
                          @endforeach

                          @if(count($task->getTaskUser()) > 1)
                            <div class="card-list-avatar lists-count">+{{ count($task->getTaskUser())-1 }}
                            </div>
                          @endif
                        </a>
                        </div>
                      </div>
                    @endif
                @else
                  <div>
                    <h3 class="card-list__heading taskName_{{ $task->id }}">{{ $task->name }}</h3>                   
                   
                  </div>
                      <div class="n-rip-card-right">
                      <p class="card-list__meta">                      
                        <span><label>Created on:</label> <date>{{ date('M d, Y', strtotime($task->created_at)) }}</date></span>
                    </p>  
                         @if(auth()->user()->can('Assign User To A Task'))
                            <div class="avtar-users">
                              <div class="avtar-add-users">
                                  <a data-toggle="modal" companyId = "{{ $task->goal->company_info->id }}" data-target="#addusers" id="addUser_{{ $task->id }}"
                                  taskId="{{ $task->id }}"
                                  class="dropdown-toggle addedTaskUser">
                                    <svg width="34" height="34" viewBox="0 0 24 24">
                                        <g fill="#2dbe74" fill-rule="nonzero">
                                          <circle cx="12.5" cy="12.5" r="8.5" fill="#2dbe74" opacity="1"></circle>
                                          <path fill="#FFF" stroke="#FFF" d="M12.77 12.23h2.96a.27.27 0 0 1 0 .54h-2.96v2.96a.27.27 0 0 1-.54 0v-2.96H9.27a.27.27 0 0 1 0-.54h2.96V9.27a.27.27 0 0 1 .54 0v2.96z"></path>
                                        </g>
                                    </svg>
                                  </a>
                              </div>
                              <div class="goalTaskUserList goalTaskUserList_{{ $task->goal_id }} taskUserList_{{ $task->id}}" taskId="{{ $task->id }}">
                              <a class="task-user-list d-flex" data-toggle="modal" data-target="#showUserListModal">
                                @foreach($task->getTaskUser() as $key=>$taskuser)
                                  @if($key < 1)
                                    <?php 
                                      $userName = explode(' ',$taskuser->name);
                                      $newUser_name = $userName[0];
                                      if(isset($userName[1])){
                                         $newUser_name.= ' '.strtoupper(substr($userName[1], 0, 1)).'.';                  
                                      }                                     
                                    ?>

                                    <div class="card-list-avatar goalTaskUser_{{ $taskuser->id }}">
                                      <img data-toggle="tooltip"
                                      @if($taskuser->image != '' && $taskuser->image != 'default.jpg') src="{{ $taskuser->userImage() }}" @endif
                                     title="{{ $taskuser->name }}">
                                    </div>
                                    <span class="username">{{ $newUser_name }}</span>
                                  @endif
                                @endforeach

                                @if(count($task->getTaskUser()) > 1)
                                  <div class="card-list-avatar lists-count">+{{ count($task->getTaskUser())-1 }}
                                  </div>
                                @endif
                              </a>
                              </div>
                                
                            </div>
                          @endif

                           @if($task->percentage_complete == 100)
                            <div class="rip-status complete-status"> Completed</div>
                           @else
                            <div class="rip-status incomplete-status">
                              INCOMPLETE
                          </div>
                           @endif
                      </div>
                      @if((auth()->user()->can('Delete Task From RIP')) || auth()->user()->can('Update The Name Of A Task'))
                      <div class="dropdown menu-top-right">
                        <div class="kebab-menu dropdown-toggle" id="kebab-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul class="dropdown-menu" id="kebab-menu" aria-labelledby="kebab-menu" style="">

                          @if(auth()->user()->can('Update The Name Of A Task'))
                            <li><a class="edittaskLink" data-toggle="modal" data-target="#editModal" href="{{ url('/').'/editTask/'.$task->id }}">
                               <svg width="25" height="25" viewBox="0 0 25 25">
                                    <path fill="#2C3542" fill-rule="nonzero" d="M6 17.084V20h2.916l8.601-8.601-2.916-2.916L6 17.083zm13.773-7.94a.774.774 0 0 0 0-1.097l-1.82-1.82a.774.774 0 0 0-1.097 0l-1.423 1.424 2.916 2.916 1.424-1.423z"/>
                                </svg>
                                Edit Task
                                </a>
                            </li>
                        @endif
                        @if(auth()->user()->can('Delete Task From RIP'))
                            <li>
                            <a class="deleteTaskItem" href="{{ url('/').'/deleteTask/'.$task->id }}" companyName="{{$task->goal->company_info->id}}">
                                    <svg width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#F14343" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"></path>
                                    </svg>
                                    <span  class="red-text">Delete Task</span>
                                </a>
                            </li>
                          @endif
                        </ul>
                      </div>
                    @endif
                @endif
            </div>
        </div>

        @if(auth()->user()->can('Complete A Task'))
            @if($task->percentage_complete != 100)
                <div class="task-confirm">
                    <span>Mark Complete</span>
                    <div>
                    <span class="icon-wrap js-task-confirm">
                    <svg class="check-icon"
                    viewBox="0 0 32 32">
                    <polygon
                    points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon>
                    </svg>
                    </span>
                    <span class="icon-wrap js-cancel-task-confirm">
                    <svg class="close-icon"
                    viewBox="0 0 32 32">
                    <polygon
                    points="24.485,27.314 27.314,24.485 18.828,16 27.314,7.515 24.485,4.686 16,13.172 7.515,4.686 4.686,7.515 13.172,16 4.686,24.485 7.515,27.314 16,18.828 "></polygon>
                    </svg>
                    </span>
                    </div>
                </div>
            @endif
            @if($task->percentage_complete == 100)
                <div class="task-undo-in">
                    <span>Mark Incomplete</span>
                    <div>
                    <span class="icon-wrap js-task-undo js-task-undo-icon">
                    <svg class="check-icon"
                     viewBox="0 0 32 32">
                    <polygon
                          points="27.672,4.786 10.901,21.557 4.328,14.984 1.5,17.812 10.901,27.214 30.5,7.615 "></polygon>
                    </svg>
                    </span>
                    <span class="icon-wrap js-cancel-task-undo">
                    <svg class="close-icon"
                     viewBox="0 0 32 32">
                  <polygon
                          points="24.485,27.314 27.314,24.485 18.828,16 27.314,7.515 24.485,4.686 16,13.172 7.515,4.686 4.686,7.515 13.172,16 4.686,24.485 7.515,27.314 16,18.828 "></polygon>
                </svg>
              </span>
                    </div>
                </div>
            @endif
        @endif


    </div><!-- /.n-panel-body-top -->
 </div>
