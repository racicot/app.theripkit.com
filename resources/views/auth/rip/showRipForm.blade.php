<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Edit RIP Period</h4>
        </div>    
    
        <div class="modal-body">
            <div class="add-user">
                <form id="editRipPeriod" method="post" autocomplete="off" action="{{ url('editRip') }}">
                    {{ csrf_field() }}
                    
                    <input type="hidden" id="rip_period_id" name="rip_period_id" value="{{ $rips->id }}">
                    <!-- Max Length added for Name -->
                    <div class="form-group">
                        <label for="inputRipName">RIP Name</label>
                        <input type="text" class="form-control" value="{{ $rips->name}}" maxlength="30" id="inputRipName" name="name"
                        placeholder="RIP Name">

                        <span class="error-msg err_rip"> </span>
                    </div>
                    <div class="form-group">
                        <label for="inputDateStart">Start Date</label>
                        <input type="text" class="form-control date-picker inputEditDateStart" value="{{ $rips->start_date }}" name="start_date" readonly placeholder="Start Date">
                        <span class="error-msg err_start"></span>
                    </div>
                    <div class="form-group">
                        <label for="inputDateEnd">End Date</label>
                        <input type="text" class="form-control date-picker inputEditDateEnd" value="{{ $rips->end_date }}" name="end_date" readonly="" placeholder="End Date">
                        <span class="error-msg err_end"></span>
                    </div>
                    <input type="hidden" value="1" name="status">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button id="editRipPeriodButton" type="button" class="btn btn-theme pull-right">Update</button>
                    </div>
                    <div id="result"></div>
                </form>
            </div><!-- /.add-user -->
        </div>
    </div>
</div>    