@extends('layouts.auth.master')
@section('title')

    Companies

@endsection
@section('content')
    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <h1>
            Companies
        </h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Companies</li>
        </ol>

    </section> -->

    <!-- Main content -->
    <section class="content">

        <div class="user-listing">
            <div class="box">
                <div class="box-header mt-30 mb-20">
                    <h1 class="main-title">Companies</h1>
                    <button type="button" class="btn btn-theme btn-lg btn-uppercase pull-right" id="addNewCompanyButton" data-toggle="modal" data-target="#addNewCompany">Add New Company</button>
                </div>

                <!-- /.box-header -->
                <div class="box-body table-middle">
                    <table id="company-table" class="table table-hover new-table-design">
                        <thead>
                        <tr>
                            <th>Company Name</th>
                            <th>Phone</th>
                            <th>Members</th>
                            <th>Plan</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
    <!-- /.content-wrapper -->



@endsection
