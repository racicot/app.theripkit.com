<a class="company-user-list d-flex" data-toggle="modal" data-target="#showUserListModal">
@foreach($users as $key=>$user)
  @if($key < 3)

    <div class="card-list-avatar goalTaskUser_{{ $user->id }}">
      <img data-toggle="tooltip"
      @if($user->image != '' && $user->image != 'default.jpg') src="{{ $user->userImage() }}" @endif
     title="{{ $user->name }}">
    </div>
  @endif
@endforeach

@if(count($users) > 3)
  <div class="card-list-avatar lists-count">+{{ count($users)-3 }}
  </div>
@endif
</a>