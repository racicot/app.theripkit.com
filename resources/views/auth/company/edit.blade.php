<div class="modal-dialog modal-edit-company" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit Company</h4>
        </div>
        <div class="modal-body">
            <form id="editCompany" method="post" enctype="multipart/form-data"
                  action="{{ url('company').'/'.base64_encode($company->id) }}" autocomplete="off">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group upload-flex">
                
                    <div class="photo-in">
                        <label for="edit-company-logo" class="after-image">
                            <img @if($company->logo) { src="{{ $company->logo ? $company->companyImage() : '' }}" } @else src="{{ url('../images/company-placeholder.jpg')}}" @endif id="uploaded-logo" alt="Company"></label>
                        <div class="wrap-file show">
                            <input type="file" name="image" class="form-control" id="edit-company-logo"></div>
                            
                    </div><!-- photo-in -->
                    <div class="b-label">
                                <div class="upload-comapny-logo">Upload Company Logo</div>
                                <div class="upload-format"> (You can upload jpeg, gif<br>
                                    and png under 1 mb)</div>
                            </div>
                    <span class="edit-logo-status"></span>
                </div><!-- /.form-group -->
                <!-- Character Limit added to Company name-->
                <div class="form-group">
                    <label for="inputName">Company Name</label>
                    <input type="text" class="form-control" id="inputName" name="company_name"
                           placeholder="Company Name"  value="{{ $company->name }}" maxlength="26">
                    <span class="help-block error-msg margin-0" id="name-err"></span>
                </div>
            <!-- Country code dropdown added -->
                <div class="form-group">
                    <label for="phone">Phone</label>
                    
                    
                    <div class="input-group account-input-group">
                        <div class="input-group-btn">
                          <select id="country_code" width="200%" data-placeholder="Select Country" name="country_code" class="form-control country common-chosen">

                            <?php  if(isset($countries)){ ?>
                                @foreach($countries as $country)
                                    @if(($company->isd_code == $country['isd_code']))
                                        <option value="+{{ $country['isd_code'] }}" @if((!empty($company)) && ($company->isd_code == $country['isd_code'])) selected @endif> 
                                           {{ $country['name'] }} </option>
                                    @else
                                        <option value="+{{ $country['isd_code'] }}" > 
                                            {{ $country['name'] }}</option>
                                    @endif
                                @endforeach
                            <?php  } ?>
                        </select>  

                        </div><!-- /btn-group -->

                        <div class="input-group-btn">
                        <input type="text" class="form-control" id="phone" name="phone"
                           value="{{ $company->phone }}" class="phone" placeholder="Phone" 
                           maxlength="10">
                        </div>
                        <input class="form-control" id="isd_code_company" name="isd_code_company" val="{{ $company->isd_code }}" type="hidden">
                    </div><!-- /input-group -->
                    <span class="help-block error-msg margin-0" id="phone-err"></span>
                    
                </div>

                @if(auth()->user()->can('Update Company Parents'))

                    <div class="form-group select-style">
                        <label>Owner       <small class="highlight">
                            (Users already assigned to a company will not be listed here)
                            </small></label>
                        <p>
                            <select class="form-control country common-chosen" name="owner">
                                <option value="">Select User</option>
                                @foreach($owners as $owner)
                                    <option value="{{ $owner->id }}"
                                            @if((!empty($currentOwner)) && ($currentOwner->id == $owner->id)) selected @endif>{{ $owner->name }}
                                        ({{ $owner->roles->pluck('name')->first() }})
                                    </option>
                                @endforeach
                            </select>
                        </p>

                    </div>
                @endif
               
                <div class="radiobox form-group">
                <span class="title">Do you want the company to be</span>
                    <label class="first">
                        <input type="radio" value="1" name="status" @if($company->status == 1)
                        checked
                                @endif
                        > <span>Active</span>
                    </label>

                     <label>
                        <input type="radio" value="0" name="status" @if($company->status == 0)
                        checked
                                @endif
                        > <span>Inactive</span>
                    </label>
                </div>

                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="editCompanyButton" class="btn btn-theme pull-right">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal-content -->

