<?php use App\Http\Controllers\RipController;?>
<html>
<head>
<link rel="stylesheet" href="{{ asset('css/new-theme.css') }}">
</head>
<body style="padding:0; margin:0;min-width:1170px;background:#fafafa;">

    <!-- Content Wrapper. Contains page content -->
        <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                     <!-- Content Header (Page header) -->

                    <table width="100%" cellpadding="0" cellspacing="0">
                        <!-- <tr>
                            <td  style="width: 50%; padding:10px;">
                                <img src="{{ \App\Model\Company::find(session('company_id'))->companyImage() }}" style="max-width: 90px; max-height: 40px;">
                            </td>

                            <td style="width: 50%; text-align: right;padding:10px;">
                                <img src="{{ url('images/bitmap@3x.png') }}" style="max-width: 90px; max-height: 40px;">
                            </td>
                        </tr> -->
                        <tr>
                            <td colspan="10" height="6" style="text-align: center;"></td>
                        </tr>
                        <tr>
                            <td colspan="2" height="30" style="text-align: left;">
                                <img src="{{ url('images/new-site/company-logo.png') }}" width="187" height="30" />
                            </td> 
                        </tr>
                        <tr>
                            <td colspan="2" height="6" style="text-align: center;"></td>
                        </tr>
                        <tr>
                            <td colspan="2" style="text-align: left;">
                                <h1 style="font-size: 20px;color: #5e6775;text-transform: uppercase;font-family: 'Montserrat', 'Open Sans', sans-serif;font-weight:400;">
                                    {{ session('company_name') }}
                                </h1>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" height="30" style="text-align: center;"></td>
                        </tr>
                    </table>
                </td>
            </tr>
       </table>
        <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                <!-- Main content -->
                    <div>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="40%" valign="top"> 
                                        <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20px"></td>
                                                            <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                1. What
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                @if($portfolio)
                                                                <?php echo strip_tags($portfolio->what,"<br>"); ?> 
                                                                @endif
                                                                </td>
                                                            </tr>
                                                            </table>
                                                            </td>
                                                            <td width="10px"></td>
                                                            <td width="60px" valign="middle">
                                                                <img src="{{ url('images/what.png') }}" width="60" height="60" />
                                                            </td>
                                                            <td width="10px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                    </table>
                                                
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr><td height="15"></td></tr>
                                                    </table>
                                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20px"></td>
                                                            <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                    3. How
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)
                                                                    <?php echo strip_tags($portfolio->how,"<br>"); ?> 
                                                                @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
                                                            <td width="10px"></td>
                                                            <td width="60px" valign="middle">
                                                                <img src="{{ url('images/how.png') }}" width="60" height="60" />
                                                            </td>
                                                            <td width="10px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                    </table>
                                            
                                                </td>
                                                <td width="30"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="20%" valign="middle" style="text-align:center; background: #fff;border: 1px solid #eaeaed;border-radius: 4px;">
                                        <i class="banner-icon" style="display:inline-block;">
                                                <img src="{{ url('images/ci.png') }}" width="159" height="134" />
                                        </i>
                                    </td>
                                    <td width="40%">
                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td width="30"></td>
                                                <td>
                                                <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10px"></td>
                                                            <td width="60px" valign="middle">
                                                                <img src="{{ url('images/why.png') }}" width="60" height="60" />
                                                            </td>
                                                            <td width="10px"></td>
                                                            <td>
                                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        2. Why
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->why,"<br>"); ?> 
                                                                        @endif
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="20px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                    </table>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr><td height="15"></td></tr>
                                                    </table>
                                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="10px"></td>
                                                            <td width="60px" valign="middle">
                                                                <img src="{{ url('images/sd.png') }}" width="60" height="60" />
                                                            </td>
                                                            <td width="10px"></td>
                                                            <td>
                                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                    <tr>
                                                                        <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        4. Strategic Differentiators
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->strategic,"<br>"); ?>               
                                                                        @endif
                                                                        </td>
                                                                    </tr>
                                                                </table>
                                                            </td>
                                                            <td width="20px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                    </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="30"></td></tr></table>
                    </div>
                </td>
            </tr>
       </table>
       <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                <div>
                            <table width="100%" cellpadding="0" cellspacing="0" valign="top">
                                        <tr>
                                            <td colspan="3" height="15"></td>
                                        </tr>
                                        <tr>
                                            <td width="33%" valign="top">    
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="60" align="center">
                                                            <img src="{{ url('images/ep.png') }}" width="60" height="60" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        5. Elevator Pitch
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)

                                                                    <?php echo strip_tags($portfolio->elevator,"<br>"); ?>
                                                                    @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="3" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="2%">
                                            <td width="33%" valign="top">
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="60" align="center">
                                                            <img src="{{ url('images/fg.png') }}" width="60" height="60" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        6. Financial Goal
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->financial,"<br>"); ?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="3" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="2%">
                                            <td width="33%" valign="top">
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="60" align="center">
                                                            <img src="{{ url('images/tbv.png') }}" width="60" height="60" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="3" height="10"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        7. Top Behavioural Values
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->behavioural,"<br>");?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                    <td colspan="3" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                            </table>
                    </div>
                </td>
            </tr>
       </table>
       <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                <div>
                        <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="85"></td></tr></table>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td colspan="3" height="15"></td>
                            </tr>
                            <tr>
                                <td colspan="3" width="100%">    
                                    <table align="center" width="70%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                        <tr>
                                            <td colspan="3" height="20"></td>
                                        </tr>
                                        <tr>
                                            <td width="20px"></td>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr>
                                                        <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                            1. Emotional 
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="30" style="border:0; font-size: 18px;color: #5e6775;text-transform: none;line-height: 24px;">
                                                            @if($portfolio)
                                                            <?php echo strip_tags($portfolio->emotional,"<br>"); ?>           
                                                            @endif
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="20px"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="60" align="center">
                                                <img src="{{ url('images/emotional.png') }}" width="60" height="60" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" height="10"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="30"></td>
                            </tr>
                            <tr>
                                <td width="40%" valign="top">
                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                            <tr>
                                                <td>
                                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                        <tr>
                                                            <td width="20px"></td>
                                                            <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                    2. Economic
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;font-weight: 600;max-width: initial;text-transform: none;line-height: 24px;">
                                                                    @if($portfolio)
                                                                    <?php echo strip_tags($portfolio->economic,"<br>"); ?>   
                                                                    @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                            </td>
                                                            <td width="10px"></td>
                                                            <td width="60px" valign="middle">
                                                                <img src="{{ url('images/economic.png') }}" width="60" height="60" />
                                                            </td>
                                                            <td width="10px"></td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="5" height="20"></td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="30"></td>
                                            </tr>
                                    </table>
                                </td>
                                <td width="20%" style="text-align:center;" valign="middle">
                                    <i class="banner-icon">
                                            <img src="{{ url('images/bp.png') }}" width="218" height="198" />
                                    </i>
                                </td>
                                <td width="40%" valign="top">
                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="65"></td>
                                            <td>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/functional.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        3. Functional
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                            <?php echo strip_tags($portfolio->functional,"<br>"); ?>  
                                                                            @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3" height="30"></td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
       </table>
       <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                <div style="background: url(../images/infinity-icon.png) center no-repeat;">
                        <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="100"></td></tr></table>
                        <table width="100%" cellpadding="0" cellspacing="0">
                            <tr>
                                <td width="40%" valign="top">  
                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td>
                                                <table width="100%" cellpadding="0" cellspacing="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                        <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                            1. Customer Problem
                                        </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->customer_problem,"<br>"); ?> 
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/cp.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr><td height="15"></td></tr>
                                                </table>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        3. Key Customer Segments
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->key_customer_segments,"<br>"); ?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/kcs.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr><td height="15"></td></tr>
                                                </table>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        5. Sales Channels
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->sales_channel,"<br>"); ?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/sc.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                            <td width="30"></td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="20%" valign="middle" style="text-align:center; background: #fff;border: 1px solid #eaeaed;border-radius: 4px;">
                                
                                    <i class="banner-icon" style="display:inline-block;">
                                            <img src="{{ url('images/csr.png') }}" width="139" height="151" />
                                    </i>
                                
                                </td>
                                <td width="40%" valign="top">
                                    <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="30"></td>
                                            <td>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/cs.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        2. Customer Solution
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->customer_solution,"<br>"); ?> 
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr><td height="15"></td></tr>
                                                </table>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/rs.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        4. Revenue Streams
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->revenue_stream,"<br>"); ?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                    <tr><td height="15"></td></tr>
                                                </table>
                                                <table width="100%" width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="10px"></td>
                                                        <td width="60px" valign="middle">
                                                            <img src="{{ url('images/vp.png') }}" width="60" height="60" />
                                                        </td>
                                                        <td width="10px"></td>
                                                        <td>
                                                            <table width="100%" width="100%" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                        6. Value Propositions
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                        @if($portfolio)
                                                                        <?php echo strip_tags($portfolio->value_proposition,"<br>"); ?>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td width="20px"></td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5" height="20"></td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
       </table>
       <table width="1270px" cellpadding="0" cellspacing="0" align="center" bgcolor="#fafafa" style="page-break-inside:avoid">
            <tr>
                <td>
                            <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="30"></td></tr></table>
                            <table width="100%" cellpadding="0" cellspacing="0" valign="top">
                                <tr><td colspan="3" height="15"></td></tr>
                                <tr>
                                    <td width="33%" valign="top">  
                                        <table width="100%" cellpadding="0" cellspacing="0" valign="top" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="60" align="center">
                                                    <img src="{{ url('images/kar.png') }}" width="60" height="60" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td width="20px"></td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                7. Key Activities &amp; Resources
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                @if($portfolio)
                                                                <?php echo strip_tags($portfolio->key_activities_resource,"<br>"); ?>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20px"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="20"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="2%"></td>
                                    <td width="33%" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="60" align="center">
                                                    <img src="{{ url('images/kp.png') }}" width="60" height="60" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td width="20px"></td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                8. Key Partners
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                @if($portfolio)
                                                                <?php echo strip_tags($portfolio->key_partner,"<br>"); ?>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20px"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="20"></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td width="2%"></td>
                                    <td width="33%" valign="top">
                                        <table width="100%" cellpadding="0" cellspacing="0" border="0" style="border: 1px solid #eaeaed;background: #f5f5f6;border-radius: 4px;min-height: 130px;">
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="60" align="center">
                                                    <img src="{{ url('images/kc.png') }}" width="60" height="60" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="10"></td>
                                            </tr>
                                            <tr>
                                                <td width="20px"></td>
                                                <td>
                                                    <table width="100%" cellpadding="0" cellspacing="0">
                                                        <tr>
                                                            <td style="border:0; font-size: 20px;color: #2c3542;font-weight: 600;margin-bottom: 5px;font-family: 'Montserrat-SemiBold','Open Sans', sans-serif;max-width: initial;text-transform: none;">
                                                                9. Key Costs
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td height="30" style="border:0; font-size: 18px;color: #5e6775;max-width: initial;text-transform: none;line-height: 24px;">
                                                                @if($portfolio)
                                                                <?php echo strip_tags($portfolio->key_cost,"<br>"); ?>
                                                                @endif
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                                <td width="20px"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3" height="20"></td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="75"></td></tr></table>
                 
                </td>
            </tr>
       </table>
        
        @if($rips->isNotEmpty())            
            @foreach($rips as $key=>$rip)
                <table width="1270px" cellpadding="0" cellspacing="0" align="center" style="page-break-inside:avoid">
                    @if($key == 0)
                    <tr>
                        <td>
                        <table width="1270px" cellpadding="0" cellspacing="0">
                        <tr>
                            <td height="25">
                            &nbsp;     
                        </td>
                        </tr>
                        <tr>
                        <td style="font-size: 25px;font-weight: 400;color: #2c3542;margin-bottom: 45px;margin-top: 20px;line-height: 1.15;text-transform: uppercase;font-family: 'Montserrat', 'Open Sans', sans-serif;">
                            RIP(s)       
                        </td>
                        </tr>
                        <tr>
                        <td height="40">
                            &nbsp;     
                        </td>
                        </tr>
                        </table>        

                        </td>
                    </tr>    
                    @endif
                    <tr>
                        <td>
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="80%"> 
                                        <h1 class="rip-heading" id="rip_content" style="font-size: 20px;font-weight: 600;margin-bottom: 0;color: #2c3542;text-transform: capitalize;">{{ $rip->name }}</h1>
                                        <table width="100%" cellpadding="0" cellspacing="0">
                                            <tr><td height="3" colspan="2"></td></tr>
                                            <tr>
                                                <td width="280"><span id="date-header" style="font-size: 20px;font-weight: 500;margin-bottom: 0;color: #ff6800;font-family: 'Montserrat', 'Open Sans', sans-serif;">{{ date('M d, Y', strtotime($rip->start_date)).' - '.date('M d, Y', strtotime($rip->end_date)) }}</span></td>
                                                <td style="font-size: 18px;font-weight: 500;margin-bottom: 0;color: #ff6800;font-family: 'Montserrat', 'Open Sans', sans-serif;"><span id="day-left" @if(\Carbon\Carbon::now()->format('Y-m-d') > $rip->end_date)style="background: #fdecec;padding: 5px 18px;border-radius: 20px;color: #f14343;" @endif> {{ with(new RipController())->datePeriod($rip) }} </span></td>
                                            </tr>
                                        </table>
                                    </td>
                                    <td align="right" width="20%">
                                        <span style="font-family: 'Montserrat-SemiBold','Open Sans', sans-serif; color:#5e6775; font-size:18px; font-weight:600;">Total Progress</span>
                                        &nbsp;&nbsp;
                                        <span class="overlay ripPer_{{ $rip->id }}">
                                                <span style="font-family: 'Montserrat-SemiBold','Open Sans', sans-serif; color:#474f5a; font-size:22px; font-weight:600;">{{ $rip->getRipPercentage() }}</span>
                                                <span style="font-family: 'Montserrat-SemiBold','Open Sans', sans-serif; color:#5e6775; font-size:18px; font-weight:600;">%</span>
                                        </span>
                                    </td>
                                </tr>
                                <tr><td height="30" colspan="2">&nbsp;</td></tr>

                            </table>
                        </td>
                        </tr>
                        </table>    
                            @if(count($rip->category) > 0)
                                @foreach($rip->category as $category) 
                                <table width="1270" align="center" cellpadding="0" cellspacing="0" style="page-break-inside:avoid">
                                    <tr>
                                        <td style="border-left: 4px solid #2dbe74; box-shadow: 0 2px 5px 0 rgba(44, 53, 66, 0.14); background-color: #fff;">
                                            <table width="100%" cellpadding="0" cellspacing="0">
                                                <tr><td height="23px" colspan="4">&nbsp;</td></tr>
                                                <tr>
                                                    <td width="55px">&nbsp;</td>
                                                    <td style="padding-right:18px;">
                                                        <h3 category_id="{{ $category->id }}" data-gramm_editor="false" aria-controls="collapseTop" contenteditable="false" style="position: relative;font-size: 18px;font-weight: 600;color: #2c3542;margin-bottom: 5px;word-break: break-all;word-wrap: break-word;line-height: 1.5;">
                                                            {{ $category->name }}
                                                        </h3>
                                                        <p style="color: #5e6775;cursor: initial;font-size: 18px;font-weight: 500;margin:0;">
                                                            {{ count($category->goals) }} RIP(s)
                                                        </p>
                                                    </td>
                                                    <td align="right"> 
                                                        <table width="110" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="font-size: 18px;color: #2c3542;font-weight: 400;vertical-align: top;">
                                                                            Progress
                                                                </td>
                                                                <td style="font-size: 18px;color: #2c3542;font-weight: 400;vertical-align: top;" align="right">
                                                                            {{ $category->getAvgPer($rip->id) }}%
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50px">&nbsp;</td>
                                                </tr>
                                                <tr><td height="22px" colspan="4">&nbsp;</td></tr>
                                            </table>
                                        </td>
                                        </tr>
                                        </table>    
                                            @if (count($category->goals) >0)
                                                <table width="1270" align="center" cellpadding="0" cellspacing="0" style="page-break-inside:avoid">
                                                    <tr>
                                                    <td width="55px" style="background-color: #ddffee;border-left: 4px solid #2dbe74;">&nbsp;</td>
                                                    <td height="40px" style="width: 100%;height: 40px;background-color: #ddffee;display: flex;align-items: center;color: #000;font-size: 18px;">RIP(s)</td></tr>
                                                </table>
                                                @foreach($category->goals as $goal)
                                                <table width="1270" align="center" cellpadding="0" cellspacing="0" style="page-break-inside:avoid">
                                                    <tr>
                                                    <td style="border-left: 4px solid #2dbe74;box-shadow: 0 2px 5px 0 rgba(44, 53, 66, 0.14);background-color: #fff;">
                                                    <table width="100%" cellpadding="0" cellspacing="0" style="page-break-inside:avoid">
                                                        <tr><td height="23px" colspan="4">&nbsp;</td></tr>
                                                        <tr>
                                                            <td width="55px">&nbsp;</td>
                                                            
                                                            <td colspan="2">  
                                                                <table width="100%" cellpadding="0" cellspacing="0">
                                                                <tr><td width="30px">
                                                                <img src="{{ url('images/new-site/rip-dark.png') }}" width="25" height="25" style="display:inline-block;vertical-align: middle;"/>
                                                                </td>
                                                                <td>
                                                                <h3 data-gramm_editor="false" aria-controls="collapseTop" contenteditable="false" style="display:inline-block; margin:0; vertical-align:middle;font-size: 18px;font-weight: 400;color: #2c3542;">
                                                                    {{ $goal->goal }}
                                                                </h3>
                                                                </td>
                                                                </tr>
                                                                </table>
                                                            
                                                            </td>
                                                            <td width="50px">&nbsp;</td>
                                                        </tr>
                                                        <tr><td height="18px" colspan="4"></td></tr>
                                                        <tr>
                                                            <td width="55px">&nbsp;</td>
                                                            <td colspan="2">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tr>
                                                                    <td width="292px">
                                                                        <span style="color: #5e6775;cursor: initial;font-size: 18px;font-weight: 500;"> {{ count($goal->task) }} Tasks</span>
                                                                        <span style="color: #5e6775;cursor: initial;font-size: 10px;font-weight: 500; margin:0px 5px;">|</span>
                                                                        <span style="color: #5e6775;cursor: initial;font-size: 18px;font-weight: 500;">{{ count($goal->comment) }} Comments</span>
                                                                    </td>
                                                                    <td width="292px" align="center">
                                                                        <span style="color: #474f5a;cursor: initial;font-size: 18px;">Due Date: </span>
                                                                        <span style="color: #474f5a;cursor: initial;font-size: 18px;">{{ date('M d, Y', strtotime($goal->due_date)) }}</span>
                                                                    </td>
                                            <td width="292px" align="center">  <?php $count = count(array_filter(explode(',', $goal->assign_to))) ?>
                                                    @if(isset($goalUsers))
                                                    @if($count > 0 && count($goalUsers)> 0)

                                                        @foreach($goalUsers as $goaluser)
                                                            @if(in_array($goaluser->user->id, explode(',', $goal->assign_to)))
                                                            <?php 
                                                                $userName = explode(' ',$goaluser->user->name);
                                                                $newUser_name = $userName[0];
                                                                if(isset($userName[1])){
                                                                    $newUser_name.= ' '.substr($userName[1], 0, 1).'.';
                                                                }                                     
                                                            ?>   
                                                                <img data-toggle="tooltip" @if($goaluser->user->image != '' && $goaluser->user->image != 'default.jpg')
                                                                            src="{{ $goaluser->user->userImage() }}"
                                                                            @else
                                                                            src=" "
                                                                        @endif  
                                                                        title="{{ $goaluser->user->name }}" width="26" height="26" style="border-radius:100%; vertical-align: middle;">
                                                                <span style="color: #6c6c6c; margin-left: 4px; cursor: initial;font-size: 18px;vertical-align: middle;">@if($newUser_name != ''){{ $newUser_name }} @else &nbsp; @endif</span>
                                                            @endif
                                                        @endforeach 
                                                    @endif
                                                    @endif
                                        
                                            </td>
                                            <td width="292px" align="right">
                                                        <table width="110" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td style="font-size: 18px;color: #2c3542;font-weight: 400;vertical-align: top;">
                                                                    Progress
                                                                </td>
                                                                <td style="font-size: 18px;color: #2c3542;font-weight: 400;vertical-align: top;" align="right">
                                                                    {{ round($goal->percentage_complete)}}%
                                                                </td>
                                                            </tr>
                                                        </table>
                                                            </td>
                                                        </tr>
                                                        </table>
                                                    </td>
                                                    <td width="50px">&nbsp;</td>
                                                </tr>
                                                <tr><td height="22px" colspan="4">&nbsp;</td></tr>
                                            </table>
                                            </td></tr>
                                        </table>    
                                        @endforeach      
                                    @endif    
                                
                                @endforeach  
                            @endif  
                       <table>
                    <tr><td height="22px"></td></tr>
                </table>    
            @endforeach                             
        @else
           <!--  <table width="100%" cellpadding="0" cellspacing="0"><tr><td height="22px">No Data.</td></tr></table> -->
        @endif

            
        <!-- /.content -->
    <!-- /.content-wrapper -->
<!-- ./wrapper -->

<!-- jQuery 3 -->

</body>

</html>