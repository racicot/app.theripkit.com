    <div class="modal-dialog user-modal-dialog add-user-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="d-flex align-items-center justify-content-between">
                    <h4 class="modal-title" id="myModalLabel">@if($count== '0')Add Users(s) @else Update Users(s) @endif</h4>
                    <span class="selected-user-items">{{ $count }} Selected</span>
                    <input type="hidden" name="old_value" value="{{ $count }}" id="old_value">
                    <input type="hidden" name="old_users" value="{{ implode(', ',$selectedUsers) }}" id="old_users">
                </div>
            </div>
            <div class="modal-body">
                    <div class="add-user">
                        <div class="add-user-search">
                            <div class="search-icon">
                                <svg width="25" height="25" viewBox="0">
                                    <path fill="#2C3542" fill-rule="nonzero" stroke="#2C3542" stroke-width=".4" d="M19.876 19.286l-3.402-3.402a5.945 5.945 0 0 0 1.465-3.915A5.976 5.976 0 0 0 11.969 6 5.976 5.976 0 0 0 6 11.97c0 3.29 2.679 5.969 5.97 5.969a5.945 5.945 0 0 0 3.914-1.465l3.402 3.402c.08.08.19.124.295.124a.42.42 0 0 0 .295-.714zM6.835 11.969a5.137 5.137 0 0 1 5.131-5.13 5.137 5.137 0 0 1 5.132 5.13 5.14 5.14 0 0 1-5.132 5.135 5.14 5.14 0 0 1-5.131-5.135z"></path>
                                </svg>
                            </div>
                            <input type="text" id="search_user_in_rip" placeholder="Search user...">
                        </div>
                        <form id="userList" method="post" autocomplete="off">

                            <div class="form-group" data-mcs-theme="dark">
                                <!-- repeat items -->
                                
                                @foreach($users as $key=>$user)
                                <div class="user-lists addedCompanyUserSelect_{{ $company_id }} @if(count($users) == $key+1) last @endif">
                                    <label class="material-checkbox">
                                        <input type="checkbox" name="addUserToCompany" value="{{ $user->id }}" 
                                            @if(in_array( $user->id ,$selectedUsers))
                                                checked
                                            @endif   
                                            @if($user->id == Auth::user()->id)
                                                disabled
                                            @endif >
                                        <span class="checkbox-items">
                                            <div class="user-pics">
                                                <img class="responsive-img" @if($user->image != '' && $user->image != 'default.jpg')
                                                src="{{ $user->userImage() }}" src="{{ $user->userImage() }}" @endif
                                                    title="{{ $user->name }}">
                                            </div>
                                            <div class="user-data">
                                                <div class="user-title">{{ $user->name}}</div>
                                                <div class="user-text">{{ $user->email }}</div>
                                            </div>
                                        </span>
                                    </label>
                                </div>
                                @endforeach
                            <input type="hidden" value="{{ $company_id }}" name="company_id">

                        </div><!-- /.add-user -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                            <button type="button" class="btn btn-theme pull-right invite-company" disabled>@if($count== 0)Add @else Update @endif</button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
    </div>