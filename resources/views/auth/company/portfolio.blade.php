<?php use App\Http\Controllers\RipController;
use App\Model\Comment;
?>
@extends('layouts.auth.master')
@section('title')
    Snapshot
@endsection
@section('content')
    <!-- Content Header (Page header) -->

<section class="content">
<section class="content-header--lg">
    <div class="snap-header portfolio-page">
        <div class="company-info">
            <span class="com-logo"></span>
        <h1>{{ session('company_name') }}</h1>
        </div>
            <a href="{{ url('portfolio/download') }}" class="btn btn-theme btn-uppercase pull-right download-portfolio1">Download</a>
    </div>
</section>
<div class="company-portfolio">
<div class="bg-wrapper no-bg">
  <div class="data-wrapper first">
    <div class="data-list">
      <div class="content-box">
        <i class="icon ci-1"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">1. What</strong>
          <p class="contentSave" id="what_{{ session('company_id') }}"
                 ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                 ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                 data-field="what"
                 @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                 @if($portfolio)
                  <?php echo strip_tags($portfolio->what,"<br>"); ?> 
                @endif
            </p>
        </div>
      </div>
    </div>

    <div class="data-banner">
      <span class="round-top"></span>
      <span class="round-top-right"></span>
      <span class="round-bottom"></span>
      <span class="round-bottom-right"></span>
      <i class="icon hand-shake">hand shake</i>
      <strong class="title">Guiding Principles</strong>
    </div>

    <div class="data-list align-right">
      <div class="content-box">
        <i class="icon ci-2"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">2. Why</strong>
          <p class="contentSave" id="why_{{ session('company_id') }}"
             ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             data-field="why"
             @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
             @if($portfolio)
              <?php echo strip_tags($portfolio->why,"<br>"); ?> 
             @endif
          </p>
        </div>
      </div>
    </div>

    <div class="data-list bottom-left">
      <div class="content-box">
        <i class="icon ci-3"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">3. How</strong>
  
          <p class="contentSave" id="how_{{ session('company_id') }}"
               ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
               ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
               data-field="how"
               @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
               @if($portfolio)
                <?php echo strip_tags($portfolio->how,"<br>"); ?> 
               @endif
          </p>
        </div>
      </div>
    </div>

    <div class="data-list align-right bottom-right">
      <div class="content-box">
        <i class="icon ci-4"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">4. Strategic Differentiators</strong>
    
            <p class="contentSave" id="strategic_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="strategic"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                <?php echo strip_tags($portfolio->strategic,"<br>"); ?>               
                @endif
            </p>
        </div>
      </div>
    </div>
  </div>
  <div class="data-wrapper bottom">
    <span class="left-corner"></span> <span class="right-corner"></span>
    <span class="line bottom"></span>
    <div class="data-list">
      <div class="content-box">
        <i class="icon ci-5"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">5. Elevator Pitch</strong>
        
          <p class="contentSave" id="elevator_{{ session('company_id') }}"
             ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             data-field="elevator"
             @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
             @if($portfolio)

            <?php echo strip_tags($portfolio->elevator,"<br>"); ?>
            @endif
            </p>
        </div>
      </div>
    </div>
    <div class="data-list center">
      <div class="content-box">
        <i class="icon ci-6"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">6. Financial Goal</strong>
    
          <p class="contentSave" id="financial_{{ session('company_id') }}"
                 ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                 ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                 data-field="financial"
                 @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                 @if($portfolio)
                <?php echo strip_tags($portfolio->financial,"<br>"); ?>
                @endif
            </p>
        </div>
      </div>
    </div>
    <div class="data-list right">
      <div class="content-box">
        <i class="icon ci-7"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">7. Top Behavioural Values</strong>
          <p class="contentSave" id="behavioural_{{ session('company_id') }}"
                     ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                     ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                     data-field="behavioural"
                     @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                     @if($portfolio)
                    <?php echo strip_tags($portfolio->behavioural,"<br>");?>
                    @endif
            </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="data-wrapper middle-section">
  <div class="data-list wide">
    <div class="content-box">
      <i class="icon bp-1"></i> <span class="line">line</span>
      <div class="text-box">
        <strong class="title">1. Emotional </strong>
        <p class="contentSave" id="emotional_{{ session('company_id') }}"
             ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             data-field="emotional"
             @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
             @if($portfolio)
              <?php echo strip_tags($portfolio->emotional,"<br>"); ?>           
            @endif
        </p>
      </div>
    </div>
  </div>
  <div class="data-list align-left">
    <div class="content-box">
      <i class="icon bp-2"></i> <span class="line">line</span>
      <div class="text-box">
        <strong class="title">2. Economic</strong>
        <p class="contentSave" id="economic_{{ session('company_id') }}"
             ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             data-field="economic"
             @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
             @if($portfolio)
              <?php echo strip_tags($portfolio->economic,"<br>"); ?>   
            @endif
        </p>
      </div>
    </div>
  </div>

  <div class="data-banner">
    <span class="round-top"></span> <span class="round-top-right"></span>
    <span class="round-bottom"></span>
    <span class="round-bottom-right"></span>
    <i class="icon hand-shake">hand shake</i>
    <strong class="title">BRAND PROMISE</strong>
  </div>

  <div class="data-list align-right">
    <div class="content-box">
      <i class="icon bp-3"></i> <span class="line">line</span>
      <div class="text-box">
        <strong class="title"> 3. Functional</strong>
        <p class="contentSave" id="functional_{{ session('company_id') }}"
             ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
             data-field="functional"
             @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
             @if($portfolio)
             <?php echo strip_tags($portfolio->functional,"<br>"); ?>  
            @endif
        </p>
      </div>
    </div>
  </div>
</div>
<div class="bg-wrapper">
  <div class="data-wrapper bottom-section">
    <div class="data-list">
      <div class="content-box">
        <i class="icon csp-1"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">1. Customer Problem</strong>
            <p class="contentSave" id="customer_problem1_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="customer_problem"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->customer_problem,"<br>"); ?> 
                @endif
            </p>
            
        </div>
      </div>
    </div>

    <div class="data-banner">
      <span class="round-top"></span>
      <span class="round-top-right"></span>
      <span class="round-bottom"></span>
      <span class="round-bottom-right"></span>
      <i class="icon cus-sol">Customer Problems &amp; Solutions</i>
      <strong class="title">Customer Problems &amp; Solutions</strong>
    </div>

    <div class="data-list align-right">
      <div class="content-box">
        <i class="icon csp-2"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">2. Customer Solution</strong>
            <p class="contentSave" id="customer_solution_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="customer_solution"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->customer_solution,"<br>"); ?> 
                @endif
            </p> 
        </div>
      </div>
    </div>

    <div class="data-list center">
      <div class="content-box">
        <i class="icon csp-3"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">3. Key Customer Segments</strong>
            <p class="contentSave" id="key_customer_segments_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="key_customer_segments"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                   <?php echo strip_tags($portfolio->key_customer_segments,"<br>"); ?>
                @endif
            </p>
            
        </div>
      </div>
    </div>
    <div class="data-list center align-right">
      <div class="content-box">
        <i class="icon csp-4"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">4. Revenue Streams</strong>
   
            <p class="contentSave" id="revenue_stream_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="revenue_stream"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->revenue_stream,"<br>"); ?>
                @endif
            </p>
        </div>
      </div>
    </div>
    <div class="data-list bottom-left">
      <div class="content-box">
        <i class="icon csp-5"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">5. Sales Channels</strong>
 
          <p class="contentSave" id="sales_channel_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="sales_channel"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->sales_channel,"<br>"); ?>
                @endif
            </p> 
              
        </div>
      </div>
    </div>

    <div class="data-list align-right bottom-right">
      <div class="content-box">
        <i class="icon csp-6"></i> <span class="line">line</span>
        <div class="text-box">
          <strong class="title">6. Value Propositions</strong>
           <p class="contentSave" id="value_proposition_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="value_proposition"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                   <?php echo strip_tags($portfolio->value_proposition,"<br>"); ?>
                @endif
          </p>
          
        </div>
      </div>
    </div>
  </div>
  <div class="data-wrapper bottom">
    <span class="left-corner"></span> <span class="right-corner"></span>
    <span class="line bottom"></span>
    <div class="data-list">
      <div class="content-box">
        <i class="icon csp-7"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">7. Key Activities &amp; Resources</strong>
     
            <p class="contentSave" id="key_activities_resource_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="key_activities_resource"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->key_activities_resource,"<br>"); ?>
                @endif
            </p>
        </div>
      </div>
    </div>
    <div class="data-list center">
      <div class="content-box">
        <i class="icon csp-8"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">8. Key Partners</strong>
           <p class="contentSave" id="key_partner_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="key_partner"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->key_partner,"<br>"); ?>
                @endif
          </p> 
        </div>
      </div>
    </div>
    <div class="data-list right">
      <div class="content-box">
        <i class="icon csp-9"></i> <span class="line bottom">line</span>
        <div class="text-box">
          <strong class="title">9. Key Costs</strong>
            <p class="contentSave" id="key_cost_{{ session('company_id') }}"
                ondragenter="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                ondragover="event.preventDefault(); event.dataTransfer.dropEffect = 'none'"
                data-field="key_cost"
                @if(auth()->user()->can('Update Portfolio Content')) contenteditable="true" @endif>
                @if($portfolio)
                  <?php echo strip_tags($portfolio->key_cost,"<br>"); ?>
                @endif
            </p>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="company-rip">
    <h3 class="title">RIP Period(s)</h3>
    @if($rips->isEmpty())
        <div class="no-rip">
            <img src="../images/new-site/empty_icon.svg" alt="">
            <h3>No active RIP period(s)</h3>
            <p>Click on the RIP period section link from the sidebar to create a new Period.</p>
        </div>
    @else    
      @foreach($rips as $rip)
        <div class="content-header content-header--lg rip-header-wrap editRip_{{ $rip->id }}">
          <div style="margin-right: auto;">
              <h1 class="rip-heading" id="rip_content">{{ $rip->name }}</h1>
              <div class="rip-date-header">
                   <span id="date-header">{{ date('M d, Y', strtotime($rip->start_date)).' - '.date('M d, Y', strtotime($rip->end_date)) }}</span>
                   <span id="day-left" @if(\Carbon\Carbon::now()->format('Y-m-d') > $rip->end_date)class="red-bg-button" @endif> {{ with(new RipController())->datePeriod($rip) }} </span>


              </div>              
          </div>
          <div class="rip-period-progress">
           <div class="progress-radial progress-radial--md progress-{{ $rip->getRipPercentage() }}">
                    <div class="overlay ripPer_{{ $rip->id }}">
                    <span>{{ $rip->getRipPercentage() }}</span>
                    <span>%</span>
                    </div>
                  </div>
          </div>
        </div>
        @if(count($rip->category) > 0)
          @foreach($rip->category as $category) 
            <div class="rip-tabs panel">
              <div class="n-panel-heading" role="tab" id="headingOne">
                  <div class="n-rip-card">
                      <div class="n-rip-card-left flex-column align-items-start flex-2 n-rip-card-flex">
                          <div id="accordionDiva" class="n-rip-card-in-left rip-panel-heading-wrap">
                            <div class="rip-card-tl">
                                <span class="error-msg cust-msg"> </span>
                                <h3 class="n-rip-card-heading categoryName categoryName_{{ $category->id }}" category_id="{{ $category->id }}" data-gramm_editor="false" aria-controls="collapseTop" contenteditable="false">
                                    {{ $category->name }}</h3>
                                  <p class="rip-card-subhead goalCount_1">{{ count($category->goals) }}
                                     RIP(s)</p>
                             </div>
                          </div>
                        </div>
                        <div class="n-rip-card-right">
                           <div class="rip-period-progress">                      
                              <div class="progress-group">
                                  <span class="progress-text">Progress</span>
                                  <span class="progress-number categoryAvgPer_1">{{ $category->getAvgPer($rip->id) }}%</span>

                                   <div class="progress sm">
                                   <div class="progress-bar progress-bar-aqua categoryAvgWidth_1"
                                                style="width: {{ $category->getAvgPer($rip->id) }}%"></div>
                                    </div>
                               </div>
                            </div>            
                          </div><!-- /.avatar-wrap -->
                      </div><!-- /.rip-card-right -->
              </div>
              @if (count($category->goals) >0)
                  <div class="rip-divider">RIP(s)</div>
                  @foreach($category->goals as $goal)
                  <div class="n-panel-body-top n-rip-card">
                      <div class="n-rip-card-left wide">
                        <div class="rip-card-in-left">
                          <div class="heading-row">
                              <!-- Button trigger modal -->
                                <div class="comment-icons">
                                    <svg width="25" height="25" viewBox="0 0 25 25" class="rip-svg">
                                    <g fill="none" fill-rule="nonzero" transform="translate(3 3)">
                                        <rect width="19" height="19" fill="#2C3542" opacity=".5" rx="2"></rect>
                                        <g fill="#FFF" transform="translate(3 6)">
                                            <rect width="9" height="1" x="3" rx=".5"></rect>
                                            <rect width="9" height="1" x="3" y="3" rx=".5"></rect>
                                            <rect width="9" height="1" x="3" y="6" rx=".5"></rect>
                                            <rect width="1" height="1" rx=".5"></rect>
                                            <rect width="1" height="1" y="3" rx=".5"></rect>
                                            <rect width="1" height="1" y="6" rx=".5"></rect>
                                        </g>
                                    </g>
                                    </svg>
                                </div>
                                <h3 class="card-list__heading font-13 goalName goalName_{{ $goal->id }}" goal_id="{{ $goal->id }}" id="goalName_{{ $goal->id }}" value="test rip">{{ $goal->goal }}</h3>
                            </div>
                            <div class="data-row">
                                <!-- <span class="error-msg cust-msg"></span> -->
                                <div class="rip-card-in-right rip-comments-desc">
                                    <p class="rip-card-subhead accordion-header taskCount_1">

                                    <span> {{ count($goal->task) }} Tasks
                                    </span>
                                    </p>
                                    <p class="margin-0">|</p>
                                    <p class="rip-card-subhead show-goal-comments accordion-header commentCount_1" goalid="1">
                                        <span>
                                            {{ count($goal->comment) }} Comments
                                        </span>
                                    </p>
                                </div>
                                <div class="n-rip-card-right goal-right due-date-wrap">
                                 <div class="rip-card-in-left">
                                      <div class="form-group due-date-container">
                                          <span class="due-date">Due Date: </span>
                                          <span>{{ date('M d, Y', strtotime($goal->due_date)) }}</span>

                                      </div>
                              </div>

                             <div class="d-flex n-avtar-outer">              
                                  <div class="n-avatar-wrap">                
                                      <div class="goalUserList goal-avatar-wrap goalUserTabList_{{ $goal->assign_to }}" goalid="{{ $goal->id }}">  
                                        <?php $count = count(array_filter(explode(',', $goal->assign_to))) ?>
                                        @if(isset($goalUsers))
                                        @if($count > 0 && count($goalUsers)> 0)

                                            @foreach($goalUsers as $goaluser)
                                                @if(in_array($goaluser->user->id, explode(',', $goal->assign_to)))
                                                  <?php 
                                                      $userName = explode(' ',$goaluser->user->name);
                                                      $newUser_name = $userName[0];
                                                      if(isset($userName[1])){
                                                         $newUser_name.= ' '.substr($userName[1], 0, 1).'.';
                                                      }                                     
                                                  ?>
                                                    <div class="d-flex card-list-avatar">
                                                        <img data-toggle="tooltip"
                                                        @if($goaluser->user->image != '' && $goaluser->user->image != 'default.jpg')
                                                            src="{{ $goaluser->user->userImage() }}"
                                                          @endif  
                                                           title="{{ $goaluser->user->name }}">
                                                    </div>
                                                    <span class="username">{{ $newUser_name }}</span>
                                                @endif
                                            @endforeach 
                                        @endif
                                        @endif
                                      </div>
                                  </div><!-- /.avatar-wrap -->                
                              </div>
                              <div class="d-flex goal-action">
                                <div class="v-bottom">
                                  <div class="rip-period-progress">                            
                                      <div class="progress-group">
                                          <span class="progress-text">Progress</span>
                                          <span class="progress-number innerGoalAvgPer_{{ $goal->id }}">{{ round($goal->percentage_complete)}}% </span>

                                          <div class="progress sm">

                                            <div class="progress-bar progress-bar-aqua innerGoalAvgWidth_{{ $goal->id }}"
                                                          style="width: {{ round($goal->percentage_complete)}}%"></div>
                                          </div>
                                      </div>
                                  </div>                   
                                </div>
                              </div>
                                            
                              </div>
                            </div><!-- /.rip-card-right -->
                          </div>
                        </div>     
                    </div>
                @endforeach
              @endif  
            </div>
          @endforeach
        @endif    
      @endforeach  
    @endif    
  
    <!--END RIP-->

</div>
</section>

@endsection