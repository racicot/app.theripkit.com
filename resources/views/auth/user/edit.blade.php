<div class="modal-dialog modal-edit-company wide" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit User</h4>
        </div>
        <div class="modal-body">


            <form id="editUser" method="POST" action="{{ url('user/'.$user->id) }}" enctype="multipart/form-data" novalidate>
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="form-group upload-flex">
                    <!-- <span class="b-label">Profile</span>-->
                    <div class="photo-in">
                        <label for="profile-picture" class="after-image">
                            <img src="{{ $user->userImage() }}" id="profileImage" alt="User"></label>
                        <div class="wrap-file show">
                            <input type="file" name="image" id="profile-picture" value="{{ $user->image }}">
                        </div>
                    </div><!-- photo-in -->
                    

                    <div class="b-label">
                                <div class="upload-comapny-logo">Upload profile picture</div> 
                                <div class="upload-format">(You can upload jpeg, gif<br>
                                    and png under 1 mb)</div>
                    </div>
                    
                    <span class="profile-upload-status"></span>
                </div><!-- /.form-group -->
                
                <div class="inline-row">
                <div class="form-group">
                    <label for="inputName">Name</label>
                    <input id="name" type="text" class="form-control" name="name" id="inputName" required
                           placeholder="Name" value="{{ $user->name }}">
                    <span class="error-msg" id="name-err"></span>
                </div>


                <div class="form-group">
                    <label for="inputEmail">Email</label>
                    <input id="email" type="text" class="form-control" name="email" placeholder="email"
                           value="{{ $user->email }}" required>
                    <span class="error-msg" id="email-err"></span>

                </div>
                </div>
                <div class="inline-row">
                @if(auth()->user()->can('Assign Companies To User From User Section'))
                    <input type="hidden" name="show_company" id="show_company" value="Y">
                    <div class="form-group user-company-dropdown">
                        <label>Companies</label>
                            <select class="form-control common-chosen selectCompany" name="assigned_companies[]" id="selectCompany" required data-placeholder="Company" multiple>
                                @foreach($companies as $company)
                                    <option value="{{ $company->id }}" @if(in_array($company->id, $companyIds)) selected @endif>
                                        {{ $company->name }}
                                    </option>
                                @endforeach
                            </select>
                            <span class="error-msg" id="company-err"></span>
                        
                      
                    </div>
                @else
                 <input type="hidden" name="show_company" id="show_company" value="N">   
                @endif
          
                <div class="form-group">
                        <label>Role</label>
                        <select class="form-control" name="role" id="userSelect"
                                @if($user->roles->isNotEmpty()) @endif>
                            <option></option>
                            @foreach($roles as $role)
                                <option value="{{ $role->name }}"
                                        @if($role->name == $user->roles->pluck('name')->first())
                                        selected
                                        @endif>{{ $role->name }}</option>
                            @endforeach
                        </select>
                        <span class="error-msg" id="role-err"></span>
                    </div>
                </div>
                <div class="inline-row wide highlight-text">
                    <div class="form-group">
                        <span class="highlight">
                            <b>Note: </b>User asssigned as owner to a company can not be un-assigned from this section
                        </span>
                    </div>
                </div>
                <div class="inline-row wide">
                @if(auth()->user()->can('Update Permissions For A Role'))
                    <div class="form-group">
                        <label>Permissions</label>
                        <p>
                            <button aria-controls="collapsePermission" aria-expanded="false"
                                    class="btn btn-default btn-block text-left collapsed" data-target="#collapsePermission"
                                    data-toggle="collapse" type="button"> Permissions <i class="fa fa-caret-down"
                                                                                         aria-hidden="true"></i>
                            </button>
                        </p>
                        <div class="collapse" id="collapsePermission">
                            <div class="well">
                                <div class="row">
                                    @foreach($groups as $group_name)
                                        <div class="col-sm-12">
                                            <div class="label checkbox">
                                                <label><input type="checkbox" class="select-all">{{ $group_name }}
                                                </label>
                                            </div>
                                            <div id="checkboxList">
                                                <div class="row">
                                                    @foreach($permissions as $permission)
                                                        @if($permission->group_name == $group_name)
                                                            <div class="checkbox col-sm-4">
                                                                <label>
                                                                    <input type="checkbox" name="permission[]"
                                                                           value="{{ $permission->name }}"
                                                                           @if(in_array($permission->name, $userPermissions)) checked
                                                                           @endif @if(in_array($permission->name, $rolePermissions)) disabled @endif> {{ $permission->name }}
                                                                </label>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endif
                </div>
                
                <div class="radiobox form-group">
                <span class="title">Do you want the user to be</span>
                    <label class="first">
                        <input type="radio" value="1" name="status" @if($user->status == 1)
                        checked
                                @endif
                        > <span>Active</span>
                    </label>

                     <label>
                        <input type="radio" value="0" name="status" @if($user->status == 0)
                        checked
                                @endif
                        > <span>Inactive</span>
                    </label>
                </div>

                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-theme pull-right">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->


