@extends('layouts.auth.master')

@section('title')
    CSV Upload
@endsection

@section('content')
    <!-- <section class="content-header content-header--lg">
        <h1>
            User Mass upload
        </h1>
    </section> -->

    <section class="content">
        <!-- Tab panes -->
        <div class="tab-content">
       
         
        <div class="user-listing">
            
            <div class="box csv-upload">
            <div class="box-header mt-30 mb-20">
                <h1 class="main-title">CSV Upload</h1>
                <button type="button" class="btn btn-theme btn-lg btn-uppercase pull-right" data-toggle="modal"
                        id="csv_upload_button" data-target="#csvupload">Upload CSV</button>
            </div>
                <div class="error"></div>
               
<!--                <div class="box-header mt-30 mb-20">
                    <h1 class="main-title">CSV Upload</h1>
                    <button type="submit" class="btn btn-theme btn-lg btn-uppercase pull-right">
                        Upload CSV
                    </button>
                </div>-->
                <!-- /.box-header -->
                <div class="box-body table-middle">
                    <table id="csv-table" class="table table-hover new-table-design dataTable no-footer">
                        <thead>
                        <tr>
                            <th>File name</th>
                            <th>Size</th>
                            <th>Uploaded by</th>
                            <th>Date of upload</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
        </div>
    </section>



@endsection


    
        