<div class="modal-dialog modal-edit-company" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                        aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Edit Permission</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" method="POST" action="{{ url('assignPermissionData').'/'.$user->id }}">
                {{ csrf_field() }}

                <div class="form-group">

                    <label class="col-md-4 control-label">Role</label>
                    <div class="col-md-6">
                        <input type="text" name="role" value="{{ $user->roles->pluck('name')->first() }}" disabled/>
                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-4 control-label">Add Permission</label>

                    <div class="col-md-6">

                        @foreach($permissions as $permission)

                            <input type="checkbox" name="permission[]" value="{{ $permission->name }}"
                                   @if(in_array($permission->name, $userPermissions))
                                   checked
                                    @endif
                            >{{ $permission->name }}
                            <br>

                            @endforeach
                            </select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Submit
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
    $('#userSelect').chosen();
</script>
