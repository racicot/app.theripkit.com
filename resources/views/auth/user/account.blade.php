@extends('layouts.auth.master')

@section('title')
    Profile
@endsection

@section('content')
    <section class="content-header content-header--lg">
        <h1>
            Account
        </h1>
    </section>

    <section class="content">
        <!-- Nav tabs -->

<div class="panel-group profile @if(!$errors->has('oldPassword') && !$errors->has('newPassword') &&
    !$errors->has('confirmPassword') && !$errors->has('motto') && !$errors->has('bg_image')) opened @endif" id="accordion">

  <div class="panel panel-default">
  <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse1" @if(!$errors->has('oldPassword') && !$errors->has('newPassword') &&
            !$errors->has('confirmPassword') && !$errors->has('motto') && !$errors->has('bg_image')) aria-expanded="true" @endif>
        Profile</a>
        <span class="sub-text">Set your information about yourself.</span>
      </h4>
    </div>

    <div id="collapse1" class="panel-collapse collapse @if(!$errors->has('oldPassword') && !$errors->has('newPassword') &&
        !$errors->has('confirmPassword') && !$errors->has('motto') && !$errors->has('bg_image')) in @endif">
       <div role="tabpanel" class="tab-pane" id="profile">
                <form id="user_profile" method="POST" action="{{ url('profile/edit') }}" enctype="multipart/form-data" autocomplete="off">
                    {{ csrf_field() }}
                    <div class="account-container">
                        <div class="form-row">
                            <div class="img-field">
                                <div class="photo-in photo-big">
                                    <label for="profile-picture">
                                        <img src="{{ $user->userImage() }}" id="profileImage" alt="User"></label>
                                    <div class="wrap-file">
                                        <input type="file" name="image" value="{{ $user->image }}" id="profile-picture">
                                        <i aria-hidden="true" class="fa fa-camera"></i></div>
                                </div><!-- photo-in -->
                                <div class="img-info">
                                        <div class="upload-comapny-logo">Upload profile picture</div> 
                                        <div class="upload-format">(You can upload jpeg, gif
                                            and png under 1 mb)</div>
                                </div>

                                <span class="profile-upload-status"></span>
                          
                            </div>
                            <div class="data-fields">
                                <div class="block-body">
                                <div class="inline-row">
                                    <div class="form-group @if($errors->has('name')) has-error @endif">
                                        <label for="inputName">Name</label>
                                        @if($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                        @endif
                                        <input type="text" class="form-control" id="inputName" name="name"
                                               value="{{ $user->name }}" placeholder="Name">
                                    </div>

                                    <div class="form-group @if($errors->has('email')) has-error @endif">
                                        <label for="inputEmail1">Email</label>
                                        @if($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                        <input type="email" class="form-control" id="inputEmail1" name="email"
                                               value="{{ $user->email }}" placeholder="Email Address">
                                    </div>
                                </div>
                                <!-- Country code dropdown added -->
                                    <div class="form-group @if($errors->has('phone')) has-error @endif">
                                        <label for="phone">Phone</label>
                                        @if($errors->has('phone'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                        @endif
                                        
                                        <div class="input-group account-input-group">
                                            <div class="input-group-btn">
                                              <select id="country_code" data-placeholder="Select Country" name="country_code" class="form-control country common-chosen">
                                                                                                
                                                <?php  if(isset($countries)){ ?>
                                                    @foreach($countries as $country)
                                                        @if(($user->isd_code == $country['isd_code']))
                                                            <option value="+{{ $country['isd_code'] }}" @if((!empty($user)) && ($user->isd_code == $country['isd_code'])) selected @endif> 
                                                               {{ $country['name'] }} </option>
                                                        @else
                                                            <option value="+{{ $country['isd_code'] }}" > 
                                                                {{ $country['name'] }}</option>
                                                        @endif
                                                    @endforeach
                                                <?php  } ?>
                                            </select>  

                                            </div><!-- /btn-group -->
  
                                            <div class="input-group-btn">
                                            <input type="text" class="form-control" id="phone" name="phone"
                                               value="{{ $user->phone }}" class="phone" placeholder="Phone" 
                                               minlength="8" maxlength="10">
                                            </div>
                                            <input class="form-control" id="isd_code" name="isd_code" val="{{ $user->isd_code }}" type="hidden">
                                        </div><!-- /input-group -->
                                        <span class="help-block" id="phone-err"></span>
                                    </div>
                                </div><!-- /.block-body -->

                           
                            </div>
                        </div>
                        <div class="block-footer">
                                    <button type="submit" id="userSubmitButton" formtarget="" class="btn btn-theme btn-block">
                                    Update Profile
                                    </button>
                                </div>
                    </div>
                </form>
            </div>
    </div>
  </div>
  <div class="panel panel-default @if($errors->has('oldPassword') || $errors->has('newPassword') ||
    $errors->has('confirmPassword')) opened @endif">
  <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse2" @if($errors->has('oldPassword') || $errors->has('newPassword') ||
            $errors->has('confirmPassword')) aria-expanded="true" @endif>
        Change Password</a>
        <span class="sub-text">Update your secret combination of letters and numbers</span>
      </h4>
    </div>
    <div id="collapse2" class="panel-collapse collapse @if($errors->has('oldPassword') || $errors->has('newPassword') ||
        $errors->has('confirmPassword')) in @endif">
    <div role="tabpanel" class="tab-pane " id="password">
                <form action="{{ url('/account/'.$user->id) }}" method="POST" class="change-password">
                    {{ csrf_field() }}
                    <div class="form-row account-container">
                        <div class="data-fields">
                        <div class="inline-row">
                        <div class="form-group @if($errors->has('oldPassword')) has-error @endif">
                            <label for="oldPassword">Current Password</label>
                            @if($errors->has('oldPassword'))
                                <span class="help-block">
                  <strong>{{ $errors->first('oldPassword') }}</strong>
                </span>
                            @endif
                            <input type="password" class="form-control" name="oldPassword" id="oldPassword"
                                   placeholder="*******">
                        </div>

                      
                 </div>
                 <div class="inline-row">
                 <div class="form-group @if($errors->has('newPassword')) has-error @endif">
                            <label for="newPassword">New Password</label>
                            @if($errors->has('newPassword'))
                                <span class="help-block">
                  <strong>{{ $errors->first('newPassword') }}</strong>
                </span>
                            @endif
                            <input type="password" class="form-control" name="newPassword" id="newPassword"
                                   placeholder="*******">
                        </div>
                        <div class="form-group @if($errors->has('confirmPassword')) has-error @endif">
                            <label for="confirmPassword">Confirm New Password</label>
                            @if($errors->has('confirmPassword'))
                                <span class="help-block">
                  <strong>{{ $errors->first('confirmPassword') }}</strong>
                </span>
                            @endif
                            <input type="password" class="form-control" name="confirmPassword" id="confirmPassword"
                                   placeholder="*******">
                        </div>

                    
                    </div>
                </div>
            
                </div>
                <div class="block-footer">
                            <button type="submit" id="changePassword" class="btn btn-theme btn-block ">
                            Change Password
                            </button>
                        </div>
                </form>

            </div>
    </div>
  </div>
@if(auth()->user()->hasRole('Super Admin'))
  <div class="panel panel-default @if($errors->has('motto') || $errors->has('bg_image')) opened @endif">
  <div class="panel-heading">
      <h4 class="panel-title">
        <a data-toggle="collapse" data-parent="#accordion" href="#collapse3" @if($errors->has('motto') || $errors->has('bg_image')) aria-expanded="true" @endif >
        Login Image &amp; Notification</a>
        <span class="sub-text">Update your login Image and Notification</span>
      </h4>
    </div>
    <div id="collapse3" class="panel-collapse collapse @if($errors->has('motto') || $errors->has('bg_image')) in @endif">
    @if(auth()->user()->can('Update Login Background and Quote'))
                <div role="tabpanel"
                     class="tab-pane" id="loginImage">
                    <form id="login_view" class="login-img" method="POST" action="{{ url('loginview') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <div class="account-container">
                            <div class="form-row">
                                <div class="img-field">
                                    <div class="photo-in photo-big">
                                        <label for="bg_image">
                                            <img src="{{ (new App\Model\LoginImage)->getImage() }}" id="bg-image"
                                                 alt="User"></label>
                                        <div class="wrap-file">
                                            <input type="file" name="bg_image"
                                                   value="{{ (new App\Model\LoginImage)->first()->name }}"
                                                   id="bg_image"> <i aria-hidden="true" class="fa fa-camera"></i>
                                        </div>
                                    </div><!-- photo-in -->
                                    <span class="img-info">
                                Upload login picture 
                                <em>(You can upload jpeg, gif and png under 4 mb)</em>
                                </span>
                                </div>
                                <div class="data-fields">
                                    <div class="block-body">
                                        <div class="form-group @if($errors->has('motto')) has-error @endif">
                                            <label for="motto">Notification</label>
                                            @if($errors->has('motto'))
                                                <span class="help-block">
                              <strong>{{ $errors->first('motto') }}</strong>
                            </span>
                                            @endif
                                            @if($errors->has('dimensions'))
                                                <span class="help-block">
                              <strong>{{ $errors->first('dimensions') }}</strong>
                            </span>
                                            @endif
                                        @if((new App\Model\LoginImage)->getMotto() != 'NULL' )   
                                            <textarea id="motto" name="motto" class="form-control" placeholder="Enter Your Motto Here"  maxlength="256">{{(new App\Model\LoginImage)->getMotto() }}</textarea>
                                        @else
                                            <textarea id="motto" name="motto" class="form-control" placeholder="Enter Your Motto Here"  maxlength="256"></textarea>
                                        @endif
                                        </div>
                                    </div><!-- /.block-body -->
                                  
                                </div>
                            </div>
                            <div class="block-footer">
                                        <button type="submit" formtarget="" class="btn btn-theme btn-block ">Update
                                        </button>
                                    </div>
                        </div>
                    </form>
                </div>
            @endif
    </div>
  </div>
@endif
</div>


    </section>
@endsection


<script>
    $(document).ready(function(){

        $('#accordionExample').collapse({
  toggle: false
})
    });
</script>