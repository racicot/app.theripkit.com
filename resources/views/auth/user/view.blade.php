@extends('layouts.auth.master')
@section('title')
    Users
@endsection
@section('content')

    <!-- Main content -->
    <section class="content">
        <div class="user-listing">
            <div class="box">
                <div class="box-header mt-30 mb-20">
                    <h1 class="main-title">Users</h1>
                    <button type="button" id="addUserButton"class="btn btn-theme btn-lg btn-uppercase pull-right" data-toggle="modal" data-target="#addNewUser">Add New User</button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-middle pt-0">
                    <table id="user-table" class="table table-hover new-table-design user-table">
                        <thead>
                        <tr>
                            <th>User Info</th>
                            <th>Role</th>
                            <th>Companies</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                    </table>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
    </section><!-- /.content -->
    <!-- /.content-wrapper -->
@endsection

