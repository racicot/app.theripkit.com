@extends('layouts.auth.master')

@section('title')
    Profile
@endsection

@section('content')
    <div class="container-sm">
        <section class="content-header">
            <h3>Profile</h3>
        </section>
        <section class="content">
            <form method="POST" action="{{ route('edit_profile') }}" enctype="multipart/form-data">
                {{ csrf_field() }}

                <div class="form-group">
                    <span class="b-label">Profile</span>
                    <div class="photo-in">
                        <label for="profile-picture">

                            <img src="{{ $user->userImage() }}" alt="User"></label>

                        <div class="wrap-file">
                            <input type="file" name="image" id="profile-picture" value="{{ $user->image }}"> <i
                                    aria-hidden="true" class="fa fa-camera"></i>
                        </div>
                    </div><!-- photo-in -->
                    @if ($errors->has('image'))
                        <span class="help-block">
            <strong>{{ $errors->first('image') }}</strong>
          </span>
                    @endif
                </div>
                <!-- /.form-group -->
                <div class="form-group">
                    <label for="inputName">Name</label>
                    @if ($errors->has('name'))
                        <span class="help-block">
            <strong>{{ $errors->first('name') }}</strong>
          </span>
                    @endif
                    <input type="text" class="form-control" name="name" id="inputName" placeholder="Name"
                           value="{{ $user->name }}" required>
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    @if ($errors->has('email'))
                        <span class="help-block">
            <strong>{{ $errors->first('email') }}</strong>
          </span>
                    @endif
                    <input id="email" type="text" class="form-control" name="email" placeholder="email"
                           value="{{ $user->email }}" disabled required>
                </div>

                <div class="form-group">
                    <label for="role">Role</label>
                    @if ($errors->has('role'))
                        <span class="help-block">
            <strong>{{ $errors->first('role') }}</strong>
          </span>
                    @endif
                    <input id="role" type="text" class="form-control" name="role" placeholder="role"
                           value="{{ $user->roles->pluck('name')->first() }}" disabled required>
                </div>
                @hasanyrole(['Admin|Super Admin'])
                @else
                    <div class="form-group">
                        <label for="company">Associated Companies</label>
                        @if ($errors->has('companies'))
                            <span class="help-block">
            <strong>{{ $errors->first('companies') }}</strong>
          </span>
                        @endif
                        @foreach ($companies as $company)
                            <input type="text" name="" class="form-control" value="{{$company->name}}" disabled
                                   required>
                        @endforeach
                    </div>
                    @endhasanyrole
                    <div class="form-group">
                        <label>Status</label>
                        <span class="col-md-offset-1 {{($user->status) ? 'bg-success' : 'bg-danger' }}">{{ ($user->status) ? 'Active' : 'Inactive' }}</span>
                    </div>

                    <div class="form-group">
                        <label for="doj">Date of joining</label>
                        <span class="col-md-offset-1">{{ date('d-m-Y',strtotime($user->created_at)) }}</span>
                    </div>

                    <div class="form-group">
                        <label>Last Online</label>
                        <p>
                            <date>Wednesday, Feburary 8, 2017 at 10:32 PM</date>
                        </p>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-theme">Save</button>
                        <button type="reset" class="btn btn-danger">Cancel</button>
                    </div>

            </form>
        </section>
    </div>
@endsection