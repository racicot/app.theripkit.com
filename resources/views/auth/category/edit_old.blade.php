<div class="modal-dialog modal-edit-company" role="document">

    <div class="modal-dialog modal-edit-company" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Category</h4>
            </div>
            <div class="modal-body">
                <form id="addCompany" method="post" enctype="multipart/form-data"
                      action="{{ url('categories').'/'.$category->id }}">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}

                    <div class="form-group">
                        <label for="inputName">Name</label>
                        @if ($errors->has('name'))
                            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
                        @endif
                        <input type="text" class="form-control" id="inputName" name="name"
                               value="{{ $category->name }}">
                    </div>

                    <div class="form-group">
                        <label>Goal Type</label>
                        <select class="form-control" name="goal_type_id">
                            <option></option>
                            <option @if(config('setting.goals.type.monthly') ==  $category->goal_type_id) selected
                                    @endif value="{{ config('setting.goals.type.monthly') }}">Monthly
                            </option>
                            <option @if(config('setting.goals.type.querterly') ==  $category->goal_type_id) selected
                                    @endif value="{{ config('setting.goals.type.querterly') }}">Quarterly
                            </option>
                            <option @if(config('setting.goals.type.yearly') ==  $category->goal_type_id) selected
                                    @endif value="{{ config('setting.goals.type.yearly') }}">Yearly
                            </option>
                            <option @if(config('setting.goals.type.longterm') ==  $category->goal_type_id) selected
                                    @endif value="{{ config('setting.goals.type.longterm') }}">Longterm
                            </option>

                        </select>
                    </div>
                    <div class="checkbox">
                        <label>
                            <input @if($category->status == 1) checked @endif type="checkbox" value="1" name="status">
                            Active
                        </label>
                    </div>
                    <div class="modal-footer row-between">
                        <div class="modal-footer-right">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                            <button type="submit" class="btn btn-theme">Save</button>
                        </div>
                    </div>
                </form>
                {{--<form method="post" id="deleteCategory" action="{{ url('categories').'/'.$category->id }}">--}}
                {{--{{ method_field('DELETE') }}--}}
                {{--{{ csrf_field() }}--}}
                {{--<button type="submit" class="modal-footer-right btn btn-danger">Delete</button>--}}
                {{----}}
                {{--</form> --}}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    <script>

        $("#deleteCategory").submit(function (event) {
            var x = confirm("Are you sure you want to delete?");
            if (x) {
                return true;
            }
            else {

                event.preventDefault();
                return false;
            }

        });

    </script>
</div><!-- /.modal-dialog -->
<script>
    $('#planSelect').chosen();
    $('#userSelect').chosen();
    $("#deleteCompany").submit(function (event) {
        var x = confirm("Are you sure you want to delete?");
        if (x) {
            return true;
        }
        else {

            event.preventDefault();
            return false;
        }

    });

</script>