<div class="modal-dialog modal-add-company" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit Category</h4>
        </div>
        <div class="modal-body">

            <form id="editCategory" method="POST" action="">
                {{ csrf_field() }}
                
                <input type="hidden" name="category_id" value="{{ $category->id }}">
                <div class="form-group">
                    <label for="name">Category Name</label>
                    <input type="text" class="form-control ajaxAddCategory" id="name" name="name"
                        placeholder="Category Name"  value="{{ $category->name }}" maxlength="256" autofocus>
                    <span class="error-msg" id="name-err"></span>
                </div>
                

                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="button" id="editCategoryButton" class="btn btn-theme pull-right">Update</button>
                    </div>
                </div>
            </form>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->