@extends('layouts.auth.master')

@section('title')
    Category
@endsection
@section('content')

    @if ($errors->has('name'))
        <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
    @endif
    @if ($errors->has('goal_type_id'))
        <span class="help-block text-danger">
                <strong>{{ $errors->first('goal_type_id') }}</strong>
              </span>
    @endif
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Categories
        </h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Categories</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="add-user">
            <div class="form-group">
                <button type="button" class="btn btn-theme btn-uppercase" data-toggle="modal" data-target="#addModal"><i
                            class="fa fa-plus" aria-hidden="true"></i> Add new Category
                </button>
            </div>
        </div>

        <div class="user-listing">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Category Data</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-middle">

                    <table class="table table-bordered" id="category-table">
                        <thead>
                        <tr>

                            <th>Name</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>

                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <div class="modal fade" tabindex="-1" role="dialog" id="addModal">
        <div class="modal-dialog modal-add-company" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Add Category</h4>
                </div>
                <div class="modal-body">
                    <form id="addCompany" method="post" enctype="multipart/form-data" action="{{ url('categories') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="inputName">Name</label>
                            @if ($errors->has('name'))
                                <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
              </span>
                            @endif
                            <input type="text" class="form-control" id="inputName" name="name" placeholder="Name">
                        </div>

                        <div class="form-group">
                            <label>Goal Type</label>
                            <select class="form-control" name="goal_type_id">
                                <option></option>
                                <option value="{{ config('setting.goals.type.monthly') }}">Monthly</option>
                                <option value="{{ config('setting.goals.type.quarterly') }}">Quarterly</option>
                                <option value="{{ config('setting.goals.type.yearly') }}">Yearly</option>
                                <option value="{{ config('setting.goals.type.longterm') }}">Longterm</option>

                            </select>
                        </div>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" value="1" name="status"> Active
                            </label>
                        </div>
                        <div class="modal-footer row-between">
                            <div class="modal-footer-right">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                <button type="submit" class="btn btn-theme">Save</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->


@endsection
