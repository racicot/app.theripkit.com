@extends('layouts.auth.master')

@section('title')
    Roles
@endsection
@section('content')

    <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
        <h1>
            Manage Roles
        </h1>
        <ol class="breadcrumb">
            <li class="active"><i class="fa fa-users"></i> Roles</li>
        </ol>

    </section> -->

    <!-- Main content -->
    <section class="content">
        @if ($errors->has('name'))
            <span class="help-block">
      <strong class="text-danger">{{ $errors->first('name') }}</strong>
    </span>
        @endif
        @if ($errors->has('permission'))
            <span class="help-block">
      <strong class="text-danger">{{ $errors->first('permission') }}</strong>
    </span>
        @endif
        @if(auth()->user()->can('Add New Role'))
            <!-- <div class="add-user">
                <div class="form-group">
                    <button type="button" class="btn btn-theme btn-uppercase" data-toggle="modal"
                            data-target="#addModal"><i class="fa fa-plus" aria-hidden="true"></i> Add new Role
                    </button>
                </div>
            </div> -->
        @endif

        <div class="user-listing">
            <div class="box">
                <div class="box-header mt-30 mb-20">
                    <h1 class="main-title">Manage Roles</h1>
                    <button type="button" id="addRoleModalButton" class="btn btn-theme btn-lg btn-uppercase pull-right" data-toggle="modal"
                            data-target="#addModal"> Add new Role
                    </button>
                </div>
                <!-- /.box-header -->
                <div class="box-body table-middle">

                    <table class="table table-hover new-table-design dataTable no-footer" id="roles-table">
                        <thead>
                        <tr>

                            <th>Role Name</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </section>
    <!-- /.content -->

    <!-- /.content-wrapper -->


@endsection
@section('modal')

    <div class="modal fade" tabindex="-1" role="dialog" id="editRoleModal">

    </div><!-- /.modal -->

    <div class="modal new-modal fade" tabindex="-1" role="dialog" id="addModal">
        <div class="modal-dialog modal-add-company" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Add Role</h4>
                </div>
                <div class="modal-body">

                    <form id="addRole" class="form-horizontal" method="POST" action="{{ url('roles') }}">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="name">Role Name</label>
                            <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}"
                                maxlength="26" autofocus>
                            <span class="help-block error-msg" id="name-err"></span>
                        </div>

                        <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" id="editCompanyButton" class="btn btn-theme pull-right">Save</button>
                    </div>
                </div>
                    </form>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
@endsection
