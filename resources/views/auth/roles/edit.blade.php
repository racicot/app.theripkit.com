<div class="modal-dialog modal-edit-company new-modal with-border" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title">Edit User Role</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" id="permissionsForm" method="POST" action="{{ url('roles').'/'.$role->id }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}

               <?php /* <div class="form-group">
                    <label>Permissions</label>
                </div> */ ?>
             
                <!--<div class="form-group">-->                    
                    <div class="form-group">
                        <div class="role-name">
                            <label for="name">Role Name</label>
                            <input class="form-control" id="role_name" name="role" value="{{ $role->name }}" type="text" disabled>
                            <span class="help-block error-msg mrt-18" id="name-err"></span>
                        </div>
                    </div>
                    <div class="form-group">
                        <div id="collapsePermission">
                            <div class="well">
                                <div class="row">
                                    @foreach($groups as $group_name)
                                        <div class="col-sm-12">
                                            <div class="label checkbox">
                                                <label><input type="checkbox" class="select-all">{{ $group_name }}</label>
                                            </div>
                                            <div id="checkboxList">
                                                <div class="row">
                                                    @foreach($permissions as $permission)
                                                        @if($permission->group_name == $group_name)
                                                            <div class="checkbox col-sm-4">
                                                                <label>
                                                                    <input type="checkbox" name="permission[]"
                                                                           value="{{ $permission->name }}"
                                                                           @if(in_array($permission->name, $rolePermissions)) checked
                                                                            @endif> {{ $permission->name }}
                                                                </label>
                                                            </div>
                                                        @endif
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <span class="help-block error-msg mrt-5" id="permission-err"></span>                        
                    </div>
                <!--</div>-->
                <div class="modal-footer">

                    <div class="modal-footer-left">
                        <button type="button" class="btn btn-link pull-left" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-theme">
                         Update
                        </button>
                    </div>
                </div>
         
                       
            </form>

        </div>
    </div>
</div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
<script>
    $('#userSelect').chosen();
    $("#deleteRole").submit(function (event) {
        var x = confirm("Are you sure you want to delete?");
        if (x) {
            return true;
        }
        else {
            event.preventDefault();
            return false;
        }

    });
</script>
