
@extends('layouts.auth.master')
@section('title')
    Overview | RIP(s)
@endsection
@section('content')

    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content">
        <section class="content-header content-header--inner flex-row justify-content-between dashboard-choose break">
            <h1 href="javascript:void(0);" class="mr10">Overview | RIP(s)</h1>
            <div class="overview-select">
            <select id="rip_overview" class="todo-select common-chosen">
                @foreach($users as $user)
                    <option value="{{ $user->id }}"
                            @if($current_id == $user->id) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>
            </div>
        </section>
        <div class="row">
            <div class="col-xs-12">
                <section class="todo-lists card-list overview">
                    <div id="ajaxTask" class="todoTasks">
                        @if($todo_rips->isNotEmpty())
                            @foreach($todo_rips as $key=> $company)
                                <div id="company_rips_{{ $company[0]->company_info->id }}">
                                <div class="rip-divider">Company : {{ $key }} </div>
                                @if($company->isNotEmpty())
                                    @foreach($company as $goal)
                                       <div id="goalStart_{{ $goal->id }}" class="overview-rips">
                                              @include('auth.overview.addOverviewGoal')
                                        </div>
                                    @endforeach                                    
                                @endif 
                                </div>
                            @endforeach
                        @else
                            <div class="card-list-item">
                                <div class="card-list-item__left no-active-task">
                                <div class="no-rip">
                    <img src="../images/todo-list-icon.svg" alt="">
                    <h3 class="text-center">Looks a little empty here.</h3>
                    <p class="text-center">You haven't created any RIP yet!
                        <br>
                        Start creating RIP from the RIP period section<br> in the Side menu bar
                    </p>
                </div>
                                </div><!-- /.card-list-item__left -->
                            </div><!-- /.card-list-item -->
                        @endif
                    </div>
                </section>
            </div>
        </div>

       
    </section>
    <!-- /.content -->

    <footer class="page-footer">© All rights reserved 2018-<?php echo date('Y'); ?>. RIPKIT Inc.</footer>
@endsection



