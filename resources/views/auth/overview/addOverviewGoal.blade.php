<?php use App\Model\Comment;?>
<div id="cat-rip-{{ $goal->id }}" class="panel-item goal-loaded">
                
    <div class="n-panel-body-top n-rip-card">  

        <div class="n-rip-card-left wide">
          
            <div class="rip-card-in-left">
                <div class="heading-row">
                    <div class="accordion-header">
                        <svg width="25" height="25" viewBox="0 0 20 25" class="panel-svg">
                            <path fill="#2C3542" fill-rule="nonzero" d="M8.949 19.782a.513.513 0 0 1-.372.154.526.526 0 0 1-.372-.897l6.539-6.539-6.539-6.538a.526.526 0 0 1 .744-.744l6.91 6.91a.526.526 0 0 1 0 .744l-6.91 6.91z"/>
                        </svg>
                    
                    </div>
                    <!-- Button trigger modal -->
                    <div class="comment-icons">
                        <svg width="25" height="25" viewBox="0 0 25 25" class="rip-svg">
                        <g fill="none" fill-rule="nonzero" transform="translate(3 3)">
                            <rect width="19" height="19" fill="#2C3542" opacity=".5" rx="2"/>
                            <g fill="#FFF" transform="translate(3 6)">
                                <rect width="9" height="1" x="3" rx=".5"/>
                                <rect width="9" height="1" x="3" y="3" rx=".5"/>
                                <rect width="9" height="1" x="3" y="6" rx=".5"/>
                                <rect width="1" height="1" rx=".5"/>
                                <rect width="1" height="1" y="3" rx=".5"/>
                                <rect width="1" height="1" y="6" rx=".5"/>
                            </g>
                        </g>
                        </svg>
                    </div>
                    <h3 class="card-list__heading font-13 goalName goalName_{{ $goal->id }}"
                    goal_id="{{ $goal->id }}" id="goalName_{{ $goal->id }}" value="{{ $goal->goal }}">{{ $goal->goal }}</h3>
                </div>
                <div class="data-row">
                    <!-- <span class="error-msg cust-msg"></span> -->
                    <div class="rip-card-in-right rip-comments-desc">
                        <p class="rip-card-subhead accordion-header taskCount_{{ $goal->id }}">

                        <span> {{ $goal->task->count() }} Tasks
                        </span>
                        </p>
                        <p class="margin-0">|</p>
                        <p class="rip-card-subhead show-goal-comments accordion-header commentCount_{{ $goal->id }}"
                        goalId= {{ $goal->id }}>
                            <span >
                                {{ $goal->comment->count() }} Comments
                            </span>
                        </p>
                    </div>
                    <div class="n-rip-card-right goal-right due-date-wrap">
            <div class="rip-card-in-left">
                <form action="{{ url("/goalDueDate")}}">               
                    <div class="form-group rip-form-group due-date-container">
                        <span class="due-date">Due Date: </span>
                       <input type="text"
                               class="form-control inputDate-tab inputDateStart_{{ $goal->id }}  dueDate dueDateUpdate_{{ $goal->id }} flatpickr-input  @if($goal->due_date < $goal->rip->start_date || $goal->due_date > $goal->rip->end_date) error @endif"
                               name="due_date"
                               value="{{ \Carbon\Carbon::parse($goal->due_date)->format('M d, Y') }}"  readonly="readonly" 
                               placeholder=""
                               @if(!auth()->user()->can('Update The Due Date Of A Goal')) disabled @endif>

                        <input type="hidden"
                               name="goal_id"
                               value="{{ $goal->id }}">
                        <svg width="25" height="25" viewBox="0 0 25 25">
                            <path fill="#2C3542" fill-rule="nonzero" d="M7.523 9.328h9.954L12.5 16.172z"/>
                        </svg> 

                    </div>
                     <span class="error-msg due-date-error">@if($goal->due_date < $goal->rip->start_date || $goal->due_date > $goal->rip->end_date)Due date is past/ahead of the rip date.@endif</span> 
                </form>
            </div>
            
            <div class="d-flex n-avtar-outer">              
                <div class="n-avatar-wrap">

                @if(auth()->user()->can('Assign User To A RIP'))
                    <div class="dropdown goalUserDropdown">
                        <a data-toggle="modal" data-target="#addusers" companyId = "{{ $goal->company_info->id }}" 
                        class="dropdown-toggle addedGoalUser" goalId="{{ $goal->id }}">
                        <svg width="35" height="35" viewBox="0 0 24 24">
                                <g fill="#2dbe74" fill-rule="nonzero">
                                    <circle cx="12.5" cy="12.5" r="8.5" fill="#2dbe74" opacity="1"></circle>
                                    <path fill="#FFF" stroke="#FFF" d="M12.77 12.23h2.96a.27.27 0 0 1 0 .54h-2.96v2.96a.27.27 0 0 1-.54 0v-2.96H9.27a.27.27 0 0 1 0-.54h2.96V9.27a.27.27 0 0 1 .54 0v2.96z"></path>
                                </g>
                            </svg>
                        </a>
                    </div>
                @endif

                    <div class="goalUserList goal-avatar-wrap goalUserTabList_{{ $goal->id}}" goalId="{{ $goal->id }}">
                          <a class="goal-user-list d-flex" data-toggle="modal" data-target="#showUserListModal">

                          @foreach($goal->getGoalUser() as $key=>$goaluser)
                              @if($key < 1)
                                <?php 
                                  $userName = explode(' ',$goaluser->name);
                                  $newUser_name = $userName[0];
                                  if(isset($userName[1])){
                                     $newUser_name.= ' '.strtoupper(substr($userName[1], 0, 1)).'.';
                                  }                                     
                                ?>

                                <div class="card-list-avatar">
                                  <img data-toggle="tooltip"
                                  @if($goaluser->image != '' && $goaluser->image != 'default.jpg') src="{{ $goaluser->userImage() }}" @endif
                                 title="{{ $goaluser->name }}">
                                </div>
                                <span class="username">{{ $newUser_name }}</span>
                              @endif
                            @endforeach

                            @if(count($goal->getGoalUser()) > 1)
                              <div class="card-list-avatar lists-count"><span class="goal-user-list">+{{ count($goal->getGoalUser())-1 }}</span>
                              </div>
                            @endif
                        </a>

                    </div>
                </div><!-- /.avatar-wrap -->
                
            </div>
            <div class="d-flex goal-action">

                <div class="v-bottom">

                <div class="rip-period-progress">                            
                    <div class="progress-group">
                        <span class="progress-text">Progress</span>
                        <a id="goal_progress"  href="{{ url('/').'/editGoal/'.$goal->id }}" data-toggle="modal" data-target="#updateripprogess"><span class="progress-number innerGoalAvgPer_{{ $goal->id }}">{{ round($goal->percentage_complete).'%' }} </span></a>

                        <div class="progress sm">
                            <div class="progress-bar progress-bar-aqua innerGoalAvgWidth_{{ $goal->id }}"
                                style="width: {{ round($goal->percentage_complete)}}%"></div>
                        </div>
                    </div>
                </div>
                    
                </div>

                @if((auth()->user()->can('Delete Goal From RIP')) || auth()->user()->can('Update Goal Percentage') ||  auth()->user()->can('Update The Name Of A Goal'))
                    <div class="dropdown menu-top-right">
                        <div class="kebab-menu dropdown-toggle"
                             id="kebab-menu"
                             data-toggle="dropdown"
                             role="button"
                             aria-haspopup="true"
                             aria-expanded="false">
                            <span></span>
                            <span></span>
                            <span></span>
                        </div>
                        <ul class="dropdown-menu"
                            id="kebab-menu"
                            aria-labelledby="kebab-menu">
                            @if(auth()->user()->can('Update The Name Of A Goal'))
                                <li><a class="editRipTabLink" data-toggle="modal" data-target="#editRipTabModal" href="{{ url('/').'/editRipTab/'.$goal->id }}">
                                   <svg width="25" height="25" viewBox="0 0 25 25">
                                        <path fill="#2C3542" fill-rule="nonzero" d="M6 17.084V20h2.916l8.601-8.601-2.916-2.916L6 17.083zm13.773-7.94a.774.774 0 0 0 0-1.097l-1.82-1.82a.774.774 0 0 0-1.097 0l-1.423 1.424 2.916 2.916 1.424-1.423z"/>
                                    </svg>
                                    Edit RIP
                                    </a>
                                </li>

                            @endif
                            @if(auth()->user()->can('Update Goal Percentage'))                                        
                                <li>
                                    <a id="goal_progress"  href="{{ url('/').'/editGoal/'.$goal->id }}" data-toggle="modal" data-target="#updateripprogess">
                                    <svg width="25" height="25" viewBox="0 0 25 25">
                                        <g fill="none" fill-rule="nonzero">
                                            <circle cx="12.5" cy="12.5" r="8.5" fill="#2C3542" opacity="1"/>
                                            <path fill="#FFF" stroke="#FFF" d="M12.77 12.23h2.96a.27.27 0 0 1 0 .54h-2.96v2.96a.27.27 0 0 1-.54 0v-2.96H9.27a.27.27 0 0 1 0-.54h2.96V9.27a.27.27 0 0 1 .54 0v2.96z"/>
                                        </g>
                                    </svg>
                                       Update Progress
                                    </a>
                                </li>
                            @endif
                            
                            @if(auth()->user()->can('Delete Goal From RIP'))
                                <li>
                                <a class="deleteGoalItem" goalName="{{$goal->goal}}"
                                       href="{{ url('/').'/deleteGoal/'.$goal->id }}">
                                       <svg width="25" height="25" viewBox="0 0 25 25">
                                            <path fill="#F14343" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"/>
                                        </svg>
                                       Delete RIP</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                @endif
                
            </div>
        </div><!-- /.rip-card-right -->
                </div>
            </div>
     
        </div>

      
    </div>
    
    <div id="collapseIn_{{ $goal->id }}"
     class="panel-body panel-w-100 n-task-panel panel-collapse collapse"
     role="tabpanel" aria-labelledby="headingIn">
        <div class="rip-detail-panel">
          <ul class="nav nav-tabs d-flex" id="myTab" role="tablist">                         
            <li role="presentation" class="active">
                <a href="#rip-addtasks_{{ $goal->id }}" aria-controls="rip-addtasks" role="tab" class="rip-active" data-toggle="tab">Tasks</a>
            </li>
         
            <li role="presentation" class="comments">
                <a href="#rip-addcomments_{{ $goal->id }}" aria-controls="rip-addcomments" role="tab" class="comment-active" data-toggle="tab">Comments</a>
            </li>    
                     
          </ul>
          <div class="tab-content">                          
                <div role="tabpanel" class="tab-pane active" id="rip-addtasks_{{ $goal->id }}">
                    <div class="tab-main tabs-tasks">
                        <div class="panel panel-default panel-theme rip-tabs">
                        <div id="ajaxTask_{{ $goal->id }}" class="todoTasks">
                          <!-- repeat div's start-->
                          @if(count($goal->task) > 0)    
                            @if(auth()->user()->can('Show Incomplete Tasks Of Goal'))
                                @foreach($goal->task->where('percentage_complete', '!=', 100 ) as $task)
                                   
                                      @include('auth.rip.addTask')
                                @endforeach
                            @endif
                            @if(auth()->user()->can('Show Complete Tasks Of Goal'))
                                @foreach($goal->task->where('percentage_complete', 100 ) as $task)                                  
                                    @include('auth.rip.addTask')
                                @endforeach
                            @endif
                          @else
                          <div class="blank-msg no-content no-active-rip ">

                            <div class="no-rip padding-top-0">
                              <img src="../images/new-site/empty_icon.svg" alt="">
                              <h3>Looks a little empty here.</h3>
                              <p>You haven’t created any Task yet!</p>
                              <p>Start creating Tasks from the bottom bar.</p>
                             </div>
                         </div>
                          @endif  
                          <!-- repeat div's end-->
                        </div>
                            
                        </div>
                        @if(auth()->user()->can('Add A Task In RIP'))
                          <div class="panel-group add-goal-panel panel-fixed-footer" id="accordion" role="tablist" aria-multiselectable="true">
                              <div class="panel-item add-panel add-goal">
                                <form method="post" id="taskForm_{{ $goal->id  }}" class="taskForm d-flex" action="{{ url('ripAddTask').'/'.base64_encode($goal->id) }}">
                                {{ csrf_field() }}  
                                  <div class="inline-form textarea-comment-box">
                                      <div class="mentions-input-box">
                                        <textarea name="task"  data-gramm_editor="false" cols="30" rows="1" placeholder="Add Task" class="textarea--full ajaxAddTask form-bg-control"></textarea>           
                                        <span class="error-msg  margin-0"> </span>
                                      </div> 
                                  </div>                                 
                                    <input type="button" class="add-cta-button add_task_button" value="Add Task">
                                </form>
                              </div><!-- /.panel-item -->
                          </div>
                        @endif    
                    </div>
                </div>
              
              <div role="tabpanel" class="tab-pane" id="rip-addcomments_{{ $goal->id }}">
                  <div class="tab-main tabs-comment">
                          <!-- repeat div's start -->
                      <div class="comment_{{ $goal->id }}">
                      @if(count($goal->comment) > 0)    
                        @foreach($goal->comment as $comment)
                          <?php 
                            $comment_arr = Comment::getMentionData($comment->message,'display'); 
                            $comment->message = $comment_arr['comment_data'];
                          ?>    
                          @include('auth.rip.ajax.comment')  

                        @endforeach 
                      @else
                      <div class="blank-msg no-content no-active-rip">

                        <div class="no-rip padding-top-0">
                          <img src="../images/new-site/empty_icon.svg" alt="">
                          <h3>Looks a little empty here.</h3>
                          <p>You haven’t created any Comment yet!</p>
                          <p>Start creating Comments from the bottom bar.</p>
                         </div>
                       </div>
                      @endif
                      </div>
                      @if(auth()->user()->can('Add New Comment'))
                        <div class="panel-group add-goal-panel comments-fixed-footer panel-fixed-footer" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel-item add-panel add-goal">
                                
                                <form method="post" id="addCommentForm_{{ $goal->id }}" class="addCommentForm d-flex" action="{{ url('comments') }}" 
                                      goalId="{{ $goal->id }}" enctype="multipart/form-data">
                                    
                                    {{ csrf_field() }}
                                    <div class="inline-form">
                                    <div class="textarea-comment-box">
                                    <textarea name="comment" id="add-comment" data-gramm_editor="false" cols="30" rows="3" placeholder="Add a new comment. Type @ to tag/notify someone." class="textarea--full addCommentSection mention-example2" companyId = "{{ $goal->company_info->id }}"></textarea>
                                    <div class="file-upload-name"></div>
                                    <div class="comment-box-options-item">
                                        <label for="upload-file_{{ $goal->id }}"><i class="fa fa-paperclip" aria-hidden="true"></i></label>
                                        <input type="file" class="custom-input-file upload-file" name="attachment[]" multiple="multiple" id="upload-file_{{ $goal->id }}">
                                    </div>
                                    <span class="help-block margin-0" id="comment-err">**Max Upload Size: 2MB</span>
                                    </div>
                                    <input type="hidden" id="goal_id" value="{{ $goal->id }}" name="goalId">
                                    <input type="hidden" name="mention_id" id="mention_id" value="">
                                    <span class="error-msg margin-0"> </span>
                                    </div>
                                    <button class="add-cta-button">Add Comment</button>
                                </form>
                            </div><!-- /.panel-item -->
                        </div>
                      @endif
                  </div>
              </div>                          
          </div>
      </div><!-- /.panel-item -->
    </div><!-- /.panel-body -->   
</div><!-- /.panel-item -->
