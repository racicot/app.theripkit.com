
@extends('layouts.auth.master')
@section('title')
    Overview | Task
@endsection
@section('content')
    <!-- Content Header (Page header) -->

    <!-- Main content -->
    <section class="content overview">
        <section class="content-header content-header--inner flex-row justify-content-between dashboard-choose break">
            <h1 href="javascript:void(0);" class="mr10">Overview | Task</h1>
            <select id="task" class="todo-select common-chosen">

                @foreach($users as $user)
                    <option value="{{ $user->id }}"
                            @if($currentUser == $user->id) selected @endif>{{ $user->name }}</option>
                @endforeach
            </select>
        </section>
        <div class="row overview-task">
            <div class="col-xs-12">
             @forelse ($tasks as $key => $todo_tasks)
             <div class="taskLength">
            <div class="rip-divider">Company: {{$key}}</div>
            
            @foreach($todo_tasks as $task)
             @include('auth.rip.addTask')
             @endforeach
            </div>
             @empty
            <div class="card-list-item">
                <div class="card-list-item__left no-active-task">
                    <div class="no-rip">
                        <img src="/images/todo-list-icon.svg" alt="">
                        <h3 class="text-center">Looks a little empty here.</h3>
                        <p class="text-center">You haven't created any Task yet!
                        <br>
                        Start creating tasks from the RIP period section<br> in the Side menu bar
                        </p>
                    </div>
                </div><!-- /.card-list-item__left -->
            </div><!-- /.card-list-item -->
             @endforelse
             <div class="card-list-item noTaskOverview" style="display:none">
                    <div class="card-list-item__left no-active-task">
                        <div class="no-rip">
                            <img src="/images/todo-list-icon.svg" alt="">
                            <h3 class="text-center">Looks a little empty here.</h3>
                            <p class="text-center">You haven't created any Task yet!
                            <br>
                            Start creating tasks from the RIP period section<br> in the Side menu bar
                            </p>
                        </div>
                    </div><!-- /.card-list-item__left -->
                </div><!-- /.card-list-item -->
            </div>
        </div>

    </section>
    <!-- /.content -->

    <footer class="page-footer">© All rights reserved 2018-<?php echo date('Y'); ?>. RIPKIT Inc.</footer>
@endsection



