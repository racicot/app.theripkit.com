@include('partials.styles')
<section class="content">
    <section class="content-header content-header--lg">
        <div class="row snap-header">
            <div class="col-sm-4">

            </div>
            <div class="col-sm-4">
                <h1>{{ session('company_name') }} Portfolio</h1>
            </div>

        </div>
    </section>

    <section class="content">
        <div class="portfolio-top">
            <div class="row">
                <div class="col-sm-4">
                    <div class="block-body portfolio-body">
                        <div class="form-group">
                            <label>Why</label>
                            <div class="contentSave" data-field="why" contenteditable="true">
                                <?php echo $portfolio->why;?>
                            </div>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <label>How</label>
                            <div class="contentSave" data-field="how" contenteditable="true">
                                <?php echo $portfolio->how;?>
                            </div>
                        </div><!--/. form-group -->

                        <div class="form-group">
                            <label>What</label>
                            <div class="contentSave" data-field="what" contenteditable="true">
                                <?php echo $portfolio->what;?>
                            </div>

                        </div><!--/. form-group -->
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="block-body portfolio-body ">
                        <div class="form-group">
                            <label>Strategic Differentiators</label>
                            <ol class="contentSave" data-field="strategic" contenteditable="true">
                                <?php echo $portfolio->strategic;?>
                            </ol>
                            <div class="addAnother">Add another...</div>
                        </div><!--/. form-group -->
                        <div class="form-group">
                            <label>Elevator Pitch</label>
                            <div class="contentSave" data-field="elevator" contenteditable="true">
                                <?php echo $portfolio->elevator;?>
                            </div>
                        </div><!--/. form-group -->
                        <div class="form-group">
                            <label>Financial Goal</label>
                            <div class="contentSave" data-field="financial" contenteditable="true">
                                <?php echo $portfolio->financial;?>
                            </div>
                        </div><!--/. form-group -->
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="block-body portfolio-body">
                        <div class="form-group">
                            <label>Top Behavioural Values</label>
                            <div class="contentSave" data-field="behavioural" contenteditable="true">
                                <?php echo $portfolio->behavioural;?>
                            </div>
                        </div><!--/. form-group -->
                        <div class="form-group">
                            <label>Brand Promise</label>
                            <div class="brand-promise-wrap">
                                <div class="text-center">
                                    <h4 class="brand-promise-label">Emotional</h4>
                                    <h3 class="brand-promise-title">
                                        <div class="contentSave" data-field="emotional" contenteditable="true">
                                            <?php echo $portfolio->emotional;?>
                                        </div>
                                    </h3>
                                </div>

                                <div class="brand-promise-in">
                                    <div class="text-right">
                                        <h4 class="brand-promise-label">Economic</h4>
                                        <h3 class="brand-promise-title">
                                            <div class="contentSave" data-field="economic" contenteditable="true">
                                                <?php echo $portfolio->economic;?>
                                            </div>
                                        </h3>
                                    </div>
                                    <div class="brand-promise-img-wrap">
                                        <img class="caret-up" src="{{ url('images/caret-up.png') }}" alt="">
                                    </div>
                                    <div class="text-left">
                                        <h4 class="brand-promise-label">Functional</h4>
                                        <h3 class="brand-promise-title">
                                            <div class="contentSave" data-field="functional" contenteditable="true">
                                                <?php echo $portfolio->functional?>
                                            </div>
                                        </h3>
                                    </div>
                                </div>

                            </div>
                        </div><!--/. form-group -->
                    </div>
                </div>

            </div>
        </div>

        <div class="snapshot-mid">
            <div class="row">
                <div class="col-sm-3 col-sm-offset-3">
                    <div class="form-group">
                        <label>Customer Solution</label>

                        <ol class="contentSave" data-field="customer_solution" contenteditable="true">
                            <?php echo $portfolio->customer_solution;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>Customer problem</label>
                        <ol class="contentSave" data-field="customer_problem" contenteditable="true">
                            <?php echo $portfolio->customer_problem;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>
            </div>

            <div class="row flex-row">
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>KEY ACTIVITIES & RESOURCE</label>
                        <ol class="contentSave" data-field="key_activities_resource" contenteditable="true">
                            <?php echo $portfolio->key_activities_resource;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                    <div class="form-group">
                        <label>KEY PARTNERS</label>
                        <ol class="contentSave" data-field="key_partner" contenteditable="true">
                            <?php echo $portfolio->key_partner;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>

                <div class="col-sm-6 h-center">
                    <div class="form-group">
                        <label>VALUE PROPOSITION</label>
                        <ol class="contentSave" data-field="value_proposition" contenteditable="true">
                            <?php echo $portfolio->value_proposition;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>

                <div class="col-sm-3">
                    <div class="form-group">
                        <label>KEY CUSTOMER SEGMENTS</label>
                        <ol class="contentSave" data-field="key_customer_segments" contenteditable="true">
                            <?php echo $portfolio->key_customer_segments;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                    <div class="form-group">
                        <label>REVENUE STREAM</label>
                        <ol class="contentSave" data-field="revenue_stream" contenteditable="true">
                            <?php echo $portfolio->revenue_stream;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>
            </div>

            <div class="row">
                <div class="col-sm-3 col-sm-offset-3">
                    <div class="form-group">
                        <label>KEY COSTS</label>
                        <ol class="contentSave" data-field="key_cost" contenteditable="true">
                            <?php echo $portfolio->key_cost;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>
                <div class="col-sm-3">
                    <div class="form-group">
                        <label>SALES CHANNELS</label>
                        <ol class="contentSave" data-field="sales_channel" contenteditable="true">
                            <?php echo $portfolio->sales_channel;?>
                        </ol>
                        <div class="addAnother">Add another...</div>
                    </div><!--/. form-group -->
                </div>
            </div>
        </div>
        <div class="portfolio-block">
            <section class="content-header content-header--inner">
                <h1>Responsibilities in perspective</h1>
            </section>

            <div class="row">
                <div class="col-sm-12">
                    <div class="card">
                        <div class="card-head card-head--small">
                            <div class="card-head__left">
                                <h3>1 Year RIPS (2016-2017)</h3>
                            </div>
                            <div class="card-head__right">
                                <date class="card__date">Aug10, 2017</date>
                            </div>
                        </div><!-- /.card-head -->
                        @foreach($goals as $goal)
                            @if($goal->category)
                                <div class="card-body card-body--small">
                                    <div class="card-item">
                                        <h3 class="card-item__title">{{ $goal->category->name }}</h3>
                                        <p class="card-item__desc">{{ $goal->goal }}</p>
                                    </div><!-- /.card-item -->
                                </div><!-- /.card-body -->
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>



    