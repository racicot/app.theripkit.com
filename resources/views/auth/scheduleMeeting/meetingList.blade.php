        @if ($count == 0)
        <div class="no-rip">
            <img src="../images/todo-list-icon.svg" alt="">
            <h3 class="text-center">Looks a little empty here.</h3>
            <p class="text-center">You haven't created any Meeting yet!
                <br>
                Please schedule meeting!
            </p>
        </div>
        @else
        <ul class="list">
            @foreach ($meetings as $meeting)
            <li class="meeting-list" id="meeting_{{$meeting->id}}">
                <div class="image-section">
                    <img src="../images/new-site/meeting-thumbnail-placeholder.png" alt="Meeting thumbnail" />
                </div>
                <div class="content-inner">
                    <div class="meeting-agenda">
                    <h6>{{$meeting->agenda}}</h6> 
                    @if($meeting->meeting_date_time)
                        <div class="meeting-expire">Expired</div>
                        @endif
                    </div>
                    <div class="date">
                        <span>Date:  {{$meeting->modified_meeting_date}}</span>
                        <span class="time">Time:  {{$meeting->modified_meeting_time}}</span>
                        </div>
                        <div class="meet-time">
                            <span> Duration(in min.):  {{$meeting->duration}}</span>
                        </div>
                    
                    <div class="user-list">
                        <div class="d-flex n-avtar-outer">
                            <div class="n-avatar-wrap">
                                <div class="goalUserList goal-avatar-wrap" >
                                    @foreach ($meeting->show_users as $user)
                                <a class="goal-user-list d-flex meetingUserList" meetingId="{{$meeting->id}}" data-toggle="modal" data-target="#showUserListModal">
                                        <div class="card-list-avatar" data-toggle="tooltip" data-original-title="{{ $user->name }}">
                                            <img data-toggle="tooltip" @if($user->image != '' && $user->image != 'default.jpg')
                                            src="{{ $user->userImage() }}" @endif >
                                        </div>
                                    </a>
                                    @endforeach
                                    @if ($meeting->remaining_user_count > 0)
                                    <a class="goal-user-list d-flex meetingUserList" meetingId="{{$meeting->id}}" data-toggle="modal" data-target="#showUserListModal">
                                        <div class="card-list-avatar lists-count">
                                           <span class="goal-user-list">+{{$meeting->remaining_user_count}}</span>
                                        </div>
                                    </a>
                                    @endif
                                </div>
                            </div><!-- /.n-avatar-wrap -->
                        </div>
                       <a href="#" title="Copy Invitation Link" data-clipboard-text="{{$meeting->join_url}}" value="{{$meeting->join_url}}" class="copy-link">Copy Invitation Link</a>
                    </div>
                  
                </div>
                <div class="dropdown menu-top-right">
                    @if($meeting->created_by == Auth::user()->id)
                    <div class="kebab-menu dropdown-toggle" id="meeting-menu" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <ul class="dropdown-menu" id="meeting-menu" aria-labelledby="meeting-menu">
                        <li>
                        <a href="#" id="candelMeeting" meetingId="{{$meeting->id}}" agenda="{{$meeting->agenda}}">Cancel Meeting</a>
                        </li>
                    </ul>
                    @endif
                </div>
            </li>
            @endforeach
        </ul>
        @endif