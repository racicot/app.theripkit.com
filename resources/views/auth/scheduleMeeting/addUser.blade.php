
<div class="participant-list">
<div class="panel-group" id="accordionPop" role="tablist" aria-multiselectable="true">
<div class="panel panel-default panel-theme">
    <div class="n-panel-heading" role="tab" id="headingOne">
        Participants
        <a class="mysvg collapsed" data-toggle="collapse" data-parent="#accordionPop" href="#participents" aria-expanded="false" role="button" aria-hidden="true">
            <svg width="25" height="25" viewBox="0 0 20 25">
                <path fill="#2C3542" fill-rule="nonzero" d="M8.949 19.782a.513.513 0 0 1-.372.154.526.526 0 0 1-.372-.897l6.539-6.539-6.539-6.538a.526.526 0 0 1 .744-.744l6.91 6.91a.526.526 0 0 1 0 .744l-6.91 6.91z"></path>
            </svg>
        </a>
    </div><!-- /.panel-heading -->

    <div id="participents" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="true">

        @if($count == 0)
        <p class="no-user">No User Found</p>
        @endif
        <!--<p class="custom-note"><b>Note:</b> You are not allowed to uncheck the super-admin or admin.</p>-->
        @if($checkedUsers)
        @foreach($checkedUsers as $checkedUser)
            <div class="user-lists participantsList">
<!--                <label class="material-checkbox disabled-element">-->
                <label class="material-checkbox">
                    <input type="checkbox"  class="participants" name="addUserToMeeting" value="{{ $checkedUser->id }}" >
                    <span class="checkbox-items">
                        <div class="user-pics">
                            <img class="responsive-img" @if($checkedUser->image != '' && $checkedUser->image != 'default.jpg')
                            src="{{ $checkedUser->userImage() }}" src="{{ $checkedUser->userImage() }}" @endif
                                title="{{ $checkedUser->name }}">
                        </div>
                        <div class="user-data">
                            <div class="user-title">{{ $checkedUser->name}}
                                {{$userType}}
                        </div>
                            <div class="user-text">{{ $checkedUser->email }}</div>
                        </div>
                    </span>
                </label>
            </div>
            @endforeach
            @endif
            @foreach($users as $user)
            <div class="user-lists participantsList">
                <label class="material-checkbox">
                    <input type="checkbox" class="participants" name="addUserToMeeting" value="{{ $user->id }}">
                    <span class="checkbox-items">
                        <div class="user-pics">
                            <img class="responsive-img" @if($user->image != '' && $user->image != 'default.jpg')
                            src="{{ $user->userImage() }}" src="{{ $user->userImage() }}" @endif
                                title="{{ $user->name }}">
                        </div>
                        <div class="user-data">
                            <div class="user-title">{{ $user->name}}</div>
                            <div class="user-text">{{ $user->email }}</div>
                        </div>
                    </span>
                </label>
            </div>
            @endforeach

    </div><!-- /.panel-collapse -->
</div>


</div>
</div>
<span class="error-msg" id="participants_err"></span>