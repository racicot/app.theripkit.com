<!-- Meeting invite sidebar -->
<div class="meeting-sidebar" id="meeting-sidebar">
        <div class="heading-box">
            <h3>Meeting Calendar<small>(Timezone: Pacific)</small></h3>
            <span class="close-btn"></span>
        </div>
        <div class="content-box" id="meetingList">
        </div>
</div>
    <!-- Meeting invite sidebar end here-->