@extends('layouts.non-auth.master')
@section('title')
    Login - Build Impossible
@endsection
@section('body-content')
    <main id="main" class="login-wrap ">
        <div class="full-bg-image">
            @if( (new App\Model\LoginImage)->getImage() )
                <img src="{{ (new App\Model\LoginImage)->getImage() }}" alt="">
            @else    
                <img src="{{ url('images/login_bg.jpeg') }}" alt="Login Background">
            @endif    
        </div>

        <div class="login">
                @include('header.non-auth.header')
                <p class="user">Login to continue to THE RIPKIT</p>
            @if((new App\Model\LoginImage)->getMotto() != 'NULL' ) 
            <div class="caption caption--login">
               
            </div> 
            @endif
            <div class="login-in">
                <div class="login__social">
                    <div class="login-btn-wrapper">
                        <a href="googleRedirect" class="btn btn__social">
                            <img src="{{ url('images/google.png') }}" alt="Google Logo" />
                            <span>Login with Google</span>
                        </a>
                    </div>
                    
                </div><!-- /.login__social -->

                <div class="login__form">
                    <form action="{{ route('login') }}" method="post">
                        {{ csrf_field() }}
                        <div class="login__row login-material">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="jemail">
                            <p class="login-seprator">OR</p>
                                <label class="mgb-0">Email</label>
                                <div class="mui-textfield padding-top-0">
                                    <input id="email" type="email" class="login__input" name="email"
                                           value="{{ old('email') }}" placeholder="Enter email" spellcheck="false" autofocus>
                                    <span class="input-loader" id="inputLoder" style="display: none;">
                                     <span class="screen-reader-text">Loading</span>
                                    </span>
                                    <span class="help-block">
                                    <strong class="text-danger" id="emailError"></strong>
                                    </span>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('email') }}</strong>
                                      </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="jpassword"
                                 style="display:none">
                                 <div class="username-pwd-box">
                                    <a href="{{ url('/login') }}">
                                        <svg width="25" height="25" viewBox="0 0 25 20">
                                            <path fill="#2C3542" fill-rule="nonzero" d="M9.879 6.19a.636.636 0 0 1 .907 0 .64.64 0 0 1 0 .898l-4.6 4.6h15.179c.354 0 .635.282.635.636a.639.639 0 0 1-.635.644H6.186l4.6 4.591a.651.651 0 0 1 0 .907.636.636 0 0 1-.907 0L4.19 12.778a.625.625 0 0 1 0-.898L9.88 6.19z"/>
                                        </svg>
                                    </a>
                                    <div class="user-login-value"></div>

                                 </div>
                                 <label class="mgb-0">Password</label>
                                <div class="mui-textfield padding-top-0">
                                
                                    <input id="password" type="password"  placeholder="Enter password" class="login__input" name="password">
                                    <span class="input-loader" id="inputLoder2" style="display: none;">
                                         <span class="screen-reader-text">Loading</span>
                                      </span>
                                                            <span class="help-block">

                                        <strong id="passwordError" class="text-danger"></strong>
                                      </span>
                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                      </span>
                                    @endif
                                </div>
                                <a id="loginButtton" href="{{ url('/password/reset') }}" class="btn-link text-right">Forgot Password?</a>
                            </div>
                        </div><!-- /.login__row -->
                        <button type="button" style="display:none" class="submitButton btn btn-theme btn-block">Log In</button>
                        
                    </form>
                        <button type="button" id="clickButton" class="clickButton btn btn-theme btn-block">Proceed</button>
                    </div><!-- /.login__form -->
                </div>
                
            @include('footer.non-auth.footer')
        </div><!-- /.login -->


    </main>
@endsection
