<ul class="control-sidebar-menu">
    @foreach( $notifications as $notification)
        <li >
            <a class="notification" id="notification_{{ $notification->id }}" href="{{ url('readNotification').'/'. $notification->id  }}">
                <i class="menu-icon fa fa-bell"></i>
            </a>
            <div class="menu-info">
           

                <h4 class="control-sidebar-subheading"> <?php  
            if($notification->data != ''){
                $notification->data  = str_replace('\n', ' ', $notification->data);
                echo str_replace('\r', ' ', $notification->data);
            }           
           
            ?></h4>

                <p>{{ \Carbon\Carbon::parse($notification->created_at)->format('M d, Y') }}</p>
            </div>

        </li>
    @endforeach
</ul>
