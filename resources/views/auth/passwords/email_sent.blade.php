@extends('layouts.non-auth.master')
@section('title')
    Reset Password - Build Impossible
@endsection
@section('body-content')
    <main id="main" class="login-wrap ">
        <div class="full-bg-image">
            <img src="{{ url('images/login_bg.jpeg') }}" alt="Login Background">
        </div>


        <div class="login success-msg">
            @include('header.non-auth.header')
            <div class="login-in reset-login">
                
                <div class="login__social">
                    

                </div><!-- /.login__social -->

                <div class="login__form">
                    <i class="success-icon">success icon</i>
                    <h2>Email Sent Successfully!</h2>
                    <p>To get back into your account, please follow the instructions we've sent to your provided Email ID.</p>
                    <p>{{ $status }}</p>
                    <a href="{{ url('/login') }}" class="btn btn-theme btn-block">Back to Login</a>
                    <!--<a href="{{ url('/login') }}" class="btn-link back-login-link">Back to Login</a>-->
                        
                </div><!-- /.login__form -->
            </div>
            @include('footer.non-auth.footer')
        </div><!-- /.login -->


    </main>
@endsection