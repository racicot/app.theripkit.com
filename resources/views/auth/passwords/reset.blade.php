@extends('layouts.non-auth.master')
@section('title')
    Reset Password - Build Impossible
@endsection
@section('body-content')
    <main id="main" class="login-wrap ">
        <div class="full-bg-image">
            @if( (new App\Model\LoginImage)->getImage() )
                <img src="{{ (new App\Model\LoginImage)->getImage() }}" alt="">
            @else    
                <img src="{{ url('images/login_bg.jpeg') }}" alt="Login Background">
            @endif
        </div>  

        <div class="login">
            @include('header.non-auth.header')
            <div class="login-in">
                <div class="login__social">
                    <h2>Reset Password?</h2>
                    <p>Reset Password with your email account</p>
                </div><!-- /.login__social -->

                <div class="login__form">
                    <form  method="POST" action="{{ route('password.request') }}">
                        {{ csrf_field() }}

                        <input type="hidden" name="token" value="{{ $token }}">


                        <div class="login-material">
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="jemail">
                                <div class="mui-textfield mui-textfield--float-label">
                                    <input id="email" type="email" class="login__input" name="email"
                                           value="{{ $email or old('email') }}" placeholder="Enter email" readonly>
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <div class="mui-textfield mui-textfield--float-label">
                                    <input id="password" type="password" class="login__input" name="password" placeholder="Password" required autofocus>
                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                <div class="mui-textfield mui-textfield--float-label">
                                    <input id="password-confirm" type="password" placeholder="Confirm Password" class="login__input"
                                           name="password_confirmation" required>
                                    @if ($errors->has('password_confirmation'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <button type="submit" class="btn btn-theme btn-block mt-30">
                                Reset Password
                            </button>
                        </div>
                    </form>

                    <span class="screen-reader-text">Loading</span>
                    </span>
                    <span class="help-block">
                <strong class="text-danger" id="emailError"></strong>
                </span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                <strong class="text-danger">{{ $errors->first('email') }}</strong>
              </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="jpassword"
                 style="display:none">
                <div class="mui-textfield">
                    <input id="password" placeholder="Password" type="password" class="login__input" name="password">
                    <span class="input-loader" id="inputLoder2" style="display: none;">
                                         <span class="screen-reader-text">Loading</span>
                                      </span>
                    <span class="help-block">

                                        <strong id="passwordError"></strong>
                                      </span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                                        <strong class="text-danger">{{ $errors->first('password') }}</strong>
                                      </span>
                    @endif
                </div>
            </div>
        </div><!-- /.login__row -->

        </form>
        </div><!-- /.login__form -->
        </div>
        @include('footer.non-auth.footer')
        </div><!-- /.login -->


    </main>
@endsection
