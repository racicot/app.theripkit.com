@extends('layouts.non-auth.master')
@section('title')
    Reset Password - Build Impossible
@endsection
@section('body-content')
    <main id="main" class="login-wrap ">
        <div class="full-bg-image">
            <img src="{{ url('images/login_bg.jpeg') }}" alt="Login Background">
        </div>


        <div class="login">
            @include('header.non-auth.header')
            <div class="login-in reset-login">
                @if(Session::has('status'))
                    @if(Session::get('status') == 'We have e-mailed your password reset link!')
                        <p class="alertForget alert-success">{{ Session::get('status') }}</p>
                    @else 
                        <p class="alertForget alert-info">{{ Session::get('status') }}</p>
                    @endif    
                @endif
                <div class="login__social">
                    <h2>Forgot Password?</h2>
                    <p>Please provide your registered Email ID.</p>

                </div><!-- /.login__social -->

                <div class="login__form">
                    <form method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}
                        <div class="login-material">

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}" id="jemail">
                            <label class="mgb-0">Email</label>
                                <div class="mui-textfiel padding-top-0">
                                    <input id="email" type="text" class="login__input" name="email"
                                           value="{{ old('email') }}" placeholder="Enter email" spellcheck="false">
                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" id="resetButton" class="btn btn-theme btn-block mt-55">Send Password Reset Link</button>
                            <a href="{{ url('/login') }}" class="btn-link back-login-link">Back to Login</a>
                        </div>
                    </form>
                </div><!-- /.login__form -->
            </div>
            @include('footer.non-auth.footer')
        </div><!-- /.login -->


    </main>
@endsection
