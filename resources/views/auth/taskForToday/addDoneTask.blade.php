<li id="taskli_{{$task->id}}" class="doneTask">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="{{$task->id}}" id="taskDone_{{$task->id}}" checked />
        <i class="custom-check"></i>
        <label class="custom-control-label striked" for="doneissue"> 
        {{$task->name}}
        </label>
    </div>
</li> 