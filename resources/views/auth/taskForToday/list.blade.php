<div class="to-do-lists">
    <div class="heading-box">
        <h3>To-Do List/Notes</h3>
        <span class="close-btn"></span>
    </div>
    <div class="content-box doneTask">
    
    </div>
    <form class="addTask-form" method="post">
            {{ csrf_field() }}
    <div class="form-group">
        <input type="text" name="taskName" class="form-control" id="newTask" placeholder="Add Task" />
        <span class="error-msg err_newTask"></span>
    </div>
    <button type="submit" class="btn primary-btn">ADD</button>
    </form>
</div>
