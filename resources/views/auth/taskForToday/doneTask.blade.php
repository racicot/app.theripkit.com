<div class="header-strip">

    <strong class="title-text">Completed <span class="count">({{$count}})</span></strong>
    <span class="clear-btn clearTask" @if ($count == 0) style="color:grey; " @else style="color:'#f14343'; cursor:pointer" @endif >Clear all</span>

    <span class="drop-icon"></span>
</div>
<ul class="data-lists checked-lists done">
    @foreach ($doneList as $task)
        <li id="taskli_{{$task->id}}" class="doneTask">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" value="{{$task->id}}" id="taskDone_{{$task->id}}" checked />
                <i class="custom-check"></i>
                <label class="custom-control-label striked" for="doneissue">
                {{$task->name}}
                </label>
            </div>
        </li>
    @endforeach
    <li class="noComplete">
        <div class="no-rip">
            <img src="../images/todo-list-icon.svg" alt="">
            <h3 class="text-center">Looks a little empty here.</h3>
            <p class="text-center">You haven't created any Task yet!
                <br>
                Start creating Task(s) from the bottom bar
            </p>
        </div>
    </li>
</ul>
