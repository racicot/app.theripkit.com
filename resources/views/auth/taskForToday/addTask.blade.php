<li id="taskli_{{$task->id}}" class="notDoneTask">
    <div class="custom-control custom-checkbox">
        <input type="checkbox" class="custom-control-input" value="{{$task->is_done}}" id="task_{{$task->id}}">
        <i class="custom-check"></i>
        <label class="custom-control-label" for="defaultUnchecked"> 
            {{$task->name}}
        </label>
    </div>
</li>