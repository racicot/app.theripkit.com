
<ul class="data-lists task_incomplete notDone">
        @foreach ($notDoneList as $task)
        <li id="taskli_{{$task->id}}" class="notDoneTask">
            <div class="custom-control custom-checkbox">
                <input type="checkbox" class="custom-control-input" value="{{$task->id}}" id="task_{{$task->id}}">
                <i class="custom-check"></i>
                <label class="custom-control-label" for="defaultUnchecked">
                   {{$task->name}}
                </label>
            </div>
        </li>
    @endforeach
</ul>
