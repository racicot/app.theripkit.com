<footer class="container-fluid static-footer">
    <div class="col-xs-12 text-center">
<!--        <ul>
            <li>
                <a href="#" title="Privacy Policy">Privacy Policy</a>
            </li>
            <li>
                <span class="round-filled"></span>
            </li>
            <li>
                <a href="#" title="Terms of use">Terms of use</a>
            </li>-->
        </ul>
    </div>
</footer>


<footer class="container-fluid login-footer hide">
    <div class="powered">
        <p><a href="#">Powered by Buisness Instincts Group</a></p>
    </div>

    <div class="social">
        <ul class="social-list">
            <li><a href="https://www.facebook.com/businessinstincts"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
            <li><a href="https://twitter.com/BinstinctsGroup"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
        </ul>
    </div>
</footer>


