$('document').ready(function () {

    /* Script to add user to new task and add new user if not in dropdown */
    $(".addUserToNewTask").chosen();

    /* Script to add user to added goal   */
    $(".addedGoalUserSelect").chosen();

    /* Script to add user to added goal */
    $('body').on('click','.invite-goal', function () {
        $current_model = $(this);
        $old_val= $current_model.parents('.modal-content').find('#old_value').val();
        var goalId = $('#userList').find('input[name=goal_id]').val();    
        var target = '.addedUserGoalList_' + goalId;
        var target1 = '.addedGoalUserSelect_' + goalId;
        var target2 = '.goalUserList_' + goalId;
        var target3 = '.goalTaskUserList_' + goalId;
        var target6 = '.addedUserGoalList_' + goalId;
        var val = $('.tab-comments').find(target1).val();
        var userList = [];
        $.each($("input[name='addUserToGoal']:checked"), function(){            
            userList.push($(this).val());
        });
        $(".loader").addClass('loading');
        $.ajax({
            url: APP_URL + '/users/assignUserToAddedGoal',
            data: {goal_id: goalId, user: userList},
            success: function (response) {

                $new_val= $current_model.parents('.modal-content').find('.selected-user-items').html();
                $new_arr = $new_val.split(' ');
                var data = response;
                var target5 = '.goalUserTabList_' + goalId;                        
                //$('body').find(target2).html(data.html);
                $('body').find(target5).html(data.tabHtml);                        
                $(".loader").removeClass('loading');
                $('body').find('.cust-msg').remove();
                $('#addusers').modal('hide');
                $('.notification-count').show();
                $('#addusers').html('');
                if($old_val <= $new_arr[0]){
                    displayGoalNotificationMsg('alert-success', 'User(s) has been successfully assigned to a RIP.');
                }
                else{
                    displayGoalNotificationMsg('alert-danger', 'User(s) has been successfully removed from RIP.');
                }
            }
        });
    });


    // Script to show user in the user modal in goal tab
    $('body').on('click', '.addedGoalUser', function () {
        $('#showUserListModal').html('');
        var goal_id;
        var company_id;
        goal_id = $(this).attr('goalId');
        company_id = $(this).attr('companyId');
        var target = '.addedUserGoalList_' + goal_id;
        $.ajax({
            url: APP_URL + '/goal/userList',
            data: {goalId: goal_id, company_id:company_id},
            success: function (response) {                
                $('#addusers').html(response);
            }
        });
    });

    /* Script to delete assign user to added goal */
    $('body').on('click', '.media-delete', function (e) {
        e.preventDefault();
        var goalId = $(this).closest('.add-user-dropdown-list').attr('data-goalid');
        var url = $(this).find('a').attr('href');
        var target = $(this).parent().parent().parent();
        var target2 = '.goalUserList_' + goalId;
        var target3 = '.goalTaskUserList_' + goalId;
        $( ".loader" ).addClass('loading');
        $.ajax({
            url: url,
            success: function (response) {
                var data = response;
                var target4 = '.goalTaskUser_' + data.userId;
                var target5 = '.goalUserTabList_' + goalId;
                var target6 = '.addedUserGoalList_' + goalId;
                $(target).html(data.dropdownHtml);
                $(target2).html(data.html);
                $(target5).html(data.tabHtml);
                $(target3).each(function () {
                    $(this).find(target4).remove();
                });
                $(".loader").removeClass('loading');
                $('body').find('.cust-msg').remove();
                $(target6).before('<span class="sucess-msg cust-msg">User deleted successfully</span>');
            }
        });
    });

    $('body').on("keypress", "#update_progress .goal-tab-percentage", function(e) {
         if(e.which === 13) {
            $('.save-btn').click();
         }
    })

    // Script to update goal percentage .
    $('body').on("click", ".save-btn", function(e) {
        e.preventDefault();
        var form = $('#update_progress');
        var percentageVal = $(form).find('input[name="percentage"]').val(); 
        var goal_id = $(form).find('input[name="goal_id"]').val(); 
            
        if (percentageVal != '') {
            $(".loader").addClass('loading');
            var url = APP_URL + '/goalPercentageUpdate/'+goal_id;
            $.ajax({
                url: url,
                type: 'POST',
                data: {'percentage': percentageVal, 'goal_id': goal_id, _token: $("input[name='_token']").val()},
                success: function (response) {
                    var data = response;                    
                    var target = '.innerGoalAvgPer_' + data.goalId;
                    var target2 = '.innerGoalAvgWidth_' + data.goalId;
                    var target1 = '.categoryAvgPer_' + data.category_id;
                    var target4 = '.categoryAvgWidth_' + data.category_id;

                    var target3 = '.ripPer_' + data.ripId;
                    var ripDivClass = 'progress-radial progress-radial--md progress-' + data.ripAvgPer
                    var percentage = data.percentage + '%';
                    var ripPercentage = '<span>'+data.ripAvgPer+'</span>' + ' <span>%</span>';
                    var catPercentage = data.categoryAvgPer + '%';
                    $(target).html(percentage);
                    $(target2).css('width', percentage);
                   
                    $(target3).html(ripPercentage);
                    $(target1).html(catPercentage);
                    $(target4).css('width', catPercentage);

                    //$('body').find('#goal-per-input').val(data.percentage);
                
                    $(target3).parent().removeClass().addClass(ripDivClass);
                    $(".loader").removeClass('loading');
                    if(response.notify > 0)
                        $('.notification-count').show();

                    $('#updateripprogess .close_modal').click(); 
                    displayGoalNotificationMsg('alert-success', 'RIP Progress has been updated.');
                }
            });
        }
        else{
            $(form).find('.error-msg').text('Please enter progress value');
        }
    });
    function autosize() {
        var el = this;
        setTimeout(function() {
          el.style.cssText = "height:20px; padding:0";
          el.style.cssText = "height:" + el.scrollHeight + "px";
        }, 0);
      }
    $('body').on('click', '.add-rip-button', function(e){ 
        
        e.preventDefault();
        $goal_button = $(this);
        $(this).closest('form').find('.error-msg').text('');
        var goalVal = $(this).closest('form').find('.ajaxAddGoal').val();
        var form = $(this).closest('form').serialize();
        if($.trim(goalVal) != '') {
            $(".loader").addClass('loading');

            $('body').find('textarea').attr("disabled", true);
            $.ajax({
                url: $(this).closest('form').attr('action'),
                type: 'POST',
                data: form,
                success: function (response) { 
                    var data = $.parseJSON(response);
                    var target1 = '#ajaxGoal_' + data.catId;
                    var target2 = '.goalCount_' + data.catId;
                    var target3 = '.categoryAvgPer_' + data.catId;
                    var target33 = '.categoryAvgWidth_' + data.catId;
                    var target4 = '.inputDateStart_' + data.goalId;
                    var target5 = '.ripPer_' + data.ripId;
                    //var target6 = '.ripPerWidth_' + data.ripId;
                    var ripDivClass = 'progress-radial progress-radial--md progress-' + data.ripAvgPer
                    var ripPercentage = '<span>'+data.ripAvgPer+'</span>' + ' <span>%</span>';
                    $(target1).append(data.html);
                    $(target2).html(data.count + ' RIP(s)');
                    $(target3).html(data.categoryAvgPer + '%');
                    $(target33).css('width', data.categoryAvgPer);
                    $(target5).parent().removeClass().addClass(ripDivClass);
                    $(target5).html(ripPercentage);
                    //$(target6).css('width', ripPercentage);
                    $(target1).find('.comment-msg').html('');
                    $('.ajaxAddGoal').val('');        
                    $('body').find(target4).flatpickr({
                        defaultDate: new Date(data.dueDate),
                        altInput: true,
                        altFormat: "M d, Y",
                        dateFormat: "Y-m-d",
                        disableMobile : true,
                    });            
                    $("textarea[id='add-comment']").each(function() {
                        this.addEventListener("keydown", autosize);
                        this.addEventListener("paste", autosize);
                      });
                    $("textarea[name='task']").each(function() {
                        this.addEventListener("keydown", autosize);
                        this.addEventListener("paste", autosize);
                      });  
                    $('body').find(target4).val(data.dueDate);
                    $(".loader").removeClass('loading');
                    $('body').find('textarea').attr("disabled", false);  
                    $('body').find('.mention-example2').mentionsInput({
                    onDataRequest: function onDataRequest(mode, query, callback) {
                        $.getJSON('assigned/user/jsonData', function (responseData) {
                            responseData = _.filter(responseData, function (item) {
                                return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
                            });
                            callback.call(this, responseData);
                        });
                    }
                });
                    displayGoalNotificationMsg('alert-success', 'RIP has been added successfully.');                      
                }  
            });
        }
        else{ 
            $(this).closest('form').find('.error-msg').text('Please Enter RIP');
        }
        
    });


    /* Script to  show the confirm  popup delete goal  */
    // To confirm user before deleting goal in About section

    $('body').on('click', '.deleteGoalItem', function(event) {
        event.preventDefault();        
        var current_page = window.location.pathname;  
        var currentPageArr = current_page.split('/'); 

        var confirm = $(this);  
        var url = $(confirm).attr('href');
        var goalName = $(confirm).attr('goalname');
        var cat_url = url.split('/');
        var goal_id = cat_url[4];
        var trimmedText = $.trim(goalName);
        if(trimmedText.length > 50){
            $('#confirmPopup .font-weight600').text('"'+trimmedText.substring(0, 50) + '..."');
        } else {
            $('#confirmPopup .font-weight600').text('"'+trimmedText+'"');
        }  
//        $('#confirmPopup .font-weight600').text('"'+goalName+'"');
        $('#confirmPopup .modal-title').text('Delete RIP');
        $('#confirmPopup .module_msg').text('Are you sure you want to delete ');
        $('#confirmPopup .module_name').text('RIP?');
        $('#confirmPopup').modal('show');
        $('#confirmPopup').unbind().on('click', '#confirm', function(e) {
            $(".loader").addClass('loading');
            $('#confirmPopup').modal('hide');
            $.ajax({
                url:url,
                context:confirm,
                success:function(response){
                    var data = $.parseJSON(response);
                    if(currentPageArr[1] == 'overview-rip'){ //..for overview section..
                        $i=0;
                        $(confirm).closest('#company_rips_'+data.company_id).find("input[name='due_date']").each(function() {
                            $i++;
                        })
                        //alert($i + '--'+data.company_id);
                        if($i == 1){ //..1 because row remove after count
                            $(confirm).closest('#company_rips_'+data.company_id).remove();
                        }
                        $rips_data = $('#ajaxTask').html();    
                        if ($.trim($rips_data) == ''){
                            $('#ajaxTask').after('<div class="comment-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any RIP yet!</p><p>Start creating RIP(s) from the menu bar.</p> </div></div>');
                        }
                    }
                    else if(data.count == 0){ 
                        $('#ajaxGoal_'+data.catId).find('.comment-msg').remove();               
                        $('#ajaxGoal_'+data.catId).find('.rip-divider').after('<div class="comment-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any RIP yet!</p><p>Start creating RIP(s) from the bottom bar.</p> </div></div>');
                    } 

                    $(confirm).closest('.panel-item ').remove();
                    var target = '.goalCount_' + data.catId;
                    var target2 = '.categoryAvgPer_'+ data.catId;
                    var target22 = '.categoryAvgWidth_' + data.catId;
                    var target3 = '.ripPer_' + data.ripId;
                    //var target4 = '.ripPerWidth_' + data.ripId;
                    var ripDivClass = 'progress-radial progress-radial--md progress-'+data.ripAvgPer;
                    var ripPercentage = '<span>'+data.ripAvgPer+'</span>' + ' <span>%</span>';
                    $(target).html(data.count + ' RIP(s)');
                    $(target2).html(data.categoryAvgPer + '%');
                    $(target22).css('width', data.categoryAvgPer);
                    $(target3).parent().removeClass().addClass(ripDivClass);
                    $(target3).html(ripPercentage);
                    //$(target4).css('width', ripPercentage);
                    $( ".loader" ).removeClass('loading');
                    $("#close-comments").trigger("click");  
                    displayGoalNotificationMsg('alert-danger', 'RIP has been deleted successfully.');
                }
            });
        });
    });

    // Script to update the due date of goal
    $('body').on('change', '.dueDate', function(){
        var target = $(this).closest('form');
        var data = target.serialize();
        var url = target.attr('action');
        $( ".loader" ).addClass('loading');      
         
        $.ajax({
            url:url,
            data:data,
            success:function (response) {
                $( ".loader" ).removeClass('loading');
                if(response > 0)
                $('.notification-count').show();
                displayGoalNotificationMsg('alert-success', 'RIP due date has been updated.');
            }
        });
    });

    //Script to update the goal name
    $('body').on('click', '#editRipTabButton', function(){ 
        $(this).parent().find('.error-msg').text('');
        
        $ajaxEditGoal = $('#editRipTab .ajaxAddGoal').val();      
        if($.trim($ajaxEditGoal) != ''){
            var goal_id = $('#editRipTab').find('input[name=goal_id]').val();
            var url = $(this).closest('form').attr('action');
            $(".loader").addClass('loading');
            $.ajax({
                url:url,
                data: { 'name': $ajaxEditGoal, 'goal_id': goal_id, _token: $("input[name='_token']").val()},
                method: "POST",
                success:function (response) {                    
                    $('#editRipTabModal').modal('hide');                
                    $('.goalName_' + response.id).html(response.name);
                    $('#editRipTabModal').html('');
                    $(".loader").removeClass('loading');
                    displayGoalNotificationMsg('alert-success', 'RIP has been updated successfully.');
                }
            });
        }
        else{
            $('#editRipTab').find('#name-err').text('Please Enter RIP Name');
        }   
    });

    //...edit goal progress
    $('body').on('click', '#goal_progress', function(){
        $('#updateripprogess').html('');
        $.ajax({
            url: $(this).attr('href'),
            method: "GET",
            success: function (response) {
                $('#updateripprogess').html(response); 
            }
        });
    });
    
    // Bind enter key to Edit Rip form
    $('body').on("keypress", "#editRipTabModal", function(e) {
        if(e.which == 13) {
            e.preventDefault();
           $('#editRipTabButton').click();
        }
    });
    
    // Show Assigned User List Modal Pop Up
    $('body').on('click','.goal-user-list', function(e) {
        $('#showUserListModal').html('');
        var goal_id;
        goal_id = $(this).parent().attr('goalId');
        $.ajax({
            url: APP_URL + '/goal/showAssignedUser',
            data: {goalId: goal_id},
            success: function (response) {
                $('#showUserListModal').html(response);
            }
        })    
    });
    
    $('body').on('keyup','.add-user-search', function () {    
        var value =  $('#search_user_in_rip').val().toUpperCase();
        $("#userList .user-lists").each(function() {
            if ($(this).text().toUpperCase().search(value) > -1) {
                $(this).show();
            }
            else {
                $(this).hide();
            }
        }); 
    });    

    $('body').on('click',"input[name='addUserToGoal']", function () { 
        
        $old_users  = $(this).parents('.modal-content').find("#old_users").val();        
        $current_checkbox = $(this).is(":checked");    

        $total = 0;
        $current_user = '';
        $.each($("input[name='addUserToGoal']:checked"), function(){            
            $(this).prop('checked', false); 
            
        });
        if($current_checkbox){
            $(this).prop('checked', true);
            $current_user = $(this).val();  
            $total = 1;
        } 
        if($old_users != $current_user){
           $(".invite-goal").attr("disabled", false); 
        } 
        else{
            $(".invite-goal").attr("disabled", true); 
        }  

        $(this).parents('.modal-content').find('.selected-user-items').html($total+' Selected');
    }); 

    function displayGoalNotificationMsg($type, $msg){ 
        if($type == 'alert-success'){
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
            $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-success-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
        else{ 
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
             $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-error-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
    } 


    $('#rip_overview').change(function () {
        var id = $(this).val();
        location.assign(APP_URL + '/overview-rip/' + id);
    });

    $('body').on('change','.dueDate', function(){
        $date = $(this).val();
        $(this).flatpickr({
          //minDate:new Date(),
          defaultDate: $date,
          dateFormat: "M d, Y"
        });        
    });
    
});
