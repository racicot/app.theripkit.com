$(document).ready(function () {
    //select2 initi
    $('select').chosen();

    //datatables
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true
    });

});