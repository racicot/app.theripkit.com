$('document').ready(function () {

    /* Script to add user to new goal  */
    $(".addUserToNewGoal").chosen();

    /* Script to add user to added task  */
    $(".addedTaskUserSelect").chosen();

    /* Script to add user to added task */
    $('body').on('click','.invite-task', function () {
        $current_model = $(this);
        $old_val= $current_model.parents('.modal-content').find('#old_value').val();
        var taskId = $('#userList').find('input[name=task_id]').val(); 
        var target3 = '.taskUserList_' + taskId;        
        var userList = [];
        $.each($("input[name='addUserToTask']:checked"), function(){            
            userList.push($(this).val());
        });
    
        $( ".loader" ).addClass('loading');
        $.ajax({
            url: APP_URL + '/task/assignUserToAddedtask',
            data: {task_id: taskId, user: userList},
            success: function (response) { 
                $new_val= $current_model.parents('.modal-content').find('.selected-user-items').html();
                $new_arr = $new_val.split(' ');
                var data = response;                    
                $('body').find(target3).html(data.imageHtml);                
                $( ".loader" ).removeClass('loading');
                $('body').find('.cust-msg').remove();
                $('#addusers').modal('hide');
                $('#addusers').html('');
                if($old_val <= $new_arr[0]){
                    displayTaskNotificationMsg('alert-success', 'User(s) has been successfully assigned to a Task.');
                }
                else{
                    displayTaskNotificationMsg('alert-danger', 'User(s) has been successfully removed from Task.');
                }         
            }
        }); 
    });

    /* Script to show added user to task dropdown */
    $('body').on('click','.addedTaskUser', function () {
        var task_id;
        var company_id;
        task_id = $(this).attr('taskId');
        company_id = $(this).attr('companyId');
        $.ajax({
            url: APP_URL + '/task/userList',
            data: {taskId: task_id, companyId: company_id},
            success: function (response) {
               $('#addusers').html(response);
            }
        });
    });

    /* Script to remove user from task dropdown as well as remove user image from task */
    $('body').on('click', '.task-media-delete', function (e) {
        e.preventDefault();
        var goalId = $(this).closest('.add-user-dropdown-list').attr('data-goalid');
        var url = $(this).find('a').attr('href');
        var target = $(this).parent().parent().parent();
        $( ".loader" ).addClass('loading');
        $.ajax({
            url: url,
            success: function (response) {
                var data = response;
                var target3 = '.taskUserList_' + data.task_id;
                var target4 = '.taskUserList_' + data.task_id;
                var target5 = '.addedUserTaskList_' + data.task_id;
                $(target).html(data.dropdownHtml);
                $(target3).html(data.imageHtml);
                $( ".loader" ).removeClass('loading');
                $('body').find('.cust-msg').remove();
                $(target5).before('<span class="sucess-msg cust-msg">User deleted successfully</span>');
            }
        });
    });


    // add task on enter press

    
    $('body').on('keyup blur', '.ajaxAddTask, .taskName', function(){  
        if($.trim($(this).val()) != ''){
            $(this).parent().find('.error-msg').text('');
        }
    })

    $('body').on('click', '.add_task_button', function(){  
        $(this).parent().find('.error-msg').text('');      
        $ajaxAddTask = $(this).parent().find('.ajaxAddTask').val(); 
        $task_button = $(this);
        if($.trim($ajaxAddTask) != ''){
            var form = $(this).parent().serialize();           
            $(".loader").addClass('loading');
            $(this).attr("disabled", true);
            $.ajax({
                url: $(this).parent().attr('action'),
                type: 'POST',
                data: form,
                success: function (response) { 
                    let data = response;
                    let target1 = '#ajaxTask_' + data.goalId;
                    let target2 = '.taskCount_' + data.goalId;
                    $(target1).append(data.html);
                    $(target2).html('<span>'+data.count + ' Tasks</span>');
                    $('.ajaxAddTask').val("").css("height", "20px");       
                    $('.addCommentSection').val("").css("height", "20px");                                 
                    $(".loader").removeClass('loading');
                    $task_button.removeAttr("disabled");
                    $task_button.parents('.tabs-tasks').find('.blank-msg').html('');
                    $(".loader").removeClass('loading');
                    
                    displayTaskNotificationMsg('alert-success', 'Task has been added successfully.');
                   }
            });            
        }
        else{
            $(this).parent().find('.error-msg').text('Please Enter Task');
            return false;
        }
    });
     var checkCompanyTask = function(id){
        var taskList = $(".company_"+id);
        if(taskList){
            var taskLength = taskList.length;
            if(taskLength == 0){
                $(".companyDiv_"+id).remove();
                var overViewLength = $(".taskLength").length
                if(overViewLength == 0)
                $(".noTaskOverview").css("display", "block");
            }
        }    
     }
    // Scropt to deleting task in RIP section
    $('body').on('click', '.deleteTaskItem', function(e) {
        e.preventDefault();
        var current_page = window.location.pathname;  
        var currentPageArr = current_page.split('/'); 
        var current = $(this);
        var url = $(current).attr('href');
        var companyid = $(current).attr('companyname');
        var task_url = url.split('/');
        var task_id = task_url[4];
        var task_name = $('.taskName_'+task_id).text();
        var trimmedText = $.trim(task_name);
        
        $(".company_"+companyid).parent().addClass("companyDiv_"+companyid);
        $('#confirmPopup .modal-title').text('Delete Task');
        $('#confirmPopup .module_msg').text('Are you sure you want to delete ');
        if(trimmedText.length > 50){
            $('#confirmPopup .font-weight600').text('"'+trimmedText.substring(0, 50) + '..."');
        } else {
            $('#confirmPopup .font-weight600').text('"'+trimmedText+'"');
        } 
        $('#confirmPopup .module_name').text('task?');        
        $('#confirmPopup').modal('show');
        $('#confirmPopup').unbind().on('click', '#confirm', function(e) {
            $(".loader").addClass('loading');
            $('#confirmPopup').modal('hide');
            $.ajax({
                url: url,
                context: current,
                success: function (response) {
                    var data = response;
                    $(current).closest('.n-rip-card').remove();
                    var target = '.taskCount_' + data.goalId;
                    $(target).find('span').html(data.count + ' Tasks');
                    $(".loader").removeClass('loading');

                    if(data.count == 0){ 
                        $('#ajaxTask_'+data.goalId).find('.blank-msg').remove();               
                        $('#ajaxTask_'+data.goalId).html('<div class="blank-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any Task yet!</p><p>Start creating Tasks from the bottom bar.</p> </div></div>');
                    }
                    displayTaskNotificationMsg('alert-danger', 'Task has been deleted successfully.');
                    $('#taskStart_'+task_id).remove();
                    if(currentPageArr[1] == 'overview-task'){ //..for overview section..
                        checkCompanyTask(companyid);
                    }    
                }
            });
        });
    });

    // Script to complete/confirm the task
    $('body').on('click','.js-task-comp', function() {
        var taskRow = $(this).closest('.task-row-wrap');
        taskRow.addClass('task-complete');

        $('body').on('click','.js-cancel-task-confirm', function() {
            var taskRow = $(this).closest('.task-row-wrap');
            if (taskRow.hasClass('task-complete')) {
                taskRow.removeClass('task-complete');
            }
        });
        $('body').on('click','.js-task-confirm', function() {
            var taskId = $(this).closest('.task-row-wrap').attr('taskid');
            $( ".loader" ).addClass('loading');
            var current_page = window.location.pathname;    
            var currentPageArr = current_page.split('/');
            $.ajax({
                url:APP_URL+'/taskComplete',
                data:{taskId:taskId,current_page:currentPageArr[1]},
                context: this,
                success:function(response){
                    var target = '#taskStart_' + response.taskId
                    $('body').find(target).html(response.taskHtml);
                    
                    
                    if($(target).closest('#ajaxTask').hasClass('todoTasks')){
                        $(target).find('.n-avatar-wrap').find('.dropdown').remove();
                        $(target).find('.del-task').remove();
                    }
                    $( ".loader" ).removeClass('loading');
                    $('body').find(target).find('.task-row-wrap').addClass("task-completing");
                    
                    displayTaskNotificationMsg('alert-success', 'Task is marked as Completed successfully.');
                }
            });
        });
        $('body').on('click','.todo-task-confirm', function() {
            var taskId = $(this).closest('.task-row-wrap').attr('taskid');
            var currId = $(this).closest('.task-row-wrap').attr('current_id');
            $.ajax({
                url:APP_URL+'/taskComplete',
                data:{taskId:taskId, current_id:currId},
                context: this,
                success:function(response){
                    var data = response;
                    var target = '#taskStart_' + response.taskId
                    $(this).closest('.task-row-wrap').html(response.taskHtml);
                    $( ".loader" ).removeClass('loading');
                }
            });
        });
    });

    // Script to incomplete/undo/confirm the task
    $('body').on('click','.js-task-undo', function() { 
        var taskRow = $(this).closest('.task-row-wrap');
        taskRow.addClass('task-undo');

        $('body').on('click','.js-cancel-task-undo', function() {
            var taskRow = $(this).closest('.task-row-wrap');
            if (taskRow.hasClass('task-undo')) {
                taskRow.removeClass('task-undo');
            }
        });
        $('body').on('click','.js-task-undo-icon', function() {
            var taskId = $(this).closest('.task-row-wrap').attr('taskid');
            $( ".loader" ).addClass('loading');
            var current_page = window.location.pathname;  
            var currentPageArr = current_page.split('/');      
            $.ajax({
                url:APP_URL+'/taskUndo',
                data:{taskId:taskId,current_page:currentPageArr[1]},
                context: this,
                success:function(response){
                    var target = '#taskStart_' + response.taskId
                    $('body').find(target).html(response.taskHtml);
                    if($(target).closest('#ajaxTask').hasClass('todoTasks')){
                        $(target).find('.n-avatar-wrap').find('.dropdown').remove();
                        $(target).find('.del-task').remove();
                    }
                    $( ".loader" ).removeClass('loading');
                    displayTaskNotificationMsg('alert-success', 'Task is marked as Incompleted successfully.');
           
                }
            });
        });

    });

    //Script to update the task name
    $('body').on('click', '.editTaskButton', function(){
        $(this).parent().find('.error-msg').text('');
        $ajaxEditTask = $('#editTask .taskName').val();  
        if($.trim($ajaxEditTask) != ''){
            var data = { 'name': $ajaxEditTask, 'task_id': $('#editTask').find('input[name=task_id]').val(), _token: $("input[name='_token']").val()}
            var url = APP_URL + '/taskName';
            $.ajax({
                url:url,
                data:data,
                success:function (response) {
                    $('#editModal').modal('hide');                
                    $('.taskName_' + response.id).html(response.name);
                    $('#editModal').html('');
                    displayTaskNotificationMsg('alert-success', 'Task has been updated successfully.');
                }
            });
        }
        else{
            $('#editTask').find('#name-err').text('Please Enter Task');
        }    
    });

    // Show complete and incomplete task in RIP
    $('body').on('click', '.taskStatus', function(e){
        e.preventDefault();
        $(".loader").addClass('loading');
        $.ajax({
            url:$(this).attr('href'),
            success:function(response){
                var data = response;
                var target = '#ajaxTask_' + data.goalId;
                var target1 = '#goalStart_' + data.goalId;
                $(target).html(data.html);
                if($(target1).find('.panel-item').hasClass('opened-goals')){}
                else {
                    $(target1).find('.fa-chevron-right').trigger('click');
                }
                $( ".loader" ).removeClass('loading');
            }
        });
    });

    $('body').on('click','.task-user-list', function(e) { 
        $('#showUserListModal').html('');
        $task_id = $(this).parent().attr('taskid');
        $.ajax({
            url: APP_URL + '/task/showAssignedUser',
            data: {task_id: $task_id},
            success: function (response) {
                $('#showUserListModal').html(response);
            }
        })    
    });

    $('body').on('click', '.edittaskLink', function(e){
        $('#editModal').html('');
        $.ajax({
            url: $(this).attr('href'),
            method: "GET",
            success: function success(response) {
                $('#editModal').html(response);
                
            }
        });
    });


    $('body').on('click',"input[name='addUserToTask']", function () { 
        
        $total = 0;
        $new_user_arr = [];
        $old_users_arr = [];
        $show_invite_task = 0;
        $old_users  = $(this).parents('.modal-content').find("#old_users").val();  

        if($.trim($old_users) != ''){
            $old_users_arr = $old_users.split(',');
        }
        $.each($("input[name='addUserToTask']:checked"), function(){ 
            $new_user_arr[$total] = $(this).val();                  
            $total++;
        });
        if($old_users_arr.length == $new_user_arr.length){
            for($ii=0; $ii<$old_users_arr.length; $ii++){
                $isexist = 0;
                for($jj=0; $jj< $new_user_arr.length; $jj++){
                    //alert('--'+$.trim($old_users_arr[$ii])+'--'+$.trim($new_user_arr[$jj])+'--');
                    if($.trim($old_users_arr[$ii]) == $.trim($new_user_arr[$jj])){
                        $isexist = 1;
                    }
                }
                if($isexist == 0){
                    $show_invite_task = 1;
                }
            }
        }
        else{
            $show_invite_task = 1;
        }
        if($show_invite_task == 1){
           $(".invite-task").attr("disabled", false); 
        } 
        else{
            $(".invite-task").attr("disabled", true); 
        }  
        $(this).parents('.modal-content').find('.selected-user-items').html($total+ ' Selected');
    });

    $('body').on('click',".rip-active, .comment-active", function () { 
        $(this).parents('.panel-item').find('.ajaxAddTask').val('');
        $(this).parents('.panel-item').find('.addCommentSection').val('');
    });    

    /* Function for display success/error notificatios 
        @inout: type, msg
    */
    function displayTaskNotificationMsg($type, $msg){ 
        if($type == 'alert-success'){
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
            $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-success-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
        else{ 
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
             $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-error-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
    }

});
