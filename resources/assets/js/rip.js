$("document").ready(function() {
  $("#checkForRecordGoal").hide();
  flatpickerDate();

  $("body").on("click", ".select-all", function() {
    var selectAllContainer = $(this).closest("div.label.checkbox");
    var checkboxList = $(selectAllContainer).siblings("#checkboxList");
    if ($(this).is(":checked")) {
      //$(checkboxList).find('input[type="checkbox"]').prop('checked', true);
      $(checkboxList)
        .find('input[type="checkbox"]')
        .each(function() {
          if (!$(this).is(":disabled")) {
            $(this).prop("checked", true);
          }
        });
    } else {
      $(checkboxList)
        .find('input[type="checkbox"]')
        .each(function() {
          if (!$(this).is(":disabled")) {
            $(this).prop("checked", false);
          }
        });
    }
  });

  // Check/uncheck permission group checkbox on individual permission checkbox click
  $("body").on("click", 'input[type="checkbox"]', function(e) {
    var check = true;
    var parentCheckbox = $(this)
      .closest("#checkboxList")
      .siblings("div.label.checkbox")
      .find('input[type="checkbox"]');
    var siblings = $(this)
      .closest("#checkboxList")
      .find('input[type="checkbox"]')
      .each(function() {
        if (!$(this).is(":checked")) {
          check = false;
          return false;
        }
      });
    if (check) parentCheckbox.prop("checked", true);
    else parentCheckbox.prop("checked", false);
  });

  $("body").on("show.bs.collapse", ".panel-group", function() {
    $(this).addClass("opened");
    $(this)
      .find(".n-panel-heading")
      .addClass("colored-header");
  });
  $("body").on("hide.bs.collapse", ".panel-group", function() {
    $(this).removeClass("opened");
    $(this)
      .find(".n-panel-heading")
      .removeClass("colored-header");
  });

  //Script add new RIP in system

  $("body").on("submit", "#addRip", function(e) {
    if ($.trim($("#addRip #inputRipName").val()) == "") {
      $(".err_rip").html("Please enter a valid RIP Name.");
      return false;
    }
    if ($.trim($("#addRip .inputDateStart").val()) == "") {
      $(".err_start").html("Please enter a valid start date.");
      return false;
    }
    if ($.trim($("#addRip .inputDateEnd").val()) == "") {
      $(".err_end").html("Please enter a valid end date.");
      return false;
    } else {
      e.preventDefault();
      var url = $(this).attr("action");
      var data = $(this).serialize();
      var counter = 0;
      $.each($(this).serializeArray(), function(i, field) {
        if ($.trim(field.value) == "") {
          counter++;
        }
      });
      if (counter == 0) {
        $("body")
          .find(".alert-danger")
          .remove();
        $(".loader").addClass("loading");
        $.ajax({
          url: url,
          type: "post",
          data: data,
          success: function(response) {
            if (response.status != "Fail") {
              var target = ".addNew_" + response.company_id;
              var test = $(target).before(response.link);
              var return_url = response.url;
              $("#addRip").modal("hide");
              if (return_url !== undefined) {
                window.location.replace(return_url);
              }
            } else {
              //..if duplicate rip period, display error message.
              $(".loader").removeClass("loading");
              $("body")
                .find(".alert-danger")
                .remove();
              $("#addRip").before(
                '<div class="alert alert-danger offset4 span4">' +
                  response.message +
                  "</div>"
              );
            }
          }
        });
      } else {
        $("body")
          .find(".alert-danger")
          .remove();
        $("#addRip").before(
          "<div class='alert alert-danger offset4 span4'>Please fill all the fields</div>"
        );
      }
    }
  });

  window.setTimeout(function() {
    $(".alert")
      .fadeTo(500, 0)
      .slideUp(500, function() {
        $(this).remove();
      });
  }, 5000);

  // Script edit the data of the RIP using ajax
  $("body").on("click", "#editRipPeriodButton", function(e) {
    if ($.trim($("#editRipPeriod #inputRipName").val()) == "") {
      $(".err_rip").html("Please enter a valid RIP Name.");
      return false;
    }
    if ($.trim($("#editRipPeriod .inputEditDateStart").val()) == "") {
      $(".err_start").html("Please enter a valid start date.");
      return false;
    }
    if ($.trim($("#editRipPeriod .inputEditDateEnd").val()) == "") {
      $(".err_end").html("Please enter a valid end date.");
      return false;
    } else {
      $("body")
        .find(".alert-danger")
        .remove();
      $(".loader").addClass("loading");
      var rip_name = $.trim($("#editRipPeriod #inputRipName").val());
      var start_date = $.trim($("#editRipPeriod .inputEditDateStart").val());
      var end_date = $.trim($("#editRipPeriod .inputEditDateEnd").val());
      var rip_id = $("#editRipPeriod #rip_period_id").val();
      var url = "/editRip";
      var data = {
        name: rip_name,
        id: rip_id,
        start_date: start_date,
        end_date: end_date,
        _token: $("input[name='_token']").val()
      };
      $.ajax({
        url: url,
        method: "POST",
        data: data,
        success: function(response) {
          if (response.status != "Fail") {
            $(".loader").removeClass("loading");
            $("#editRipModal").modal("hide");
            //                      // Update Text on Rip View
            $(".editRip_" + response.id)
              .find(".rip-heading")
              .text(response.name);
            $(".editRip_" + response.id)
              .find(".rip-date-header #date-header")
              .text(response.start_date + " - " + response.end_date);
            $(".editRip_" + response.id)
              .find(".rip-date-header #left-days")
              .text(response.left);
            // Update text on Rip Panel
            $(".ripTab_" + response.id)
              .find(".expired-title .exp-days")
              .text(response.name);
            $(".ripTab_" + response.id)
              .find(".rip-date-header #date-header-menu")
              .text(response.start_date + " - " + response.end_date);
            $(".ripTab_" + response.id)
              .find(".rip-date-header #left-days-menu")
              .text(response.left);

            $("body")
              .find("input[name='due_date']")
              .each(function() {
                $date = $(this).val();
                if (
                  new Date($date) < new Date(response.start_date) ||
                  new Date($date) > new Date(response.end_date)
                ) {
                  $(this)
                    .parent()
                    .parent()
                    .find(".error-msg")
                    .html("Due date is past/ahead of the RIP.");
                  $(this).addClass("error");
                } else {
                  $(this)
                    .parent()
                    .parent()
                    .find(".error-msg")
                    .html("");
                  $(this).removeClass("error");
                }
              });
            $("#editRipModal").html("");
            displayRipNotificationMsg(
              "alert-success",
              "RIP Period has been updated successfully."
            );
          } else {
            //..if duplicate rip period, display error message.
            $(".loader").removeClass("loading");
            $("body")
              .find(".alert-danger")
              .remove();
            $("body #editRipPeriod").before(
              '<div class="alert alert-danger offset4 span4">' +
                response.message +
                "</div>"
            );
          }
        }
      });
    }
  });

  // Script show the data of the RIP
  $("body").on("click", ".ripTab", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    $(".nav-tabs--custom li").removeClass("active");
    $(".loader").addClass("loading");
    $.ajax({
      url: url,
      context: this,
      success: function(response) {
        $("#tabContent").html(response);
        $(this)
          .parent()
          .addClass("active");
        $("#goal-per-input").keyup(function(e) {
          if ($.isNumeric($(this).val())) {
            if ($(this).val() > 100) $(this).val("100");
          } else {
            $(this).val("");
          }
        });
        $(".goal-tab-percentage").keyup(function(e) {
          if ($.isNumeric($(this).val())) {
            if ($(this).val() > 100) $(this).val("100");
          } else {
            $(this).val("");
          }
        });

        flatpickerDate();
        $(".loader").removeClass("loading");
      }
    });
  });
  $("body").on("click", ".viewHistoryRip", function(e) {
    $(".loader").addClass("loading");
    e.preventDefault();
    var url = $(this).attr("href");
    $.ajax({
      url: url,
      context: this,
      success: function(response) {
        $("#tabContent").html(response);
        $("#tabContent")
          .find(".add-icon")
          .remove();
        $("#tabContent")
          .find(".rip-expired")
          .remove();
        $("#tabContent")
          .find(".view-history-rip")
          .show();
        $("#goal-per-input").keyup(function(e) {
          if ($.isNumeric($(this).val())) {
            if ($(this).val() > 100) $(this).val("100");
          } else {
            $(this).val("");
          }
        });
        $(".goal-tab-percentage").keyup(function(e) {
          if ($.isNumeric($(this).val())) {
            if ($(this).val() > 100) $(this).val("100");
          } else {
            $(this).val("");
          }
        });

        flatpickerDate();
        $(".loader").removeClass("loading");
      }
    });
  });

  // Script use to delete the RIP
  $("body").on("click", "#deleteTab", function(e) {
    e.preventDefault();
    var current = $(this);
    var url = $(current).attr("href");
    $("#confirmPopup .font-weight600").text(
      '"' + $("#rip_content").text() + '"'
    );
    $("#confirmPopup .modal-title").text("Delete RIP Period");
    $('#confirmPopup .module_msg').text('Are you sure you want to delete ');
    $("#confirmPopup .module_name").text("RIP Period?");
    $("#confirmPopup").modal("show");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $(".loader").addClass("loading");
        $("#confirmPopup").modal("hide");
        $.ajax({
          url: url,
          success: function(response) {
            if (response != undefined || response != "") {
              window.location.replace(response);
            }
          }
        });
      });
  });

  //Script to swith to history section and back to current section of Rip
  $("body").on("click", ".rip-history", function(e) {
    $("#serach_ripperiod").val("");
    $("#serach_ripperiod").keyup();
    $(".rip-period").html("Expired RIP Period(s)");
    $(".rip-listing-sidebar").hide();
    $(".rip-download-links").hide();
    $("#expired-rip-listing").show();
    $(".back-to-rip").show();
    $(".no-expired-rip").show();
    $(".no-active-rip").hide();
    $(".footer-rip-button").hide();
  });

  // Common code for riptab edit/add, Goal duedate
  function flatpickerDate() {
    var current_date = new Date();
    $(".inputDateStart").flatpickr({
      minDate: new Date(),
      altInput: true,
      altFormat: "M d, Y",
      dateFormat: "Y-m-d",
      disableMobile : true,
      onChange: function(selectedDates, dateStr, instance) {
        $(".inputDateEnd").removeAttr("disabled");
        $(".inputDateEnd").flatpickr({
          minDate: $(".inputDateStart").val(),
          altInput: true,
          altFormat: "M d, Y",
          dateFormat: "Y-m-d",
          disableMobile : true
        });
      }
    });

    var end_date = $("body")
      .find(".inputEditDateEnd")
      .val();
    var start_date = $("body")
      .find(".inputEditDateStart")
      .val();

    $("body")
      .find(".inputEditDateStart")
      .flatpickr({
        minDate: start_date,
        altInput: true,
        altFormat: "M d, Y",
        dateFormat: "Y-m-d",
          disableMobile : true
      });
    $("body")
      .find(".inputEditDateEnd")
      .flatpickr({
        minDate: start_date,
        altInput: true,
        altFormat: "M d, Y",
        dateFormat: "Y-m-d",
          disableMobile : true
      });
    $("body")
      .find(".inputEditDateStart")
      .change(function() {
        var change_date = $("body")
          .find(".inputEditDateStart")
          .val();
        var change_enddate = $("body")
          .find(".inputEditDateEnd")
          .val();
        $("body")
          .find(".inputEditDateEnd")
          .val(change_enddate);
        $("body")
          .find(".inputEditDateEnd")
          .flatpickr({
            defaultDate: end_date,
            minDate: change_date,
            altInput: true,
            altFormat: "M d, Y",
          disableMobile : true
          });
      });
    $("body")
      .find(".inputEditDateEnd")
      .change(function() {
        var st_date = $("body")
          .find(".inputEditDateStart")
          .val();
        $("body")
          .find(".inputEditDateStart")
          .val(st_date);
        $("body")
          .find(".inputEditDateStart")
          .flatpickr({
            minDate: st_date,
            defaultDate: st_date,
            maxDate: $("body")
              .find(".inputEditDateEnd")
              .val(),
            altInput: true,
            altFormat: "M d, Y",
          disableMobile : true
          });
      });
  }

  //...due date date picker for overview-rip page load
  var current_page = window.location.pathname;
  var currentPageArr = current_page.split("/");
  if (currentPageArr[1] == "overview-rip") {
    $("body")
      .find("input[name='due_date']")
      .each(function() {
        $date = $(this).val();
        $(this).flatpickr({
          //minDate:new Date(),
          defaultDate: $date,
          dateFormat: "M d, Y",
          disableMobile : true
        });
      });
  }

  //...due date date picker for rip page on click
  $("body").on("click", ".accordionDiv", function(e) {
    $(this)
      .parents(".checkForRecord")
      .find("input[name='due_date']")
      .each(function() {
        $date = $(this).val();
        $(this).flatpickr({
          //minDate:new Date(),
          defaultDate: $date,
          dateFormat: "M d, Y",
          disableMobile : true
        });
      });
  });

  $("#planSelect").chosen();
  $("#userSelect").chosen();

  function confirmResult(confirm, e) {
    if (confirm == 1) {
      return true;
    } else {
      e.preventDefault();
      return false;
    }
  }

  $("body").on("click", ".rip-dropdown", function(e) {
    e.preventDefault();
    $(this)
      .siblings(".dropdown-menu")
      .show();
  });

  $("body").on("click", ".close-dropdown", function() {
    $(this)
      .closest(".dropdown-menu")
      .hide();
    $(this)
      .closest(".dropdown-menu")
      .find(".cust-msg")
      .remove();
    if (
      $(this)
        .closest(".dropdown-menu")
        .find("form")
        .attr("id") == "addRip"
    ) {
      $(this)
        .closest(".dropdown-menu")
        .find('input[name="name"]')
        .val("");
      $(this)
        .closest(".dropdown-menu")
        .find('input[name="start_date"]')
        .val("");
      $(this)
        .closest(".dropdown-menu")
        .find('input[name="end_date"]')
        .val("");
    }
  });

  // Script use to mark expired the RIP
  $("body").on("click", "#markExpired", function(e) {
    e.preventDefault();
    var current = $(this);
    var url = $(current).attr("href");
    $("#markExpiredConfirmPopup .font-weight600").text(
      '"' + $("#rip_content").text() + '"'
    );
    $("#markExpiredConfirmPopup").modal("show");
    $("#markExpiredConfirmPopup").on("click", "#expiredConfirm", function(e) {
      $(".loader").addClass("loading");
      $("#markExpiredConfirmPopup").modal("hide");
      $.ajax({
        url: url,
        success: function(response) {
          if (response != undefined || response != "") {
            window.location.replace(response);
          }
        }
      });
    });
  });

  //...rip period menu click
  $("body").on("click", "#rip_menu", function() {
    $("#serach_ripperiod").val("");
    $("#serach_ripperiod").keyup();
    $(".rip-period").html("RIP Period(s)");
    $(".rip-listing-sidebar").show();
    $(".rip-download-links").show();
    $("#expired-rip-listing").hide();
    $(".back-to-rip").hide();
    $(".no-expired-rip").hide();
    $(".no-active-rip").show();
    $(".footer-rip-button").show();
    $(".rip-column").mCustomScrollbar();
    $(".wrapper-overlay").show();
    var hashes = window.location.href
      .slice(window.location.href.indexOf("?") + 1)
      .split("&");
    var hash = hashes[0].split("/rip/");
    if (hash[1] > 0) {
      $(".ripTab_" + hash[1])
        .parent()
        .addClass("active ");
    } else {
      var hash = hashes[0].split("/viewHistoryRip/");
      if (hash[1] > 0) {
        $(".ripTab_" + hash[1])
          .parent()
          .addClass("active ");
      }
    }
    $("#ripMenu_list").removeClass("rip-col");
    $("#ripMenu_list").addClass("rip-col-active");
    $(".navbar-static-top").addClass("rip-navbar");
    $(".wrapper").addClass("rip-wrapper");
    $("body").removeClass("sidebar-open");
    $("body").addClass("sidebar-collapse");
    $(".to-do-lists").removeClass("show");
    $(".notification-sidebar-full").removeClass("show");
    var parentHeight = $(".nav-tabs-rip").innerHeight();
    $(".nav-tabs-rip li.rip-column").height(parentHeight - 126);
  });

  //..close rip menu
  $("body").on("click", ".close_rip", function() {
    $("#ripMenu_list").removeClass("rip-col-active");
    $("#ripMenu_list").addClass("rip-col");
    $(".navbar-static-top").removeClass("rip-navbar");
    $(".wrapper").removeClass("rip-wrapper");
    $(".wrapper-overlay").hide();
  });

  //...show back rip list
  $("body").on("click", "#back_ro_rip", function() {
    $("#rip_menu").click();
  });

  //..rip period menu search
  $("body").on("keyup", "#serach_ripperiod", function() {
    var value = $(this)
      .val()
      .toUpperCase();
    $(".rip-listing-sidebar .rip-box").each(function() {
      if (
        $(this)
          .text()
          .toUpperCase()
          .search(value) > -1
      ) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });

    $("#expired-rip-listing .rip-box").each(function() {
      if (
        $(this)
          .text()
          .toUpperCase()
          .search(value) > -1
      ) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  });

  // JS to call Edit RIP Period Modal
  $(".editRipLink").on("click", function() {
    $("#editRipModal").html("");
    $.ajax({
      url: $(this).attr("href"),
      method: "GET",
      success: function success(response) {
        $("#editRipModal").html(response);
        flatpickerDate();
      }
    });
  });

  window.setTimeout(function() {
    $(".alert-msg")
      .fadeTo(500, 0)
      .slideUp(500, function() {
        $(this).remove();
      });
  }, 5000);

  $("body").on("click", ".add-rip", function(e) {
    $(".error-msg").html("");
    $("#inputRipName").val("");
    $(".inputDateStart").val("");
    $(".inputDateEnd").val("");
    $("body")
      .find("#addripPeriod .alert-danger")
      .remove();
  });

  // Bind enter key to Edit Rip Period form
  $("body").on("keypress", "#editRipModal", function(e) {
    if (e.which == 13) {
      $("#editRipPeriodButton").click();
    }
  });

  // JS to call Edit RIP Modal
  $("body").on("click", ".editRipTabLink", function(e) {
    $("#editRipTabModal").html("");
    $.ajax({
      url: $(this).attr("href"),
      method: "GET",
      success: function success(response) {
        $("#editRipTabModal").html(response);
      }
    });
  });

  $("body").on("click", ".fa-angle-right", function() {
    $(this)
      .parent()
      .parent()
      .parent()
      .parent()
      .parent()
      .find(".error-msg")
      .text("");
  });

  // will if no categories(in rip section) found and show no record found page
  if ($(".checkForRecord")[0]) {
    $("#checkForRecordGoal").hide();
  } else {
    $("#checkForRecordGoal").show();
  }

  function displayRipNotificationMsg($type, $msg) {
    if ($type == "alert-success") {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-success-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    } else {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-error-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    }
  }
});
