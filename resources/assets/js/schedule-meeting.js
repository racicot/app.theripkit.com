$("document").ready(function() {
  new ClipboardJS(".copy-link");
  // Remove validation messages on modal show
  $("#scheduleForRip").on("click", function() {
    $("#agenda_err").hide();
    $("#meetingDate_err").hide();
    $("#meetingTime_err").hide();
    $("#duration_err").text("");
  });
  $("#scheduleMeeting").on("show.bs.modal", function() {
    $("#agenda").css("height", "20px");
    $(".meetingDate").flatpickr({
      altInput: true,
      altFormat: "M d, Y",
      minDate: "today",
      dateFormat: "Y-m-d"
    });
    $(".meetingTime").flatpickr({
      enableTime: true,
      noCalendar: true,
      minuteIncrement: 30,
      dateFormat: "H:i",
      time_24hr: true
    });
    $("#agenda").val("");
    $("#duration").val("");
    $(".meetingDate").val("");
    $(".addParticipants").empty();
    addUser();
  });
  $(".meetingDate").on("change", function() {
    $("#meetingDate_err").hide();
  });
  $(".meetingTime").on("change", function() {
    $("#meetingTime_err").hide();
  });
  $("#agenda").on("blur change keypress keyup", function() {
    if (!$("#agenda").val()) {
      $("#agenda_err").text("Meeting Title is required");
      $("#agenda_err").show();
    } else {
      $("#agenda_err").hide();
    }
  });

    $("#duration").on("keypress keyup blur",function (event) {
        $(this).val($(this).val().replace(/[^\d].+/, ""));
        if($(this).val() > 600){
            $("#duration_err").text('Duration should not be greater than 600')
        } else {
            $("#duration_err").text('')
        }
        if ((event.which < 48 || event.which > 57)) {
            event.preventDefault();
        }
        if($(this).val() < 1 ){
            $("#duration_err").text('Duration should be greater than 1 minute')
        } else {
            $("#duration_err").text('')
        }
     });

  var volidateDateTime = function() {
    var meetingdate = true;
    var meetingtime = true;
    var agenda = true;
    var duration = true;
    if (!$("#agenda").val()) {
      $("#agenda_err").text("Meeting Title is required");
      $("#agenda_err").show();
      agenda = false;
      return false;
    } else {
      agenda = true;
      $("#agenda_err").hide();
    }
    if (!$(".meetingDate").val()) {
      $("#meetingDate_err").text("Meeting date is required");
      $("#meetingDate_err").show();
      meetingdate = false;
      return false;
    } else {
      meetingdate = true;
      $("#meetingTime_err").hide();
    }
    if (!$(".meetingTime").val()) {
      $("#meetingTime_err").text("Meeting time is required");
      $("#meetingTime_err").show();
      meetingtime = false;
      return false;
    } else {
      meetingtime = true;
      $("#meetingTime_err").hide();
    }
    if (!$("#duration").val()) {
      $("#duration_err").text("Duration is required");
      $("#duration_err").show();
      duration = false;
      return false;
    } else if ($("#duration").val() > 600) {
      $("#duration_err").text("Duration should not be greater than 600");
      duration = false;
      return false;
    } else if ($("#duration").val() < 1) {
      $("#duration_err").text("Duration should not be less than 1 minute");
      duration = false;
      return false;
    } else {
      duration = true;
      $("#duration_err").hide();
    }

    var participants = $(".participants:checked")
      .map(function() {
        return this.value;
      })
      .get();

    if (participants.length == 0) {
      $("#participants_err").text("Please select atleast one participant in order to schedule the meeting.");
      $("#participants_err").show();
    } else {
      $("#participants_err").hide();
    }

    if (
      meetingdate &&
      meetingtime &&
      agenda &&
      duration &&
      participants.length > 0
    ) {
      return true;
    } else {
      return false;
    }
  };

  $("body").on("submit", "#addMeeting", function(e) {
    e.preventDefault();
    if (!volidateDateTime()) {
      return false;
    } else {
      addMeeting();
    }
  });

    var addMeeting = function() {
      var participants = $(".participants:checked")
        .map(function() {
          return this.value;
        })
        .get();
      var data = {
        rip_id: $("#scheduleForRip").val(),
        agenda: $("#agenda").val(),
        meeting_date: $(".meetingDate").val(),
        meeting_time: $(".meetingTime").val(),
        participants: participants,
        duration: $("#duration").val()
      };
      var url = "/createMeeting";
      $(".loader").addClass("loading");
      $.ajax({
        url: url,
        type: "POST",
        data: data,
        success: function(response) {
          $("#scheduleMeeting").modal("hide");
          $(".loader").removeClass("loading");
          var msg = "Meeting has been scheduled successfully";
          displayCategoryNotificationMsg("alert-success", msg);
        },
        error: function(error) {
          $("#scheduleMeeting").modal("hide");
          $(".loader").removeClass("loading");
          var msg =
            "Meeting not save, internal server error. Please try again later.";
          displayCategoryNotificationMsg("--", msg);
          console.log(error.responseText);
        }
      });
    };

    var addUser = function() {
      var url = "/getCompanyUser";
      $.ajax({
        url: url,
        type: "GET",
        success: function(response) {
          $(".addParticipants").append(response.html);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    };
    $("body").on("click", "#meetingCalenderForRip", function() {
      getMeetingList($("#meetingCalenderForRip").val());
    });
    var getMeetingList = function(ripId) {
      var url = "/getMeetings/" + ripId;
      $.ajax({
        url: url,
        type: "GET",
        success: function(response) {
          $("#meetingList").html(response.html);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    };

    $(document).on("click", ".copy-link", function() {
      msg = "Invitation link copied successfully!";
      displayCategoryNotificationMsg("alert-success", msg);
    });

    $("body").on("click", ".meetingUserList", function(e) {
      var meetingId = $(this).attr("meetingid");
      $.ajax({
        url: "/getParticipants/" + meetingId,
        success: function(response) {
          $("#showUserListModal").html(response.html);
        },
        error: function(error) {
          console.log(error.responseText);
        }
      });
    });

    $("body").on("click", "#candelMeeting", function(e) {
      e.preventDefault();
      var meetingId = $(this).attr("meetingId");
      var agenda = $(this).attr("agenda");
      var url = "/deleteMeeting/" + meetingId;
      $("#cancelMeetingModel .font-weight600").text(' "' + agenda + '"');
      $("#cancelMeetingModel").modal("show");
      $("#cancelMeetingModel")
        .unbind()
        .on("click", "#confirmCancel", function(e) {
          $(".loader").addClass("loading");
          $.ajax({
            url: url,
            success: function(response) {
              if (response.count == 0) {
                $("#meetingList").html(response.html);
              }
              $(".loader").removeClass("loading");
              $("#cancelMeetingModel").modal("hide");
              $("#meeting_" + meetingId).remove();
              msg = "Meeting has been deleted successfully";
              displayCategoryNotificationMsg("--", msg);
            },
            error: function(error) {
              $(".loader").removeClass("loading");
              $("#cancelMeetingModel").modal("hide");
              msg = "Interval server error. Please try again later";
              displayCategoryNotificationMsg("--", msg);
            }
          });
        });
    });
  
    $("body").on("keyup", ".duration", function() {
        if ($.isNumeric($(this).val())) {
          if ($(this).val() > 600) $(this).val("600");
        } else {
          $(this).val("");
        }
    });

    function displayCategoryNotificationMsg($type, $msg) {
        if ($type == "alert-success") {
            $("#displayAjaxMessage").removeClass("alert-none");
            $("#displayAjaxMessage").addClass("alert-show");
            $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
            $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
            $("#displayAjaxMessage .alert-msg-ajax").html(
              '<img src="../images/toast-success-icon.svg"> ' + $msg
            );

            window.setTimeout(function() {
              $("#displayAjaxMessage").removeClass("alert-show");
              $("#displayAjaxMessage").addClass("alert-none");
              $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
              $("#displayAjaxMessage .alert-msg-ajax").html("");
            }, 5000);
        } else {
            $("#displayAjaxMessage").removeClass("alert-none");
            $("#displayAjaxMessage").addClass("alert-show");
            $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
            $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
            $("#displayAjaxMessage .alert-msg-ajax").html(
              '<img src="../images/toast-error-icon.svg"> ' + $msg
            );

            window.setTimeout(function() {
              $("#displayAjaxMessage").removeClass("alert-show");
              $("#displayAjaxMessage").addClass("alert-none");
              $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
              $("#displayAjaxMessage .alert-msg-ajax").html("");
            }, 5000);
        }
    }
    
    // remove validation message on state change
    $("body").on("click", '.participantsList', function() {
        $("#participants_err").hide();
    })
});
