$('document').ready(function () {

    var pusher = new window.Pusher(process.env.MIX_PUSHER_APP_KEY, {
        cluster: process.env.MIX_PUSHER_CLUSTER,
        encrypted: process.env.MIX_PUSHER_ENCRYPTED,
    });


    var channel = pusher.subscribe('categories');
    $.ajaxSetup({
        beforeSend: function(xhr) {
            xhr.setRequestHeader("X-Socket-Id",pusher.connection.socket_id);

        }});
    channel.bind('add-task', function(data)  {
        $.ajax({
            url: APP_URL + '/pusher/showTask',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target1 = '#ajaxTask_' + data.goalId;
                var target2 = '.taskCount_' + data.goalId;
                $(target1).parents('.tabs-tasks').find('.blank-msg').html('');
                $(target1).append(data.html);
                $(target2).html('<span>'+data.count+' Tasks </span>');
                $('.ajaxAddTask').val('');
                $('body').find('.inputDateStart').flatpickr({
                    minDate:  new Date(),
                    onChange: function(selectedDates, dateStr, instance) {
                        $('body').find('.inputDateEnd').removeAttr('disabled')
                        $('body').find('.inputDateEnd').flatpickr({
                            minDate:$('body').find('.inputDateStart').val()
                        });
                    },
                });
            }
        });
    });


    channel.bind('task-updated', function(data_val) {
        var current_page = window.location.pathname;  
        var currentPageArr = current_page.split('/'); 
        $.ajax({
            url: APP_URL + '/pusher/updateTask',
            type: 'POST',
            data:{taskId:data_val.data,current_page:currentPageArr[1]},
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target1 = '#taskStart_' + data.taskId;
                $(target1).html('');
                $(target1).html(data.html);
                $('.ajaxAddTask').val('');
                $('body').find('.inputDateStart').flatpickr({
                    minDate:  new Date(),
                    onChange: function(selectedDates, dateStr, instance) {
                        $('body').find('.inputDateEnd').removeAttr('disabled')
                        $('body').find('.inputDateEnd').flatpickr({
                            minDate:$('body').find('.inputDateStart').val()
                        });
                    },
                });
            }
        });
    });


    channel.bind('task-deleted', function(data) {
        console.log(data.data);
        var target = '#taskStart_' + data.data.taskId;
        var target2 = '.taskCount_' + data.data.goalId;
        $(target2).html('<span>'+data.data.count+' Tasks </span>');
        $(target).remove();
        if(data.data.count == 0){ 
            $('#ajaxTask_'+data.data.goalId).find('.blank-msg').remove();               
            $('#ajaxTask_'+data.data.goalId).html('<div class="blank-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any Task yet!</p><p>Start creating Tasks from the bottom bar.</p> </div></div>');
        }

    });

    channel.bind('task-complete', function(data_val)  {
        var current_page = window.location.pathname;  
        var currentPageArr = current_page.split('/'); 
        $.ajax({
            url: APP_URL + '/pusher/showCompletedTask',
            type: 'POST',
            data:{taskId:data_val.data,current_page:currentPageArr[1]},
            success:function(response){ 
                var target = '#taskStart_' + response.taskId
                $(target).html(response.taskHtml);
                if($(target).closest('#ajaxTask').hasClass('todoTasks')){
                    $(target).find('.n-avatar-wrap').find('.dropdown').remove();
                    $(target).find('.del-task').remove();
                }
            }
        });
    });

    channel.bind('task-undo', function(data_val)  {
        var current_page = window.location.pathname; 
        var currentPageArr = current_page.split('/'); 
        $.ajax({
            url: APP_URL + '/pusher/showUndoTask',
            type: 'POST',
            data:{taskId:data_val.data,current_page:currentPageArr[1]},
            success:function(response){  
                var target = '#taskStart_' + response.taskId
                $(target).html(response.taskHtml);
                if($(target).closest('#ajaxTask').hasClass('todoTasks')){
                    $(target).find('.n-avatar-wrap').find('.dropdown').remove();
                    $(target).find('.del-task').remove();
                }
            }
        });
    });

    channel.bind('add-goal', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/showGoal',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target1 = '#ajaxGoal_' + data.catId;
                var target2 = '.goalCount_' + data.catId;
                var target3 = '.dueDateUpdate_' + data.goalId;
                $(target1).find('.comment-msg').html('');
                $(target1).append(data.html);
                $(target2).html(data.count+' RIP(s)');
                $('.ajaxAddGoal').val('');


                $('body').find(target3).flatpickr({
                    minDate:  new Date(),
                    disableMobile : true,
                });
            }
        });
    });
    channel.bind('goal-updated', function(data)  {
        $.ajax({
            url: APP_URL + '/pusher/updateGoal',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                var target1 = '#goalStart_' + data.goalId;
                var target2 = '.goalCount_' + data.catId;
                var target3 = '.categoryAvgPer_'+ data.catId;
                var catDivClass = 'progress-radial progress-radial--md progress-'+data.categoryAvgPer
                $(target1).html(data.html);
                $(target2).html(data.count+' Goals');
                $(target3).html(data.categoryAvgPer + '%');
                $(target3).parent().removeClass().addClass(catDivClass);
                $('.ajaxAddGoal').val('');
                $('body').find('.inputDateStart').flatpickr({
                    minDate:  new Date(),
                    onChange: function(selectedDates, dateStr, instance) {
                        $('.inputDateEnd').removeAttr('disabled')
                        $('.inputDateEnd').flatpickr({
                            minDate:$('.inputDateStart').val()
                        });
                    },
                });
            }
        });
    });
    channel.bind('add-name', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/showName',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target1 = '.goalName_' + data.goalId;
                $(target1).html(data.name);

            }
        });
    });

    channel.bind('add-task-name', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/showTaskName',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target1 = '.taskName_' + data.taskId;

                $(target1).html(data.name);

            }
        });
    });

    channel.bind('add-category-name', function(data) {
        $.ajax({
        url: APP_URL + '/pusher/showCategoryName',
        type: 'POST',
        data:data,
        success:function(response){
            var data = response;
            if(data.dataNull)
               return false
            var target1 = '.categoryName_' + data.categoryId;

            $(target1).html(data.name);

        }
    });
    });

    channel.bind('due-date', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/showDueDate',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false

         var target1 = '.dueDateUpdate_' + data.goalId;    

        $(target1).flatpickr({
          //minDate:new Date(),
          defaultDate: data.duedate,
          dateFormat: "M d, Y",
          disableMobile : true
        }); 

               
                $(target1).val(data.duedate);

                if(data.display_msg != ''){ 
                    $(target1).parent().parent().find('.error-msg').html(data.display_msg);
                    $(target1).addClass('error');
                }
                else{
                    $(target1).parent().parent().find('.error-msg').html('');
                    $(target1).removeClass('error');
                }
            }
          
        });
    });

    channel.bind('percentage-update', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/showPercentage',
            type: 'POST',
            data:data,
            success:function(response){
                    var data = response; 
                    if(data.dataNull)
                    return false
                    var target = '.innerGoalAvgPer_' + data.goalId;
                    var target2 = '.innerGoalAvgWidth_' + data.goalId;
                    var target1 = '.categoryAvgPer_' + data.catId;
                    var target4 = '.categoryAvgWidth_' + data.catId;

                    var target3 = '.ripPer_' + data.ripId;
                    var ripDivClass = 'progress-radial progress-radial--md progress-' + data.ripAvgPer
                    var percentage = data.percentage + '%';
                    var ripPercentage = data.ripAvgPer + '%';
                    var catPercentage = data.categoryAvgPer + '%';
                    $(target).html(percentage);
                    $(target2).css('width', percentage);
                   
                    $(target3).html(ripPercentage);
                    $(target1).html(catPercentage);
                    $(target4).css('width', catPercentage);

                    //$('body').find('#goal-per-input').val(data.percentage);
                
                    $(target3).parent().removeClass().addClass(ripDivClass);
                    
                    if(response.notify > 0)
                        $('.notification-count').show();

            }
        });
    });


    channel.bind('assign-user', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/assignUser',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                var target = '.addedUserGoalList_' + data.goalId;
                var target1 = '.addedGoalUserSelect_' + data.goalId;
                var target2 = '.goalUserList_' + data.goalId;
                var target3 = '.goalTaskUserList_' + data.goalId;
                var target4 = 'goalTaskUser_' + data.taskUserId;
                var target5 = '.goalUserTabList_' + data.goalId;
                //$('body').find(target).html(data.dropdownHtml);
                //$('body').find(target2).html(data.html);
                $('body').find(target5).html(data.tabHtml);
                $('body').find(target3).each(function(){
                    if(!$(this).find('.card-list-avatar').hasClass(target4)){
                        $(this).append(data.taskNewUser);
                    }
                });

            }
        });
    });

    channel.bind('assign-user-delete', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/removeUser',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                var target = '.addedUserGoalList_' + data.goalId;
                var target2 = '.goalUserList_' + data.goalId;
                var target3 = '.goalTaskUserList_' + data.goalId;
                var target4 = '.goalTaskUser_' + data.userId;
                var target5 = '.goalUserTabList_' + data.goalId;
                $(target).html(data.dropdownHtml);
                $(target2).html(data.html);
                $(target5).html(data.tabHtml);
                $(target3).each(function () {
                    $(this).find(target4).remove();
                });

            }
        });
    });

    channel.bind('goal-deleted', function(datainfo) {
        var current_page = window.location.pathname;  
        var currentPageArr = current_page.split('/'); 

        //console.log(datainfo);
        var data = datainfo.data;
        var target = '#goalName_' + data.goalId;
       $('body').find('#close-comments').trigger('click');
       if(currentPageArr[1] == 'overview-rip'){ //..for overview section..
            $i=0;
            $(target).closest('#company_rips_'+data.company_id).find("input[name='due_date']").each(function() {
                $i++;
            })
          
            if($i == 1){ //..1 because row remove after count
                $(target).closest('#company_rips_'+data.company_id).remove();
            }
            $rips_data = $('#ajaxTask').html();    
            if ($.trim($rips_data) == ''){
                $('#ajaxTask').after('<div class="comment-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any RIP yet!</p><p>Start creating RIP(s) from the menu bar.</p> </div></div>');
            }
        }
        else if(data.count == 0){ 
            $('#ajaxGoal_'+data.catId).find('.comment-msg').remove();               
            $('#ajaxGoal_'+data.catId).find('.rip-divider').after('<div class="comment-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any RIP yet!</p><p>Start creating RIP(s) from the bottom bar.</p> </div></div>');
        }
        $(target).parents('.panel-item ').remove();
        var target = '.goalCount_' + data.catId;
        var target2 = '.categoryAvgPer_'+ data.catId;
        var target22 = '.categoryAvgWidth_' + data.catId;
        var target3 = '.ripPer_' + data.ripId;
        var ripDivClass = 'progress-radial progress-radial--md progress-'+data.ripAvgPer;
        var ripPercentage = data.ripAvgPer + '%';
        $(target).html(data.count + ' RIP(s)');
        $(target2).html(data.categoryAvgPer + '%');
        $(target22).css('width', data.categoryAvgPer);
        $(target3).parent().removeClass().addClass(ripDivClass);
        $(target3).html(ripPercentage);

    });

    channel.bind('add-comment', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/addComment',
            type: 'POST',
            data:data,
            success:function(response){
                var data = response;
                if(data.dataNull)
                return false
                // alert(data.html);
                var target = '#addCommentForm_' + data.goalId;
                $(target).parents('.tabs-comment').find('.blank-msg').html('');
                var target1 = '.comment_' + data.goalId;
                var target2 = '.commentCount_' + data.goalId;
                $(target1).append(data.html);
                $(target2).html('<span>'+data.count + ' Comments</span>');
                $(target).find('textarea').val('');
                $(target).find('select').val('');
                $(target).find('input:file' ).val('');
                $('textarea.mention-example3').mentionsInput({
                    onDataRequest:function (mode, query, callback) {
                        $.getJSON('assigned/user/jsonData', function(responseData) {
                            responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                            callback.call(this, responseData);
                        });
                    }

                });

            }
        });
    });
    channel.bind('edit-comment', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/editComment',
            type: 'POST',
            data:data,
            success:function(response){
               var data = response;
               if(data.dataNull)
               return false
               var target1 = '#commentData_' + data.commentId;
               var target2 = '#commentVal_' + data.commentId;
               var target3 = '.currentCommentData_' + data.commentId;
               var target4 = '.attachmentList_' + data.commentId;
               var target5 = '.commentStart_'+ data.commentId;
               $(target1).find('.rip-comments-msg').html(data.message);
               $(target2).val(data.message);
               $(target3).show();
               $(target4).html(data.attachment)
                $('textarea.mention-example3').mentionsInput({
                    onDataRequest:function (mode, query, callback) {
                        $.getJSON('assigned/user/jsonData', function(responseData) {
                            responseData = _.filter(responseData, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });
                            callback.call(this, responseData);
                        });
                    }

                });

            }
        });
    });
    channel.bind('delete-comment', function(data) {
        console.log(data);
        var target = '.mediaComment_'+data.data.id;
        var target2 = '.commentCount_'+data.data.goalId;        
        $(target2).html('<span>'+data.data.count+ ' Comments</span>');
        if(data.data.count == 0){ 
            $('.comment_'+data.data.goalId).find('.blank-msg').remove();               
            $('.comment_'+data.data.goalId).html('<div class="blank-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any Comment yet!</p><p>Start creating Comments from the bottom bar.</p> </div></div>');
        }
        console.log(target, $(target));
        $(target).remove();
    });

    channel.bind('add-category', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/broadcastCategory',
            type: 'POST',
            data:data,
            success:function(response){
                if(response.dataNull)
                return false
                var target = '#ajaxCategory_' + response.rip_id;
                $(target).append(response.html);
                $('.ajaxAddCategory').val('');
            }


        });
    });

    channel.bind('delete-category', function(data) {
        var target = '#categoryStart_' + data.data;
        $(target).remove();


    });

    channel.bind('edit-rip-tab', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/editRipTab',
            type: 'POST',
            data:data,
            success:function(response){ 
                if(response.dataNull)
                return false
                $('.editRip_'+response.id).find('.rip-heading').text(response.name);
                $('.editRip_'+response.id).find('.rip-date-header #date-header').text(response.start_date + ' - ' + response.end_date );
                $('.editRip_'+response.id).find('.rip-date-header #left-days').text(response.left );
                // Update text on Rip Panel 
                $('.ripTab_'+response.id).find('.expired-title .exp-days').text(response.name);
                $('.ripTab_'+response.id).find('.rip-date-header #date-header-menu').text(response.start_date + ' - ' + response.end_date );
                $('.ripTab_'+response.id).find('.rip-date-header #left-days-menu').text(response.left );

            }

        });


    });

    channel.bind('expire-rip-tab', function(data) {
        var current_page = window.location.href;
        if(data.data == current_page){
            window.location.reload();
        }        
    });

    channel.bind('revert-expire-rip-tab', function(data) {
        var current_page = window.location.href; 
        if(data.data == current_page){
            window.location.reload();
        }  
    });       

    channel.bind('delete-rip-tab', function(data) {
       var current_page = window.location.href;
        if(data.data == current_page){
            window.location.reload();
        }        
       // window.location.replace(data.data);
        // var target = '.editRip_' + data.data;
        // var target1 = '.ripTab_' + data.data;
        // var target3 = $('body').find('.nav-tabs--custom').find('.active').find('a');

        // $(target3).parent().prev().addClass('active');
        // //$('body').find('.nav-tabs--custom').find('.active').find('a').trigger('click');
        // if($(target1).parent().prev().length){
        //     $(target1).parent().prev().find('a').trigger('click');
        //     $(target).remove();
        //     $(target1).parent().remove();
        // }else if ($(target1).parent().next().hasClass("add-tab")) {
        //     $('.tab-content').html('<div class="no-content"><img src="'+ APP_URL+'/images/no-content.jpeg" alt=""></div>')
        //     $(target).remove();
        //     $(target1).parent().remove();
        // }else{
        //     $(target).remove();
        //     $(target1).parent().next().find('a').trigger('click');
        //     $(target1).parent().remove();
        // }

    });

    channel.bind('add-rip-tab', function(data) {
        $.ajax({
            url: APP_URL + '/pusher/addRipTab',
            type: 'POST',
            data:data,
            success:function(response){ 
                if(response.dataNull)
                return false
                if($('#rip-listing_'+response.company_id).find('#mCSB_1_container')){
                    $('#rip-listing_'+response.company_id).find('#mCSB_1_container').append(response.html);
                }
                else{
                    $('#rip-listing_'+response.company_id).append(response.html);
                }
            }
        });
    });


    channel.bind('portfolio-update', function(data)  {
        $.ajax({
            url: APP_URL + '/pusher/portfolioUpdate',
            type: 'POST',
            data:data,
            success:function(response){
                var target = '#' + response.field + '_' + response.company_id;
                var test = $(target).html(response.data);
            }
        });

    });
});
