$('document').ready(function () {
     var showHideNoRecordFound = function(){
        if ($(".checkForRecord")[0]){
            $("#checkForRecordGoal").hide()
        } else {
            $("#checkForRecordGoal").show()
        } 
     }
     showHideNoRecordFound()
     var setAccordian = function(accordianArray, dom){
        accordianArray.forEach(element => {
            if(dom.attr('href') != element){
                closeAllRipSection(accordianInfoRip())              
               $(element).removeClass('in')
               eleArray = element.split("#collapse");
               $('#categoryStart_'+eleArray[1]).find('div:first').removeClass('opened');              
            }
         });
     }

     var closeAllRipSection = function(accordianArray){
        accordianArray.forEach(element => {
               $('#'+element).removeClass('opened-goals')
               eleArray = element.split("-");
               $('#collapseIn_'+eleArray[2]).css('display', '');
         });
     }

     var setAccordianRip = function(accordianArray, dom){
        accordianArray.forEach(element => {
            if(dom.attr('id') != element){             
               $('#'+element).removeClass('opened-goals')
               eleArray = element.split("-");
               $('#collapseIn_'+eleArray[2]).css('display', '');              
            }
         });
     }

     var accordianInfo = function(accorId = null){
        var accordianArray = []
        if ($(".accordionDiv")[0]){
            $(".accordionDiv").each(function(){
                accordianArray.push($(this).attr('href'));
            })
            if(accorId){
                accordianArray.push(accorId);
            }        
        }        
        return accordianArray;
     }

     var accordianInfoRip = function(){
        var accordianArrayRip = []
        if ($(".panel-item")[0]){
            $(".panel-item").each(function(){
                if($(this).attr('id'))
                accordianArrayRip.push($(this).attr('id'));
            })                  
        }        
        return accordianArrayRip;
     }

    $(document).on("click",".accordionDiv",  function(e){
        setAccordian(accordianInfo(), $(this))
    })
    
    $(document).on("click",".rip-card-subhead, .accordion-header",  function(e){
        console.log($(this).parents(".panel-item "))
        setAccordianRip(accordianInfoRip(), $(this).parents(".panel-item "))
    })
     accordianInfo()

    // add Category from ajax
    $('body').on('submit', '#categoryForm', function(e){
//        if(e.which === 13) {
            e.preventDefault();
            $(this).parent().find('.error-msg').text('');
            var form = $(this).serialize();
            $ajaxAddCategory = $('.ajaxAddCategory').val();
            if($.trim($ajaxAddCategory) != '') {
                $( ".loader" ).addClass('loading');
                $('body').find('textarea').attr("disabled",true);
                $.ajax({
                    url: $(this).closest('form').attr('action'),
                    type: 'POST',
                    data: form,
                    success: function (response) {
                        var target = '#ajaxCategory_' + response.rip_id;
                        $(target).append(response.html);
                        $( ".loader" ).removeClass('loading');
                        $('.ajaxAddCategory').val('');
                        $('body').find('textarea').attr("disabled",false);
                        showHideNoRecordFound()
                        displayCategoryNotificationMsg('alert-success', 'Category has been added successfully.');
                       
                    }
                });
            }
            else{
                $(this).parent().find('.error-msg').text('Please Enter Category Name');
            }
    });


    // To confirm user before deleting category
    $('body').on('click', '.deleteCategoryItem', function(event) {
        event.preventDefault();
        var current = $(this);
        var url = $(current).attr('href');    
        var cat_name = $(current).attr('catname');
        var cat_url = url.split('/');
        var cat_id = cat_url[4];     
        var trimmedText = $.trim(cat_name);
        if(trimmedText.length > 50){
            $('#confirmPopup .font-weight600').text('"'+trimmedText.substring(0, 50) + '..."');
        } else {
            $('#confirmPopup .font-weight600').text('"'+trimmedText+'"');
        }  
        //$('#confirmPopup .font-weight600').text('"'+cat_name+'"');
        $('#confirmPopup .modal-title').text('Delete Category');
        $('#confirmPopup .module_msg').text('Are you sure you want to delete ');
        $('#confirmPopup .module_name').text('category?'); 
        $('#confirmPopup').modal('show');
        $('#confirmPopup').unbind().on('click', '#confirm', function(e) {
            $(".loader").addClass('loading');
            $('#confirmPopup').modal('hide');
            $.ajax({
                url:url,
                context:this,
                success:function(response){
                    var target = '#categoryStart_' + response.id;
                    var target1 = '.ripPer_' + response.ripId;
                    var ripDivClass = 'progress-radial progress-radial--md progress-' + response.ripAvgPer
                    var ripPercentage = '<span>'+ response.ripAvgPer+'</span>' + ' <span>%</span>';
                    $('body').find(target).remove();
                    $(target1).html(ripPercentage);
                    $(target1).parent().removeClass().addClass(ripDivClass);
                    showHideNoRecordFound()
                    $('body').find("#close-comments").trigger('click');
                    $( ".loader" ).removeClass('loading');
                    displayCategoryNotificationMsg('alert-danger', 'Category has been deleted successfully.');
                    
                }
            });
        });
    });

    //Script to update the category name
    $('body').on('click', '#editCategoryButton', function(){
        
        $(this).parent().find('.error-msg').text('');
        if($.trim($('#editCategory #name').val()) == '') {
            $('#editCategory #name-err').html('Please Enter Category Name');
        }
        
        $ajaxEditCategory = $('#editCategory .ajaxAddCategory').val();      
        if($.trim($ajaxEditCategory) != ''){
            $cat_id = $('#editCategory').find('input[name=category_id]').val();                     
            var url = APP_URL + '/categoryName';
            $.ajax({
                url: url,
                data: {'category_name': $ajaxEditCategory, 'category_id': $cat_id,_token: $("input[name='_token']").val()},
                method: "POST",
                success:function (response) { 
                    response = jQuery.parseJSON(response);                      
                    $('#editCategoryModal').modal('hide');                
                    $('.categoryName_'+response.id).html(response.name);
                    $('#editCategoryModal').html('');
                    displayCategoryNotificationMsg('alert-success', 'Category has been updated successfully.');
                }
            });
        }
        else{
            $(this).parent().find('.error-msg').text('Please Enter Category Name');
        }

    });

    $('body').on('keyup blur', '.ajaxAddCategory, .ajaxAddGoal', function(e){            
        if($.trim($(this).val()) != ''){
            $(this).parent().find('.error-msg').text('');
        }
    });
    
    $('body').on('click', '.editCategoryLink', function(e){
        $('#editCategoryModal').html('');
        $.ajax({
            url: $(this).attr('href'),
            method: "GET",
            success: function success(response) {
                $('#editCategoryModal').html(response);
                
            }
        });
    });
    
    // Bind enter key to Edit Category form
    $('body').on("keypress", "#editCategoryModal", function(e) {
         if(e.which === 13) {
             e.preventDefault();
            $('#editCategoryButton').click();
         }
    });

    function displayCategoryNotificationMsg($type, $msg){ 
        if($type == 'alert-success'){
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
            $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-success-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
        else{ 
            $('#displayAjaxMessage').removeClass('alert-none');
            $('#displayAjaxMessage').addClass('alert-show');
             $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-success');
            $('#displayAjaxMessage .alert-msg-ajax').addClass('alert-danger');
            $('#displayAjaxMessage .alert-msg-ajax').html('<img src="../images/toast-error-icon.svg"> '+$msg);

            window.setTimeout(function () {
                $('#displayAjaxMessage').removeClass('alert-show');
                $('#displayAjaxMessage').addClass('alert-none');
                $('#displayAjaxMessage .alert-msg-ajax').removeClass('alert-danger');
                $('#displayAjaxMessage .alert-msg-ajax').html('');
            }, 5000);
        }
    } 
    
});