$('document').ready(function () {

    var getDoneTask = function () {
        var url = '/user/doneTask'
        $.ajax({
            url: url,
            success: function (response) {
                $(".doneTask").append(response.html)
                checkTheChildren();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    };

    var getNotDoneTask = function () {
        var url = '/user/notDoneTask'
        $.ajax({
            url: url,
            success: function (response) {
                $(".doneTask").append(response.html)
                getDoneTask();
                checkTheChildren()
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    };

    var checkTheChildren = function(){
        var done = $('.done li:first-child').attr('class')
        var notDone = $('.notDone li:first-child').attr('class')
        if ($('.notDone').children().length == 0) {
            $('.notDone').hide()
        } else {
            $('.notDone').show()
        }
        if ( done == 'noComplete' && $('.done').children().length == 1) {
            $('.noComplete').show()
        }else {
            $('.noComplete').hide()
        }
    }

    getNotDoneTask();

    $(document).on('click', '.notDoneTask', function (e) {
        if(!$(this).attr('id'))
        return false
        var taskli = $(this).attr('id').split('_');
        var taskId = taskli[1]
        $('#task_'+taskId).attr("checked", "checked");
        var taskStatus = $('#task_'+taskId).prop('checked');
        var url = '/user/taskStatus/'+taskId;
        $.ajax({
            url: url,
            type: 'PUT',
            data: {'taskStatus':1},
            success: function (response) {
                $("#taskli_"+taskId).remove();
                $(".done").prepend(response.html)
                $(".count").text('('+response.count+')')
                checkTheChildren();
                if($('.count').text() != '(0)') {
                    $(".clearTask").css({ color: "#f14343", cursor:"pointer" });
                    return false;
                }
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    })

    $(document).on('click', '.doneTask', function (e) {
        if(!$(this).attr('id'))
        return false
        var taskli = $(this).attr('id').split('_');
        var taskId = taskli[1]
        $('#taskDone_'+taskId).attr("checked", false);
        var taskStatus = $('#taskDone_'+taskId).prop('checked');
        var url = '/user/taskStatus/'+taskId;
        $.ajax({
            url: url,
            type: 'PUT',
            data: {'taskStatus':0},
            success: function (response) {
                $("#taskli_"+taskId).remove();
                $(".notDone").prepend(response.html)
                $(".count").text('('+response.count+')')
                checkTheChildren();
                if($('.count').text() == '(0)') {
                    $(".clearTask").css({ color: "grey", cursor: "initial" });
                    return false;
                }
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    })

    $(document).on('click', '.clearTask', function(e) {
        if($('.count').text() == '(0)') 
            return false
        var url = '/user/clearDoneTask'

        $('#confirmPopup .font-weight600').text("all the completed");
        $('#confirmPopup .modal-title').text('Clear All Completed Task');
        $('#confirmPopup .module_name').text('task(s)?');
        $('#confirmPopup').modal('show');
        $('#confirmPopup').unbind().on('click', '#confirm', function(e) {
            $(".loader").addClass('loading');
            $('#confirmPopup').modal('hide');
            $.ajax({
                url: url,
                success:function(response) {
                    $(".loader").removeClass('loading');
                    $(".done").empty();
                    $(".count").text('(0)')
                    if($('.count').text() == '(0)') {
                        $(".clearTask").css({ color: "grey", cursor: "initial" });
                        return false;
                    }
                    $(".done").prepend('<li class="noComplete"> <div class="no-rip"><img src="../images/todo-list-icon.svg" alt=""><h3 class="text-center">Looks a little empty here.</h3><p class="text-center">You haven\'t created any Task yet!<br>Start creating Task(s) from the bottom bar</p></div></li>')
                },
                error: function (error) {
                    console.log(error.responseText);
                }
            });
        });
    })

    $('body').on('keydown', '.addTask-form', function (e) {
        $(".err_newTask").text('')
    })

    var addTask = function(){
        
        var newTask = $("#newTask").val();
        if (!newTask) {
            $(".err_newTask").text('Please enter task!')
            return false
        }
        $(".err_newTask").text('')
        var data = {
            'task': newTask
        };
        var url = '/user/createTask';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            success: function (response) {
                $('.notDone').show()
                $(".notDone").prepend(response.html)
                $("#newTask").val('');
                checkTheChildren();
            },
            error: function (error) {
                console.log(error.responseText);
            }
        })
    }

    $('body').on('submit', '.addTask-form', function (e) {
        e.preventDefault();
        addTask()
    });

    $('body').on('keypress', '#newTask',function(e) {
        if(e.which == 13) {
            e.preventDefault();
            addTask()
        }
    });
    
    $('body').on('click', '.to-do-list', function() {
        
        $(".data-lists").mCustomScrollbar();
    });
    
    $('body').on('change', '.data-lists', function() {
        
        $(".data-lists").mCustomScrollbar();
    });
    
    
});