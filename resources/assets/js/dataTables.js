$("document").ready(function() {
  // Js for company Listing start

  $("#company-table").on("click", ".editCompanyButton", function() {
    $("#editModal").html("");
    $.ajax({
      url: $(this).attr("href"),
      success: function(response) {
        $("#editModal").html(response);
        $(".country").chosen();
        var isd_code = $("#country_code")
          .find("option:selected")
          .val();
        if ($("#phone").val() == "") {
          $("#country_code_chosen a span")
            .text("ISD Code")
            .trigger("chosen:updated");
        } else {
          $("#country_code_chosen a span")
            .text(isd_code)
            .trigger("chosen:updated");
        }
      }
    });
  });

  $("#company-table")
    .on("processing.dt", function(e, settings, processing) {
      if (!processing) {
        $(".loader").removeClass("loading");
      }
    })
    .DataTable({
        //processing: true,
        responsive: true,
        serverSide: true,
        ajax: APP_URL + "/company/ajaxData",
        columns: [
          { data: "image", name: "image" },
          { data: "phone", name: "phone" },
          { data: "members", name: "members" },
          { data: "plan", name: "plan" },
          { data: "status", name: "status" },
          { data: "actions", name: "actions" }
        ],
        oLanguage: {
          sEmptyTable:
            "<div class='no-content'><div class='no-rip'><img src='../images/new-site/empty_icon.svg' alt=''><h3>Looks a little empty here.</h3><p>You aren’t associated with any company yet!</p></div></div>"
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('companyDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('companyDataTables'));
        }
    });
  $("#company-table_filter input").attr("placeholder", "Search Company");
  $('input[type="search"]').val('').keyup();

  // Js for company listing end

  // Js for User Listing start
  $("#user-table").on("click", ".editUserButton", function() {
    $("#editUserModal").html("");
    $.ajax({
      url: $(this).attr("href"),
      success: function(response) {
        $("#editUserModal").html(response);
        $("body")
          .find(".select-all")
          .each(function() {
            var check = true;
            var check = true;
            $(this)
              .parents(".label.checkbox")
              .siblings("#checkboxList")
              .find('input[type="checkbox"]')
              .each(function() {
                if (!$(this).is(":checked")) {
                  check = false;
                  return false;
                }
              });
            if (check) $(this).prop("checked", true);
            else $(this).prop("checked", false);
          });
        $("#planSelect").chosen();
        $("#userSelect").chosen();
        $("body")
          .find(".common-chosen")
          .chosen();

        $("#deleteUser").submit(function(event) {
          var x = confirm("Are you sure you want to delete?");
          if (x) {
            return true;
          } else {
            event.preventDefault();
            return false;
          }
        });
      }
    });
  });

  $("#user-table").on("click", ".editPermissionsButton", function() {
    $("#userPermissionModal").html("");
    var id = $(this).attr("data-id");
    $.ajax({
      url: $(this).attr("href"),
      data: { id: id },
      success: function(response) {
        $("#userPermissionModal").html(response);
      }
    });
  });

  $("#user-table")
    .on("processing.dt", function(e, settings, processing) {
      if (!processing) {
        console.log("object");
        $(".loader").removeClass("loading");
      }
    })
    .DataTable({
        // processing: true,
        responsive: true,
        serverSide: true,
        order: [5, "desc"],
        ajax: APP_URL + "/user/ajaxData",
        columns: [
          { data: "image", name: 'users.name'},
          { data: "role", name:'roles.name'},
          { data: "company" , name:'company_name', searchable:false},
          { data: "status", name: 'users.status' },
          { data: "created_at", name: 'users.created_at'},
          { data: "actions" }
        ],
        columnDefs: [
          { responsivePriority: 1, targets: 0 },
          { responsivePriority: 2, targets: -2 }
        ],
        oLanguage: {
          sEmptyTable:
            "<div class='no-content'><div class='no-rip'><img src='../images/new-site/empty_icon.svg' alt=''><h3>Looks a little empty here.</h3><p>You haven’t added any user yet!</p><p>Please add a user.</p></div></div>"
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('usersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('usersDataTables'));
        }
    });
  $("#user-table_filter input").attr("placeholder", "Search User");
  $('input[type="search"]').val('').keyup();
  // Js for User Listing end 
  

  // Js for Roles Listing start

  $("#roles-table").on("click", ".editRoleButton", function(event) {
    event.preventDefault();
    $("#editRoleModal").html("");
    $.ajax({
      url: $(this).attr("href"),
      success: function(response) {
        var $html = $(response);
        $("#editRoleModal").html($html);
        $("body")
          .find(".select-all")
          .each(function(checkbox) {
            var check = true;
            var parentCheckbox = $(this).find('input[type="checkbox"]');
            var check = true;
            $(this)
              .parents(".label.checkbox")
              .siblings("#checkboxList")
              .find('input[type="checkbox"]')
              .each(function() {
                if (!$(this).is(":checked")) {
                  check = false;
                  return false;
                }
              });
            if (check) $(this).prop("checked", true);
            else $(this).prop("checked", false);
          });
      }
    });
  });

  $("#roles-table")
    .on("processing.dt", function(e, settings, processing) {
      if (!processing) {
        $(".loader").removeClass("loading");
      }
    })
    .DataTable({
    //processing: true,
    responsive: true,
    serverSide: true,
    ajax: APP_URL + "/ajaxData",
    columns: [
        { data: "name", name: "name" },
        { data: "created_at", name: "created_at" },
        { data: "updated_at", name: "updated_at" },
        {
          data: "actions",
          name: "Action",
          searchable: "false",
          orderable: "false"
        }
    ],
    oLanguage: {
        sEmptyTable:
          "<div class='no-content'><div class='no-rip'><img src='../images/new-site/empty_icon.svg' alt=''><h3>Looks a little empty here.</h3><p>No Roles are added yet!</p><p>Please add a Role.</p></div></div>"
        },
        "bStateSave": true,
        "fnStateSave": function (oSettings, oData) {
            localStorage.setItem('usersDataTables', JSON.stringify(oData));
        },
        "fnStateLoad": function (oSettings) {
            return JSON.parse(localStorage.getItem('usersDataTables'));
        }
    });
    $("#roles-table_filter input").attr("placeholder", "Search Role");
    $('input[type="search"]').val('').keyup();
});
// Js for Roles Listing end

// Js for CSV Listing start

var csvtable = $("#csv-table")
  .on("processing.dt", function(e, settings, processing) {
    if (!processing) {
      $(".loader").removeClass("loading");
    }
  })
  .DataTable({
    //processing: true,
    responsive: true,
    serverSide: true,
    ajax: APP_URL + "/csvData",
    columns: [
      { data: "file_name", name: "file_name" },
      { data: "size", name: "size" },
      { data: "uploader", name: "uploader" },
      { data: "created_at", name: "created_at" },
      { data: "link", name: "link" }
    ],
    oLanguage: {
      sEmptyTable:
        "<div class='no-content'><div class='no-rip'><img src='../images/new-site/empty_icon.svg' alt=''><h3>Looks a little empty here.</h3><p>You haven’t uploaded any CSV yet!</p><p>Please upload CSV file to import data.</p></div></div>"
    }
  });
  $("#csv-table_filter input").attr("placeholder", "Search File");
  // Js for CSV Listing end

    // Download sample csv
    $('#sample-csv').click(function () { 
        window.location = APP_URL + '/sampleCsv';
    });

    // Validate uploaded CSV on change
    $('#csv-upload').change(function () {
        validateCsv(this);
    });
    $(".add-edit-csv").chosen();
    $('body').on('click', '#csv_upload_button', function(){
      $('.add-edit-csv').empty()
      var newOption = $('<option value="add">Add</option><option value="edit">Update</option>');
        $('.add-edit-csv').append(newOption);
        $('.add-edit-csv').trigger("chosen:updated");
       $('#csv-upload').val(''); 
       $('.error').html('');
       $('#csv-upload-status').html('');
       $('.title').html('choose file');
    });
    
// Validate uploaded CSV
    $('body').on('click', '#validate-csv', function(){
        var status = $('#csv-form #csv-upload-status');
        if ($('#csv-upload').val() == ''){
            status.show();
            status.addClass('csv_upload_status');
            status.text('Please choose a valid CSV file.');
            $('#validate-csv').attr('disabled',true);
            return false;
        } else {     
            var target = '#csv-form';
            var form = $(target)[0];
            var formdata = new FormData(form);

            var url = 'validateCsv';
            $(".loader").addClass("loading");
            $('#csvupload').modal('hide');
            $.ajax({
                url: url,
                type: 'POST',
                data: formdata,
                cache: false,
                contentType: false,
                processData: false,
                success:function (response) {
                    if(response.fail){
                        $(".loader").removeClass("loading");
                        displayNotificationMsg('fail',response.fail);
                    } else {
                        $(".loader").removeClass("loading");
                        displayNotificationMsg("alert-success", "CSV Uploaded Successfully.");
                        csvtable.ajax.reload();
                    }    
                },
                error: function(data) { 
                    $(".loader").removeClass("loading");
                    $('.error').html(data.responseJSON.errors);
                }
            }); 
        }    
    })
    
    function validateCsv(input) {
        if(input.files && input.files[0]) {
            var type = input.files[0].type;
            var status = $('#csv-form #csv-upload-status');
            if(type.startsWith('text/csv') || type.startsWith('application/vnd.ms-excel')) {
                status.hide();
                $('#validate-csv').attr('disabled',false);
            } else {
                status.show();
                status.addClass('csv_upload_status');
                status.text('The file you are trying to upload is not a CSV file.');
                $('#validate-csv').attr('disabled',true);
                return false;
            }
        }
    }
    // File Upload

    $("body").on("change", "#csv-upload", function() {
        var target = $(this)[0];
        var filestr = "";
        var fileSize = 0;
        for (var i = 0; i < target.files.length; i++) {
            if (filestr == "") {
              filestr += target.files[i].name;
            } else {
              filestr += ", " + target.files[i].name;
            }
            fileSize = fileSize + target.files[i].size;
        }
        if (fileSize > 2097152) {
            $(this)
              .parents(".csv_form")
              .find(".error-msg")
              .html("File size must not be more than 2 MB");
            $(".title").html("");
        } else {
            $(this)
              .parents(".csv_form")
              .find(".error-msg")
              .html("");
            $(this)
              .parents(".csv_form")
              .find(".title")
              .html(filestr);
        }
    });

//add custom items
var customItems = $("#customItems");
var goalItem = $("#goalItem");
var itemHtml = $(".custom-item");

// Js for Team member Listing start

$("#team-member-table").on("click", ".editTeamMemberButton", function(event) {
  event.preventDefault();
  $("#editTeamMemberModal").html("");
  $.ajax({
    url: $(this).attr("href"),
    success: function(response) {
      $("#editTeamMemberModal").html(response);
    }
  });
});

$("#team-member-table").DataTable({
  //processing: true,
  serverSide: true,
  ajax: APP_URL + "/team/ajaxData",
  columns: [
    { data: "name", name: "name" },
    { data: "company", name: "company" },
    { data: "role", name: "role" },
    { data: "actions", name: "Action" }
  ]
});

/**
 * Function to display Toast messages for Success/Fail
 * @param {type} $type
 * @param {type} $msg
 * @returns {undefined}
 */
function displayNotificationMsg($type, $msg) {
  if ($type == "alert-success") {
    $("#displayAjaxMessage").removeClass("alert-none");
    $("#displayAjaxMessage").addClass("alert-show");
    $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
    $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
    $("#displayAjaxMessage .alert-msg-ajax").html(
      '<img src="../images/toast-success-icon.svg"> ' + $msg
    );

    window.setTimeout(function() {
      $("#displayAjaxMessage").removeClass("alert-show");
      $("#displayAjaxMessage").addClass("alert-none");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").html("");
    }, 5000);
  } else {
    $("#displayAjaxMessage").removeClass("alert-none");
    $("#displayAjaxMessage").addClass("alert-show");
    $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
    $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
    $("#displayAjaxMessage .alert-msg-ajax").html(
      '<img src="../images/toast-error-icon.svg"> ' + $msg
    );

    window.setTimeout(function() {
      $("#displayAjaxMessage").removeClass("alert-show");
      $("#displayAjaxMessage").addClass("alert-none");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").html("");
    }, 5000);
  }
}
