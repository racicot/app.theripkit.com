/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example', require('./components/Example.vue'));

// const app = new Vue({
//     el: '#app'
// });

require('./mui.min');
require('./adminlte.min');
require('./mCustomScrollbar.concat.min.js');
require('./jQuery-plugin-progressbar');
require('./chosen.jquery.min');
require('./datatables.min');
require('daterangepicker');
require('flatpickr');
require('./login');
require('./taskForToday');
require('./schedule-meeting');
require('./dataTables');
require('./rip');
require('./category');
require('./task');
require('./tour');
require('./goal');
require('./longtermgoal');
require('underscore');
require('./jquery.elastic');
require('./jquery.mentionsInput');
require('./comment');
require('./masonry.pkgd.min');
require('./custom');
require('./pusher');



