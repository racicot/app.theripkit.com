$('document').ready(function () {

    $('.yearlyinvite').click(function () {
        //alert();
        var target = '.yearlyAssignUserToGoalList';
        var target1 = '.yearlyAssignUserToGoalSelect';
        var target3 = '.goalUserList';
        var val = $(target1).val();

        $.ajax({
            url: APP_URL + '/users/yearlyAssignUserToAddedGoal',
            data: {user: val},
            success: function (response) {
                var data = $.parseJSON(response);
                $(target).html(data.html);
                $(target3).html(data.imageHtml);
            }
        });
    });
    $('.yearlyAddedGoalUser').click(function () {
        //alert('subhav');
        var target = '.yearlyAssignUserToGoalList';
        $.ajax({
            url: APP_URL + '/yearly/goal/user',
            success: function (response) {
                // alert('jain');
                $(target).html(response);

            }
        });
    });

    $('.add-user-dropdown-list').on('click', '.yearly-media-delete', function (e) {
        e.preventDefault();
        var url = $(this).find('a').attr('href');
        var target = '.yearlyAssignUserToGoalList';
        var target1 = '.yearlyAssignUserToGoalSelect';
        var target2 = '.goalUserList';
        $.ajax({
            url: url,
            success: function (response) {
                var data = $.parseJSON(response);
                $(target).html(data.html);
                $(target2).html(data.imageHtml);

            }
        });
    });

    $(".yearly-save-btn").on("click", function(e) {
        e.preventDefault();
        var form = $('#yearlyPercentageUpdate');
        // alert($(form).attr('action'));
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success:function (response) {


            }
        });
        var progress = $(this)
            .parents(".dropdown")
            .siblings(".progress-radial");
        var progressClassList = progress[Object.keys(progress)[0]];

        if (progress.is('[class*="progress-radial--white"]')) {
            var progressClassSec = progressClassList.className.match(
                /progress-radial--white/
            )[0];
        }
        //store current value text inside progress bar
        var progressText = $(this)
            .parents(".dropdown")
            .siblings(".progress-radial")
            .find("#progress-edit");
        var oldInput = progressText.text();

        //Store input value
        var inputVal = $(this)
            .parent(".edit-buttons")
            .siblings(".edit-input")
            .find(".form-control")
            .val();


        //runs for progress with progress-radial--white class
        if (progressClassSec) {
            var progressClass = progressClassList.className.match(
                /progress-green-sec-/
            )[0];

            $(this)
                .parents(".dropdown")
                .removeClass("open");

            var newProgress = progressClass + Math.round(inputVal);
            var oldProgress = progressClass + oldInput;
            progress.removeClass(oldProgress).addClass(newProgress);
            progressText.text(Math.round(inputVal));
        } else {
            var progressClass = progressClassList.className.match(
                /progress-green-/
            )[0];

            $(this)
                .parents(".dropdown")
                .removeClass("open");

            var newProgress = progressClass + Math.round(inputVal);
            var oldProgress = progressClass + oldInput;
            progress.removeClass(oldProgress).addClass(newProgress);
            progressText.text(Math.round(inputVal));
        }
    });


    // longterm


    $('.longterminvite').click(function () {
//alert();
        var target = '.longtermAssignUserToGoalList';
        var target1 = '.longtermAssignUserToGoalSelect';
        var target3 = '.goalUserList';
        var val = $(target1).val();

        $.ajax({
            url: APP_URL + '/users/longtermAssignUserToAddedGoal',
            data: {user: val},
            success: function (response) {
                var data = $.parseJSON(response);
                $(target).html(data.html);
                $(target3).html(data.imageHtml);
            }
        });
    });
    $('.longtermAddedGoalUser').click(function () {
        //alert('subhav');
        var target = '.longtermAssignUserToGoalList';
        $.ajax({
            url: APP_URL + '/longterm/goal/user',
            success: function (response) {
                // alert('jain');
                $(target).html(response);

            }
        });
    });

    $('.add-user-dropdown-list').on('click', '.longterm-media-delete', function (e) {
        e.preventDefault();
        var url = $(this).find('a').attr('href');
        var target = '.longtermAssignUserToGoalList';
        var target1 = '.longtermAssignUserToGoalSelect';
        var target2 = '.goalUserList';
        $.ajax({
            url: url,
            success: function (response) {
                var data = $.parseJSON(response);
                $(target).html(data.html);
                $(target2).html(data.imageHtml);

            }
        });
    });
    $(".longterm-save-btn").on("click", function(e) {
        e.preventDefault();
        var form = $('#longtermPercentageUpdate');
        // alert($(form).attr('action'));
        $.ajax({
            url: $(form).attr('action'),
            data: $(form).serialize(),
            success:function (response) {


            }
        });
        var progress = $(this)
            .parents(".dropdown")
            .siblings(".progress-radial");
        var progressClassList = progress[Object.keys(progress)[0]];

        if (progress.is('[class*="progress-radial--white"]')) {
            var progressClassSec = progressClassList.className.match(
                /progress-radial--white/
            )[0];
        }
        //store current value text inside progress bar
        var progressText = $(this)
            .parents(".dropdown")
            .siblings(".progress-radial")
            .find("#progress-edit");
        var oldInput = progressText.text();

        //Store input value
        var inputVal = $(this)
            .parent(".edit-buttons")
            .siblings(".edit-input")
            .find(".form-control")
            .val();


        //runs for progress with progress-radial--white class
        if (progressClassSec) {
            var progressClass = progressClassList.className.match(
                /progress-green-sec-/
            )[0];

            $(this)
                .parents(".dropdown")
                .removeClass("open");

            var newProgress = progressClass + inputVal;
            var oldProgress = progressClass + oldInput;
            progress.removeClass(oldProgress).addClass(newProgress);
            progressText.text(inputVal);
        } else {
            var progressClass =
                progressClassList.className.match(/progress-green-/) ||
                progressClassList.className.match(/progress-/);

            $(this)
                .parents(".dropdown")
                .removeClass("open");

            var newProgress = progressClass + inputVal;
            var oldProgress = progressClass + oldInput;
            progress.removeClass(oldProgress).addClass(newProgress);
            progressText.text(inputVal);
        }
    });
});