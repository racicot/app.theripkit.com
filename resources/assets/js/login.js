$('document').ready(function () {

// Js for login form
    
    $('#clickButton, #resetButton').click(function () {
        var emailRegex = new RegExp(/^([\w\.\-\+]+)@([\w\-]+)((\.(\w){2,3})+)$/i);
        var valid = emailRegex.test($('#email').val());
        $('#inputLoder').show();
        if ($('#email').val() != '') {
            if (valid) {
                var ajaxurl = "/ajaxValidateLogin";
                $.ajax({
                    type: 'POST',
                    url: ajaxurl,
                    data: {email: $('#email').val(), _token: $("input[name='_token']").val()},

                }).done(function (data) {
                    $('#inputLoder').hide();
                    if (data['error'] == 0) { 
                        $('#jemail').attr('style', 'display:none');
                        $('#jpassword').attr('style', 'display:block');
                        $('#clickButton').attr('style', 'display:none');
                        $('#loginButtton').attr('style', 'display:block');
                        $('.clickButton').removeAttr('id');
                        $('.submitButton').attr('style', 'display:block');
                        $('.submitButton').attr('id', 'submitButton');
                        $('#jpassword .user-login-value').html($('#email').val());
                        $('#password').focus();
                    } else {
                        $('#emailError').html(data['message']);
                    }
                });
            } else {
                $('#inputLoder').hide();
                $('#emailError').html('Email address not valid');
            }
        } else {
            $('#inputLoder').hide();
            $('#emailError').html('Please enter your email');
        }
    });
    $(window).keydown(function (event) {
        if (event.keyCode == 13) {
            $('#clickButton').click();
            $('#submitButton').click();
        }
    });
    $('.login__form').on('click', '#submitButton', function () {
        if ($('#password').val() != '') {
            var ajaxurl = "/ajaxLogin";
            $.ajax({
                type: 'POST',
                url: ajaxurl,
                data: {
                    email: $('#email').val(),
                    _token: $("input[name='_token']").val(),
                    password: $('#password').val()
                },

            }).done(function (data) {
                if (data['error'] == 0) {
                    window.location.replace("/overview-rip");
                } else {    
                    if (data['loginAttempts'] < 5) {
                        $('#password').html('');
                        $('#passwordError').html(data['message']);
                    } else {
                        displayNotificationMsg('fail','You have exceeded the number of sign-in attempts. Please check your email');
                        $.ajax({
                            type: 'POST',
                            url: "/bad_credentials",
                            data: {
                                email: $('#email').val(),
                                _token: $("input[name='_token']").val()
                            },
                            success: function () { console.log('here');
                                $('#password').val('');                                
                                $('#passwordError').html('Too many unsuccessful sign-in attempts. Please check your email');
                            }
                        });
                        
                        
                    }
                }
            });
        }
    });
    $('body').on('click', '#forgotevent', function (e) {
        e.preventDefault();
        var email = $('#email').val();
        var url = $(this).attr('href') + '/' + email;
        var check = confirm('You will get the new password in your registered email id. Confirm reset password ?');
        if (check) {
            $('#inputLoder2').show();
            $.ajax({
                type: "GET",
                url: url,
                success: function () {
                    $('#inputLoder2').hide();
                    alert('Password updated successfully. Please check your email');
                    location.assign(APP_URL);
                }
            });
        }

    });
    
    /**
   * Function to display message toasts for success/delete messages
   * @param {type} $type
   * @param {type} $msg
   * @returns {undefined}
   */
  function displayNotificationMsg($type, $msg) {
    if ($type == "alert-success") {
        $("#displayAjaxMessage").removeClass("alert-none");
        $("#displayAjaxMessage").addClass("alert-show");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
        $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
        $("#displayAjaxMessage .alert-msg-ajax").html(
          '<img src="../images/toast-success-icon.svg"> ' + $msg
        );
        window.setTimeout(function() {
          $("#displayAjaxMessage").removeClass("alert-show");
          $("#displayAjaxMessage").addClass("alert-none");
          $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
          $("#displayAjaxMessage .alert-msg-ajax").html("");
        }, 8000);
    } else {
        $("#displayAjaxMessage").removeClass("alert-none");
        $("#displayAjaxMessage").addClass("alert-show");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
        $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
        $("#displayAjaxMessage .alert-msg-ajax").html(
          '<img src="../images/toast-error-icon.svg"> ' + $msg
        );
        window.setTimeout(function() {
          $("#displayAjaxMessage").removeClass("alert-show");
          $("#displayAjaxMessage").addClass("alert-none");
          $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
          $("#displayAjaxMessage .alert-msg-ajax").html("");
        }, 8000);
    }
  }
});