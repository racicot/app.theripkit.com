$(window).on("load", function() {
  var currentUrl = $(location)
    .attr("href")
    .split("/");
  var routes = ["user", "company", "roles", "massUpload"];

  if ($.inArray(currentUrl[currentUrl.length - 1], routes) == -1) {
    $(".loader").removeClass("loading");
  }

  $.ajax({
    type: "GET",
    url: "/checkTourStatus",
    success: function(response) {
      if (response == 0) {
        $(".tour-icon").click();
      }
    }
  });
});

$("document").ready(function() {
  // Display user's profile image on upload complete in account and user edit section
  $("body").on("change", "#profile-picture", function() {
    updateImage(this, "profile-upload-status", "profileImage");
  });

  //Display login background image on upload complete
  $("#bg_image").change(function() {
    updateImage(this, "bg-upload-status", "bg-image");
  });

  //Display uploaded image on add company page
  $("#company-logo").change(function() {
    updateImage(this, "logo-upload-status", "logo");
  });

  //Display uploaded image on edit company page
  $("body").on("change", "#edit-company-logo", function() {
    updateImage(this, "edit-logo-status", "uploaded-logo");
  });

  //Common function to upload image
  function updateImage(input, classInput, imageId) {
    $(".loader").addClass("loading");
    var target = "." + classInput;
    var target1 = "#" + imageId;
    var status = $(target);
    if (input.files.length != 0) {
      if (input.files && input.files[0]) {
        if (input.files[0].type.startsWith("image/")) {
          var reader = new FileReader();
          if (input.files[0].size / 1024 <= 1024) {
            reader.onload = function(e) {
              $(target1).attr("src", e.target.result);
              status.removeClass("error-msg text-danger");
              status.addClass("text-success");
              status.text(
                'Image uploaded successfully. Click "Save" to proceed'
              );
            };
          } else {
            status.addClass("error-msg text-danger");
            status.text("Image size greater than 1mb. Upload unsuccessfull!");
          }
          $(".loader").removeClass("loading");
          reader.readAsDataURL(input.files[0]);
        } else {
          $(".loader").removeClass("loading");
          status.removeClass("text-success");
          status.addClass("text-danger");
          status.text(
            "The file you are trying to upload is not an image file."
          );
          $(".loader").removeClass("loading");
        }
      }
    } else {
      $(".loader").removeClass("loading");
      status.removeClass("text-success");
      status.addClass("text-danger");
      status.text("Either file is not in proper format or too large to handle");
      $(".loader").removeClass("loading");
    }
  }

  $("body").on("click", ".userResetPassword", function(event) {
    $("#confirmPopup").modal("show");
    var url = $(this).attr("href");
    $("#confirmPopup")
      .find(".modal-title")
      .html("Reset Password");
    $("#confirmPopup")
      .find(".form-group")
      .html("Are you sure want to reset the Password for this user ?");
    $("#confirmPopup")
      .find("#confirm")
      .html("Yes");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $("#confirmPopup").modal("hide");
        window.location.href = url;
      });
    event.preventDefault();
  });

  // Submit company delete form on delete link click
  $("body").on("click", ".deleteCompany", function(event) {
    event.preventDefault();
    var company_id = $(this).attr("companyId");
    var company_name = $(this).attr("companyname");
    $("#confirmPopup").modal("show");
    $("#confirmPopup .modal-title").text("Delete Company");
    $("#confirmPopup .font-weight600").text('"' + company_name + '"');
    $("#confirmPopup .module_name").text("company?");
    $("#confirmPopup").modal("show");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $("#confirmPopup").modal("hide");
        var target = "#deleteCompany_" + company_id;
        $(target).submit();
      });
  });

  // Submit role delete form on delete link click
  $("body").on("click", ".delete-role", function(event) {
    $("#confirmPopup").modal("show");
    var roleId = $(this).attr("roleId");

    $("#confirmPopup .font-weight600").text(
      '"' + $(".role_name_" + roleId).text() + '"'
    );
    $("#confirmPopup .modal-title").text("Delete Role");
    $("#confirmPopup .module_name").text("role?");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $("#confirmPopup").modal("hide");
        var target = "#deleteRole_" + roleId;
        $(target).submit();
      });
    event.preventDefault();
  });

  //Show the user's to do tasks
  $("#todo").change(function() {
    var id = $(this).val();
    location.assign(APP_URL + "/todo/" + id);
  });

  $(".progress-bar").loading();
  $("body")
    .find(".common-chosen")
    .chosen();
  $("#companyselect").chosen();

  $(".treeview").hover(function() {
    $("#companyselect").trigger("chosen:open");
  });

  $("#companyselect_chosen .chosen-results").click(function() {
    var id = $("#companyselect_chosen").prev().attr('id');

    var url = $("#"+id).val().split('/changeCompany/');   
    var newUrl = url[1].split('-');
    if(newUrl[1] == '0'){
      displayNotificationMsg('', 'The company "'+url[2]+'" has been deactivated by admin.')
      return false
    }
    window.location.href = url[0]+'/changeCompany/'+newUrl[0];
  });

  $("#companyselectmobile").on('change', function() {
   var selected = $('option:selected', this).attr('selected') ? true : false;
   if(selected)
   return false
    var id = $(this).val();
    var url = id.split('-');
    if(url[1] == '0'){
      displayNotificationMsg('', 'The company "'+url[2]+'" has been deactivated by admin.')
      return false
    }
    window.location.href = url[0];
  });

  $(".li-company").on('click', function() {
     var id = $(this).data('value');
     var url = id.split('-');
     if(url[1] == '0'){
       displayNotificationMsg('', 'The company "'+url[2]+'" has been deactivated by admin.')
       return false
     }
     window.location.href = url[0];
   });  

  $("body")
    .find("#editCompany .common-chosen")
    .chosen();
  $("#editCompany .country").trigger("chosen:open");

  //content sidebar
  $("#c-sidebar-toggle").on("click", function() {
    var sideBar = $("#content-sidebar");
    sideBar.toggleClass("collapsed");

    //rotate icon
    if (sideBar.hasClass("collapsed")) {
      $(this)
        .find(".fa")
        .removeClass("fa-chevron-left")
        .addClass("fa-chevron-right");
    } else {
      $(this)
        .find(".fa")
        .removeClass("fa-chevron-right")
        .addClass("fa-chevron-left");
    }
  });

  // Remove validation messages on button click
  $("body").on("click", "#addRoleModalButton", function(e) {
    $("#name-err").html("");
    $("#name").val("");
  });

  //Secondary content sidebar - Discovery pages
  $("#c-sidebar-toggle").on("click", function() {
    var sideBar = $("#content-sidebar");
    sideBar.toggleClass("slide");
    //rotate icon
    if (sideBar.hasClass("slide")) {
      $(this)
        .find(".fa")
        .removeClass("fa-chevron-left")
        .addClass("fa-chevron-right");
    } else {
      $(this)
        .find(".fa")
        .removeClass("fa-chevron-right")
        .addClass("fa-chevron-left");
    }
  });

  $("body").on("click", ".accordion-header", function(e) {
    e.preventDefault();
    $type = $.trim(
      $(this)
        .find("span")
        .html()
    ).split(" ");
    $(this)
      .parents(".panel-item")
      .find(".rip-detail-panel")
      .find(".error-msg")
      .text("");

    if ($type[1] == "Comments") {
      $(this)
        .parents(".panel-item")
        .find(".comment-active")
        .click();
    } else {
      $(this)
        .parents(".panel-item")
        .find(".rip-active")
        .click();
    }

    if ($type[1] == "Tasks" || $type[1] == "Comments") {
      $(this)
        .closest(".panel-item")
        .addClass("opened-goals");
      $(this)
        .parents(".panel-item")
        .find(".panel-body")
        .slideDown();
    } else {
      if (
        $(this)
          .closest(".panel-item")
          .hasClass("opened-goals")
      ) {
        $(this)
          .closest(".panel-item")
          .removeClass("opened-goals");
        $(this)
          .parents(".panel-item")
          .find(".panel-body")
          .slideUp();
      } else {
        $(this)
          .closest(".panel-item")
          .addClass("opened-goals");
        $(this)
          .parents(".panel-item")
          .find(".panel-body")
          .slideDown();
      }
    }
  });

  $(".panel-collapse .panel-item:not(:first-child) .panel-body").hide(); //Hide other except first one

  //Add user dropdown
  $("body").on(
    "click",
    ".card-list-avatar.dropdown-toggle, .edit-progress.dropdown-toggle,  .js-comment-box-item",
    function(e) {
      e.preventDefault();
      $(this)
        .parent()
        .toggleClass("open");

      //close dropdown when clicked outside

      $(document).click(function(event) {
        if (!$(event.target).closest(".dropdown").length) {
          if ($(".dropdown").is(":visible")) {
            $(".dropdown").removeClass("open");
          }
        }
      });
    }
  );

  //close btn js
  $(".js-close-dropdown").on("click", function(e) {
    e.preventDefault();
    if ($(this).closest(".dropdown").length) {
      if ($(".dropdown").is(":visible")) {
        $(".dropdown").removeClass("open");
      }
    }
  });

  // Rip comments and custom category script start
  // Opens dropdown according to document height
  function determineDropDirection() {
    $(".dropdown-menu").each(function() {
      // Invisibly expand the dropdown menu so its true height can be calculated
      $(this).css({
        visibility: "hidden",
        display: "block"
      });
      // Necessary to remove class each time so we don't unwantedly use dropup's offset top
      $(this)
        .parent()
        .removeClass("dropup");
      // Determine whether bottom of menu will be below window at current scroll position
      if (
        $(this).offset().top + $(this).outerHeight() >
        $(window).innerHeight() + $(window).scrollTop()
      ) {
        $(this)
          .parent()
          .addClass("dropup");
      }
      // Return dropdown menu to fully hidden state
      $(this).removeAttr("style");
    });
  }
  determineDropDirection();
  $(window).scroll(determineDropDirection);

  //custom category
  $("#category-select").on("change", function() {
    var id = $(this)
      .find("option:selected")
      .attr("id");
    if (id) {
      $(".custom-category").show();
    } else {
      $(".custom-category").hide();
    }
  });

  //close btn css
  $(".close-btn").on("click", function(e) {
    e.preventDefault();
    $(this)
      .parents(".dropdown")
      .removeClass("open");
  });

  $("body").on("keyup", ".goal-tab-percentage", function() {
    if ($.isNumeric($(this).val())) {
      if ($(this).val() > 100) $(this).val("100");
    } else {
      $(this).val("");
    }
  });

  $("body")
    .find("textarea.mention-example2")
    .mentionsInput({
      onDataRequest: function(mode, query, callback) {
        $company_id = $(this).attr("companyId");
        $.getJSON("assigned/user/jsonData?id=" + $company_id, function(
          responseData
        ) {
          responseData = _.filter(responseData, function(item) {
            return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1;
          });
          callback.call(this, responseData);
        });
      }
    });

  //Show the user's tasks overview
  $("#task").change(function() {
    var id = $(this).val();
    location.assign(APP_URL + "/overview-task/" + id);
  });

  // Portfolio
  $(".contentSave").focusout(function() {
    var data = $(this).html();
    var current = $(this);
    var field = $(this).attr("data-field");
    $.ajax({
      url: APP_URL + "/portfolio/save",
      data: { field: field, data: data },
      success: function(response) {
        current.html(response);
      }
    });
  });

  $(".addAnother").click(function() {
    $(this)
      .parent()
      .find(".contentSave")
      .append("<li></li>");
  });

  var howmanyslides = $(".swiper-wrapper").children(".swiper-slide").length;
  var swiper = new Swiper(".swiper-container", {
    slidesPerView: 5,
    spaceBetween: 5,
    initialSlide: howmanyslides - 1,
    scrollbar: {
      el: ".swiper-scrollbar",
      hide: true
    },
    breakpoints: {
      400: {
        slidesPerView: 1
      },
      640: {
        slidesPerView: 2
      },
      750: {
        slidesPerView: 3
      }
    }
  });

  $(".goals-block .row").masonry({
    itemSelector: ".grid-item", // use a separate class for itemSelector, other than .col-
    columnWidth: ".grid-sizer",
    percentPosition: true,
    horizontalOrder: true
  });

  $(".company-logo-link").on("click", function(e) {
    e.preventDefault();
    $(this)
      .parent()
      .toggleClass("open-list");
    //close dropdown when clicked outside
    $(document).click(function(event) {
      if (!$(event.target).closest(".treeview").length) {
        if ($(".treeview").is(":visible")) {
          $(".treeview").removeClass("open-list");
        }
      }
    });
  });

  //Script mark the notification as read and remove the content from the sidebar
  $("body").on("click", ".notification", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    $.ajax({
      url: url,
      success: function(response) {
        var target = "#notification_" + response.id;
        if (response.count == 0) {
          $(".notification-count").hide();
          $(".read-notification").show();
        }
        $(target)
          .parent()
          .remove();
      }
    });
  });

  $(".sidebar-toggle").click(function() {
    $("#ripMenu_list").removeClass("rip-col-active");
    $("#collapseExample").collapse("hide");
  });
  $("#notification-bell").click(function() {
    $(".loader").addClass("loading");
    $(".notification-sidebar-full").toggleClass("show");
    $("#ripMenu_list").removeClass("rip-col-active");
    $(".wrapper").removeClass("rip-wrapper");
    $(".main-header .navbar-fixed-top").removeClass("rip-navbar");
    $("body").removeClass("sidebar-open");
    $("body").addClass("sidebar-collapse");
    $.ajax({
      url: APP_URL + "/notificationData",
      success: function(response) {
        if (response.count == 0) {
          $(".read-notification").show();
        } else {
          $(".read-notification").hide();
        }
        $(".loader").removeClass("loading");
        $("#control-sidebar-home-tab").html(response.html);
      }
    });
  });

  //Script remove the alert message after 3 seconds of page load
  setTimeout(function() {
    $(".alert").html("");
    $(".alert").removeClass();
  }, 5000);

  setTimeout(function() {
    $(".alertForget").html("");
    $(".alertForget").removeClass();
  }, 20000);

  $(".goalName, .categoryName").keydown(function(e) {
    var count = $(this).text().length;
    if (count > 100) {
      if (e.keyCode == 8 || e.keyCode == 46) {
      } else {
        e.preventDefault();
      }
    }
  });

  // Jquery to validate phone number
  $("body").on("keypress", "#phone", function(e) {
    // var specialKeys = new Array();
    // specialKeys.push(8);
    // specialKeys.push(13);
    // if (!e.shiftKey){
    //     var keyCode = e.which ? e.which : e.keyCode
    //     var ret = ((keyCode >= 48 && keyCode <= 57) || keyCode >= 96 && keyCode <= 105 || specialKeys.indexOf(keyCode) != -1);
    //     $("#phone-err").css({"display": (ret ? "none" : "inline"), "color":"red"}).html('Only numbers are allowed!');
    //     return ret;
    // } else {
    //     $("#phone-err").css({"display": (ret ? "none" : "inline"), "color":"red"}).html('Only numbers are allowed!');
    //     return false;
    // }

    //if the letter is not digit then display error and don't type anything
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      //display error message
      $("#phone-err")
        .css({ display: "inline", color: "red" })
        .html("Only numbers are allowed!");
      return false;
    } else {
      $("#phone-err")
        .css({ display: "none", color: "red" })
        .html("");
    }
  });
  // $('body').on('paste', '#phone', function (e) {
  //     return false;
  // });
  $("body").on("drop", "#phone", function(e) {
    return false;
  });

  // Validate Edit Company Form
  $("body").on("submit", "#editCompany", function(e) {
     e.preventDefault();
    if ($.trim($("#inputName").val()) == "") {
      $("#name-err")
        .css({ display: "inline", color: "red" })
        .html("Please enter a valid Company Name.");
      return false;
    }
    if ($("#phone").val().length != 10) {
      $("#phone-err")
        .css({ display: "inline", color: "red" })
        .html("Please enter valid phone number.");
      return false;
    }
    if ($("#phone").val().length == 10) {
      if ($.trim($("#country_code").val()) == "Select Country") {
        $("#phone-err")
          .css({ display: "inline", color: "red" })
          .html("ISD Code not selected");
        return false;
      } else {
        var isd_code = $.trim($("#country_code option:selected").val());
        $("#isd_code_company").val(isd_code);
        $("#country_code_chosen a span").text($("#isd_code_company").val());
      }
    }
//    if ( $("#assigned_parent").find(":selected").text() == "" ) {
//      $("#parent-err")
//        .css({ display: "inline", color: "red" })
//        .html("Please select a Parent.");
//      return false;
//    } 
//    else 
        
    if ($("#inputName").val() != "") {
      var formData = new FormData($(this)[0]);
      var url = $(this).attr("action");
      $(".loader").addClass("loading");
      $.ajax({
        url: url,
        type: "POST",
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $("#editCompany").modal("hide");
          location.reload();
          displayNotificationMsg(
            "alert-success",
            "Company has been Updated Successfully."
          );
        },
        error: function(data) {
          var responseJSON = data.responseJSON.company_name;
          $(".loader").removeClass("loading");
          $("#name-err")
            .css({ display: "inline", color: "red" })
            .html(responseJSON);
          return false;
        }
      });
    } else {
      return true;
    }
  });

  $("body").on("click", "#addNewCompanyButton", function(e) {
    $("#inputCompanyName").val("");
    $(".help-block").html("");
  });

  $("body").on("click", "#addUserButton", function(e) {
    $("#inputName").val("");
    $("#inputEmail").val("");
    $("#selectRole")
      .val("Admin")
      .trigger("chosen:updated");
    $("#selectCompany")
      .val("")
      .trigger("chosen:updated");
    $("#email-err").html("");
    $("#name-err").html("");
    $("#company-err").html("");
    $("#add-user-company").show();
  });

  $("body").on("click", "#editUserButton", function(e) {
    $("#email-err").html("");
    $("#name-err").html("");
    $("#company-err").html("");
  });

  // Validate Add Company
  $("body").on("submit", "#addCompany", function(e) {
    if ($.trim($("#inputCompanyName").val()) == "") {
      $(".help-block")
        .css({ display: "inline", color: "red" })
        .html("Please enter a valid Company Name.");
      return false;
    } else if ($.trim($("#inputCompanyName").val()) != "") {
      e.preventDefault();
      var data = $(this).serialize();
      $(".loader").addClass("loading");
      $.ajax({
        url: $(this).attr("action"),
        method: "post",
        data: data,
        success: function(response) {
          $("#addCompany").modal("hide");
          location.reload();
          displayNotificationMsg(
            "alert-success",
            "Company has been Added Successfully."
          );
        },
        error: function(data) {
          var responseJSON = data.responseJSON.company_name;
          $(".loader").removeClass("loading");
          $(".help-block")
            .css({ display: "inline", color: "red" })
            .html(responseJSON);
          return false;
        }
      });
    } else {
      return true;
    }
  });

  // JS script for Select Company chosen to remove error message
  $("body").on("change", ".selectCompany", function(e) {
    if (
      $("#editUser #selectCompany")
        .find(":selected")
        .text() !== ""
    ) {
      $("#company-err").html("");
    }
    if (
      $("#addUser #selectCompany")
        .find(":selected")
        .text() !== ""
    ) {
      $("#company-err").html("");
    }
  });

  // Validate Email Format on Edit form
  $("body").on("submit", "#editUser", function(e) {
    $("#name-err").html("");
    $("#email-err").html("");
    $("#company-err").html("");

    if ($.trim($("#name").val()) == "") {
      $("#name-err").html("Please enter a valid User Name");
      return false;
    }   
    else if (validateEmail($("#email").val())) {
      if ($("#editUser #show_company").val() == "Y") {
        if (
          $("#editUser #selectCompany")
            .find(":selected")
            .text() == ""
        ) {
          $("#editUser #company-err").html("Please select a Company");
          return false;
        }
      }
      e.preventDefault();
      var data = new FormData($(this)[0]);
      var url = $(this).attr("action");
      $.ajax({
        url: url,
        method: "post",
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        success: function(response) {
          $("#editUser").modal("hide");
          location.reload();
        },
        error: function(data) {
          var responseJSON = data.responseJSON.email;
          $("#email-err").html(responseJSON);
          return false;
        }
      });

      return true;
    } else {
      $("#email-err").html("Invalid Email Address");
      e.preventDefault();
    }
  });

  $("body").on("change", "#selectRole", function() {
    var val = $("#selectRole")
      .find(":selected")
      .val();
    if (val == "Super Admin") {
      $("#add-user-company").hide();
    } else {
      $("#add-user-company").show();
    }
  });

  // Validate Add User Form
  $("body").on("submit", "#addUser", function(e) {
    $("#name-err").html("");
    $("#email-err").html("");
    $("#company-err").html("");

    if ($.trim($("#inputName").val()) == "") {
      $("#name-err").html("Please enter a valid User Name.");
      return false;
    }
    else if (validateEmail($("#inputEmail").val())) {
      if (
        $("#selectCompany")
          .find(":selected")
          .text() == "" &&
        $("#selectRole")
          .find(":selected")
          .val() != "Super Admin"
      ) {
        $("#company-err").html("Please select a Company.");
        return false;
      }
      $(".loader").addClass("loading");
      e.preventDefault();
      var data = $(this).serialize();
      var url = $(this).attr("action");
      $.ajax({
        url: url,
        method: "post",
        data: data,
        success: function(response) {
          $("#addUser").modal("hide");
          location.reload();
          displayNotificationMsg(
            "alert-success",
            "User has been Added Successfully."
          );
        },
        error: function(data) {
          var responseJSON = data.responseJSON.email;
          $(".loader").removeClass("loading");
          $("#email-err").html(responseJSON);
          return false;
        }
      });
    } else {
      $("#email-err")
        .css({ display: "inline", color: "red" })
        .html("Please enter valid Email Id.");
      $(".loader").removeClass("loading");
      e.preventDefault();
    }
  });

  /**
   * Email Validation function
   *
   * @param {type} sEmail
   * @returns {Boolean}
   */
  function validateEmail(sEmail) {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,50}|[0-9]{1,3})(\]?)$/;
    if (filter.test(sEmail)) {
      return true;
    } else {
      return false;
    }
  }

  // Maintain Tab Id on Page Reload
  $('a[data-toggle="tab"]').on("show.bs.tab", function(e) {
    localStorage.setItem("activeTab", $(e.target).attr("href"));
  });
  var activeTab = localStorage.getItem("activeTab");
  if (activeTab) {
    $('#myTab a[href="' + activeTab + '"]').tab("show");
  }

  // JS for Dropdown Menu
  $(".country")
    .chosen()
    .change(function() {
      var isd_code = $(this)
        .find("option:selected")
        .val();
      $("#isd_code").val(isd_code);

      if (isd_code != "") {
        $("#country_code_chosen a span")
          .text(isd_code)
          .trigger("chosen:updated");
      } else {
        $("#country_code_chosen a span").display($(this).text());
      }
    });

  // display code in isd_code dropdown on page load
  var isd_code = $("#country_code")
    .find("option:selected")
    .val();
  if ($("#phone").val() == "") {
    $("#country_code_chosen a span")
      .text("ISD Code")
      .trigger("chosen:updated");
  } else {
    $("#country_code_chosen a span")
      .text(isd_code)
      .trigger("chosen:updated");
  }

  // JS for displaying selected country code in dropdown
  $("body").on("click", "#userSubmitButton", function(e) {
    if ($("#phone").val().length == 10) {
      if ($.trim($("#country_code_chosen a span").text()) == "ISD Code") {
        $("#phone-err")
          .css({ display: "inline", color: "red" })
          .html("ISD Code not selected");
        return false;
      } else {
        var isd_code = $("#country_code")
          .find("option:selected")
          .val();
        $("#isd_code").val(isd_code);
        $("#country_code_chosen a span").text(isd_code);
      }
    } else if (
      $("#phone").val().length != 10 &&
      $.trim($("#country_code_chosen a span").text()) !== "ISD Code"
    ) {
      $("#phone-err")
        .css({ display: "inline", color: "red" })
        .html("Enter valid phone number");
      return false;
    }
  });
  // Hide error span
  $("body").on("click", "#country_code", function(e) {
    $("#phone-err").html("");
  });

  // Validate Add Role
  $("body").on("submit", "#addRole", function(e) {
    $("#name-err").html("");

    if ($.trim($("#name").val()) == "") {
      $(".help-block")
        .css({ display: "inline", color: "red" })
        .html("Please enter a valid Role.");
      return false;
    } else if ($.trim($("#name").val()) != "") {
      e.preventDefault();
      var data = $(this).serialize();
      $.ajax({
        url: $(this).attr("action"),
        method: "post",
        data: data,
        success: function(response) {
          $("#addRole").modal("hide");
          location.reload();
          displayNotificationMsg(
            "alert-success",
            "Role has been Added Successfully."
          );
        },
        error: function(data) {
          var responseJSON = data.responseJSON.name;
          $(".help-block")
            .css({ display: "inline", color: "red" })
            .html(responseJSON);
          return false;
        }
      });
    } else {
      return true;
    }
  });

  $("body").on("click", "#addRoleButton", function(e) {
    $("#name").val("");
    $("#name-err").html("");
  });

  // JS for Dropdown Menu
  $("body").on("change", ".country", function(e) {
    var isd_code = $(this)
      .find("option:selected")
      .val();
    $("#isd_code").val(isd_code);
    if (isd_code != "") {
      $("#country_code_chosen a span")
        .text(isd_code)
        .trigger("chosen:updated");
    } else {
      $("#country_code_chosen a span").display($(this).text());
    }

    $(".country")
      .chosen()
      .change(function() {
        console.log("change chosen");
        var isd_code = $(this)
          .find("option:selected")
          .val();

        $("#isd_code").val(isd_code);

        if (isd_code != "") {
          $("#country_code_chosen a span")
            .text(isd_code)
            .trigger("chosen:updated");
        } else {
          $("#country_code_chosen a span").display($(this).text());
        }
      });
  });

  $("body").on("click", ".to-do-list", function() {
    $(".to-do-lists").toggleClass("show");
    $("#ripMenu_list").removeClass("rip-col-active");
    $(".wrapper").removeClass("rip-wrapper");
    $(".main-header .navbar-fixed-top").removeClass("rip-navbar");
    $("body").removeClass("sidebar-open");
    $("body").addClass("sidebar-collapse");

    //    $('.content-wrapper').toggleClass('drag-left');
    //    $('.navbar-static-top').toggleClass('drag-left');
  });
  $("body").on("click", ".close-btn", function() {
    $(".to-do-lists").removeClass("show");
    $(".notification-sidebar-full").removeClass("show");
    $("#meeting-sidebar").removeClass("show");
    // $('.content-wrapper').removeClass('drag-left');
    // $('.navbar-static-top').removeClass('drag-left');
  });
  $("body").on("click", ".drop-icon", function() {
    $(this).toggleClass("close");
    $(".checked-lists").slideToggle();
  });

  //   Function for meeting sideBar open
  $("body").on("click", "#meetingCalenderForRip", function() {
    $("#meeting-sidebar").toggleClass("show");
  });

  // Show Assigned User List Modal Pop Up
  $("body").on("click", ".company-user-list", function(e) {
    $("#showUserListModal").html("");
    $company_id = $(this)
      .parent()
      .attr("company_id");
    $.ajax({
      url: APP_URL + "/company/showAssignedUsers",
      data: { company_id: $company_id },
      success: function(response) {
        $("#showUserListModal").html(response);
      }
    });
  });

  // Remove validation messages from input items
  $("body").on("keypress", "#inputCompanyName", function() {
    $(".error-msg").html("");
  });
  $("body").on("keypress", "#inputName", function() {
    $(".error-msg").html("");
  });
  $("body").on("keypress", "#name", function() {
    $(".error-msg").html("");
  });
  $("body").on("keypress", "#inputEmail", function() {
    $(".error-msg").html("");
  });

  // Script to show user in the user modal in goal tab
  $("body").on("click", ".addedCompanyUser", function() {
    $company_id = $(this).attr("company_id");
    $.ajax({
      url: APP_URL + "/company/companyUserList",
      data: { company_id: $company_id },
      success: function(response) {
        $("#addusers").html(response);
      }
    });
  });

  $("body").on("click", "input[name='addUserToCompany']", function() {    
    $total = 0;
    $new_user_arr = [];
    $old_users_arr = [];
    $show_invite_company = 0;
    $old_users  = $(this).parents('.modal-content').find("#old_users").val();  

    if($.trim($old_users) != ''){
        $old_users_arr = $old_users.split(',');
    }
    $.each($("input[name='addUserToCompany']:checked"), function() {
      $new_user_arr[$total] = $(this).val(); 
      $total++;
    });
    if($old_users_arr.length == $new_user_arr.length){
      for($ii=0; $ii<$old_users_arr.length; $ii++){
          $isexist = 0;
          for($jj=0; $jj< $new_user_arr.length; $jj++){
              //alert('--'+$.trim($old_users_arr[$ii])+'--'+$.trim($new_user_arr[$jj])+'--');
              if($.trim($old_users_arr[$ii]) == $.trim($new_user_arr[$jj])){
                  $isexist = 1;
              }
          }
          if($isexist == 0){
              $show_invite_company = 1;
          }
      }
    }
    else{
        $show_invite_company = 1;
    }
    if($show_invite_company == 1){
       $(".invite-company").attr("disabled", false); 
    } 
    else{
        $(".invite-company").attr("disabled", true); 
    } 
    $(this).parents(".modal-content").find(".selected-user-items").html($total + " Selected");
  });

  /* Script to add user to added company*/
  $("body").on("click", ".invite-company", function() {
    $current_model = $(this);
    $old_val = $current_model
      .parents(".modal-content")
      .find("#old_value")
      .val();
    var companyId = $("#userList")
      .find("input[name=company_id]")
      .val();
    var target3 = ".ComanyUserList_" + companyId;
    var userList = [];
    $.each($("input[name='addUserToCompany']:checked"), function() {
      userList.push($(this).val());
    });

    $(".loader").addClass("loading");
    $.ajax({
      url: APP_URL + "/company/assignCompanyUser",
      data: { company_id: companyId, user: userList },
      success: function(response) {
        $new_val = $current_model
          .parents(".modal-content")
          .find(".selected-user-items")
          .html();
        $new_arr = $new_val.split(" ");
        var data = response;
        $("body")
          .find(target3)
          .html(data.imageHtml);
        $(".loader").removeClass("loading");
        $("body")
          .find(".cust-msg")
          .remove();
        $("#addusers").modal("hide");
        $("#addusers").html("");
        if ($old_val <= $new_arr[0]) {
          displayNotificationMsg(
            "alert-success",
            "User(s) has been successfully assigned to Company."
          );
        } else {
          displayNotificationMsg(
            "alert-danger",
            "User(s) has been successfully removed from Company."
          );
        }
      }
    });
  });

  /**
   * Function to display message toasts for success/delete messages
   * @param {type} $type
   * @param {type} $msg
   * @returns {undefined}
   */
  function displayNotificationMsg($type, $msg) {
    if ($type == "alert-success") {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-success-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    } else {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-error-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    }
  }

  $(".overlay-on").click(function() {
    $("html, body").animate({ scrollTop: 0 }, 0);
    $(".introjs-tooltip.introjs-bottom-left-aligned")
      .parent(".introjs-fixedTooltip")
      .css({ top: "2px" });
    $("body").addClass("modal-open");
    $(".to-do-lists").removeClass("show");
    $(".notification-sidebar-full").removeClass("show");
    $("#ripMenu_list").removeClass("rip-col-active");
    $(".wrapper").removeClass("rip-wrapper");
    $(".main-header .navbar-fixed-top").removeClass("rip-navbar");
    $("body")
      .removeClass("sidebar-open")
      .addClass("sidebar-collapse");
    $(".introjs-fixedTooltip").css("left", "155px");
  });
  var checkitem = function() {
    var $this;
    $this = $("#setup-slider");
    if ($("#setup-slider .carousel-inner .item:first").hasClass("active")) {
      $this.children(".left").hide();
      $this.children(".right").show();
    } else if (
      $("#setup-slider .carousel-inner .item:last").hasClass("active")
    ) {
      $this.children(".right").hide();
      $this.children(".left").show();
    } else {
      $this.children(".carousel-control").show();
    }
  };

  checkitem();

  $("#setup-slider").on("slid.bs.carousel", "", checkitem);
 /* $("#setup-slider").swiperight(function() {
    $(this).carousel("prev");
  });
  $("#setup-slider").swipeleft(function() {
    $(this).carousel("next");
  });*/

  // Validate Edit Role
  $("body").on("submit", "#permissionsForm", function(e) {
    $("#name-err").html("");

    if ($.trim($("#role_name").val()) == "") {
      $("#name-err").text("Please enter a valid Role.");
      return false;
    } else if ($.trim($("#role_name").val()) != "") {
      e.preventDefault();
      $(".loader").addClass("loading");
      var data = $(this).serialize();
      $.ajax({
        url: $(this).attr("action"),
        method: "post",
        data: data,
        success: function(response) {
          $("#editRoleModal").modal("hide");
          location.reload();
          //                    displayNotificationMsg("alert-success", "Role has been Updated Successfully.");
        },
        error: function(data) {
          var responseJSON = data.responseJSON.permission;
          $(".loader").removeClass("loading");
          $("#permission-err").text(responseJSON);
          return false;
        }
      });
    } else {
      return true;
    }
  });
  $("#setup-slider").on("slide.bs.carousel", function() {
    //alert("A new slide is about to be shown!");
    //videojs("my-video").pause();
    var myPlayer = videojs("my-video");
    myPlayer.pause();
    myPlayer.on("pause", function() {
      this.posterImage.show();
    });
  });
});

// Code to increase comment textarea height

$("textarea[id='add-comment']").each(function() {
  this.addEventListener("keydown", autosize);
  this.addEventListener("paste", autosize);
});
var meetTitle = document.getElementById("agenda");
meetTitle.addEventListener("keydown", autosize);
meetTitle.addEventListener("paste", autosize);

$("textarea[name='task']").each(function() {
  this.addEventListener("keydown", autosize);
  this.addEventListener("paste", autosize);
});

function autosize() {
  var el = this;
  setTimeout(function() {
    el.style.cssText = "height:20px; padding:0";
    el.style.cssText = "height:" + el.scrollHeight + "px";
  }, 0);
}

function removeValidation(target, attribute) {
  $("body").on("keypress", target, function() {
    $("#" + attribute).text("");
  });
}

// Portfolio download tost
$(".download-portfolio").click(function() {
  $("#displayAjaxMessage").removeClass("alert-none");
  $("#displayAjaxMessage").addClass("alert-show");
  $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
  $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
  $("#displayAjaxMessage .alert-msg-ajax").html(
    '<img src="../images/toast-success-icon.svg"> This feature is coming soon!'
  );

  window.setTimeout(function() {
    $("#displayAjaxMessage").removeClass("alert-show");
    $("#displayAjaxMessage").addClass("alert-none");
    $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
    $("#displayAjaxMessage .alert-msg-ajax").html("");
  }, 5000);

  return false;
});
