$("document").ready(function() {
  /* Script to disable submit button on first click */

  $("form").submit(function() {
    $(".disableClass").attr("disabled", "disabled");
  });

  /* Script to add and show comment in a task */

  $("body").on("submit", ".addCommentForm", addCommentForm);
  function addCommentForm(e) {
    var goal_id = $(this).attr("goalId");
    var target = $(this);
    var form = $(target)[0]; // You need to use standard javascript object here
    var formdata = new FormData(form);
    e.preventDefault();
    $mention_data = $.trim(
      $(this)
        .find("#add-comment")
        .parent()
        .find(".mentions")
        .find("div")
        .html()
    );
    //alert($mention_data);
    formdata.append("mention_data", $mention_data);

    $(this)
      .parent()
      .find(".error-msg")
      .text("");
    if (
      $.trim(
        $(this)
          .find("#add-comment")
          .val()
      ) != ""
    ) {
      //$('body').find('textarea').attr("disabled", true);
      $(".loader").addClass("loading");
      $(".sucess-msg").remove();

      $.ajax({
        type: "POST",
        enctype: "multipart/form-data",
        url: $(this).attr("action"),
        data: formdata,
        processData: false,
        contentType: false,
        success: function(response) {
          var data = response;
          var target1 = ".comment_" + goal_id;
          var target2 = ".commentCount_" + goal_id;
          $(target1).append(data.html);
          $(target2).html("<span>" + data.count + " Comments</span>");
          $(target)
            .find("textarea")
            .val("")
            .css("height", "20px");
          $('.ajaxAddTask').val("").css("height", "20px");          
          $(target)
            .find(".textarea-comment-box")
            .find(".mentions-input-box")
            .find(".mentions")
            .find("div")
            .html("");
          $(target)
            .find("select")
            .val("");
          $(target)
            .find("input:file")
            .val("");
          $(target)
            .find(".file-upload-name")
            .html("");
          $("body")
            .find("textarea")
            .attr("disabled", false);
          target
            .parents(".tabs-comment")
            .find(".blank-msg")
            .html("");
          $("textarea.mention-example3").mentionsInput({
            onDataRequest: function(mode, query, callback) {
              $.getJSON("assigned/user/jsonData", function(responseData) {
                responseData = _.filter(responseData, function(item) {
                  return (
                    item.name.toLowerCase().indexOf(query.toLowerCase()) > -1
                  );
                });
                callback.call(this, responseData);
              });
            }
          });
          $(".loader").removeClass("loading");
          displayNotificationMsg(
            "alert-success",
            "Comment has been added successfully."
          );
        }
      });
    } else {
      $(this)
        .parent()
        .find(".error-msg")
        .text("Please Enter Comment");
      return false;
    }
  }

  $("body").on("keyup blur", ".addCommentSection", function() {
    if ($.trim($(this).val()) != "") {
      $(this)
        .parent()
        .parent()
        .parent()
        .find(".error-msg")
        .text("");
    }
  });

  /* Script to edit and show comment in a task */

  //$('body').on('submit','.editCommentForm', editCommentForm);
  $("body").on("click", ".editCommentButton", editCommentForm);
  function editCommentForm(e) {
    var current = $(this).parents(".editCommentForm");
    var comment_id = current.attr("commentId");
    var target = "#editCommentForm_" + comment_id;
    var form = $(target)[0];
    var formdata = new FormData(form);
    $mention_data = $.trim(
      current
        .find("#edit-comment")
        .parent()
        .find(".mentions")
        .find("div")
        .html()
    );
    //alert($mention_data);
    formdata.append("mention_data", $mention_data);

    if ($.trim(current.find("#edit-comment").val()) != "") {
      e.preventDefault();
      $(".loader").addClass("loading");
      $(".sucess-msg").remove();
      $.ajax({
        type: "POST",
        enctype: "multipart/form-data",
        url: current.attr("action"),
        data: formdata,
        processData: false,
        contentType: false,
        success: function(response) {
          var data = response;
          var target1 = "#commentData_" + data.commentId;
          var target2 = "#commentVal_" + data.commentId;
          var target3 = ".currentCommentData_" + data.commentId;
          var target4 = ".attachmentList_" + data.commentId;
          var target5 = ".commentStart_" + data.commentId;
          $(target1)
            .find(".rip-comments-msg")
            .html(data.message);
          $(target2).val(data.message2);
          $(".edit-comment-box").hide();
          $(target3).show();
          $(".media-attachment").show();
          //if (data.hasAttachment == 1) {
          $(target4).html(data.attachment);
          //}
          $(".media-meta").show();
          $(target)
            .find("textarea")
            .val("");
          $(target)
            .find("input:file")
            .val("");
          $("#editModal").modal("hide");
          $("body")
            .find("textarea")
            .attr("disabled", false);
          $(".loader").removeClass("loading");
          displayNotificationMsg(
            "alert-success",
            "Comment has been updated successfully."
          );
        }
      });
    } else {
      $("#edit-comment-error").text("Please Enter Comment");
      return false;
    }
  }
  /* Script to add user to comment in a task */

  //$('.adduserToComment').click(function () {
  $(".panel-body").on("click", ".adduserToComment", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    $.ajax({
      url: url,
      success: function(response) {
        var data = $.parseJSON(response);
        var target = ".addTaskUserSelect_" + data.taskId;
        $(".panel-body")
          .find(target)
          .html(data.html);
        $(".panel-body")
          .find(target)
          .chosen();
        $(".panel-body")
          .find(target)
          .trigger("chosen:updated");
      }
    });
  });

  //edit comment box
  var currentComment = $(".current-comment");
  var metaAttachment = $(".media-attachment");
  var editCommentBox = $(".edit-comment-box");
  var metaComment = $(".media-meta");
  var mediaComment = $(".media-comment");

  $("body").on("click", ".js-edit-comment", function(e) {
    e.preventDefault();
    var target = $(this)
      .parent()
      .parent()
      .parent()
      .parent();
    var inputText = target.find(".rip-comments-msg").html();
    var url = $(this).attr("href");
    var comment_url = url.split("/");
    var comment_id = comment_url[4];
    $company_id = $(this).attr("companyId");

    $("#editModal").html("");
    $.ajax({
      url: url,
      method: "GET",
      success: function success(response) {
        $("#editModal").html(response);
        $comment_data = $("#editCommentForm_" + comment_id)
          .find("textarea.mention-example2")
          .val();
        $mention_data = $("#mention_comment").val();

        $("#editCommentForm_" + comment_id)
          .find(".mention-example2")
          .mentionsInput({
            onDataRequest: function onDataRequest(mode, query, callback) {
              $.getJSON("assigned/user/jsonData?id" + $company_id, function(
                responseData
              ) {
                responseData = _.filter(responseData, function(item) {
                  return (
                    item.name.toLowerCase().indexOf(query.toLowerCase()) > -1
                  );
                });
                callback.call(this, responseData);
              });
            },
            defaultValue: $mention_data
          });
      }
    });
  });

  //    Delete comment JS
  $("body").on("click", ".js-delete-comment", function(e) {
    e.preventDefault();

    var url = $(this).attr("href");
    var target = $(this)
      .parent()
      .parent()
      .parent()
      .parent();
    var inputText = target.find(".rip-comments-msg").text();
    var trimmedText = $.trim(inputText);
    if (trimmedText.length > 50) {
      $("#confirmPopup .font-weight600").text(
        '"' + trimmedText.substring(0, 50) + '..."'
      );
    } else {
      $("#confirmPopup .font-weight600").text('"' + trimmedText + '"');
    }
    $("#confirmPopup .modal-title").text("Delete Comment");
    $("#confirmPopup .module_msg").text(
      "Are you sure you want to delete comment: "
    );
    $("#confirmPopup .module_name").text("?");
    $("#confirmPopup").modal("show");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $(".loader").addClass("loading");
        $("#confirmPopup").modal("hide");
        $(".loader").addClass("loading");
        $.ajax({
          url: url,
          success: function(response) {
            var data = response;
            var target = ".mediaComment_" + data.id;
            var target2 = ".commentCount_" + data.goalId;
            if (data.count == 0) {
              $(".comment_" + data.goalId)
                .find(".blank-msg")
                .remove();
              $(".comment_" + data.goalId).html(
                '<div class="blank-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any Comment yet!</p><p>Start creating Comments from the bottom bar.</p> </div></div>'
              );
            }
            $(target).remove();
            $(target2).html("<span>" + data.count + " Comments</span>");
            $(".loader").removeClass("loading");
            displayNotificationMsg(
              "alert-danger",
              "Comment has been deleted successfully."
            );
          }
        });
      });
  });

  //  JS for deleting attachment
  $("body").on("click", ".delete-attachment", function(e) {
    e.preventDefault();
    var url = $(this).attr("href");
    var filename = $(this).attr("filename");
    $("#confirmPopup .font-weight600").text(filename);
    $("#confirmPopup .modal-title").text("Delete Attachment");
    $("#confirmPopup .module_name").text("attachment?");
    $("#confirmPopup").modal("show");
    $("#confirmPopup")
      .unbind()
      .on("click", "#confirm", function(e) {
        $(".loader").addClass("loading");
        $("#confirmPopup").modal("hide");
        $(".loader").addClass("loading");
        $.ajax({
          url: url,
          context: this,
          success: function(response) {
            var target = ".attachmentList_" + response.comment_id;
            var target1 = ".commentAttachment_" + response.attachment_id;
            var count = response.count + " attachments";
            if (response.count == 0) {
              $("body")
                .find(target)
                .find(".attachment-heading")
                .html("");
            } else {
              $(target)
                .find(".attachment-heading")
                .html(count);
            }
            $("body")
              .find(target1)
              .remove();
            $(".loader").removeClass("loading");
          }
        });
      });

    //    Delete comment JS
    $("body").on("click", ".js-delete-comment", function(e) {
      e.preventDefault();

      var url = $(this).attr("href");
      var target = $(this)
        .parent()
        .parent()
        .parent()
        .parent();
      var inputText = target.find(".rip-comments-msg").text();
      var trimmedText = $.trim(inputText);
      if (trimmedText.length > 50) {
        $("#confirmPopup .font-weight600").text(
          '"' + trimmedText.substring(0, 50) + '..."'
        );
      } else {
        $("#confirmPopup .font-weight600").text('"' + trimmedText + '"');
      }
      $("#confirmPopup .modal-title").text("Delete Comment");
      $("#confirmPopup .module_msg").text(
        "Are you sure you want to delete the comment: "
      );
      $("#confirmPopup .module_name").text("?");
      $("#confirmPopup").modal("show");
      $("#confirmPopup")
        .unbind()
        .on("click", "#confirm", function(e) {
          $(".loader").addClass("loading");
          $("#confirmPopup").modal("hide");
          $(".loader").addClass("loading");
          $.ajax({
            url: url,
            success: function(response) {
              var data = response;
              var target = ".mediaComment_" + data.id;
              var target2 = ".commentCount_" + data.goalId;
              if (data.count == 0) {
                $(".comment_" + data.goalId)
                  .find(".blank-msg")
                  .remove();
                $(".comment_" + data.goalId).html(
                  '<div class="blank-msg no-content no-active-rip"><div class="no-rip"> <img src="../images/new-site/empty_icon.svg" alt=""><h3>Looks a little empty here.</h3> <p>You haven’t created any Comment yet!</p><p>Start creating Comments from the bottom bar.</p> </div></div>'
                );
              }
              $(target).remove();
              $(target2).html("<span>" + data.count + " Comments</span>");
              $(".loader").removeClass("loading");
              displayNotificationMsg(
                "alert-danger",
                "Comment has been deleted successfully."
              );
            }
          });
        });
      $("#goal-per-input").keyup(function(e) {
        if ($.isNumeric($(this).val())) {
          if ($(this).val() > 100) $(this).val("100");
        } else {
          $(this).val("");
        }
      });
      $(".addCommentSection").focus(function() {
        $(this).addClass("comment-focused");
      });
      $(".addCommentSection").focusout(function() {
        $(this).removeClass("comment-focused");
      });

      $(".loader").removeClass("loading");
    });
  });

  $("body").on("click", ".js-show-comments", function() {
    $(".js-show-comments")
      .closest(".n-panel-body-top")
      .removeClass("active-task");
    $(".js-show-comments").removeClass("active-task-btn");
    $(this).addClass("active-task-btn");
    $(this)
      .closest(".n-panel-body-top")
      .addClass("active-task")
      .closest(".tab-pane")
      .addClass("show-comments");
  });

  //close btn css
  $(".tab-content").on("click", ".close-btn", function(e) {
    e.preventDefault();
    $(this)
      .parents(".dropdown")
      .removeClass("open");
  });

  // Script to close the comment section
  $("body").on("click", "#close-comments", function() {
    $(".goalUserDropdown").show();
    $(this)
      .closest(".tab-pane")
      .removeClass("show-comments");
    $(".js-show-comments")
      .closest(".n-panel-body-top")
      .removeClass("active-task");
    $(".js-show-comments").removeClass("active-task-btn");
  });

  // Script to display uploaded attachment name
  $("body").on("change", ".upload-file", function() {
    var target = $(this)[0];
    var filestr = "";
    var fileSize = 0;
    for (var i = 0; i < target.files.length; i++) {
      if (filestr == "") {
        filestr += target.files[i].name;
      } else {
        filestr += ", " + target.files[i].name;
      }
      fileSize = fileSize + target.files[i].size;
    }
    if (fileSize > 2097152) {
      $(this)
        .parents(".addCommentForm")
        .find(".error-msg")
        .html("File size must not be more than 2 MB");
      $(this)
        .parents(".editCommentForm")
        .find("#edit-attach-error")
        .html("File size must not be more than 2 MB");
      $(".upload-file").val("");
      $(".file-upload-name").html("");
    } else {
      $(this)
        .parents(".addCommentForm")
        .find(".error-msg")
        .html("");
      $(this)
        .parents(".editCommentForm")
        .find("#edit-attach-error")
        .html("");
      $(this)
        .parents(".addCommentForm")
        .find(".file-upload-name")
        .html(filestr);
      $(this)
        .parents(".editCommentForm")
        .find(".file-upload-name")
        .html(filestr);
    }
  });

  /* Function for display success/error notificatios 
        @inout: type, msg
    */
  function displayNotificationMsg($type, $msg) {
    if ($type == "alert-success") {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-success-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    } else {
      $("#displayAjaxMessage").removeClass("alert-none");
      $("#displayAjaxMessage").addClass("alert-show");
      $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-success");
      $("#displayAjaxMessage .alert-msg-ajax").addClass("alert-danger");
      $("#displayAjaxMessage .alert-msg-ajax").html(
        '<img src="../images/toast-error-icon.svg"> ' + $msg
      );

      window.setTimeout(function() {
        $("#displayAjaxMessage").removeClass("alert-show");
        $("#displayAjaxMessage").addClass("alert-none");
        $("#displayAjaxMessage .alert-msg-ajax").removeClass("alert-danger");
        $("#displayAjaxMessage .alert-msg-ajax").html("");
      }, 5000);
    }
  }

  //...textarea on focus ..
  $("body").on(
    "focus",
    ".textarea-comment-box, .textarea-comment-group, .textarea-wrap",
    function() {
      $(this).addClass("focus");
    }
  );
  $("body").on(
    "focusout",
    ".textarea-comment-box, .textarea-comment-group",
    function() {
      $(this).removeClass("focus");
    }
  );
});
