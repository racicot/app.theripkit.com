<?php
namespace App\Zoom;
use App\Zoom\ZoomApi;
class Zoom{

    public static function getZoomUserId()
    {
        $userClient = new ZoomApi();
        $userEmail = "noreply@theripkit.com";
        $request = $userClient->getUserInfoByEmail($userEmail); //create user
        return $request;

    }

    public static function createZoomMeeting($req)
    {
        $userClient = new ZoomApi();
        $userId = self::getZoomUserId()->id;
        $request = $userClient->createAMeeting($userId, $req);
        return $request;
    }

    public static function deleteZoomMeeting($meetingId, $userId)
    {
        $userClient = new ZoomApi();
        $request = $userClient->deleteAMeeting($meetingId, $userId);
        return $request;
    }
}