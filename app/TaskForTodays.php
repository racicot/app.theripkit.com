<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class TaskForTodays extends Model
{

    /** Make all task's first letter capital
     * @return json
     */
    public function getNameAttribute($val)
    {
        return ucwords($val);
    }

    /** Get all Done task
     * @return json
     */
    public static function getDoneTask()
    {
        try {
            $data = self::where('user_id', Auth::id())->where('is_done', 1)->orderBy('id', 'desc')->get();
            $response['html'] = view('auth.taskForToday.doneTask')->with([
                'doneList' => $data,
                'count' => ($data->count() < 10 && $data->count() > 0) ? "0{$data->count()}" : $data->count(),
            ])->render();
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }

    }

    /** Get all task which are not completed
     * @return json
     */
    public static function getNotDoneTask()
    {
        try {
            $data = self::where('user_id', Auth::id())->where('is_done', 0)->orderBy('id', 'desc')->get();
            $response['html'] = view('auth.taskForToday.notDoneTask')->with([
                'notDoneList' => $data,
            ])->render();
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Clear all completed task
     * @return json
     */
    public static function clearDoneTask()
    {
        try {
            $data = self::where('user_id', Auth::id())->where('is_done', 1)->delete();
            return response()->json($data, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Add new task
     * @return json
     */
    public static function createTask()
    {
        try {
            $input = request();
            $insert = new self;
            $insert->name = $input->task;
            $insert->user_id = Auth::id();
            $insert->save();

            $data = self::find($insert->id);
            $response['html'] = view('auth.taskForToday.addTask')->with([
                'task' => $data,
            ])->render();
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Update task status, done or not done
     * @return json
     */
    public static function taskStatus($taskId)
    {
        try {
            $taskStatus = request();
            $taskDone = self::find($taskId);
            $taskDone->is_done = $taskStatus->taskStatus;
            $taskDone->save();
            $template = $taskStatus->taskStatus ? 'auth.taskForToday.addDoneTask' : 'auth.taskForToday.addTask';
            $doneTaskCount = self::where('user_id', Auth::id())->where('is_done', 1)->count();
            $response['html'] = view($template)->with([
                'task' => $taskDone,
            ])->render();
            $response['count'] = ($doneTaskCount < 10 && $doneTaskCount > 0) ? "0{$doneTaskCount}" : $doneTaskCount;
            return response()->json($response, 200);
        } catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

}
