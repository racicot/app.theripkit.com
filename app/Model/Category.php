<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Goal;

class Category extends Model
{
    protected $guarded = [];


    /**
     * Function create the linking between category table and company
     * @return Data
     */
    public function company()
    {
        return $this->belongsTo(Category::class, 'company_id');
    }

    public function goals()
    {
        return $this->hasMany(Goal::class);
    }

    public function getAvgPer($goal_type_id)
    {
        return round(Goal::where([
            'category_id' => $this->id,
            'goal_type_id' => $goal_type_id
        ])->avg('percentage_complete'));
    }

    /**
     * function override the core delete function and delete goals before deleting the category
     */
    public function delete()
    {
        $this->goals()->each(function ($goal, $key) {
            $goal->delete();
        });

        return parent::delete();
    }

    public function rip()
    {
        return $this->belongsTo(Rip::class);
    }
}
