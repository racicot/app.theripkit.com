<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Attachment extends Model
{
    protected $guarded = [];

    public function attachmentUrl()
    {

        return \Storage::url('system_photos' . '/' . $this->attachment);
    }

    /** Add Duplicate attachments for duplicate comments
     * @param int $newCommentId 
     * @param object $value
     */
    public static function newAttachment($value, $newCommentId)
    {
        $attachment = new self;
        $attachment->comment_id = $newCommentId;
        $attachment->attachment = $value->attachment;
        $attachment->created_at = $value->created_at;
        $attachment->updated_at = $value->updated_at;
        $attachment->save();
    } 
}
