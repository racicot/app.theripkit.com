<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Rip extends Model
{
    protected $fillable = ['name', 'company_id', 'start_date', 'end_date'];

    /**
     * function override the core delete function and delete category before deleting the rips
     */
    public function delete()
    {
        Category::where('goal_type_id', $this->id)->each(function ($cat, $key) {
            $cat->delete();
        });

        return parent::delete();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function category()
    {
        return $this->hasMany(Category::class, 'goal_type_id', 'id');
    }

    public function getRipPercentage()
    {
        return round(Goal::where('goal_type_id', $this->id)->avg('percentage_complete'));
    }

    public function getRipGoals()
    {
        return Goal::where('goal_type_id', $this->id)->get();
    }

    /**
     * function for checking duplicate rip period before create/update rip period.
     * @param $data
     * @return count
     */
    public function checkExistingRipPeriod($data){   
        if(isset($data['id']) && $data['id'] > 0){
            $chk_rip_count = $this->where([
                ['company_id', '=', session('company_id')],
                ['name', '=', strip_tags($data['name'])],
                ['start_date', '=', $data['start_date']],
                ['end_date', '=', $data['end_date']],
                ['id', '!=', $data['id']]
            ])->count();
        }
        else{
            $chk_rip_count = $this->where([
                ['company_id', '=', session('company_id')],
                ['name', '=', strip_tags($data['name'])],
                ['start_date', '=', strip_tags($data['start_date'])],
                ['end_date', '=', strip_tags($data['end_date'])]
            ])->count();
        }    
        return $chk_rip_count;
    }

}
