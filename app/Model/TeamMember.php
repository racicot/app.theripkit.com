<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\User;

class TeamMember extends Model
{
    protected $guarded = [];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
