<?php

namespace App\Model;

use App\User;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $guarded = [];

    /**
     * funtion retrns the user image of the comment
     * @return string
     */
    public function userImage()
    {
        $user = User::find($this->from_id);
        return $user->userImage();
    }

    /**
     * function return the name of the user of the comment
     * @return mixed
     */
    public function userName()
    {
        $user = User::find($this->from_id);
        return $user->name;
    }

    public function attachment()
    {
        return $this->hasMany(Attachment::class, 'comment_id');
    }

    public function assignUserHtml()
    {
        $userHtml = '';
        $userArray = User::whereIn('id', explode(',', $this->to_id))->get();
        foreach ($userArray as $user) {
            $userHtml .= '@' . $user->name . ' ';
        }

        return $userHtml;
    }

    public function delete()
    {
        $this->attachment()->delete();
        return parent::delete();
    }

    public function isAllowed()
    {
        if(!\Auth::check())
        return false;
        
        $user_id = \Auth::user()->id;
        if ($this->from_id == $user_id) {
            return true;
        } else {
            return false;
        }
    }

    public function unassignUser($userId)
    {
        Attachment::where('comment_id', $this->id)->each(function ($attachent) {
            $attachent->delete();
        });
        $this->delete();
    }

    /** Add Duplicate comments for duplicate rip
     * @param int $copiedGoalId 
     * @param object $val
     * @return int $commentId; 
     */
    public static function newComment($val, $copiedGoalId)
    {
        $comment = new self;
        $comment->message = $val->message;
        $comment->goal_id = $copiedGoalId;  
        $comment->from_id = $val->from_id;
        $comment->to_id = $val->to_id;
        $comment->created_at = $val->created_at;
        $comment->updated_at = $val->updated_at;
        $comment->save();
        return $comment->id;
    }

     /* Function to update user name from ids  in comment
    * @param $comment_data,$from
    * @return array
    */
    public static function  getMentionData($comment_data,$from){
        $mention_id= '';
        if($comment_data != ''){
            $mention_arr = explode('@[',$comment_data);
            if(count($mention_arr) > 1){
                foreach($mention_arr as $key=>$val){                    
                    if($val != ''){
                         $mentionName_arr = explode(']',$val);
                         $user_name= $mentionName_arr[0];
                         $mentionId_arr = explode('(contact:',$val);                        
                         if(count($mentionId_arr) > 1){
                            $mentionId_arr1 = explode(')',$mentionId_arr[1]);
                            $user_id = $mentionId_arr1[0];

                            if(is_numeric($user_id)){

                                $find = '@['.$user_name.'](contact:'.$user_id.')';                           
                                if($from == 'edit'){ 
                                    $comment_data = strip_tags(str_replace($find, '@'.$user_name, $comment_data));
                                }
                                else{ 
                                    $comment_data = str_replace($find, '<span class="mention-name">'.$user_name.'</span>', $comment_data);
                                }
                                
                                if($mention_id == ''){
                                    $mention_id = $user_id;
                                }
                                else{
                                    $mention_id.= ','.$user_id;
                                }
                            }
                         }                         
                    }
                }
            }
        }
        return array('comment_data'=> stripcslashes($comment_data), 'mention_id'=>$mention_id);
        
    }

}
