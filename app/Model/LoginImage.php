<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class LoginImage extends Model
{
    protected $fillable = ['name', 'motto'];

    public function getImage()
    {
        $storageCheck = \Storage::disk('system_photos')->exists($this->first()->name);
        if ($this->first()->name == null || !$storageCheck) {
            return asset('images/defaultCompany.jpg');

        }
        return \Storage::url('system_photos' . '/' . $this->first()->name);
    }

    public function getMotto()
    {
        return $this->first()->motto;
    }
}
