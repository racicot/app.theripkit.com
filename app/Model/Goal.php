<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use App\Model\Task;
use App\Model\Company;
use App\Model\Comment;
use App\User;

class Goal extends Model
{
    protected $guarded = [];

    protected $appends = ['company_info'];

    public function getCompanyInfoAttribute()
    {
        return Company::find($this->company_id);
    }
    public function rip()
    {
        return $this->belongsTo(Rip::class, 'goal_type_id', 'id');
    }

    public function category()
    {
        return $this->hasOne(Category::class, 'id', 'category_id');
    }

    public function task()
    {
        return $this->hasMany(Task::class, 'goal_id');
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getUser()
    {
        return $userArray = User::whereIn('id', explode(',', $this->assign_to))->get();
    }

    public function assignUserImages()
    {
        $userImage = array();
        $userArray = User::whereIn('id', explode(',', $this->assign_to))->get();
        foreach ($userArray as $user) {
            $userImage[] = $user->userImage();
        }
        return $userImage;
    }

    public function isAllowed()
    {
        $user_id = \Auth::user()->id;
        if (in_array($user_id, explode(',', $this->assign_to))) {
            return true;
        } else {
            return false;
        }
    }

    public function comment()
    {
        return $this->hasMany(Comment::class, 'goal_id');
    }

    public function getGoals($goalId)
    {
        $goals = $this->with(['category'])->where([
            'company_id' => session('company_id'),
            'goal_type_id' => $goalId
        ])->get();
        return $goals;
    }

    /** this function override the parent delete function which first delete the linked table data and the parent delete function will run
     *
     */
    public function delete()
    {
        $this->comment()->each(function ($comment, $key) {
            $comment->delete();
        });
        $this->task()->delete();
        return parent::delete();
    }

    public function unassignUser($userId)
    {
        /*Task::where('goal_id', $this->id)->get()->each(function($task) use ($userId){
           $task->unassignUser($userId);
        });*/
        Comment::where('goal_id', $this->id)->get()->each(function ($comment) use ($userId) {
            $comment->unassignUser($userId);
        });
        /*$assign_to = explode(',', $this->assign_to);
        if(in_array($userId, $assign_to)) {
            $key = array_search($userId, $assign_to);
            array_splice($assign_to, $key, 1);
            $this->assign_to = implode('', $assign_to);
            $this->save();
        }*/
    }

    /**
     * Function to remove user from goals
     * @param  array $companyIds
     * @param  int $userId
     * @return boolean
     */ 
    public static function removeUser($companyIds, $userId)
    {
        if(is_array($userId)){ //..update from company page
            $goals = self::where('company_id', $companyIds)->where('assign_to', '!=', '')->get();
        }
        else{
            $goals = self::whereIn('company_id', $companyIds)->where('assign_to', '!=', '')->get();
        }

        if(count($goals) > 0){
            foreach ($goals as $key => $value) {
                $userIds = explode(',', $value->assign_to);
                if(is_array($userId)){ //..update from company page    
                    $new_user = array_diff($userIds,$userId);                    
                    $value->assign_to = implode(',', $new_user);
                        $value->save();
                }
                else{
                    if(in_array($userId, $userIds)){ // if user id found in array                   
                        if (($key = array_search($userId, $userIds)) !== false) { // search for user id's key in array
                            unset($userIds[$key]);                       
                            $value->assign_to = implode(',', $userIds);
                            $value->save();                       
                        }
                    }
                }    
            }
        } 
        return true;
    }

    /**
     *get goal user 
     * @return \Illuminate\Support\Collection
     */
    public function getGoalUser()
    {
        return $userArray = User::whereIn('id', explode(',', $this->assign_to))->get();
    }


}
