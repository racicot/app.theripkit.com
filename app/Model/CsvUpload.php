<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class CsvUpload extends Model
{
    protected $fillable = ['file_name', 'uploader','company_id'];

    /**
     * @return mixed
     */
    public function size()
    {
        $size = (Storage::size('public/system_files/' . $this->file_name) / 1024);
        if ($size < 1024) {
            return round($size, 2) . ' KB';
        } else {
            return round($size / 1024, 2) . ' MB';
        }

    }

    /**
     * @return string
     */
    public function getLink()
    {
        return Storage::url('public/system_files') . '/' . $this->file_name;
    }
}
