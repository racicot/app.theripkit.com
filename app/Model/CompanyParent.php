<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class CompanyParent extends Model
{
    protected $guarded = [];


    public function companies()
    {
        return $this->hasOne(Company::class, 'id', 'company_id');
    }

    public function user()
    {
        return $this->hasOne(User::class, 'id', 'parent_id');
    }

}
