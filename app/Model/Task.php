<?php

namespace App\Model;

use App\User;

use App\ModelHasRole;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $guarded = [];



    /**
     * @return bool
     */
    public function isAllowed()
    {
        $user = auth()->user();
        $user_id = $user->id;
        $company_id = session('company_id');
        $parentCount = CompanyParent::where(['company_id' => $company_id, 'parent_id' => $user_id])->count();
        if ($user->hasAnyRole(['Super Admin'])) {
            return true;
        } elseif ($parentCount) {
            return true;
        } elseif (in_array($user_id, explode(',', $this->assign_to))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function getTaskUser()
    {
        return $userArray = User::whereIn('id', explode(',', $this->assign_to))->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function goal()
    {
        return $this->belongsTo(Goal::class);
    }

    public function unassignUser($userId)
    {
        $assign_to = explode(',', $this->assign_to);
        if (in_array($userId, $assign_to)) {
            $key = array_search($userId, $assign_to);
            array_splice($assign_to, $key, 1);
            $this->assign_to = implode('', $assign_to);
            $this->save();
        }
    }

    /** Add Duplicate tasks for duplicate rip
     * @param int $copiedGoalId 
     * @param object $val
     */
    public static function newTask($val, $copiedGoalId)
    {
        $task = new self;
        $task->goal_id = $copiedGoalId;  
        $task->name = $val->name;
        $task->status = $val->status;
        $task->assign_to = $val->assign_to; 
        $task->created_at = $val->created_at;
        $task->updated_at = $val->updated_at;
        $task->percentage_complete = $val->percentage_complete;
        $task->due_date = $val->due_date;
        $task->save();
    }

    /**
     * Function to remove user from task
     * @param  array $companyIds
     * @param  int $userId
     * @return boolean
     */
    public static function removeUser($companyIds, $userId)
    {   
        if(is_array($userId)){ //..update from company page
            $goals = \App\Model\Goal::where('company_id', $companyIds)->get();
        }
        else{
            $goals = \App\Model\Goal::whereIn('company_id', $companyIds)->get();
        }
        $goalIds = [];
        $testarray = [];
        if(count($goals) > 0){
            foreach ($goals as $key => $value) { // get all the goal ids
                array_push($goalIds, $value->id);
            }
        }  

        $task = self::whereIn('goal_id', $goalIds)->where('assign_to', '!=', '')->get();
        if(count($task) > 0){
            foreach ($task as $key => $value) {
                $userIds = explode(',', $value->assign_to);
                if(is_array($userId)){ //..update from company page    
                    $new_user = array_diff($userIds,$userId);                    
                    $value->assign_to = implode(',', $new_user);
                        $value->save();
                }
                else{
                    if (in_array($userId, $userIds)) { // if user id found in array                   
                        if (($key = array_search($userId, $userIds)) !== false) { // search for user id's key in array
                            unset($userIds[$key]);
                            $value->assign_to = implode(',', $userIds);
                            $value->save();
                            array_push($testarray, $value);
                        }
                    }
                }
                
            }
        }    
        return $testarray;
    }

    /**
     * Function to get task overiew
     * @param $currentUserId
     * @return array
     */
    public static function taskOverviewOps($user_id)
    {
        $user = self::getCompanyUsers();
        $currentUserId = User::getUserExceptAdmin($user_id, $user);
        $companyInfo = self::getUserAccociatedCompany($currentUserId);
        if(!$companyInfo)
        return view('auth.overview.taskOverview')->with(['tasks' => [], 'users' => $user, 'currentUser' => $currentUserId,]);
        $todo_tasks = Task::with(['goal', 'goal.rip'])->whereHas('goal', function ($query) use($currentUserId, $companyInfo) {
            $query->whereIn('company_id', $companyInfo->company);
        })->whereHas('goal.rip', function ($query) {
            $query->where('expired', '0');
        })->orderBy('created_at', 'desc')->get()->filter(function ($value) use($currentUserId, $companyInfo) {
            if($companyInfo->role != config('setting.roles.superadmin')){
                return (in_array($currentUserId, explode(',', $value->assign_to)));
            }
            return true;
        });

       $todo_tasks = $todo_tasks->groupBy('goal.company_info.name');
       return view('auth.overview.taskOverview')->with([
            'tasks' => $todo_tasks,
            'users' => $user,
            'userLimit' => config('magicNumbers.TASKOVERVIEWUSERLIMIT'),
            'currentUser' => $currentUserId,
            'showAction' => true,
            'showAssignUser' => true,
            'showRipInfo' => false,
            ]);
    }

    /**
     * Function to get user's companies ids
     * @param $currentUserId
     * @return array
     */ 

     public static function getUserAccociatedCompany($currentUserId)
     {
        $companyIds = [];
       $userRole = ModelHasRole::where('model_id', $currentUserId)->first();
        if($userRole){
            if($userRole->role_id != config('setting.roles.superadmin')){
                $usersCompany = CompanyParent::join('companies', 'company_parents.company_id', 'companies.id')
                ->where('parent_id', $currentUserId)
                ->where('companies.status', 1)
                ->get();
                foreach ($usersCompany as $key => $value) {
                   array_push($companyIds, $value->company_id);
                }
                return (object)['company' => $companyIds, 'role' =>$userRole->role_id];
            }
            $usersCompany = Company::all();
                foreach ($usersCompany as $key => $value) {
                   array_push($companyIds, $value->id);
                }
                return (object)['company' => $companyIds, 'role' => $userRole ? $userRole->role_id : ''];
        }
        else{
            return (object)['company' => [], 'role' => $userRole ? $userRole->role_id : ''];
        }
     }

     /**
     * Function to get current company's user
     * 
     * @return array
     */ 
     public static function getCompanyUsers()
     {
       return $users = User::whereHas('parent', function ($query) {
            $query->where('company_id', session('company_id'));
        })->get()->unique();
     }
}
