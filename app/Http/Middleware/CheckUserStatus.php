<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class CheckUserStatus
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            if (auth()->user()->status == 1) {
                return $next($request);
            } else {
                Auth::logout();
                return redirect('login')->with(['flashMessage' => 'Dear user, your account has been deactivated. Please contact the admin for further queries',
                    'class' => 'alert-error']);
            }
        } else {
            return redirect('login');
        }
    }
}
