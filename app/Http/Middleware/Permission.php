<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Route;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission as SpatiePermissions;

class Permission
{
    /**
     * Handle an incoming request.
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        $permissionName = Route::current()->getName();
        if ($user->hasAnyRole(['Super Admin'])) { 
            return $next($request);
        } elseif ($user->hasPermissionTo($permissionName)) { 
            return $next($request);
        } elseif ($request->ajax()) { 
            echo '<div class="ajax-permi-err"><span class="text-danger"><i class="fa fa-lock" aria-hidden="true"></i>
Access Denied. </span><div><p>Please contact Admin for any query.</p></div></div>';die;
        } else { 
            return redirect('home')->with(['flashMessage' => 'You do not have the permission to access this item',
                'class' => 'alert-error']);
        }
    }
}
