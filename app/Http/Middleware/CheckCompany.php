<?php

namespace App\Http\Middleware;

use App\Model\Company;
use App\User;
use App\Model\CompanyParent;


use Closure;

class CheckCompany
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userId = \Auth::user()->id;
        $user = User::find($userId);
        $currentCompany = Company::where('id', session('company_id'))->get();
        if ($user->hasAnyRole(['Super Admin'])) {
            $companies = Company::orderBy('name')->get();
            $firstCompany = Company::orderBy('created_at', 'desc')->first();
            if (!empty($firstCompany)) {
                if (empty(session('company_id'))) {
                    session()->put(['company_id' => $firstCompany->id]);
                    session()->put(['company_name' => $firstCompany->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($firstCompany->name));
                    session()->put(['initials' => $initials]);

                }
                if ($currentCompany->isEmpty()) {
                    session()->put(['company_id' => $firstCompany->id]);
                    session()->put(['company_name' => $firstCompany->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($firstCompany->name));
                    session()->put(['initials' => $initials]);
                } elseif ($currentCompany->first()->name != session(['company_name'])) {
                    session()->put(['company_name' => $currentCompany->first()->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($currentCompany->first()->name));
                    session()->put(['initials' => $initials]);
                }
                session()->put(['companies' => $companies]);
            } else {
                session()->put(['company_id' => 0]);
                session()->put(['company_name' => 'Build Impossible']);
                $initials = 'BI';
                session()->put(['initials' => $initials]);
                session()->put(['companies' => '']);
            }
        } else {
            $companies = CompanyParent::with('companies')->whereHas('companies', function ($query) {
                $query->orderBy('name')->where('status', 1);
            })->where('parent_id', $userId)->get()->unique('company_id');
            $firstCompany = $companies->first();
            if (!empty($firstCompany)) {
                if (empty(session('company_id'))) {
                    session()->put(['company_id' => $firstCompany->companies->id]);
                    session()->put(['company_name' => $firstCompany->companies->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($firstCompany->companies->name));
                    session()->put(['initials' => $initials]);

                }
                if ($currentCompany->isEmpty()) {
                    session()->put(['company_id' => $firstCompany->companies->id]);
                    session()->put(['company_name' => $firstCompany->companies->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($firstCompany->companies->name));
                    session()->put(['initials' => $initials]);
                } elseif ($currentCompany->first()->name != session(['company_name'])) {
                    session()->put(['company_name' => $currentCompany->first()->name]);
                    session()->put(['company_logo' => $firstCompany->logo]);
                    $initials = strtoupper($this->initials($currentCompany->first()->name));
                    session()->put(['initials' => $initials]);
                }
                session()->put(['companies' => $companies]);
            } else {
                User::where('id',$userId)->update(['wp_token' => '']);
                \Auth::logout($user);
                return redirect('login')->with(['flashMessage' => 'You are not associated with any company, Please ask admin to assign company',
                    'class' => 'alert-error']);
            }
        }


        return $next($request);
    }

    private function initials($name)
    {
        $words = explode(" ", $name);
        $initials = "";
        $i = 0;
        foreach ($words as $w) {
            if ($i > 2) {
                break;
            }
            $initials .= $w[0];
            $i++;
        }
        return $initials;
    }
}
