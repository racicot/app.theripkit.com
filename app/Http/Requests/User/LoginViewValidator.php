<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class LoginViewValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bg_image' => 'max:4096|mimes:jpeg,png,gif',
        ];
    }

    /**
     * Format the error messages
     * @return array
     */
    public function messages()
    {
        return [
            'bg_image.max' => 'The image size cannot be greater than 4 MB',
            'bg_image.mimes' => 'The image must be a file of type: jpeg, png or gif'
        ];
    }
}
