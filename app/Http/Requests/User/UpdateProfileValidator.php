<?php

namespace App\Http\Requests\User;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateProfileValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(Request $request)
    {
        return [
            'name' => 'required|max:255',
            'email' => 'required|email|' .
                Rule::unique('users')->ignore(auth()->user()->id),
            'phone' => 'nullable|numeric',
            'image' => 'max:1024|mimes:jpeg,png'
        ];
    }

    /**
     * Format the error messages
     * @return array
     */
    public function messages()
    {
        return [
            'image.max' => 'The image size cannot be greater than 1 MB'
        ];
    }
}
