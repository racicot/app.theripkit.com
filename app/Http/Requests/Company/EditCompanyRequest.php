<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class EditCompanyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
            [
                'image' => 'mimes:jpeg,png,gif',
                'company_name' => 'required|min:2|' . Rule::unique('companies','name')->ignore(base64_decode(request()->company)),
                'phone' => 'required|numeric',
            ];
    }

    public function messages()
    {
        return [
            'owner.owner' => 'The owner field is required'
        ];
    }
}
