<?php

namespace App\Http\Requests\Role;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleData extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'permission' => 'required',
            'name' => 'unique:roles,name',
        ];
    }
    
    public function messages()
    {
        return [
            'name.required' => 'Role name cannot be empty',
            'name.unique' => 'A role with this name already exists'
        ];
    }
}
