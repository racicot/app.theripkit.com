<?php

namespace App\Http\Requests\Rip;

use Illuminate\Foundation\Http\FormRequest;

class AddCategoryValidator extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category' => 'required',
            'goal' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'category.required' => 'Please choose a category first',
            'goal.required' => 'Goal cannot be empty'
        ];
    }
}
