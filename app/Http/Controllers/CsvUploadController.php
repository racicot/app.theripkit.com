<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\CompanyParent;
use App\Model\CsvUpload;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\MessageBag;
use Illuminate\Validation\Rule;
use App\User;
use App\Model\Company;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\User\ImportCsvRequest;
use Yajra\Datatables\Datatables;
use Maatwebsite\Excel\Facades\Excel;
use App\Exceptions\ImportException;
use Illuminate\Support\Facades\DB;

class CsvUploadController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        $this->middleware('permissions')->except(['csvData', 'sampleCsv', 'updateCsvData', 'validateCsv']);

    }

    public function massUpload()
    {
        return view('auth.user.mass_upload');
    }

    /**
     * @param ImportCsvRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function validateCsv(ImportCsvRequest $request)
    {
        $errors = new MessageBag();
        $mimeType = $_FILES['csv']['type'];
        try {
            if ($request->hasFile('csv') && (in_array($mimeType, config('setting.csv_mime_type')))) {
                $path = $request->file('csv')->getRealPath();
                $name = $request->file('csv')->getClientOriginalName();
                $file_name = Carbon::now()->timestamp . '_' . $request->file('csv')->getClientOriginalName();
                Excel::load($path, function ($reader) use ($errors, $request) {
                    $rows = $reader->get();
                    if($request->action == 'add') {
                        $errors = $this->checkCsvValidation($rows, $errors);
                    }elseif ($request->action == 'edit'){
                        $errors = $this->editCsvValidation($rows, $errors);

                    }
                });
                if ($errors->any()) {
                    $htm = '';                    
                    $errorArray = $errors->toArray();
                    foreach ($errorArray as $err) { 
                        
                        $htm.= "<div class='has-error'>
                                    <span class='help-block'>
                                        <strong>".$err[0]."</strong>
                                    </span>
                                </div>"; 
                    }
                    
                    return response(['errors'=> $htm], 422);
//                    return back()->with('errors', $errors);
                } else {
                    
                    $request->file('csv')->storeAs('', $file_name, 'system_files');
//                    return view('auth.user.mass_upload')->with([
//                    
//                        'success' => 'Your file is valid. Please click "DONE" to upload the file',
//                        'file_name' => $file_name,
//                        'name' => $name,
//                        'isValidated' => true
//                    ]);
                     
                    return $this->updateCsvData($request, $file_name);
                }
            } else {
                return response(['fail'=> config('flashmessage.VALIDCSV')]);

            }
        } catch (\Exception $e) {
            return back()->with(['fail' => $e->getMessage()]);

        }
    }

    /** Function use to update the csv data in the database after validation
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateCsvData(Request $request, $file_name = '')
    {
        try {

            $fileName = $request->fileName ? $request->fileName : $file_name;
            $path = storage_path('app/public/system_files') . '/' . $fileName;
            DB::beginTransaction();
            Excel::load($path, function ($reader) {
                $rows = $reader->get();
                foreach ($rows as $key => $value) {
                    $name = ucfirst($value->fname) . ' ' . ucfirst($value->lname);
                    $emailtoken = base64_encode(\Carbon\Carbon::now());
                    $password = uniqid();
                    $email = $value->email;
                    $user = User::updateOrcreate([
                        'email' => $value->email],[
                        'name' => $name,
                        'password' => Hash::make($password),
                        'emailToken' => $emailtoken,
                        'status' => 1,
                    ]);
                    $user->syncRoles($value->role);
                    CompanyParent::where('parent_id', $user->id)->delete();

                    Company::whereIn('name',
                        explode(config('setting.csv_actions.csv_deliminator'), session('company_name')))->each(function (
                        $company
                    ) use ($user) {
                        CompanyParent::create([
                            'parent_id' => $user->id,
                            'company_id' => $company->id
                        ]);
                    });


                    $url = url('/') . '/userLogin/' . $emailtoken;
                    \Mail::send('email.createUserEmail',
                        ['url' => $url, 'password' => $password, 'email' => $email, 'name' => $name],
                        function ($message) use ($email) {
                            $message->subject('BuildApp:Account Created');
                            $message->to($email);
                            $message->replyTo(config('setting.email.super_admin'));
                        });
                }
            });

            CsvUpload::create([
                'file_name' => $fileName,
                'uploader' => auth()->user()->name,
                'company_id'=>session('company_id'),
            ]);
            DB::commit();
            session()->put('success', config('flashmessage.CSVUPLOAD'));
            return redirect('massUpload');

        } catch (\Exception $e) {
            return back()->with(['fail' => $e->getMessage()]);        }
    }

    /**Function use to show the data in csv datatables
     * @return mixed
     */
    public function csvData()
    {
        $csv = CsvUpload::where('company_id', session('company_id'));
        return Datatables::of($csv)
            ->addcolumn('size', function ($file) {
                return $file->size();
            })
            ->addColumn('created_at', function ($file) {
                $dt = Carbon::parse($file->created_at);
                $content = $dt->format('M j, Y \a\t H:i A');
                return $content;
            })
            ->addcolumn('link', function ($file) {
                $content = "<a href='" . $file->getLink() . "'>Download</a>";
                return $content;
            })
            ->rawColumns(['link'])->make(true);
    }

    public function sampleCsv()
    {  
        $path = storage_path('app/public/sample.csv');
        return response()->download($path);
    }

    /** function to validate uploaded csv data
     * @param $rows
     * @param $errors
     * @return mixed
     */
    private function checkCsvValidation($rows, $errors){
        
        foreach ($rows as $key => $value) {
            session(['key' => $key + 1]);
            $validator = Validator::make($value->toArray(), [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email|unique:users,email|duplicate',
                //'company' => 'required|multiple',
                'role' => ['required', 'exists:roles,name', Rule::notIn(['Super Admin'])]
            ],
                [
                    'fname.required' => 'Error: At line ' . ($key + 1) . '. Empty first name',
                    'lname.required' => 'Error: At line ' . ($key + 1) . '. Empty last name',
                    //'company.required' => 'Error: At line ' . ($key + 1) . '. Empty company name',
                    //'company.multiple' => '',
                    'role.required' => 'Error: At line ' . ($key + 1) . '. Empty role name',
                    'role.exists' => 'Error: At line ' . ($key + 1) . '. The role named ' . $value->role . ' does not exist in the database',
                    'role.not_in' => 'Error: At line ' . ($key + 1) . '. You cannot create a user with the role "' . $value->role . '"',
                    'email.required' => 'Error: At line ' . ($key + 1) . '. Empty email field',
                    'email.unique' => 'Error: At line ' . ($key + 1) . '. A user with the email ' . $value->email . ' already exist in the database',
                    'email.email' => 'Error: At line ' . ($key + 1) . '. Invalid email format',
                ]);

            if ($validator->fails()) {
                $errorArray = array_flatten($validator->errors()->toArray());
                foreach ($errorArray as $errorMessage) {
                    $errors->add($key + 1, $errorMessage);
                }
            }
            session()->pull('key');
        }
        return $errors;
    }

    /** function to validate uploaded csv data in edit mode
     * @param $rows
     * @param $errors
     * @return mixed
     */
    private function editCsvValidation($rows, $errors){
        foreach ($rows as $key => $value) {
            session(['key' => $key + 1]);
            $validator = Validator::make($value->toArray(), [
                'fname' => 'required',
                'lname' => 'required',
                'email' => 'required|email|duplicate',
               // 'company' => 'required|multiple',
                'role' => ['required', 'exists:roles,name', Rule::notIn(['Super Admin'])]
            ],
                [
                    'fname.required' => 'Error: At line ' . ($key + 1) . '. Empty first name',
                    'lname.required' => 'Error: At line ' . ($key + 1) . '. Empty last name',
                    //'company.required' => 'Error: At line ' . ($key + 1) . '. Empty company name',
                    //'company.multiple' => '',
                    'role.required' => 'Error: At line ' . ($key + 1) . '. Empty role name',
                    'role.exists' => 'Error: At line ' . ($key + 1) . '. The role named ' . $value->role . ' does not exist in the database',
                    'role.not_in' => 'Error: At line ' . ($key + 1) . '. You cannot create a user with the role "' . $value->role . '"',
                    'email.required' => 'Error: At line ' . ($key + 1) . '. Empty email field',
                    'email.email' => 'Error: At line ' . ($key + 1) . '. Invalid email format',
                ]);

            if ($validator->fails()) {
                $errorArray = array_flatten($validator->errors()->toArray());
                foreach ($errorArray as $errorMessage) {
                    $errors->add($key + 1, $errorMessage);
                }
            }
            session()->pull('key');
        }
        return $errors;
    }
}
