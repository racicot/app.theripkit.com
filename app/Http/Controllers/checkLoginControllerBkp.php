<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Company;
use App\Model\Portfolio;
use App\Model\CompanyParent;
use App\Model\Rip;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class CheckLoginController extends Controller
{   

    /**
     * getUserdetails function
     *
     * @param Request $request
     * @return blade
     */
    public function getRipkitRipdetails(Request $request)
    {
        $action = '';
        if($request->company_name == ''){
            return 'You need to add your company name in "BuildImpossible" to access RIPKIT';
        }
        if($request->email == ''){
            return 'You need to add your email in "BuildImpossible" to access RIPKIT';
        }
        if($request->user_name == ''){
            return 'You need to add your name in "BuildImpossible" to access RIPKIT';
        }
        if(isset($request->action)){
            $action = $request->action;
        }
        
        $company_name = base64_decode($request->company_name);
        $email = base64_decode($request->email);
        $user_name = base64_decode($request->user_name);
        $user = DB::table('users')->where('email',$email)->first();
        if($user){      

            if(isset(Auth::user()->id) && Auth::user()->id == $user->id){ //..if user already exist
                
                if($user->status != 1){
                    return 'Dear user, your account has been deactivated. Please contact the admin for further queries';
                }   
    
                $company = DB::table('users')
                ->Join('company_parents', 'users.id', '=', 'company_parents.parent_id')
                ->leftJoin('companies', 'company_parents.company_id', '=', 'companies.id')
                ->where('email',$email)->where('companies.name',$company_name)->select('email', 'companies.*')->first();
                            
                if(!$company){
                   $company =  $this->createCompany($company_name,$user);  
                }
                else if($company->status != 1){
                    return 'Dear user, your company has been deactivated. Please contact the admin for further queries';
                }    
                
                $this->createUserCompanyRelation($user->id, $company->id);
                 //..get rip data...
    
                 $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '0')->latest()->first();
                 $left = 0;
                 if($rips){
                    $ripObject = new RipController();
                    $left = $ripObject->datePeriod($rips);
                 }

                 $this->setCompanySession($company);
                return view('wordpress-template.rip_period_progress')->with(['email' => $request->email, 'currentRip' => $rips, 'left' => $left]);
    
            }
            else if($action == 'dologin'){
                return $this->loginUser($user_name, $email, $company_name, $user);
            }
            else{
                return view('wordpress-template.wordpress_login_to_ripkit')->with([ 'company_name' =>$request->company_name, 'email' =>$request->email, 'user_name' =>$request->user_name]);
            }
        }
        else if($action == 'dologin'){
            return $this->loginUser($user_name, $email, $company_name, '');
        }
        else{  
            return view('wordpress-template.wordpress_login_to_ripkit')->with([ 'company_name' =>$request->company_name, 'email' =>$request->email, 'user_name' =>$request->user_name]);        
         
        }
    }

    /**
     * loginUser function
     *
     * @param [type] $user_name
     * @param [type] $email
     * @param [type] $company_name
     * @param [type] $user
     * @return view
     */
    public function loginUser($user_name, $email, $company_name, $user){

        // Register User and Company 
        DB::beginTransaction();
            try {
                if($user == ''){ //..register user
                    $user = User::create([
                        'name' => $user_name,
                        'email' => $email,
                        'password' => bcrypt(str_random(10)),
                    ]);
                    $user->assignRole('Admin');
                }               
              
                $company = DB::table('companies')->where('name', $company_name)->first();
               
                if(!$company){
                    $company = $this->createCompany($company_name, $user);
                }                
                
                $this->createUserCompanyRelation($user->id, $company->id);
                DB::commit();
                $this->doLogin($user->id, $company);  
                return redirect('overview-rip');
            } catch (\Exception $e) {
                DB::rollback();
            }
    }

    /**
     * createUserCompanyRelation function
     *
     * @param [type] $userId
     * @param [type] $companyId
     * @return void
     */
    public function createUserCompanyRelation($userId, $companyId){
      
        $relation = CompanyParent::where('parent_id', $userId)->where('company_id', $companyId)->first();
        if(!$relation){
            CompanyParent::create([
                'parent_id' => $userId,
                'company_id' => $companyId
            ]);
        }

    }

    /**
     * createCompany function
     *
     * @param [type] $name
     * @param [type] $user
     * @return company
     */
    public function createCompany($name,  $user){
        $company =   Company::create([
            'name' => $name,
            'status' => 1,
        ]);

        Portfolio::create([
            'company_id' => $company->id
        ]);
        return $company;
    }

    /**
     * doLogin function
     *
     * @param [type] $user_id
     * @param [type] $compamy
     * @return void
     */
     function doLogin($user_id, $company){
        Auth::loginUsingId($user_id);
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        //..set company session
        $this->setCompanySession($company);
     }

     /**
      * setCompanySession function
      *
      * @param [type] $company
      * @return void
      */
    public function  setCompanySession($company){
        session()->put(['company_id' => $company->id]);
        session()->put(['company_name' => $company->name]);
        session()->put(['company_logo' => $company->logo]);
        $initials = strtoupper($this->initials($company->name));
        session()->put(['initials' => $initials]);
        session()->put(['companies' => $company]);
     }

     /**
      * initials function
      *
      * @param [type] $name
      * @return initials
      */
     public function initials($name)
    {
        $words = explode(" ", $name);
        $initials = "";
        $i = 0;
        foreach ($words as $w) {
            if ($i > 2) {
                break;
            }
            $initials .= $w[0];
            $i++;
        }
        return $initials;
    }
    
}
