<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ScheduleMeeting as Meeting;

class ScheduleMeeting extends Controller
{
    /** Create Meeting
     * @return json
     */
    public function createMeeting()
    {
        return Meeting::createMeeting();
    }

    /** Get all company users
     * @return json
     */
    public function getCompanyUserList()
    {
        return Meeting::getCompanyUserList();
    }

    /** Get Rips meeting list
     * @param int $ripId
     * @return json
     */
    public function getMeetings($ripId)
    {
        return Meeting::getMeetings($ripId);
    }

    /** Get participants
     * @param int $meetingId
     * @return json
     */
    public function getParticipants($meetingId)
    {
        return Meeting::getParticipants($meetingId);
    }

    /** Delete Meeting
     * @param int $meetingId
     * @return json
     */
    public function deleteMeeting($meetingId)
    {
        return Meeting::deleteMeeting($meetingId);
    }
}
