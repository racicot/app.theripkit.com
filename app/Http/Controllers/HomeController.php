<?php

namespace App\Http\Controllers;

use App\Model\CompanyParent;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Model\Company;
use App\User;
use App\Model\Rip;
use App\Model\Category;
use App\Model\TeamMember;
use App\Model\Task;
use App\Model\Goal;
use Illuminate\Database\Eloquent\Collection;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware(['auth', 'CheckCompany', 'userstatus']);
    }

    /**
     * Show the application dashboard.
     * Show the assigned companies in the dashboard company  dropdown
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        return redirect('/overview-rip');
        //return view('home');
    }

    /**
     * Show the to do items for selected user
     * @return \Illuminate\Http\Response
     */
    public function showTodo($id)
    {
        return view('home');
    }

    /**
     * [changeCompany function use to change the company for multi company user]
     * @param  $id
     * @return  Home
     */
    public function changeCompany($id)
    {
        $userId = \Auth::user()->id;
        $company = Company::find($id);
        
        /*Check whether user is assigned to the selected company*/
        if (!auth()->user()->isCompanyAssigned($id)) {
            $msg_validation = config('flashmessage.ASSOCIATEDWITH'). $company->name .config('flashmessage.COMPANY');
            return back()->with(['fail' => $msg_validation]);
        }
        /*Check whether user is accessing an active company or not*/
        if (!auth()->user()->isCompanyActive($id)) {            
            $msg_validation = config('flashmessage.THECOMPANY'). $company->name .config('flashmessage.HASBEENDEACTIVATED');
            return back()->with(['fail' => $msg_validation]);
        }
        
        $companies = CompanyParent::where('parent_id', $userId)->get();
        if (!empty($company->id)) {
            session(['company_id' => $company->id]);
            session(['company_name' => $company->name]);
            $initials = strtoupper($this->initials($company->name));
            session(['initials' => $initials]);
            session(['companies' => $companies]);
            session()->pull('what_does', 'default');
            session()->pull('what_matter', 'default');            
            return back()->with(['success' => str_replace('<company_name>', $company->name, config('flashmessage.COMPANYSWITCH'))]);
        } else {
            return back()->with(['fail' => config('flashmessage.COMPANYREMOVED')]);
        }
    }

    /**
     * [initials function get the initial of the company name]
     * @param   $name
     * @return  $initials
     */
    private function initials($name)
    {
        $words = explode(" ", $name);
        $initials = "";
        $i = 0;
        foreach ($words as $w) {
            if ($i > 2) {
                break;
            }
            $initials .= $w[0];
            $i++;
        }

        return $initials;
    }
    
    /**
     * Get Guided Tour Watched status 
     * for enabling it on first login
     * @return type
     */
    public function checkTourValue() {
        $tour_watched = 0;
        if(isset(auth()->user()->watched_guided_tour)) {
            $tour_watched = auth()->user()->watched_guided_tour;
            $user = auth()->user();
            $user->watched_guided_tour = 1;
            $user->save();
        }
        if(Company::ifLoginDeviceMobile() == 1){
            $tour_watched = 1;
        }
//        if($tour_watched == 0){
//            $email = auth()->user()->email;
//            $user_name = auth()->user()->name;
//            $subject = '🎉 Announcing the launch of RIPKIT V3 ';
//            $mailInfo = ['email' => $email, 'name' => $user_name];
//
//            \Mail::to($email)->send(new \App\Mail\AnnouncementMail($mailInfo, $subject));
//        }
        return $tour_watched;
    }

}
