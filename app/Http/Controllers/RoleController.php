<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Http\Requests\Role\StoreRoleData;
use App\Http\Requests\Role\UpdateRoleData;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;

class RoleController extends Controller
{
    /**
     * RoleController constructor.
     */
    public function __construct()
    {
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        $this->middleware('permissions')->except(['ajaxData']);
    }

    /**
     * Display a listing of the Roles.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles = Role::all();
        return view('auth.roles.index')->with(compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleData $request)
    {
        \DB::beginTransaction();
        $role = Role::create(['name' => $request->name]);
        $permission = Permission::create([
            'name' => 'Create ' . $role->name,
            'group_name' => 'Users',
            'guard_name' => 'web'
        ]);
        Role::findByName('Super Admin')->givePermissionTo($permission->name);
        Role::findByName('Admin')->givePermissionTo($permission->name);
        \DB::commit();
        session()->put('success', config('flashmessage.ROLECREATED'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = base64_decode($id);
        $role = Role::find($id);
        $user = auth()->user();
        $userPermissions = $user->getPermissionsViaRoles()->filter(function ($item) {
            return ($item->group_name == 'Users');
        })->pluck('name')->toArray();
        $editableRoles = Role::all()->filter(function ($item) use ($userPermissions) {
            return (in_array('Create ' . $item->name, $userPermissions));
        })->pluck('id')->toArray();
        if (!in_array($role->id, $editableRoles)) {
            dd('Dear user. You do not have the permission to edit this role');
        }
        $role = Role::with('permissions')->find($id);
        $rolePermissions = $role->permissions->sortBy('group_name')->pluck('name')->toArray();
        $permissions = Permission::where('group_name', '!=', null)->get()->sortBy('group_name');
        $groups = Permission::where('group_name', '!=',
            null)->get()->sortBy('group_name')->pluck('group_name')->unique();
        return view('auth.roles.edit')->with([
            'role' => $role,
            'permissions' => $permissions,
            'rolePermissions' => $rolePermissions,
            'groups' => $groups,
            'id' => $id
        ]);
    }

    /**
     * Update the specified role in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleData $request, $id)
    {
        $role = Role::find($id);
        $role->updated_at = date('Y-m-d H:i:s');
        $role->save();
        if ($role->name == 'Admin') {
            $nullPermissions = Permission::where('group_name', null)->get()->pluck('name')->toArray();
            $permissionsArray = array_merge($request->permission, $nullPermissions);
            $role->syncPermissions($permissionsArray);
        }
        $role->syncPermissions($request->permission);
        session()->put('success', config('flashmessage.ROLEUPDATED'));
//        return redirect('roles')->with(['success' => config('flashmessage.ROLEUPDATED')]);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        Permission::where('name', 'Create ' . $role->name)->delete();
        $role->delete();
        app()['cache']->forget('spatie.permission.cache');
        session()->put('fail', config('flashmessage.ROLEDELETED'));
        return back();

    }

    /**
     * Get the ajax data for the grid.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxData()
    {
        $user = auth()->user();
        $userPermissions = $user->getPermissionsViaRoles()->filter(function ($item) {
            return ($item->group_name == 'Users');
        })->pluck('name')->toArray();
        $roles = Role::all();
        $editableRoles = Role::all()->filter(function ($item) use ($userPermissions) {
            return (in_array('Create ' . $item->name, $userPermissions));
        })->pluck('id')->toArray();
        return Datatables::of($roles)
            ->addColumn('created_at', function ($user) {
                $dt = Carbon::parse($user->created_at);
                $content = $dt->format('M j, Y \a\t H:i A');
                return $content;
            })
            ->addColumn('updated_at', function ($user) {
                $dt = Carbon::parse($user->updated_at);
                $content = $dt->format('M j, Y \a\t H:i A');
                return $content;
            })
            ->addColumn('actions', function ($role) use ($user, $editableRoles) {
            if (in_array($role->id, $editableRoles)) {
                $content = '<a href="' . url('roles/' . base64_encode($role->id) . '/edit') . '" class="editRoleButton" data-toggle="modal" data-target="#editRoleModal" >Edit</a>';
                if (auth()->user()->can('Delete A Role') && ($role->name != 'Admin' && $role->name != 'Super Admin')) {
                    $content .= '<form method="post" id="deleteRole_' . $role->id . '" class="delete-form roleDelete" action="' . url("roles") . '/' . $role->id . '">
                    ' . method_field("DELETE") .
                        csrf_field() . '
                     <button type="submit" roleId="' . $role->id . '" class="btn-link delete-role">Delete</button>
                    </form>';
                }
            } else {
                $content = '<span class="error-msg cust-msg">Permission Denied</span>';
            }
            return $content;
        })
                
        ->addColumn('name', function ($role) {
            $content = "<div class='role_name_" . $role->id . "'>".$role->name."</div>";
            return $content;
        })->rawColumns(['actions','name'])->make(true);
    }
}
