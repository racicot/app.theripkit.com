<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Rip;
use App\Model\Goal;
use App\User;
use App\Model\Comment;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class RipExportController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
    }

    /** Download all rip period company wise in Excle
     * @param $request
     * @return
     */
    public function downloadExcel(){

		$rips = Rip::with(['category', 'category.goals', 'category.goals.task', 'category.goals.comment'])->where('company_id', session('company_id'))->where('expired', '=', '0')->get()->toArray();

 		$fname = "download-rip-period-".date("m-d-Y");
		header('Content-Description: File Transfer');
		header('Content-type: application/ms-excel');
		header("Content-Disposition: attachment; filename=".$fname.".xls");
		header('Content-Transfer-Encoding: binary');
		header('Connection: Keep-Alive');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');

		print "<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\"><style>

		@page
		    {margin:1.0in .75in 1.0in .75in;
		    mso-header-margin:.5in;
		    mso-footer-margin:.5in;}
		tr
		    {mso-height-source:auto;}
		col
		    {mso-width-source:auto;}
		br
		    {mso-data-placement:same-cell;
		    }
		.head{
			font-size:12.0pt;
		}    
		.style0
		    {mso-number-format:General;
		    text-align:general;
		    vertical-align:bottom;
		    white-space:nowrap;
		    mso-rotate:0;
		    mso-background-source:auto;
		    mso-pattern:auto;
		    color:windowtext;
		    font-size:12.0pt;
		    font-weight:400;
		    font-style:normal;
		    text-decoration:none;
		    font-family:Arial;
		    mso-generic-font-family:auto;
		    mso-font-charset:0;
		    border:none;
		    mso-protection:locked visible;
		    mso-style-name:Normal;
		    mso-style-id:0;}
		td
		    {mso-style-parent:style0;
		    padding-top:1px;
		    padding-right:1px;
		    padding-left:1px;
		    mso-ignore:padding;
		    color:windowtext;
		    font-size:14px;
		    background:#333;
		    font-weight:400;
		    font-style:normal;
		    text-decoration:none;
		    font-family:Arial;
		    mso-generic-font-family:auto;
		    mso-font-charset:0;
		    mso-number-format:General;
		    text-align:general;
		    vertical-align:bottom;
		    border:none;
		    mso-background-source:#333333;
		    mso-pattern:auto;
		    mso-protection:locked visible;
		    white-space:nowrap;
		    mso-rotate:0;}
		.grids
		    {mso-style-parent:style0;
		    border:.5pt solid windowtext;}.head{
		    font-weight:bold;
		    background:#333;
		    color:#fff;
		}

		</style>
		<head>
		<!--[if gte mso 9]><xml>
		<x:ExcelWorkbook>
		<x:ExcelWorksheets>
		<x:ExcelWorksheet>
		<x:Name>Rip Period</x:Name>
		<x:WorksheetOptions>
		<x:Print>
		</x:Print>
		</x:WorksheetOptions>
		</x:ExcelWorksheet>
		</x:ExcelWorksheets>
		</x:ExcelWorkbook>
		</xml>
		<![endif]-->
		</head>
		<body>";
		print '<table width="90%" border="1" style="border-collapse: collapse;" cellspacing="1" cellpadding="2" class="tbl_content">';
		if(count($rips) > 0){
			foreach($rips as $key=>$vals){
						print '<tr class="head" valign="bottom">
					<td align="center" style="background:#f5f5f5;color:#ff6800;text-align: center;font-size:22px;"><b> '.$vals["name"].'</b> 
					</td>
					<tr>
					<td align="center" style="background:#f5f5f5;text-align: center;font-size:22px;">
					 <span style="text-align:center;font-size:14px;"> '.date("F d Y", strtotime($vals["start_date"])).' - '.date("F d Y", strtotime($vals["end_date"])).'  </span></td>
					 </tr> 
					 <tr><td>';
					print '<table width="100%" border="1"  cellspacing="1" cellpadding="2" class="tbl_content">';
					print '<tr class="head" valign="bottom">
						<td align="left" width="5%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>RIP Period(%)</b></td>
						<td align="left" width="10%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>Category</b></td>
						<td align="left" width="5%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>Category(%)</b></td>
						<td align="left" width="10%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;" ><b>Rip(s)</b></td>
						<td align="left" width="10%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;" ><b>Rip(s) Due Date</b></td>
						<td align="left" width="5%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;" ><b>Rip (%)</b></td>
						<td align="left" width="10%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;" ><b>Rip User(s)</b></td>	
						<td align="left" width="15%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>Task(s)</b></td>
						<td align="left" width="5%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>Task Status</b></td>
						<td align="left" width="10%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;"><b>Task User(s)</b></td>
						<td align="left" width="15%" style="background:#ff6800;color:#fff;text-align:center;font-size:14px;" ><b>Comment(s)</b></td>
						</tr>';
						print '<tr class="head" valign="bottom"><td align="center" style="background:#f5f5f5;color:#008000;text-align: center;font-size:14px;"><b>('.$this->getRipPercentage($vals['id']).'% Completed)</b></td>
						<td align="center" colspan="10" style="background:#f5f5f5;color:#008000;text-align: center;font-size:14px;"> </td>
						</tr>';

				if(count($vals['category']) > 0){
					$categoryCount = 1;
					foreach($vals['category'] as $cat_key=>$cat){
						print '<tr class="head" valign="bottom">
						<td align="center" valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;padding:10px 0 10px 0;"> </td>
						<td align="center"  valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;padding:10px 0 10px 0;">#'.$categoryCount.' '.$cat['name'].' </td>
						<td align="center"  valign=top style="background:#f5f5f5;color:#008000;text-align:left;font-size:14px;padding:10px 0 10px 0;"><b>('.$this->getAvgPer($vals['id'],$cat['id']).'% Completed)</b></td>
						<td align="center" colspan="8" style="background:#f5f5f5;color:#008000;text-align: center;font-size:14px;"> </td>
						</tr>';
						if(count($cat['goals']) > 0){
							$ripCount = 1;
							foreach($cat['goals'] as $goal_key=>$goal) {
								//..user
								$ripuser= '';
								if($goal['assign_to'] != ''){
							        $ripAssignedUserArray = explode(',',$goal['assign_to']);
									$ripAssignedUsers = User::find($ripAssignedUserArray);
									if(count($ripAssignedUsers) > 0){
										foreach($ripAssignedUsers as $kk=>$vv){
											if($ripuser == ''){
												$ripuser = $vv->name;
											}
											else{
												$ripuser.= ', '.$vv->name;
											}
										}
									}
								}

								print ' <tr class="head" valign="bottom">
								<td align="center"  colspan= "3" style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;"></td>
								<td align="left" valign=top style="background:#f5f5f5;padding:10px 0 10px 0;width:200px;color:#000;text-align:left;font-size:14px;padding-right:5px;" >'.$ripCount.') '.trim($goal['goal']).' </td>
								<td align="center"  valign=top style="background:#f5f5f5;text-align:left;font-size:14px;padding:10px 0 10px 0;">'.date("F d Y", strtotime($goal["due_date"])).'</td>
								<td align="center"  valign=top style="background:#f5f5f5;color:#008000;text-align:left;font-size:14px;padding:10px 0 10px 0;"><b>('.$goal['percentage_complete'].'% Completed)</b></td><td align="center"  valign=top style="background:#f5f5f5;color:#ff6800;text-align:left;font-size:14px;padding:10px 0 10px 0;">'.$ripuser.'</td>
								<td align="left" colspan= "4" style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;"></td>
								</tr>';
								if(count($goal['task']) > 0 || count($goal['comment']) > 0){

									print ' <tr class="head" valign="bottom">
									<td align="center"  style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;" colspan="7"></td>									
									<td align="left" valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;" colspan="3">';
									$taskcount = 1;
									foreach($goal['task'] as $task_key=>$task){
										
										$task_status = '<span style="color:#2dbe74;"><b>COMPLETED</b></span>';
								        if($task['percentage_complete'] != 100){
								        	$task_status = '<span style="color:#f14343;"><b>INCOMPLETE</b></span>';
								        }

								        //..user
								        $assignedUserArray = explode(',',$task['assign_to']);
										$assignedUsers = User::find($assignedUserArray);
								        $taskuser= '';
										if(count($assignedUsers) > 0){
											foreach($assignedUsers as $kk=>$vv){
												if($taskuser == ''){
													$taskuser = $vv->name;
												}
												else{
													$taskuser.= ', '.$vv->name;
												}
											}
										}
										print '<table width="100%" border="0"  > <tr><td align="left" valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;">';
										print $taskcount.") ".trim($task['name']).'</td>
										<td align="left" valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;"> '.$task_status.'</td> <td align="left" valign=top style="background:#f5f5f5;color:#ff6800;text-align:left;font-size:14px;">'.$taskuser.'</td> </tr> </table>';
										$taskcount++;
									}
									print '</td ><td align="left" valign=top style="background:#f5f5f5;color:#000;text-align:left;font-size:14px;">';
									$commentcount = 1;
									foreach($goal['comment'] as $c_key=>$comment){
										$mentionData = Comment::getMentionData($comment['message'],'edit');
										print $commentcount.") ".$mentionData['comment_data'].'<br>';
										$commentcount++;
									}
									print '</td></tr>';
								}
							$ripCount++;
							}


						}
					$categoryCount++;
					}					
				}
				print '</table></td> </tr>';
				print '<tr><td style="background:#f5f5f5;color:#ff6800;text-align:left;font-size:12px;"> </td></tr>';			
			}			
		}

		print '</table></body></html>';

	}

	public function getAvgPer($goal_type_id,$category_id)
    {
        return round(Goal::where([
            'category_id' => $category_id,
            'goal_type_id' => $goal_type_id
        ])->avg('percentage_complete'));
    }

     public function getRipPercentage($rip_period_id)
    {
        return round(Goal::where('goal_type_id', $rip_period_id)->avg('percentage_complete'));
    }
}
