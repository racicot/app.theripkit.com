<?php

namespace App\Http\Controllers;

use App\Events\AddCategoryName;
use App\Events\DeleteCategory;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Goal;
use Yajra\Datatables\Datatables;
use App\Http\Requests\Category\StoreCategoryRequest;
use App\Http\Requests\Category\EditCategoryRequest;


class CategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'CheckCompany']);
    }

    /**
     * Display a listing of the category.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('auth.category.index');
    }

    /**
     * Show the form for creating a new category.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created category in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCategoryRequest $request)
    {
        // dd($request);
        Category::create([
            'name' => $request->name,
            'goal_type_id' => $request->goal_type_id,
            'status' => $request->status == null ? 0 : 1,
        ]);
        return redirect('categories')->with(['flashMessage' => config('flashmessage.CATEGORYCREATED'), 'class' => 'alert-success']);

    }

    /**
     * Display the specified category.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified category.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('auth.category.edit')->with('category', $category);
    }

    /**
     * Update the specified category in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCategoryRequest $request, $id)
    {
        //dd($request);
        Category::find($id)->update([
            'name' => $request->name,
            'goal_type_id' => $request->goal_type_id,
            'status' => $request->status == null ? 0 : 1,
        ]);

        return redirect('categories')->with(['flashMessage' => config('flashmessage.CATEGORYUPDATED'), 'class' => 'alert-success']);

    }

    /**
     * Remove the specified category from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function deleteCategory(Request $request)
    {
        $category = Category::find($request->id);
        $goal_type_id = $category->goal_type_id;
        $category->delete();
        $response['ripAvgPer'] = round(Goal::where(['goal_type_id' => $goal_type_id])->avg('percentage_complete'));
        $response['ripId'] = $goal_type_id;
        $response['id'] = $request->id;
        broadcast(new DeleteCategory($request->id))->toOthers();
        return response()->json($response);
    }

    public function categoryName(Request $request)
    { 
        $category = Category::find($request->category_id);
        $category->update([
            'name' => strip_tags($request->category_name)
        ]);
        
        broadcast(new AddCategoryName($category->id))->toOthers();
        $response['name'] = $request->category_name;
        $response['id'] = $request->category_id;       
        echo json_encode($response);
    }

    /** function to broadcast the updated catgory name  on other connections
     * @param Request $request
     * @return json response
     */

    public function showCategoryName(Request $request)
    {
        $category = Category::find($request->data);
        if(!$category){
            $return['dataNull'] = true;
            return response()->json($return);
        }

        $return['dataNull'] = false;
        $return['name'] = $category->name;
        $return['categoryId'] = $category->id;
        return response()->json($return);
    }

    /**
     * Return the ajax data for the grid.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxData()
    {
        $category = Category::all();
        return Datatables::of(Category::query())->addColumn('actions', function ($category) {
            $content = '<a href="' . url('categories/' . $category->id . '/edit') . '" class="editCategoryButton" data-toggle="modal" data-target="#editCategoryModal" >Edit</a>';
            return $content;
        })->rawColumns(['actions'])->make(true);
    }
    
}
