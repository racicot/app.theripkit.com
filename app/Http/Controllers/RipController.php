<?php

namespace App\Http\Controllers;

use App\Events\AddCategory;
use App\Events\AddRipTab;
use App\Events\DeleteRipTab;
use App\Events\EditRipTab;
use App\Events\markExpiredRipTab;
use App\Events\revertExpiredRipTab;
use App\Jobs\ProcessEvent;
use App\Model\CompanyParent;
use App\Model\Notification;
use App\Notifications\NewEvent;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Model\Category;
use App\Model\Goal;
use App\Model\Rip;
use App\Model\Task;
use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Http\Traits\GoalTrait;
use App\Http\Traits\TaskTrait;
use App\Model\Comment;
use App\Model\Attachment;
use Illuminate\Notifications\Notifiable;


class RipController extends Controller
{
    use GoalTrait;
    use TaskTrait;

    public function __construct()
    {

        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        $this->middleware('permissions')->except([
            'ripTab',
            'showTask',
            'updateTask',
            'updateGoal',
            'showGoal',
            'showName',
            'showTaskName',
            'showDueDate',
            'showPercentage',
            'assignUser',
            'removeUser',
            'addComment',
            'editComment',
            'broadcastCategory',
            'editRipTab',
            'addRipTab',
            'portfolioUpdate',
            'showCompletedTask',
            'showUndoTask',
            'currentRip',
            'duplicateRip',
            'viewHistoryRip',
            'readNotification',
            'notificationData',
            'markExpired',
            'revertExpiredRip',
            'editGoal',
            'editRipPeriod',
            'editRipTabName',
            'userList',
            'editTask',
            'ripOverview'
        ]);
    }


    /**
     * Index function show the main page of RIP
     * @return View
     */
    Public function index(Request $request)
    {
        $goalUsers = [];
        if (session('company_id') == 0) {
            return redirect('home')->with(['flashMessage' => 'Please select the company', 'class' => 'alert-success']);

        }
       
        $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '0')->get();
        if ($request->id > 0) {
            $rips = Rip::where('company_id', session('company_id'))->get();
            
            $currentRip = Rip::find($request->id);
            if(empty($currentRip->id)){
                return redirect('home');
            }

            $companies = Auth::user()->getCompanies()->filter(function($value, $key) use($currentRip){ 
                return $value->id == $currentRip->company_id;
            });   
            if($companies->isEmpty()){
                return redirect('home')->with(['flashMessage' => config('flashmessage.RIPNOTASSOCIATED'), 'class' => 'alert-error']);
            }
            if($currentRip->company_id != session('company_id')){
                return redirect('home')->with(['fail' => config('flashmessage.RIPNOTASSOCIATED')]);
            }
        }         
        else {
            $currentRip = $rips->first();
        }
     
        if ($rips->isNotEmpty()) {
            $company_id = session('company_id');
            $categories = Category::with(['goals', 'goals.comment', 'goals.comment.attachment'])->where([
                'goal_type_id' => $currentRip->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
            $goals = Goal::with(['category', 'comment', 'comment.attachment'])->where([
                'goal_type_id' => $currentRip->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $url = url('addMonthlyGoal');
            $left = $this->datePeriod($currentRip);
        } else {
            $goals = array();
            $categories = array();
            $goalUsers = array();
            $url = '';
            $left = 0;
        }
        
        return view('auth.rip.index')->with([
            'goals' => $goals,
            'categories' => $categories,
            'goalusers' => $goalUsers,
            'url' => $url,
            'rips' => $rips,
            'left' => $left,
            'currentRip' => $currentRip,
            'current_page'=> '',
        ]);
    }

    /** Function add new rip in a company
     * @param Request $request
     * @return string
     */
    public function addRip(Request $request)
    {   
        /*
        check for duplicate rip period 
        */
        $rip = new Rip();
        $chek_duplicate_rp =  $rip->checkExistingRipPeriod(['name' => $request->name,'start_date' => $request->start_date, 'end_date' => $request->end_date]);
        
        if($chek_duplicate_rp == 0){

            $rip = Rip::create([
                'name' => strip_tags($request->name),
                'company_id' => session('company_id'),
                'start_date' => $request->start_date,
                'end_date' => $request->end_date
            ]);
            $url = url('rip/' . $rip->id);
            $response['company_id'] = $rip->company_id;
            $response['url'] = $url;
            $users = User::all(); 
            session()->put('success', config('flashmessage.RIPCREATE'));
            broadcast(new AddRipTab($rip->id))->toOthers();            
        }
        else{
            $response['status'] = 'Fail';
            $response['message'] = config('flashmessage.DUPLICATERIP');             
        } 
        return response()->json($response);   
    }

    /** Function to broadcas new rip in a company
     * @param Request $request
     * @return string
     */
    public function addRipTab(Request $request)
    {
        $rip = Rip::find($request->data);
        if(!$rip){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        $url = url('rip/' . $rip->id);
        $response['html'] = view('auth.rip.ajax.ajaxRipPeriod')->with(['rip' => $rip])->render();
        $response['company_id'] = $rip->company_id;
        $response['dataNull'] = false;
        return response()->json($response);
    }

    /** Function add new rip in a company
     * @param Request $request
     * @return string
     */
    public function editRip(Request $request)
    {   
        /*
        check for duplicate rip period 
        */
        $rip = new Rip();
        $chek_duplicate_rp =  $rip->checkExistingRipPeriod(['name' => $request->name,'start_date' => $request->start_date, 'end_date' => $request->end_date, 'id' => $request->id]);
        if($chek_duplicate_rp == 0){
            $rip = Rip::find($request->id);
            $rip->update(
                [
                    'name' => strip_tags($request->name),
                    'company_id' => session('company_id'),
                    'start_date' => $request->start_date,
                    'end_date' => $request->end_date
                ]);
            
            broadcast(new EditRipTab($rip->id))->toOthers();
            $response['status'] = 'Success';
            $response['id'] = $rip->id;
            $response['name'] = strip_tags($request->name);
            $response['start_date'] = date('M d, Y', strtotime($request->start_date));
            $response['end_date'] = date('M d, Y', strtotime($request->end_date));
            $response['left'] = $this->datePeriod($request);
            return response()->json($response);
        }    
        else{
            $response['status'] = 'Fail';
            $response['message'] = config('flashmessage.DUPLICATERIP'); 
            return response()->json($response);            
        }    
    }

    /** Function broadcast edited  rip content in other connections
     * @param Request $request
     * @return string
     */
    public function editRipTab(Request $request)
    {
        $input = $request->all();
        $rip = Rip::find($input['data']);
        if(!$rip){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        $response['id'] = $rip->id;
        $response['name'] = strip_tags($rip->name);
        $response['start_date'] = date('M d Y', strtotime($rip->start_date));
        $response['end_date'] = date('M d Y', strtotime($rip->end_date));
        $response['left'] = $this->datePeriod($rip);
        $response['dataNull'] = false;
        return response()->json($response);
    }

    /** Function returns the view for the new RIP
     * @param Request $request
     * @return View
     */
    public function ripTab(Request $request)
    {
        $company_id = session('company_id');
        $rip = Rip::find($request->id);
        $left = $this->datePeriod($rip);
        $categories = Category::with(['goals', 'goals.comment', 'goals.comment.attachment'])->where([
            'goal_type_id' => $rip->id,
            'company_id' => $company_id,
            'status' => 1
        ])->get();
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $goals = Goal::with(['category', 'comment', 'comment.attachment'])->where([
            'goal_type_id' => $request->id,
            'company_id' => $company_id,
            'status' => 1
        ])->get();
        $html = view('auth.rip.ajax.rip_template')->with([
            'goals' => $goals,
            'categories' => $categories,
            'goalusers' => $goalUsers,
            'currentRip' => $rip,
            'left' => $left
        ])->render();
        return $html;
    }

    /** Function delete the newly created RIP
     * @param Request $request
     * @return View
     */
    public function deleteTab(Request $request)
    {   
        $rip = Rip::find($request->id);
        $rip->delete();
        broadcast(new DeleteRipTab(url('/rip/'.$request->id)))->toOthers();
        session()->put('fail',config('flashmessage.RIPDELETE'));
        return url('/');
    }

    /**
     * [createUser function will create new  team member
     * @param  [type] $email [description]
     * @return [type]        [description]
     */
    private function createUser($email)
    {
        $password = uniqid();
        $email = $email;
        $name = $email;
        $company_id = session('company_id');
        $emailtoken = base64_encode(\Carbon\Carbon::now());
        \DB::beginTransaction();
        $user = User::create([
            'email' => $email,
            'name' => $name,
            'password' => Hash::make($password),
            'emailToken' => $emailtoken,
            'status' => 1,
        ]);

        $team_member = TeamMember::create([
            'company_id' => $company_id,
            'user_id' => $user->id,
            'status' => 1,
        ]);
        $user->syncRoles('Team Member');
        \DB::commit();

        $url = url('/') . '/updatePassword/' . $emailtoken;
        \Mail::send('email.createUserEmail', ['url' => $url, 'password' => $password, 'email' => $email],
            function ($message) use ($email) {
                $message->subject('RIPKIT: Account Created');
                $message->to($email);
                $message->replyTo(config('setting.email.super_admin'));
            });
        return $user->id;
    }

    /*
     * Function to show comment section through ajax
     */
    public function ajaxCommentSection(Request $request)
    {
        $goal = Goal::with(['comment', 'comment.attachment'])->where('id', $request->goalId)->get()->first();
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        
        return view('auth.rip.ajax/commentSection')->with(['goal' => $goal, 'goalusers' => $goalUsers])->render();
    }


    /** function add categoty in the current RIP through ajax
     * @param Request $request
     * @return mixed
     */
    public function addCategory(Request $request)
    {
        $company_id = session('company_id');
        $category = Category::create([
            'name' => strip_tags($request->category),
            'company_id' => $company_id,
            'goal_type_id' => $request->goal_type,
            'status' => 1
        ]);
        $currentRip = Rip::find($category->goal_type_id);
        $response['html'] = view('auth.rip.ajax.addCategory')->with([
            'category' => $category,
            'currentRip' => $currentRip
        ])->render();
        $response['rip_id'] = $request->goal_type;
        $response['cat_id'] = $category->id;
        broadcast(new AddCategory($category->id))->toOthers();
        return response()->json($response);
    }

    /** function to broacast newly added category  on other connections
     * @param Request $request
     * @return json response
     */

    public function broadcastCategory(Request $request)
    {
        $category = Category::find($request->data);
        if(!$category){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        $currentRip = Rip::find($category->goal_type_id);
        $response['rip_id'] = $category->goal_type_id;
        $response['dataNull'] = false;
        $response['html'] = view('auth.rip.ajax.addCategory')->with([
            'category' => $category,
            'currentRip' => $currentRip
        ])->render();
        return response()->json($response);
    }

    /** Function return the RIP history View
     * @param
     * @return view
     */
    public function ripTabHistory()
    {
        //$historyDate = Carbon::now()->format('Y/m/d');
        $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '1')->get();
        if ($rips->isNotEmpty()) {
            $company_id = session('company_id');
            $categories = Category::with(['goals', 'goals.comment', 'goals.comment.attachment'])->where([
                'goal_type_id' => $rips->first()->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
            $goals = Goal::with(['category', 'comment', 'comment.attachment'])->where([
                'goal_type_id' => $rips->first()->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $url = url('addMonthlyGoal');
            $left = $this->datePeriod($rips->first());
        } else {
            $goals = array();
            $categories = array();
            $goalUsers = array();
            $url = '';
            $left = 0;
        }
        return view('auth.rip.ajax.rip_history')->with([
            'goals' => $goals,
            'categories' => $categories,
            'goalusers' => $goalUsers,
            'url' => $url,
            'rips' => $rips,
            'left' => $left,
            'currentRip' => $rips->first()
        ])->render();
    }

    /** Function return the current RIP  View
     * @param
     * @return view
     */
    public function currentRip()
    {
        $now = Carbon::now()->format('Y/m/d');
        $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '0')->get();
        if ($rips->isNotEmpty()) {
            $company_id = session('company_id');
            $categories = Category::with(['goals', 'goals.comment', 'goals.comment.attachment'])->where([
                'goal_type_id' => $rips->first()->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
            $goals = Goal::with(['category', 'comment', 'comment.attachment'])->where([
                'goal_type_id' => $rips->first()->id,
                'company_id' => $company_id,
                'status' => 1
            ])->get();
            $url = url('addMonthlyGoal');
            $left = $this->datePeriod($rips->first());
        } else {
            $goals = array();
            $categories = array();
            $goalUsers = array();
            $url = '';
            $left = 0;
        }
        return view('auth.rip.ajax.current_rip')->with([
            'goals' => $goals,
            'categories' => $categories,
            'goalusers' => $goalUsers,
            'url' => $url,
            'rips' => $rips,
            'left' => $left,
            'currentRip' => $rips->first()
        ])->render();
    }

    /** function returns the view of the history rips
     * @param Request $request
     * @return mixed
     */
    function viewHistoryRip(Request $request)
    {
        $company_id = session('company_id');
        $rip = Rip::find($request->id);
        $left = $this->datePeriod($rip);
        $categories = Category::with(['goals', 'goals.comment', 'goals.comment.attachment'])->where([
            'goal_type_id' => $rip->id,
            'company_id' => $company_id,
            'status' => 1
        ])->get();
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $goals = Goal::with(['category', 'comment', 'comment.attachment'])->where([
            'goal_type_id' => $request->id,
            'company_id' => $company_id,
            'status' => 1
        ])->get();
        $html = view('auth.rip.ajax.rip_template')->with([
            'goals' => $goals,
            'categories' => $categories,
            'goalusers' => $goalUsers,
            'currentRip' => $rip,
            'left' => $left
        ])->render();
        return $html;
    }
    /**Function to duplicate the RIP period along with the related data
     * @param Request $request
     */
    public function duplicateRip(Request $request)
    {
        \DB::beginTransaction();
        DB::enableQueryLog();
        $oldData = Rip::with('category', 'category.goals')->find($request->id);
        $lastId =  $this->duplicateRelation($oldData, true);
        $this->duplicateComments($lastId, $request->id);
        $this->duplicateTasks($lastId, $request->id);
        \DB::commit();
        broadcast(new AddRipTab($lastId))->toOthers();  
        return redirect('rip/'.$lastId)->with(['success' => config('flashmessage.CURRENTRIP')]);
    }
    

    /** Recursive function to duplicate the RIP and related data
     * @param $model
     * @param bool $isOriginal
     * @param null $parentModel
     * @param string $relationKey
     */
    private function duplicateRelation($model, $isOriginal = false, $parentModel = null, $relationKey = "") {
        /**
         * @var Model $model
         */

        /**
         * @var Model $copiedModel
         */
        $copiedModel = $model->replicate();
        if($isOriginal) {
            $start = Carbon::now();
            $end = Carbon::now()->endOfMonth()->toDateString();
            $copiedModel->start_date = $start;
            $copiedModel->end_date = $end;
            $copiedModel->expired = '0';
        }elseif (!$isOriginal) {

            if ($relationKey == 'goals') {
                $copiedModel->goal_type_id = $parentModel->goal_type_id;
            }
            $copiedModel->{$parentModel->{$relationKey}()->getForeignKeyName()} = $parentModel->{$relationKey}()->getParentKey();
        }

        $copiedModel->save();

        foreach($model->getRelations() as $relationKey => $relation) {
            if($relation instanceof Model){
                $this->duplicateRelation($relation, false, $copiedModel, $relationKey);
            }elseif($relation instanceof Collection) {
                foreach ($relation as $item) {
                    $this->duplicateRelation($item, false, $copiedModel, $relationKey);
                }
            }
        }
        return $copiedModel->id;
    }

    /** Add Duplicate comments for duplicate rip
     * @param int $newRipId 
     * @param int $oldId
     */
    public function duplicateComments($newRipId, $oldId)
    {
        $oldData = Goal::where('goal_type_id', $oldId)->get();
        foreach ($oldData as $key => $value) {
            $copiedGoalId = $this->getCopiedGoalId($key, $newRipId);
            $oldComments = Comment::where('goal_id', $value->id)->get();
            foreach ($oldComments as $key => $val) {
                if ($copiedGoalId) {
                    $lastInsertedId = Comment::newComment($val, $copiedGoalId);
                    $this->addAttachmentsInDuplicateComments($val->id, $lastInsertedId);
                }
            }
        }
        return true;
    }

    /** Add Duplicate attachments for duplicate comments
     * @param int $oldCommentId 
     * @param int $newCommentId
     */
    public function addAttachmentsInDuplicateComments($oldCommentId, $newCommentId)
    {
        $attachments = Attachment::where('comment_id', $oldCommentId)->get();
        if (count($attachments) > 0) {
            foreach ($attachments as $key => $value) {
                Attachment::newAttachment($value, $newCommentId);
            }
        }
        return true;
    }

    /** Add Duplicate tasks for duplicate rip
     * @param int $newRipId 
     * @param int $oldId
     */
    public function duplicateTasks($newRipId, $oldId)
    {
        $oldData = Goal::where('goal_type_id', $oldId)->get();
        foreach ($oldData as $key => $value) {
            $copiedGoalId = $this->getCopiedGoalId($key, $newRipId);
            $oldTask = Task::where('goal_id', $value->id)->get();
            foreach ($oldTask as $key => $val) {
                if ($copiedGoalId) {
                    Task::newTask($val, $copiedGoalId);
                }
            }
        }
        return true;
    }

    /** Map old rip goal ids to duplicate rip goal ids
     * @param int $Oldkey 
     * @param int $copiedGoalId
     */
    public function getCopiedGoalId($Oldkey, $copiedGoalId)
    {
        $copyData = Goal::where('goal_type_id', $copiedGoalId)->get();
        foreach ($copyData as $key => $value) {
            if ($Oldkey == $key)
                return $value->id;
        }
        return null;
    }

    /** Function return the days left in the rip to end
     * @param $currentRip
     * @return int|string
     */
    public function datePeriod($currentRip)
    {
        $now = Carbon::now()->format('Y/m/d');
        $now1 = Carbon::now();
        $start = Carbon::parse($currentRip->start_date)->format('Y/m/d');
        $start1 = Carbon::parse($currentRip->start_date);
        $end = Carbon::parse($currentRip->end_date)->format('Y/m/d');
        $end1 = Carbon::parse($currentRip->end_date);
        if ($now > $end) {
            $left = '0 Day(s) left';
        } elseif ($now < $start) {
            $left = $now1->diffInDays($start1);
            $left = (int)$left + 1 . ' Day(s) To Start';
        } elseif ($now == $end) {
            $left = '0 Day(s) left';
        } else {
            $left = $now1->diffInDays($end1);
            $left = (int)$left + 1 . ' Day(s) Left';
        }
        return $left;
    }
    /** Function mark the notification as read
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */

    public function readNotification(Request $request)
    {
        $notification = Notification::find($request->id);
        $notification->update(['read_at' => Carbon::now()]);
        if(Auth::user()->hasAnyRole(['Super Admin'])) {
            $count = Notification::whereNull('read_at')->count();
            $response['id'] = $request->id;
            $response['count'] = $count;
        }else{
            $count = Notification::where('notifiable_id', Auth::user()->id)->whereNull('read_at')->count();
            $response['id'] = $request->id;
            $response['count'] = $count;
        }
        return response()->json($response);

    }

    /** Function return the notification data to notification sidebar
     * @return mixed
     */
    public function notificationData()
    {
        if(Auth::user()->hasAnyRole(['Super Admin'])) {
            $notifications = Notification::whereNull('read_at')->orderBy('created_at', 'desc')->get();
        } else{
            $notifications = Notification::where('notifiable_id', auth()->user()->id)->whereNull('read_at')->orderBy('created_at', 'desc')->get();
        }
        $response['html'] = view('auth.notification.show-notification')->with('notifications', $notifications)->render();
        $response['count'] = $notifications->count();
        return response()->json($response);
    }

    /**Function to move rip period expired.
     * @param Request $request
     */
    public function markExpiredRip(Request $request)
    { 
        $rip = Rip::find($request->id);
        $rip->expired = '1' ;
        $rip->save();
        broadcast(new markExpiredRipTab(url('/rip/'.$request->id)))->toOthers();
        session()->put('success',config('flashmessage.EXPIREDRIP'));
        echo $url = url('rip/' . $request->id);
    }

    /**Function to Expired RIP period in active tab
     * @param Request $request
     */
     public function revertExpiredRip(Request $request)
    {
        $rip = Rip::find($request->id);
        $rip->expired = '0' ;
        $rip->save();  
        broadcast(new revertExpiredRipTab(url('/rip/'.$request->id)))->toOthers();      
        return redirect('rip/'.$request->id)->with(['success' => config('flashmessage.REVERTRIP')]);
    }

    /**Function to display rip period.
     * @param Request $request
     */

    public function ripMenu(Request $request)
    {   
        $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '0')->get();        
        return view('auth.rip.index')->with([ 'rips' => $rips]);
    }
    
    /**
     * Function to call Edit Rip Period form
     * @param Request $request
     * @return int
     */
    public function editRipPeriod(Request $request) {
        $rip_id = $request->id;
        $rips = Rip::where('company_id', session('company_id'))->where('id', $rip_id)->get();        
        return view('auth.rip.showRipForm')->with([ 'rips' => $rips[0]]);        
    }
    
    /**
     * Function to call Edit Rip form
     * @param Request $request
     * @return int
     */
    public function editRipTabName(Request $request) {
        $rip_id = $request->id;
        $rip = Goal::find(['goal_type_id' => $rip_id]);
        
        return view('auth.rip.pusher.editRipTab')->with(['currentRip' => $rip[0]]);  
    }

    /**
     * Task overview
     * @param Request $request
     * @return int
     */
    public function taskOverview(Request $request) {
        return Task::taskOverviewOps($request->id);
    }
}
