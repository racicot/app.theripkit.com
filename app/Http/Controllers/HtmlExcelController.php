<?php
namespace App\Http\Controllers;
class HtmlExcelController extends Controller{
    public function __construct()
    {

    }
    private $root = '/';
    private $folder = '/';
    private $path = '/';
    private $mainFile = 'spreadsheet.htm';
    private $spreadsheets = array();
    private $info = array();
    private $css;

    /*  function to add sheets to excel */
    public function addSheet($name, $contents){
        $this->info[] = $name;
        $this->spreadsheets[] = $contents;
    }

    /*  function to add css to excel */
    public function setCss($css)
    {
        $this->css = $css;
    }

    /*  function to set header information */
    public function headers($name = "spreadsheet.xls"){
        header("Content-type: application/vnd.ms-excel; charset=UTF-8");
        header("Content-type: application/force-download");
        header("Content-Disposition: attachment; filename=".$name);
    }

    /*  function to build excel file */
    public function buildFile(){
        $count = count($this->info);
        if ($count == 0) return '';
        elseif ($count == 1) return $this->buildSingleSheet();
        else return $this->buildMultiSheet();
    }

    /*  function to build excel with multiple tabs */
    private function buildMultiSheet(){
        $boundary = "----=_NextPart_01D21572.46A0BD00";
        $parts = array();
        $parts[] = 'MIME-Version: 1.0
X-Document-Type: Workbook
Content-Type: multipart/related; boundary="'.$boundary.'"';

        $parts[] = $this->buildMain();

        if (!empty($this->css)){
            //$parts[] = $this->buildCss();
        }

        foreach ($this->info as $k => $v) {
            $parts[] = $this->buildSheet($k);
        }
        $ret = implode("\r\n\r\n--{$boundary}\r\n", $parts);
        $ret .= "\r\n--{$boundary}--\r\n";
        return $ret;
    }

    /*  function to build excel with single tab */
    private function buildSingleSheet(){
            $ret='<html xmlns:x="urn:schemas-microsoft-com:office:excel">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <!--[if gte mso 9]>
    <xml>
        <x:ExcelWorkbook>
            <x:ExcelWorksheets>
                <x:ExcelWorksheet>
                    <x:Name><![CDATA['.substr($this->info[0], 0, 31).']]></x:Name>
                    <x:WorksheetOptions>
                        <x:Print>
                            <x:ValidPrinterInfo/>
                        </x:Print>
                    </x:WorksheetOptions>
                </x:ExcelWorksheet>
            </x:ExcelWorksheets>
        </x:ExcelWorkbook>
    </xml>
    <![endif]-->
    <style type="text/css">
'.$this->css.'
    </style>
</head>
<body>';
            $ret.=$this->spreadsheets[0];
            $ret.='
</body>
</html>';
            return $ret;
    }

 /*  function to build excel */
 private function buildMain(){
        $ret='Content-Location: file:///'.$this->root.'/'.$this->mainFile.'
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="UTF-8"

<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:x=3D"urn:schemas-microsoft-com:office:excel"
xmlns=3D"http://www.w3.org/TR/REC-html40">

<head>
<meta name=3D"Excel Workbook Frameset">
<meta http-equiv=3DContent-Type content=3D"text/html; charset=3Dutf-8">
<meta name=3DProgId content=3DExcel.Sheet>
<meta name=3DGenerator content=3D"Microsoft Excel 14">
<link rel=3DFile-List href=3D"'.$this->folder.'/filelist.xml">';

$ret.='
<link id=3D"shLink">
<!--[if gte mso 9]><xml>
 <x:ExcelWorkbook>
  <x:ExcelWorksheets>';
        foreach ($this->info as $k => $v) {
            $ret.='   <x:ExcelWorksheet>
    <x:Name><![CDATA['.substr($v, 0, 31).']]></x:Name>
    <x:WorksheetSource HRef=3D"'.$this->folder.'/'.$k.'.htm"/>
   </x:ExcelWorksheet>';
        }

        $ret.='  </x:ExcelWorksheets>';
        if (!empty($this->css))
            $ret.= '<x:Stylesheet HRef=3D"'.$this->folder.'/stylesheet.css"/>';
        $ret.='
 </x:ExcelWorkbook>
</xml><![endif]-->
</head>
<body>
</body>
</html>';
        return $ret;
    }

  /*  function to build excel sheet */
  private function buildSheet($key){
        $ret = 'Content-Location: file:///'.$this->path.'/'.$key.'.htm
Content-Transfer-Encoding: quoted-printable
Content-Type: text/html; charset="UTF-8"
<html xmlns:v=3D"urn:schemas-microsoft-com:vml"
xmlns:o=3D"urn:schemas-microsoft-com:office:office"
xmlns:x=3D"urn:schemas-microsoft-com:office:excel"
xmlns=3D"http://www.w3.org/TR/REC-html40">
<head>
<meta http-equiv=3DContent-Type content=3D"text/html; charset=3Dutf-8">
<meta name=3DProgId content=3DExcel.Sheet>
<meta name=3DGenerator content=3D"Microsoft Excel 14">
<![if IE]>
<![endif]>
';
$ret.='
</head>
<body>';
        $ret.=str_replace("=", "=3D", $this->spreadsheets[$key]);
        $ret.='</body>
</html>';
        return $ret;
    }

/*  function to build css sheet */
    private function buildCss(){
        $ret='Content-Transfer-Encoding: quoted-printable
Content-Type: text/css; charset="utf-8"';
        $ret .= $this->css;
        return $ret;
    }
}
