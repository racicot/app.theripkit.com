<?php

namespace App\Http\Controllers;

use App\Http\Requests\Company\StoreCompanyRequest;
use App\Http\Requests\Company\EditCompanyRequest;
use App\Model\Company;
use App\Model\CompanyParent;
use App\Model\Portfolio;
use App\User;
use Redirect;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Notifications\NewEvent;
use App\Model\Countries;
use App\Model\Goal;
use App\Model\Task;
use Illuminate\Support\Facades\Auth;


class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'CheckCompany', 'userstatus']);
        $this->middleware('permissions')->except(['ajaxData','showAssignedUsers','companyUserList','assignCompanyUser']);
    }

    /**
     * Display a listing of the Company.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* Show only those companies whose parent is current user */
        $users = User::all();
        $parents = $users->filter(function ($value) {
            return $value->can('Add As Company Parent');
        });
        $owners = User::doesntHave('company')->get()->filter(function ($value) {
            return $value->can('Add As Company Owner');
        });

        return view('auth.company.index', ['parents' => $parents, 'owners' => $owners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created company in storage.
     * @param  \Illuminate\Http\Request\Company\StoreCompanyRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCompanyRequest $request)
    {           
        $user = auth()->user();

        \DB::beginTransaction();
        $company = Company::create([
            'name' => $request->company_name,
            'status' => $request->status,
        ]);

        Portfolio::create([
            'company_id' => $company->id
        ]);
        if (!$user->hasAnyRole(['Super Admin'])) {
            CompanyParent::create([
                'parent_id' => $user->id,
                'company_id' => $company->id
            ]);        }

        \DB::commit();
        session()->pull('company_id', []);
        session()->put('success',config('flashmessage.COMPANYCREATED'));
        return ['flashMessage' => config('flashmessage.COMPANYCREATED'), 'class' => 'alert-success'];

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified company.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $id = base64_decode($id);
        if (auth()->user()->roles->first()->name != 'Super Admin') {
            $user = User::whereHas('parent', function ($query) use ($id) {
                $query->where(['parent_id' => auth()->user()->id, 'company_id' => $id]);
            })->orWherehas('company', function ($query) use ($id) {
                $query->where(['user_id' => auth()->user()->id, 'id' => $id]);
            })->get()->unique('id');
            if ($user->isEmpty()) {

                dd(config('flashmessage.COMPANY-NOPERMISSION'));
            }
        }
        $users = User::all();
        $role = Role::find($id);
        $countries = Countries::all()->toArray();
        $parents = $users->filter(function ($value) {
            return $value->can(config('flashmessage.CANADDCOMPANYPARENT'));
        });
        //$parentsArray = CompanyParent::where('company_id', $id)->get()->pluck('parent_id')->toArray();
        $owners = User::doesntHave('company')->orWhereHas('company', function ($query) use ($id) {
            $query->where('id', $id);
        })->get()->filter(function ($value) {
            return $value->can(config('flashmessage.CANADDCOMPANYOWNER'));
        });
        $currentOwner = User::whereHas('company', function ($query) use ($id) {
            $query->where('id', $id);
        })->first();
        $company = Company::find($id);
        return view('auth.company.edit', [
            'company' => $company,
            'parents' => $parents,
            'owners' => $owners,
            'currentOwner' => $currentOwner,
            'role' => $role,
            'countries' => $countries
        ]);
    }

    /**
     * Update the specified company in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditCompanyRequest $request, $id)
    {
        $id = base64_decode($id);
        $company = Company::find($id);
        //$parents = CompanyParent::where('company_id', $id)->get();
        if ($request->hasFile('image')) {
            $exists = \Storage::disk('system_photos')->exists($company->logo);
            if ($exists) {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);

                \Storage::disk('system_photos')->delete($company->logo);
            } else {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);
            }
        } else {
            $file = $company->logo;
        }
        \DB::beginTransaction();
        $company->update([
            'name' => $request->company_name,
            'user_id' => $request->owner,
            'phone' => $request->phone,
            'logo' => $file,
            'plan' => $request->plan,
            'status' => $request->status,
            'isd_code' => $request->isd_code_company,
        ]);
        
        \DB::commit();
        if ((!$request->status) && ($company->id == session('company_id'))) {
            session()->pull('company_id', []);
        }
        session()->put("success",config('flashmessage.COMPANYUPDATED'));
    }
    

    /**
     * Remove the specified company from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        $company = Company::find($id);
        $exists = \Storage::disk('system_photos')->exists($company->logo);
        if ($exists) {
            \Storage::disk('system_photos')->delete($company->logo);
        }
        if (session('company_id') == $id) {
            session()->pull('company_id', []);
        }
        $company->delete();
        return redirect('company')->with(['fail' => config('flashmessage.COMPANYDELETED'), 'class' => 'alert-success']);


    }

    /* company datatable list
     * 
     */
    public function ajaxData()
    {
        $user = auth()->user();
        $user_id = $user->id;
        if ($user->hasAnyRole(['Super Admin'])) {
            $companies = Company::all();
        } else {
            $companies = Company::whereHas('parent', function ($query) use ($user_id) {
                $query->where('parent_id', $user_id);
            })->orWhere('user_id', $user_id)->get()->unique('id');
        }
        return Datatables::of($companies)
            ->addColumn('phone', function ($company) {
                if(isset($company->isd_code)) {
                    return $company->isd_code.'-'.$company->phone;
                } else {
                    return $company->phone;
                }
            })
             ->addColumn('members', function ($company) {

                $users = User::whereHas('parent', function ($query)  use ($company){
                $query->where('company_id', $company->id);
                })->get()->unique('id');
                
                $content ='<div class="avtar-users">
                <div class="ComanyUserList ComanyUserList_' . $company->id . '" company_id="' . $company->id . '">';
                if ($users->isNotEmpty()) {    
                    $content.= view('auth.company.companyUserImage')->with('users', $users)->render();
                }

                $content.='
                  </div>';
                if(auth()->user()->can('Assign Users To Company From Company Section')){
                    $content.='<div class="avtar-add-users">
                      <a data-toggle="modal" data-target="#addusers" id="addUser_' . $company->id . '"
                      company_id="' . $company->id . '"
                      class="dropdown-toggle addedCompanyUser">
                        <svg width="34" height="34" viewBox="0 0 24 24">
                            <g fill="#2c3542" fill-rule="nonzero">
                              <circle cx="12.5" cy="12.5" r="8.5" fill="#2dbe74" opacity="1"></circle>
                              <path fill="#FFF" stroke="#FFF" d="M12.77 12.23h2.96a.27.27 0 0 1 0 .54h-2.96v2.96a.27.27 0 0 1-.54 0v-2.96H9.27a.27.27 0 0 1 0-.54h2.96V9.27a.27.27 0 0 1 .54 0v2.96z"></path>
                            </g>
                        </svg>
                      </a>
                  </div>';
                }
                $content.='</div>';
               
                return $content;
            })
            ->addColumn('plan', function ($company) {
                return $company->plan;
            })
            ->addColumn('status', function ($company) {
                if($company->status){
                    $content = "<span class='status-badge'>Active</span>";
                } else{
                    $content = "<span class='status-badge inactive'>Inactive</span>";
                }
                return $content;
            })
            ->addColumn('actions', function ($company) {
                if (auth()->user()->can('Update Company Details')) {
                    $content = "<a href=" . url('/company/' . base64_encode($company->id) . '/edit') . " class='editCompanyButton green-text'
                data-toggle='modal' data-target='#editModal'>Edit</a>";
                } else {
                    $content = '<span class="error-msg cust-msg">Permission Denied</span>';
                }
                if (auth()->user()->can('Delete A Company')) {
                    $content .= '<form method="post" id="deleteCompany_' . $company->id . '" class="delete-form" action="' . url("company") . '/' . $company->id . '">
                ' . method_field("DELETE") .
                        csrf_field() . '
                 <button type="submit" companyName="'. $company->name .'" companyId="' . $company->id . '" class="btn-link deleteCompany">Delete</button>
                </form>';
                } else {
                    $content = '<span class="error-msg cust-msg">Permission Denied</span>';
                }
                return $content;
            })
            ->addColumn('image', function ($company) {
                $company_image = $company->companyImage();
                if(!empty($company_image) && $company_image != asset('images/defaultCompany.jpg')){
                    $content = "<div class='photo-in'><label for='profile-picture'><img src=" . $company_image. " alt='company logo'></div> <span>".$company->name."</span>";
                }
                else{
                    $content =  "<div class='photo-in'><label for='profile-picture'><img src='../images/new-site/company-thumbnail-placeholder.svg' alt='company logo'></div> <span>".$company->name."</span>";
                }
                return $content;
            })
            ->rawColumns(['actions','members','status','image'])->make(true);

    }

    /**
     * Function to get User List for Company
     * @param Request $request
     * @return type html
     */
    public function showAssignedUsers(Request $request) {
        $company_id = $request->company_id;
        $users = User::whereHas('parent', function ($query)  use ($company_id){
        $query->where('company_id', $company_id);
        })->get()->sortBy('name')->unique('id');
        //if ($users->isNotEmpty()) {
            return view('auth.company.showAddedUserCompany')->with(['company_id' => $company_id, 'selectedUsers' => $users]);
        //}
    }

    /**
     * Function to get User List for Company
     * @param Request $request
     * @return type html
     */
    public function companyUserList(Request $request) {
        $company_id = $request->company_id;

        $assignedUsers = User::whereHas('parent', function ($query)  use ($company_id){
        $query->where('company_id', $company_id);
        })->get()->unique('id');

        //$users = User::all();//->whereNotIn("id",Auth::user()->id);       
        $users = User::orderBy('name')->get();
        $new_assign_array = array();
        if(count($assignedUsers) > 0){
            foreach($assignedUsers as $key=>$val){
                $new_assign_array[] = $val->id;
            }
        }
        $count = count($new_assign_array);        
        return view('auth.company.addUserToCompany')->with(['users' => $users, 'company_id' => $company_id, 'selectedUsers' => $new_assign_array, 'count' => $count]);
    }   
    
    /** function  assign user to company
     * @param Request $request
     * @return html of the asssign user
     */

    public function assignCompanyUser(Request $request)
    {
        $imageHtml = "";
        $company_id = $request->company_id;
        $company_name = Company::where('id', $company_id)->pluck('name');
        
        $assignUsers = CompanyParent::where('company_id', $company_id)->get();
        if($request->user == ''){
            $request->user = array();
        }
        $old_assigned_users = array();
        $removedUser = $this->getRemovedUser($request->user, $assignUsers);
        if(count($removedUser) > 0){
            Goal::removeUser($company_id, $removedUser); //unassignUser from the goal
            Task::removeUser($company_id, $removedUser); //unassignUser from the task 
        }
        foreach($assignUsers as $users) {
            $old_assigned_users[] = $users->parent_id;
        }    
        $added_new = array_diff($request->user, $old_assigned_users);
        if(count($assignUsers) > 0){
            $assignUsers->each(function ($item) {           
                $item->delete();
            });
        }
        if(count($request->user) > 0){
            foreach ($request->user as $user_id) {
                CompanyParent::create([
                    'parent_id' => $user_id,
                    'company_id' => $company_id
                ]);
                if(in_array($user_id, $added_new)){
                    $assignUser = User::find($user_id);
                    $mailMessage['msg_start'] = config('setting.notification.company_assigned');
                    $mailMessage['rip_name'] = '';
                    $mailMessage['msg_part'] = '';
                    $mailMessage['company_name'] = $company_name[0];
                    $mailMessage['msg_end'] = ".";
                    $subject = 'RIPKIT: '.config('setting.notification.company_assigned_to_user');
                    $assignUser->notify(new NewEvent($mailMessage, 1, $assignUser->name,$subject));

                    // In-app Notification
                    $message = config('setting.notification.company_assigned')."'".session('company_name')."'.";
                    $assignUser->notify(new NewEvent($message, 0, $assignUser->name,$subject));                        
                }
            }
            $userArray = User::whereIn('id', $request->user)->get();         
            $imageHtml.= view('auth.company.companyUserImage')->with('users', $userArray)->render(); 
        }
        $response['imageHtml'] = $imageHtml;        
        return response()->json($response);
    }

    /**
     * Function to get removed user id 
     * @param  array $latestAssignedUsers
     * @param  object $assignedUser
     * @return array $romvedUserIds
     */ 
    public function getRemovedUser($latestAssignedUsers, $assignedUser)
    {
        $romvedUserIds = [];
        if(count($assignedUser) > 0){            
            foreach ($assignedUser as $key => $value) {
                if(count($latestAssignedUsers) > 0){
                    if(!in_array($value->parent_id, $latestAssignedUsers)){
                         array_push($romvedUserIds, $value->parent_id);
                    }
                }
                else{
                    array_push($romvedUserIds, $value->parent_id);
                } 
            }
                           
        }    
        return $romvedUserIds;
    }

}
