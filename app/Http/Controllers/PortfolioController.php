<?php

namespace App\Http\Controllers;

use App\Events\PortfolioUpdate;
use App\Model\Category;
use App\Model\Rip;
use Illuminate\Http\Request;
use User;
use App\Model\Portfolio;
use App\Model\Goal;
use Mpdf\Mpdf;
use Carbon\Carbon;
use App\Model\CompanyParent;

class PortfolioController extends Controller
{
    public function __construct()
    {

        $this->middleware(['userstatus', 'CheckCompany']);
        $this->middleware('permissions')->except(['download', 'portfolioUpdate']);
    }

    public function index()
    {
        $portfolio = Portfolio::where('company_id', session('company_id'))->first();
        $rips = Rip::with(['category', 'category.goals', 'category.goals.task', 'category.goals.comment'])->where('company_id', session('company_id'))->where('expired', '=', '0')->get();
        $goalUsers = CompanyParent::with('user')->where('company_id', session('company_id'))->get()->unique('parent_id');
        if (isset($portfolio)) {
            $portfolio = $portfolio;
            $portfolio['customer_problem'] = $portfolio['customer_problem'];        // Removing html formatiing, if any
            $portfolio['customer_solution'] = $portfolio['customer_solution'];      // Removing html formatiing, if any
        } else {
            $portfolio = [];
        }
        return view('auth.company.portfolio')->with(['portfolio' => $portfolio, 'rips' => $rips,'goalUsers'=>$goalUsers]);
    }

    public function save(Request $request)
    {
        $portfolio = Portfolio::updateOrCreate(
            ['company_id' => session('company_id')],
            [
                $request->field => strip_tags($request->data,'<br>')
            ]
        );
        $data['company_id'] = session('company_id');
        $data['field'] = $request->field;
        $data['data'] = strip_tags($request->data,'<br>');
        broadcast(new PortfolioUpdate($data))->toOthers();
        echo strip_tags($request->data,'<br>');
        die;
    }

    public function portfolioUpdate(Request $request)
    {
        return response()->json($request->data);
    }

    public function download()
    {
        $portfolio = Portfolio::where('company_id', session('company_id'))->first();
        $rips = Rip::with(['category', 'category.goals', 'category.goals.task', 'category.goals.comment'])->where('company_id', session('company_id'))->where('expired', '=', '0')->get();
        $goalUsers = CompanyParent::with('user')->where('company_id', session('company_id'))->get()->unique('parent_id');
        if (isset($portfolio)) {
            $portfolio = $portfolio;
        } else {
            $portfolio = [];
        }

        $html = view('auth.company.pdf.portfolio', ['portfolio' => $portfolio, 'rips' => $rips,'goalUsers'=>$goalUsers])->render();
               
        $mpdf = new Mpdf([
            'format' => 'A4',
            'orientation' => 'P'
        ]);
        $mpdf->DefHTMLFooterByName(
            'Chapter2Footer',
            '<div style="width: 50%; float: left; text-align: left; font-weight: bold; font-size: 8pt;">Build Impossible</div>
                <div style="width: 50%; float: left; text-align: right; font-weight: bold; font-size: 8pt;"> '.Carbon::now()->format('d-m-Y').'</div>'
        );

        $mpdf->SetHTMLFooterByName('Chapter2Footer');
        $mpdf->autoPageBreak = true;
        $mpdf->WriteHTML($html);

        //$mpdf->Output();
        $mpdf->Output('Snapshot.pdf', 'D');
    }
}
