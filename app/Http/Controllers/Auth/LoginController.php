<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\LoginAttempt;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/overview-rip';
    
    public $token;

    /**
     * The hashing key.
     *
     * @var string
     */
    public $hashKey;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    protected function validateLogin(Request $request)
    {
        
        $this->validate($request, [
            $this->username() => 'required|exists:users',
            'password' => 'required|string',
        ], [
            $this->username() . '.exists' => 'The selected email is not available or not verified .'
        ]);
    }

    /**
     * Validate the user login request for email existence 
     *
     * @param  \Illuminate\Http\Request $request
     * @return void
     */
    public function ajaxValidateLogin(Request $request)
    {
        $email = $request->email;
        $user = User::where(['email' => $request->email, 'status' => 1])->first();
        if (isset($user->id)) {
            $data['error'] = 0;
            $data['message'] = '';
        } else {
            $data['error'] = 1;
            $data['message'] = 'The selected email is not available or not verified';
        }
        return $data;
    }
    
    /**
     * Validate the User Login request by validating password and email combination
     * 
     * @param Request $request
     * @return int
     */
    public function ajaxLogin(Request $request)
    {
        //  $data = Hash::check(Input::get('old_password'), Auth::user()->password)
        $email = $request->email;

        $password = $request->password;
        
        $credentials = ['email' => $email, 'password' => $password];
        // Get Login attempts from Login Attempts table
        $loginAttempts = User::where(['email' => $email])->first()['login_attempts'];
        
        if (Auth::attempt($credentials)) {

            $data['error'] = 0;
            $data['message'] = '';
            $loginAttempts = 0;
            $data['loginAttempts'] = 0;         // Set Login attempts field back to 0 on successful attempt
        } else {
            $data['error'] = 1;
            $data['message'] = 'Wrong Password';
            $loginAttempts++;                   // Increment login attempt field on unsuccessful attempt
            $data['loginAttempts'] = $loginAttempts;
        }        
        
        // Update Users table with the loginAttempts value
        User::where('email', $email)->update([
            'login_attempts' => $loginAttempts,
        ]);
        
        return $data;
    }

    public function forgotPassword($email)
    {
        $user = User::where('email', $email)->first();
        $pass = str_random(5);
        $password = bcrypt($pass);
        $user->password = $password;
        $user->save();
        \Mail::send('email.forgotPassword', ['password' => $pass], function ($message) use ($email, $user) {
            $message->subject('New Password');
            $message->to($user->email);
            $message->replyTo(config('setting.email.super_admin'));
        });
        
        return view('auth.passwords.email_sent');

    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    protected function credentials(\Illuminate\Http\Request $request)
    {
        return ['email' => $request->{$this->username()}, 'password' => $request->password, 'status' => 1];
    }
    
    /**
     * Send mail to user on/post limited Unsuccessful Attempts 
     * 
     * @param type $email
     */
    public function wrongPassword(Request $request)
    {
        $email = $request->email;
        if(isset($email)) {
            $user = User::where('email', $email)->first();
            
            $token = $this->token;       // Random token value
            if (isset($user)) {
                \Mail::send('email.wrongPasswordEmail', ['name' => $user->name, 'data' =>  $token, 'email' => encrypt($email)], function ($message) use ($email,$user) {
                    $message->subject('RIPKIT: Suspicious Account Activity');
                    $message->to($email);
                    $message->replyTo(config('setting.email.super_admin'));
                });
            }
        }    
       // return redirect('/home');
    }

    /** 
     * Check if welocme popup already open or not if no not then open   
    */

    public function checkWelcomePopup()
    {
        if(Cookie::get('welcome_popup') == 1){
            echo 1;
        }
        else{
             echo 0;
        }
    }

    /** 
     * update welocme popup : set value in cookie.   
    */

    public function updateWelcomePopup()
    {
        Cookie::queue('welcome_popup', 1, 1440*360);
    }


    /**
     * Override Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {

        User::where('id', Auth::user()->id)->update(['wp_token' => '']);
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect('/');
    }


    
}
