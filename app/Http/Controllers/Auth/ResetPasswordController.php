<?php

namespace App\Http\Controllers\Auth;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;
    
    /**
     * Override showResetForm() to check the validity of reset token
     * 
     * @param Request $request
     * @param type $token
     * @return type
     */
    public function showResetForm(Request $request, $token = null)
    {
        $check_time = '';
        $created_at = '';
        $checkEmailExists = 0;
        $this->logoutUserForResetPasswordLink();
        // Dynamically updating auth expire Token variable in config/auth.php
        config(['auth.passwords.users.expire' => 1440]);
        
        // Fetching the token expiry time from auth.php
        $config_time = config("auth.passwords.users.expire");
        
        // Decrypting the email fetched through Request        
        $email = isset($request->email) ? decrypt($request->email) : '';

        // Checking if email exists in password_resets table
        $checkEmailExists = DB::table("password_resets")
                        ->where("email","=",$email)
                        ->count();
        if($checkEmailExists > 0) { 
            $created_at = DB::table("password_resets")
                            ->where("email","=",$email)
                            ->first()->created_at;    
            
            if(!empty($created_at)) {
                $check_time = (strtotime($created_at) + ($config_time*60));
                
                if($check_time >= time()) {
                    return view('auth.passwords.reset')->with(
                        ['token' => $token, 'email' => $email]
                    );
                } else {
                    return view('errors.token_err');
                }
            } 
        } else {
            return view('errors.token_err');
        }        
    }

    public function logoutUserForResetPasswordLink()
    {
        if (Auth::check()) {
            Auth::logout();
        }
    }

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('guest');
    }
}
