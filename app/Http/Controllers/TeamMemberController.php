<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\TeamMember;
use Yajra\Datatables\Datatables;

class TeamMemberController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $team_member = TeamMember::with(['company', 'user'])->get();
        return view('auth.team_member.index')->with('team_member', $team_member);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Return the ajax data for the grid.
     *
     *
     * @return \Illuminate\Http\Response
     */
    public function ajaxData()
    {
        $team_member = TeamMember::with(['company', 'user'])->get();
        return Datatables::of(TeamMember::with(['company', 'user'])->get())
            ->addColumn('name', function ($team_member) {
                $content = $team_member->user->name;
                return $content;
            })->addColumn('company', function ($team_member) {
                $content = $team_member->company->name;
                return $content;
            })->addColumn('role', function ($team_member) {
                $content = $team_member->user->roles->pluck('name')->first();
                return $content;
            })->addColumn('actions', function ($team_member) {
                $content = '<a href="' . url('team/' . $team_member->id . '/edit') . '" class="editTeamMemberButton" data-toggle="modal" data-target="#editTeamMemberModal" >Edit</a>';
                return $content;
            })->rawColumns(['actions'])->make(true);
    }
}
