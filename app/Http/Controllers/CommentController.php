<?php

namespace App\Http\Controllers;

use App\Events\AddComment;
use App\Events\DeleteComment;
use App\Events\EditComment;
use App\Model\Comment;
use App\Model\Goal;
use App\Model\Task;
use App\Model\Attachment;
use App\Model\CompanyParent;
use App\Notifications\NewEvent;
use App\User;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CommentController extends Controller
{
    public function __construct()
    {

    }
    
    /**
     * Store a newly created comment with attachment and user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return return the html of the comment
     */
    public function store(Request $request)
    {   
        if($request->mention_data != null){
            $comment_data = strip_tags($request->mention_data);
        }
        else{
            $comment_data = strip_tags($request->comment);
        }
        
        if ($request->to_id == null) {
            $toid = $request->to_id;
        } else {
            $toid = implode(',', $request->to_id);
        }
        \DB::beginTransaction();
        $comment = Comment::create([
            'goal_id' => $request->goalId,
            'from_id' => \Auth::user()->id,
            'to_id' => $toid,
            'message' => htmlspecialchars($comment_data),
        ]);        

        if ($request->hasFile('attachment')) {
            $files = $request->file('attachment');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                Storage::disk('system_photos')->putFileAs(null, $file, $filename);
                $attachment = Attachment::create([
                    'comment_id' => $comment->id,
                    'attachment' => $filename,
                ]);
            }
        }
        \DB::commit();

        //..update mention ids into name.
        $mentionData = Comment::getMentionData($comment_data,'display');        
        $comment->message = $mentionData['comment_data'];
        $mention_id = $mentionData['mention_id'];

        $user = User::find(\Auth::user()->id);

        $goal = Goal::find($request->goalId);

        $html = View::make('auth.rip.ajax.comment', [
            'comment' => $comment,
            'taskId' => $request->taskId,
            'goal'=>$goal
        ])->render();

        $count = Comment::where('goal_id', $request->goalId)->count();
        $response['html'] = $html;
        $response['count'] = $count;
        $response['goalId'] = $request->goalId;
        $eventResponse['userId'] = $user->id;
        $eventResponse['commentId'] = $comment->id;
        /**
         * Code for Comment added Notifications
         */
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        $subject = '';
        User::whereIn('id',  $assing_to)->each(function($user) use($request,$goal,$subject){
            $message = config('setting.notification.comment_added'). strip_tags($request->comment).config('setting.notification.added_on_rip').$goal->goal."' of Company: '".session('company_name');
            $user->notify(new NewEvent($message, $user->getNotificationAction(config('setting.notification.comment_on_assign_rip')), $user->name,$subject));
        });
        
        $action = \Config::get('magicNumbers.MEETINGMAILACTION');
        $mailSubject = 'RIPKIT: '.config('setting.notification.comment_on_rip');
        User::whereIn('id', $assing_to)->each(function($user) use($request,$goal,$action, $mailSubject){
            $mailMessage['msg_start'] = config('setting.notification.comment_added').$request->comment.config('setting.notification.added_on_rip');
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');

            $user->notify(new NewEvent($mailMessage, $action, $user->name, $mailSubject));
        });
        
        /**
         * Code for Mention(@) on Comments Notifications
         */
        
        $mailSubject = 'RIPKIT: '.config('setting.notification.mention_on_comments');
        $mention_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $mention_id));
        User::whereIn('id', $mention_to)->each(function($user) use($request,$goal, $subject){
            $message = config('setting.notification.mentioned_in_comment')."'".strip_tags($request->comment)."' on RIP:" .$goal->goal."' of Company: '".session('company_name');
            $user->notify(new NewEvent($message, $user->getNotificationAction(config('setting.notification.mention_on_comments')), $user->name, $subject));
            
        }); 
        
        User::whereIn('id', $mention_to)->each(function($user) use($request,$goal,$action, $mailSubject){
            $mailMessage['msg_start'] = config('setting.notification.mentioned_in_comment')."'".$request->comment."' on RIP:";
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');

            $user->notify(new NewEvent($mailMessage, $action, $user->name, $mailSubject));
        });
        
        broadcast(new AddComment($eventResponse))->toOthers();
        return response()->json($response);

    }

    /**
     * Broadcast the newly created comment with attachment and user.
     *
     * @param  \Illuminate\Http\Request $request
     * @return return the html of the comment
     */
    public function addComment(Request $request)
    {
        $user = User::find($request->data['userId']);
        
        $comment = Comment::find($request->data['commentId']);
        if(!$comment){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        //..update mention ids into name.
        $mentionData = Comment::getMentionData($comment->message,'display');
        $comment->message = $mentionData['comment_data'];
        $comment->message = htmlspecialchars_decode($comment->message);
        $userHtml = '';
        $attachments = Attachment::where('comment_id', $comment->id)->get();
        $attachmentHtml = '';
        $attachmentHtml = '<div class="media-attachment attachmentList_' . $comment->id . '">';
        if (count($attachments) > 0) {
            $attachmentHtml .= '<span class="attachment-heading">' . count($attachments) . ' attachments</span>';
            foreach ($attachments as $attachment) {
                $attachmentHtml .= '<div class="commentAttachment_' . $attachment->id . '">
                <ul class="attachment-list">
                  <li><a fileName="'.$attachment->attachment.'"  class="mr10" href="' . $attachment->attachmentUrl() . '" target="_blank">
                  <i class="fa fa-picture-o" aria-hidden="true"></i>
                       ' . $attachment->attachment . '
                  </a><i class="fa fa-trash delete-attachment" href="' . url("deleteAttachment") . '/' . $attachment->id . '" aria-hidden="true"></i>
                  </li>
               </ul>
              </div>';
            }
        }
        $attachmentHtml .= '</div>';

        $goal = Goal::find($comment->goal_id);
       

        $html = View::make('auth.rip.ajax.comment', [
            'comment' => $comment,
            'user' => $user,
            'userHtml' => $userHtml,
            'attachmentHtml' => $attachmentHtml,
            'taskId' => $comment->taskId,
            'goal'=>$goal
        ])->render();

        $count = Comment::where('goal_id', $comment->goal_id)->count();
        $response['html'] = $html;
        $response['count'] = $count;
        $response['goalId'] = $comment->goal_id;
        $response['dataNull'] = false;

        return response()->json($response);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $comment = Comment::with(['attachment'])->where('id', $id)->get()->first();  
        //..update mention ids into name.
        $mentionData = Comment::getMentionData($comment->message,'edit');   
        $comment->mention_comment = htmlspecialchars_decode($comment->message);
        $comment->message = $mentionData['comment_data'];
        return view('auth.rip.pusher.editComment')->with('comment', $comment);  
    }

    /**
     * Update the specified comment in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $userHtml = '';
        $attachmentHtml = '';
        $comment = Comment::find($id);
        
        if($request->mention_data != null){
            $comment_data = strip_tags($request->mention_data);
        } else {
            $comment_data = $request->comment;
        }

        $comment->update([
            'id' => $id,
            'message' => $comment_data,
        ]);
        
        $old_comment = $comment->message;
       
        //..update mention ids into name.
        $mentionData = Comment::getMentionData($comment_data,'display');        
        
        $comment->message = $mentionData['comment_data'];
        $mention_id = $mentionData['mention_id'];

        if ($request->hasFile('attachment')) {
            $hasAttachment = 1;
            $files = $request->file('attachment');
            foreach ($files as $file) {
                $filename = $file->getClientOriginalName();
                Storage::disk('system_photos')->putFileAs(null, $file, $filename);
                $attachment = Attachment::create([
                  'attachment' => $filename,
                  'comment_id' => $id,
                ]);
            }
        } else {
            $hasAttachment = 0;
        }

        $attachments = Attachment::where('comment_id', $id)->get();
        if (count($attachments) > 0) {
            $attachmentHtml = $this->attachmentHtml($attachments);
        }
        $response['message'] = $userHtml . ' ' . $comment->message;
        $response['message2'] = $comment->message;
        $response['commentId'] = $id;
        $response['attachment'] = $attachmentHtml;
        $response['hasAttachment'] = $hasAttachment;
        
        $goal = Goal::find($comment->goal_id);

        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));               
        $subject = 'RIPKIT: '.config('setting.notification.comment_updated');
        User::whereIn('id',  $assing_to)->each(function($user) use($request,$goal,$subject){
            $message = config('setting.notification.comment_added').strip_tags($request->comment).config('setting.notification.comment_edited').$goal->goal."' of Company: '".session('company_name');
            $user->notify(new NewEvent($message, $user->getNotificationAction(config('setting.notification.comment_on_assign_rip')), $user->name,$subject));
        });
        
        // For Mail Notification    
        $action = \Config::get('magicNumbers.MEETINGMAILACTION');      
        User::whereIn('id', $assing_to)->each(function($user) use($request,$goal, $old_comment, $action, $subject){
            $mailMessage['msg_start'] = config('setting.notification.comment_added').$old_comment."'".config('setting.notification.comment_edited').strip_tags($request->comment)."'".config('setting.notification.on_rip');
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');

            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
            
        });        
        
        $subject = 'RIPKIT: '.config('setting.notification.mention_on_comments');
        $mention_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $mention_id));
        User::whereIn('id', $mention_to)->each(function($user) use($request,$goal,$subject){            
            $message = config('setting.notification.mentioned_in_comment')."'".strip_tags($request->comment)."' on RIP:" .$goal->goal."' of Company: '".session('company_name');
            $user->notify(new NewEvent($message, $user->getNotificationAction(config('setting.notification.mention_on_comments')), $user->name, $subject));
        
        });
        // For Mail Notification    
        
        User::whereIn('id', $mention_to)->each(function($user) use($request,$goal, $action, $subject){
            $mailMessage['msg_start'] = config('setting.notification.mentioned_in_comment').$request->comment."' on RIP:";
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');

            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
            
        });
        
        broadcast(new EditComment($id))->toOthers();
        return response()->json($response);
    }

    /**
     * Broadcast the edited comment with user and attachment.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function editComment(Request $request)
    {
        $comment = Comment::find($request->data);
        if(!$comment){
            $response['dataNull'] = true;
            return response()->json($response);
        }

        //..update mention ids into name.
        $mentionData = Comment::getMentionData($comment->message,'display');
        $comment->message = $mentionData['comment_data'];

        $userHtml = '';
        $attachmentHtml = '';
        $attachments = Attachment::where('comment_id', $comment->id)->get();
        if (count($attachments) > 0) {
            $attachmentHtml = $this->attachmentHtml($attachments);
        }

        $user = User::find($comment->from_id);
        $goal = Goal::find($comment->goal_id);
        $html = View::make('auth.rip.ajax.comment', [
            'comment' => $comment,
            'user' => $user,
            'userHtml' => $userHtml,
            'attachmentHtml' => $attachmentHtml,
            'taskId' => $comment->taskId,
            'goal'=>$goal
        ])->render();
        $response['dataNull'] = false;
        $response['message'] = $comment->message;
        $response['commentId'] = $comment->id;
        $response['attachment'] = $attachmentHtml;

        return response()->json($response);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function deleteComment(Request $request)
    {
        $id = $request->id;
        \DB::beginTransaction();
        Attachment::where('comment_id', $id)->delete();
        $comment = Comment::find($id);
        $response['goalId'] = $comment->goal_id;
        $comment->delete();
        \DB::commit();        
        $count = Comment::where('goal_id', $response['goalId'])->count();
        $response['id'] = (int)$id;
        $response['count'] = $count;
        event(new DeleteComment($response));
        broadcast(new DeleteComment($response))->toOthers();
        return response()->json($response);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        dd($id);
    }

    /**function return the user assign to the task
     *
     */
    public function commentTaskUser(Request $request)
    {
        $task_id = $request->id;
        $taskUser = Task::where('id', $task_id)->first();
        $userArray = User::whereIn('id', explode(',', $taskUser->assign_to))->get();
        $html = "";
        foreach ($userArray as $user) {
            $html .= '<option value="' . $user->id . '">' . $user->name . '</option>';
        }
        $response['html'] = $html;
        $response['taskId'] = $task_id;
        return json_encode($response);
    }

    /*
     * Mention a user in a comment
     */
    public function jsonData(Request $request)
    {   
        if($request->id != ''){$company_id = $request->id ;} else{$company_id = session('company_id');}
        $userArray = CompanyParent::with('user')->where('company_id', $company_id)->get()->sortBy('user.name')->unique('parent_id');        
        foreach ($userArray as $user) {
            $dataArray[] = array(
                'id' => $user->user->id,
                'name' => $user->user->name,
                'avatar' => $user->user->userImage(),
                'type' => 'contact'
            );
        }
        return json_encode($dataArray);
    }

    /* Function delete the attachment from the comment
    * @param $request
    * @return json
    */
    public function deleteAttachment(Request $request)
    {
        $attachment = Attachment::find($request->id);
        $comment_id = $attachment->comment_id;
        $response['attachment_id'] = $attachment->id;
        $attachment->delete();
        $count = Attachment::where('comment_id', $comment_id)->count();
        Attachment::destroy($request->id);
        $response['comment_id'] = $comment_id;
        $response['count'] = $count;
        broadcast(new EditComment($comment_id))->toOthers();
        return response()->json($response);
    }
    
    /**
     * Function to display attachments in Comment
     * @param type $attachments
     * @return string
     */
    public function attachmentHtml($attachments){
        $attachmentHtml = '';
        foreach ($attachments as $attachment) {
            $attachmentHtml .='<div class="comments-links-items commentAttachment_'.$attachment->id.'">
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 25 25">
            <path fill="#2C3542" fill-rule="nonzero" d="M11.6 12.03a.631.631 0 0 0-.892-.892l-3.339 3.339a2.943 2.943 0 0 0-.877 2.108c0 .8.308 1.538.877 2.107a2.971 2.971 0 0 0 4.216 0l7.538-7.538a4.131 4.131 0 0 0 1.215-2.939c0-1.107-.43-2.153-1.215-2.938a4.16 4.16 0 0 0-5.892 0L5.708 12.8a5.306 5.306 0 0 0-1.57 3.785 5.28 5.28 0 0 0 1.57 3.784 5.306 5.306 0 0 0 3.784 1.57 5.28 5.28 0 0 0 3.785-1.57l3.338-3.338a.631.631 0 0 0-.892-.893l-3.338 3.339c-.77.77-1.8 1.2-2.893 1.2a4.074 4.074 0 0 1-2.892-1.2c-.77-.77-1.2-1.8-1.2-2.892 0-1.093.43-2.123 1.2-2.893l7.523-7.523a2.9 2.9 0 0 1 4.092 0 2.9 2.9 0 0 1 0 4.092L10.677 17.8c-.662.662-1.754.662-2.43 0a1.711 1.711 0 0 1 0-2.43l3.353-3.34z"></path>
            </svg>
            <div class="comments-show-file">
            <a class="mr10" href="' . $attachment->attachmentUrl() . '" target="_blank">
            <i class="fa fa-picture-o" aria-hidden="true"></i>
            '.$attachment->attachment.'
            </a>
            </div>
            <div class="delete-btn">
            <a fileName="'.$attachment->attachment.'" href="' . url("deleteAttachment") . '/' . $attachment->id . '" aria-hidden="true" class="delete-attachment">
            <svg width="25" height="25" viewBox="0 0 25 25">
            <path fill="#2C3542" fill-rule="nonzero" d="M7.833 18.333c0 .917.75 1.667 1.667 1.667h6.667c.916 0 1.666-.75 1.666-1.667v-10h-10v10zm10.834-12.5H15.75L14.917 5H10.75l-.833.833H7V7.5h11.667V5.833z"></path>
            </svg>
            </a>    
            </div>
            </div>';
        }
        return $attachmentHtml;
    }
   
}
