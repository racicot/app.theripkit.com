<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Company;
use App\Model\Portfolio;
use App\Model\CompanyParent;
use App\Model\Rip;
use App\ModelHasRole;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CheckLoginController extends Controller
{   

    /**
     * getUserdetails function
     *
     * @param Request $request
     * @return blade
     */
    public function getRipkitRipdetails(Request $request)
    {
        $action = '';        
        if(isset($request->action)){
            $action = $request->action;
        }
        $email = base64_decode($request->email);
        $user_name = base64_decode($request->user_name);
        $company = base64_decode($request->company);
        $returnUrl = urldecode($request->return_url);      
        $user = DB::table('users')->where('email',$email)->first();
      
        if((isset($request->laravel_token) && isset($user) && $request->laravel_token == $user->wp_token) || $action == 'dologin'){   
            
            if($request->email == ''){
                return  $this->returnError($user, $action,  $request,$returnUrl,'You need to add your email in "BuildImpossible" to access RIPKIT');
            }
            if($request->user_name == ''){
                return  $this->returnError($user, $action,  $request,$returnUrl,'You need to add your name in "BuildImpossible" to access RIPKIT');
            }
            if($request->company == ''){
                return  $this->returnError($user, $action, $request,$returnUrl, 'You are not associated with any company on "BuildImpossible". Please specify your company name under profile to access RIPKIT');
            }            
            
            return $this->loginUser($request, $user_name, $email, $user, $returnUrl, $company, $action);
        }
        else{
            return view('wordpress-template.wordpress_login_to_ripkit')->with([ 'company' =>$request->company, 'email' =>$request->email, 'user_name' =>$request->user_name, 'return_url' => $returnUrl, 'message' => '']);
        }
    }
 
    public function returnError($user, $action, $request,$returnUrl, $message){
        if($user){
            User::where('id',$user->id)->update(['wp_token' => '']);
            Auth::logout($user);
        }        
        if( $action == 'dologin'){
            return redirect('login')->with(['flashMessage' => $message,
             'class' => 'alert-error']);
        }
        else{
            return view('wordpress-template.wordpress_login_to_ripkit')->with([ 'company' =>$request->company, 'email' =>$request->email, 'user_name' =>$request->user_name, 'return_url' => $returnUrl, 'message' => $message]);
        }
        
    }

    /**
     * loginUser function
     *
     * @param [type] $user_name
     * @param [type] $email
     * @param [type] $company_name
     * @param [type] $user
     * @return view
     */
    public function loginUser($request, $user_name, $email,  $user, $returnUrl, $companyName='', $action = ''){
        
      
        // Register User and Company 
        DB::beginTransaction();
            try {
                $password = uniqid();

                if($user != ''){
                    $role = ModelHasRole::where('model_id', $user->id)->first();                    
                    if($user->status != 1){
                        return  $this->returnError($user, $action,  $request,$returnUrl,'Your email "'.$email.'" is not verified in RIPKIT');                        
                      } 
                      else if($role->role_id != 2){
                        return  $this->returnError($user, $action, $request,$returnUrl, 'You do not have admin role rights on RIPKIT to access application');   
                      }
                }
                $company = '';
                if($companyName != ''){                    
                    $company = DB::table('companies')->where(DB::raw("LOWER(name)"), strtolower($companyName))->first();
                    if($company && $company->status != 1){
                        return  $this->returnError($user, $action, $request,$returnUrl, 'Your Build Impossible company name "'.$companyName.'" is deactivated in RIPKIT');    
                       
                    }
                    else if($user != '' && !$company){
                        return  $this->returnError($user, $action,  $request,$returnUrl,'Your Build Impossible company name "'.$companyName.'" does not exist in RIPKIT');    
                       
                    }

                }    
                if($user == ''){ //..register user
                    $emailtoken = base64_encode(\Carbon\Carbon::now());
                    $user = User::create([
                        'name' => $user_name,
                        'email' => $email,
                        'password' => Hash::make($password),
                        'email_token' => $emailtoken,
                    ]);
                    $user->assignRole('Admin');
                    $url = url('/') . '/userLogin/' . $emailtoken;
                    \Mail::send('email.createUserEmail',
                        ['url' => $url, 'password' => $password, 'email' => $email, 'name' => $user->name],
                        function ($message) use ($email) {
                            $message->subject('RIPKIT: Account Created');
                            $message->to($email);
                            $message->replyTo(config('setting.email.super_admin'));
                        });                   
                    if(!$company){
                        $company = $this->createCompany($companyName);                        
                    }                   
                } 

                if($company){
                    $this->createUserCompanyRelation($user->id, $company->id);
                }                 
               
                DB::commit();
                $this->doLogin($user->id, $company, $action);  
                if($action == 'dologin'){
                    $token = bin2hex(random_bytes(16));
                    User::where('id', $user->id)->update(['wp_token' => $token]); 
                    if (parse_url($returnUrl, PHP_URL_QUERY)){
                        $redirectTo = $returnUrl.'&token='.$token;
                    }   
                    else{
                        $redirectTo = $returnUrl.'?token='.$token;
                    }      
                    return redirect($redirectTo);
                }
                else{
                   return $this->getRiPDetail($request);
                }
                
            } catch (\Exception $e) {
                DB::rollback();
            }
    }

    public function getRiPDetail($request){
        $rips = Rip::where('company_id', session('company_id'))->where('expired', '=', '0')->latest()->first();
        $left = 0;
        if($rips){
           $ripObject = new RipController();
           $left = $ripObject->datePeriod($rips);
        }
       return view('wordpress-template.rip_period_progress')->with(['email' => $request->email, 'currentRip' => $rips, 'left' => $left]);
    }

   /**
     * createUserCompanyRelation function
     *
     * @param [type] $userId
     * @param [type] $companyId
     * @return void
     */
    public function createUserCompanyRelation($userId, $companyId){
      
        $relation = CompanyParent::where('parent_id', $userId)->where('company_id', $companyId)->first();
        if(!$relation){
            CompanyParent::create([
                'parent_id' => $userId,
                'company_id' => $companyId
            ]);
        }

    }

    /**
     * createCompany function
     *
     * @param [type] $name
     * @param [type] $user
     * @return company
     */
    public function createCompany($name){
        $company =   Company::create([
            'name' => $name,
            'status' => 1,
        ]);

        Portfolio::create([
            'company_id' => $company->id
        ]);
        return $company;
    }

    /**
     * doLogin function
     *
     * @param [type] $user_id
     * @param [type] $compamy
     * @return void
     */
     function doLogin($user_id, $company, $action){
        if($action == 'dologin'){
            Auth::loginUsingId($user_id);
        }    
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        //..set company session
        if($company != ''){
            $this->setCompanySession($company);
        }
        
     }

     /**
      * setCompanySession function
      *
      * @param [type] $company
      * @return void
      */
    public function  setCompanySession($company){
        session()->put(['company_id' => $company->id]);
        session()->put(['company_name' => $company->name]);
        session()->put(['company_logo' => $company->logo]);
        $initials = strtoupper($this->initials($company->name));
        session()->put(['initials' => $initials]);
        session()->put(['companies' => $company]);
     }

     /**
      * initials function
      *
      * @param [type] $name
      * @return initials
      */
     public function initials($name)
    {
        $words = explode(" ", $name);
        $initials = "";
        $i = 0;
        foreach ($words as $w) {
            if ($i > 2) {
                break;
            }
            $initials .= $w[0];
            $i++;
        }
        return $initials;
    }

    
}
