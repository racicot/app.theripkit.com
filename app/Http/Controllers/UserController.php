<?php

namespace App\Http\Controllers;

use App\Model\CompanyParent;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;
use App\TaskForTodays;
use App\Model\Company;
use App\Model\Goal;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Model\LoginImage;
use App\Http\Requests\User\UpdateUserValidator;
use App\Http\Requests\User\CreateUserValidator;
use App\Http\Requests\User\UpdateProfileValidator;
use App\Http\Requests\User\LoginViewValidator;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;
use App\Model\Countries;
use App\Model\Task;
use Carbon\Carbon;
use App\Notifications\NewEvent;


class UserController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth', 'userstatus', 'CheckCompany']);
        $this->middleware('permissions')->except([
            'ajaxData',
            'edit',
            'update',
            'csvData',
            'sampleCsv',
            'updateCsvData',
            'validateCsv'
        ]);
    }

    /**
     * Function to show the view for the user
     * @return View
     */
    public function index()
    {
        $userPermissions = auth()->user()->getAllPermissions()->filter(function ($item) {
            return ($item->group_name == 'Users');
        })->pluck('name')->toArray();
        $userRole = User::find(auth()->user()->id);
        $roles = Role::all()->filter(function ($item) use ($userPermissions, $userRole) {
            if($userRole->user_role == 1){
                return $userPermissions;
            }
            return (in_array('Create ' . $item->name, $userPermissions));
        });
        $companies = auth()->user()->getCompanies();
        $users = User::orderBy('name')->get();   
        
        return view('auth.user.view')->with(['roles' => $roles, 'users' => $users, 'companies' => $companies]);
    }    

    /**
     * Function to create the user from the backend with assign roles
     * @param  Request $request
     * @return redirect to user view
     */
    public function store(CreateUserValidator $request)
    {
        $password = uniqid();
        $user_id = \Auth::user()->id;
        $email = $request->email;
        $name = $request->name;
        $companyId = $request->company;
        $emailtoken = base64_encode(\Carbon\Carbon::now());

        DB::beginTransaction();
        $user = User::create([
            'email' => $email,
            'name' => $name,
            'password' => Hash::make($password),
            'email_token' => $emailtoken,
            'status' => \Config::get('magicNumbers.ONE'),
            'parent_id' => $user_id,
        ]);
        $user->syncRoles($request->role);
        if($request->role != \Config::get('setting.roles.one')){
            foreach ($request->company as $companyId) {
                if (!auth()->user()->isCompanyAssigned($companyId)) {
                    return back()->with(['flashMessage' => \Config::get('flashmessage.NOPERMISSIONTOCREATEUSER'),
                        'class' => 'alert-error']);
                }
                CompanyParent::create([
                    'parent_id' => $user->id,
                    'company_id' => $companyId,
                ]);
            }
        }

        DB::commit();

        $url = url('/') . '/userLogin/' . $emailtoken;
        \Mail::send('email.createUserEmail',
            ['url' => $url, 'password' => $password, 'email' => $email, 'name' => $user->name],
            function ($message) use ($email) {
                $message->subject('RIPKIT: Account Created');
                $message->to($email);
                $message->replyTo(config('setting.email.super_admin'));
            });
        session()->put('success', config('flashmessage.USERCREAT'));
    }

    /**
     * Display the user account page.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showAccount()
    {
        $user = auth()->user();
        $countries = Countries::all()->toArray();
        return view('auth.user.account')->with(['user' => $user, 'countries' => $countries]);
    }

    /**
     * Update the user details in the database
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function editProfile(UpdateProfileValidator $request)
    {
        $id = Auth::user()->id;
        $user = User::find(intval($id));

        if ($request->hasFile('image')) {
            $exists = \Storage::disk('system_photos')->exists($user->image);
            if ($exists) {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);

                \Storage::disk('system_photos')->delete($user->image);
            } else {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);
            }
        } else {
            $file = $user->image;
        }
        
        User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'image' => $file,
            'phone' => $request->phone,
            'isd_code' => $request->isd_code,
        ]);
        session()->put('success', \Config::get('flashmessage.PROFUPDATE'));
        return back();
    }

    /**
     * UpdatePassword function to update account password for user
     **/
    public function updatePassword(Request $request)
    {
        $errors = new MessageBag();
        $id = $request->id;
        $user = User::find($id);
        if ((\Hash::check($request->oldPassword, $user->password))) {
            \Validator::make($request->all(),
                [
                    'newPassword' => 'required|min:6',
                    'confirmPassword' => 'required|min:6|same:newPassword'
                ])->validate();
            $user->password = bcrypt($request->newPassword);
            $user->save();
            $flashMessage = \Config::get('flashmessage.PWDUPDATE');
        } else {
            \Validator::make($request->all(),
                [
                    'oldPassword' => 'required',
                    'newPassword' => 'required|min:6',
                    'confirmPassword' => 'required|min:6|same:newPassword'
                ])->validate();
            $errors->add('oldPassword', 'Incorrect Password');
            return back()->with('errors', $errors);
        }
        session()->put('success', $flashMessage);
        return back();
    }

    /**
     * Function to show the view for the edit user page to admin
     * @param Request $request , int  $id
     * @return View
     */
    public function edit($id)
    {
        $user = User::find(base64_decode($id));
        $userPermissions = auth()->user()->getPermissionsViaRoles()->filter(function ($item) {
            return ($item->group_name == 'Users');
        })->pluck('name')->toArray();
        $roles = Role::all()->filter(function ($item) use ($userPermissions) {
            return (in_array('Create ' . $item->name, $userPermissions));
        });
        
        if (($user->roles->isNotEmpty() && !in_array($user->roles->pluck('name')->first(),
                $roles->pluck('name')->toArray()))
        ) {
            echo '<div class="ajax-permi-err"><span class="text-danger"><i class="fa fa-lock" aria-hidden="true"></i>
                    Access Denied. </span><div><p>Please contact Admin for any query.</p></div></div>';die;
        }
        $groups = Permission::where('group_name', '!=',
            null)->get()->sortBy('group_name')->pluck('group_name')->unique();
        $userPermissions = $user->getAllPermissions()->sortBy('group_name')->pluck('name')->toArray(); // All permissions assigned to the user
        $rolePermissions = $user->getPermissionsViaRoles()->sortBy('group_name')->pluck('name')->toArray(); // Permission assigned only to the user's role
        $permissions = Permission::where('group_name', '!=', null)->get()->sortBy('group_name');
        $companies = Company::all();
        $companyIds = $user->getCompanies()->pluck('id')->toArray();
        return view('auth.user.edit')->with([
            'user' => $user,
            'roles' => $roles,
            'groups' => $groups,
            'userPermissions' => $userPermissions,
            'rolePermissions' => $rolePermissions,
            'permissions' => $permissions,
            'companies' => $companies,
            'companyIds' => $companyIds
        ]);
    }

    /**
     * Function to update the user details by admin
     * @param  Request $request , int $id
     * @return redirect to user edit user page
     */
    public function update(UpdateUserValidator $request, $id)
    {

        $user = User::find(intval($id));
        $assignCompanies = CompanyParent::where('parent_id', $id)->get();

        $image = $user->image;
        if ($request->hasFile('image')) {
            $exists = \Storage::disk('system_photos')->exists($user->logo);

            if ($exists) {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);

                \Storage::disk('system_photos')->delete($image->logo);
            } else {
                $file = $request->file('image')->store('', [
                    'disk' => 'system_photos'
                ]);
            }
        } else {
            $file = $user->image;
        }
        \DB::beginTransaction();
        User::where('id', $id)->update([
            'name' => $request->name,
            'email' => $request->email,
            'image' => $file,
            'status' => $request->status,
        ]);
        $old_assigned_companies = [];
        foreach($assignCompanies as $company){
            $old_assigned_companies[] = "$company->company_id";
        }
        if ($request->has('assigned_companies')) {
            $removedCompany = $this->getRemovedCompany($request->assigned_companies, $assignCompanies);
            $this->removeUserFromGoalsAndTask($removedCompany, $id);
            $assignCompanies->each(function ($item) {
                $item->delete();
            });
            foreach ($request->assigned_companies as $company_id) {
                CompanyParent::create([
                    'parent_id' => $id,
                    'company_id' => $company_id
                ]);
                                
            }
            $new_assigned = $request->assigned_companies;
            $new_company_added = array_diff($new_assigned, $old_assigned_companies);
            // Mail notification for newly assigned company            
            foreach($new_company_added as $company_id) {                
                $assignUser = User::find($id);
                $assignedCompany = Company::where('id', $company_id)->pluck('name')->first();
                $mailMessage['msg_start'] = config('setting.notification.company_assigned');
                $mailMessage['rip_name'] = '';
                $mailMessage['msg_part'] = '';
                $mailMessage['company_name'] = $assignedCompany;
                $mailMessage['msg_end'] = ".";
                $subject = 'RIPKIT: '.config('setting.notification.company_assigned_to_user');
                $assignUser->notify(new NewEvent($mailMessage, 1, $assignUser->name,$subject));
                
                // In-app Notification
                $message = config('setting.notification.company_assigned')."'".$assignedCompany."'.";
                $assignUser->notify(new NewEvent($message, 0, $assignUser->name,$subject));
                       
            }
            
            
            session()->put(['company_id' => '']);
        }
        if ($request->has('permission')) {
            $user->syncPermissions($request->permission);
        } else {
            $user->syncPermissions([]);
        }
        \DB::commit();
        if (!empty($request->role)) {
            $user->syncRoles($request->role);
        }
        session()->put('success', \Config::get('flashmessage.UPDATEUSER'));
    }

    /**
     * Function to get removed company id by admin
     * @param  array $latestAssignedCompanies
     * @param  object $assignedCompanies
     * @return array $romvedCompanyIds
     */ 
    public function getRemovedCompany($latestAssignedCompanies, $assignedCompanies)
    {
        $romvedCompanyIds = [];
        foreach ($assignedCompanies as $key => $value) {
            if(!in_array($value->company_id, $latestAssignedCompanies)){
                 array_push($romvedCompanyIds, $value->company_id);
            }
        }
        return $romvedCompanyIds;
    }
    /**
     * Function to remove user from goals and tasks
     * @param  array $companyIds
     * @param  int $userId
     * @return boolean
     */ 
    public function removeUserFromGoalsAndTask($companyIds, $userId)
    {
         Goal::removeUser($companyIds, $userId); //unassignUser from the goal
         Task::removeUser($companyIds, $userId); //unassignUser from the task
         return true;
    }

    /**
     * Function to delete the user details
     * @param  int $id
     * @return redirect to user edit user page
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect('/user')->with(['flashMessage' => \Config::get('flashmessage.USERDEL'),
            'class' => 'alert-success']);
    }

    /**
     * @param LoginViewValidator $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function updateLoginView(LoginViewValidator $request)
    {
        $loginImage = LoginImage::all()->first();
        if ($request->hasFile('bg_image')) {
            $file_name = $request->file('bg_image')->store('', [
                'disk' => 'system_photos'
            ]);
            Storage::disk('system_photos')->delete($loginImage->name);
        } else {
            $file_name = $loginImage->name;
        }
        if($request->motto == '') {
            $request->motto = 'NULL';
        }
        $loginImage->update([
            'name' => $file_name,
            'motto' => $request->motto
        ]);
        session()->put('success',\Config::get('flashmessage.LOGINBGMTUPDATE'));
        return back();        
    }
    
     public function userPasswordReset(Request $request){
            $user = User::find(base64_decode($request->id));
            $password = uniqid();
            $user->password = Hash::make($password);
            $user->save();
            $emailtoken = base64_encode(\Carbon\Carbon::now());
            $url = url('/') . '/userLogin/' . $emailtoken;
            $email = $user->email;
            \Mail::send('email.resetUserPasswordEmail',
                ['url' => $url, 'password' => $password, 'email' => $email, 'name' => $user->name],
                function ($message) use ($email) {
                    $message->subject('RIPKIT: New Password');
                    $message->to($email);
                    $message->replyTo(config('setting.email.super_admin'));
                });
        session()->put('success', \Config::get('flashmessage.EMAILPWD'));
        return back();
     }

    /**
     * Returns the users to be displayed on the datatables
     * @return mixed
     */
    public function ajaxData()
    {
        $userPermissions = auth()->user()->getAllPermissions()->pluck('name')->toArray();
        $roles = Role::all()->filter(function ($item) use ($userPermissions) {
            return (in_array('Create ' . $item->name, $userPermissions));
        });
        if (auth()->user()->hasAnyRole(['Super Admin'])) {
            $users = User::companyjoin()->rolejoin();
        } else {
            $users = User::companyjoin()->rolejoin()->companyparent();
        }

        return Datatables::of($users)
            ->addColumn('role', function ($user) {
                $content = $user->roles_name;
                return $content;
            })
            ->addColumn('company', function ($user) {
                $companies = Company::whereHas('parent', function ($query) use ($user) {
                    $query->where('parent_id', $user->id);
                })->orWhere('user_id', $user->id)->get()->unique('id');
                $companyHtml = '<ul>';
                if ($companies->isNotEmpty()) {
                    foreach ($companies as $company) {
                        $companyHtml .= '<li>' . $company->name . '</li>';
                    }
                    $companyHtml .= '</ul>';
                    $content = $companyHtml;
                } else {
                    $content = '--';
                }
                return $content;
            })
            ->addColumn('status', function ($user){
                if($user->status){
                    $content = "<span class='status-badge'>Active</span>";
                } else{
                    $content = "<span class='status-badge inactive'>Inactive</span>";
                }
                return $content;
            })
            ->addColumn('created_at', function ($user) {
                $dt = Carbon::parse($user->created_at);
                $content = $dt->format('M j, Y');
                return $content;
            })
            ->addColumn('actions', function ($user) use ($roles) {
                if ((in_array($user->roles->pluck('id')->first(), $roles->pluck('id')->toArray()))) {
                    if($this->checkEditPermission(auth()->user()->id)){
                        $content = "<a href=" . url('/user/' . base64_encode($user->id) . '/edit') . " class='editUserButton green-text' data-toggle='modal' data-target='#editUserModal' title='Edit'>Edit</a>";
                     } else{
                         $content = '<span class="error-msg cust-msg">Limited Access</span>';
                     }
                } elseif ((auth()->user()->hasAnyRole(['Super Admin'])) && $user->roles->isEmpty()) {
                    $content = "<a href=" . url('/user/' . base64_encode($user->id) . '/edit') . " class='editUserButton green-text' data-toggle='modal' data-target='#editUserModal' title='Edit'>Edit</a>";

                } else {
                    $content = '<span class="error-msg cust-msg">Permission Denied</span>';
                }
                if ((auth()->user()->hasAnyRole(['Super Admin']) && $user->roles->pluck('name')->first() != 'Super Admin')){
                    $content .=  "<a href=" . url('/user/resetPassword/' . base64_encode($user->id)) . " class='userResetPassword orange-text' title='Reset Password'>Reset Password</a>";
                }
                return $content;
            })
            ->addColumn('image', function ($user) {
                $content = "<div class='photo-in'><label for='profile-picture'><img src=" . $user->userImage() . " alt='User'></div><span>".$user->name."</span><small>".$user->email."</small>";
                return $content;
            })->rawColumns(['image', 'actions', 'company','status'])->make(true);
    }

    /** Get auth user's edit permission
     * @param int $id
     * @return json
     */
    public function checkEditPermission($id)
    {
        $user = User::find($id);
        $userPermissions = auth()->user()->getPermissionsViaRoles()->filter(function ($item) {
            return ($item->group_name == 'Users');
        })->pluck('name')->toArray();
        $roles = Role::all()->filter(function ($item) use ($userPermissions) {
            return (in_array('Create ' . $item->name, $userPermissions));
        });

        if (($user->roles->isNotEmpty() && !in_array($user->roles->pluck('name')->first(),
                $roles->pluck('name')->toArray()) && $user->user_role != \Config::get('magicNumbers.ONE'))
        ) {
            return false;
        } else {
            return true;
        }
    }

    /** Get all Done task
     * @return json
     */
    public function getDoneTask()
    {
        return TaskForTodays::getDoneTask();
    }

    /** Get all task which are not completed
     * @return json
     */
    public function getNotDoneTask()
    {
        return TaskForTodays::getNotDoneTask();
    }

    /** Add new task
     * @return json
     */
    public function addTask()
    {
        return TaskForTodays::createTask();
    }

    /** Update task status, done or not done
     * @return json
     */
    public function taskStatus($taskId)
    {
        return TaskForTodays::taskStatus($taskId);
    }

    /** Clear all completed task 
     * @return json
     */
    public function clearDoneTask()
    {
        return TaskForTodays::clearDoneTask();
    }
}
