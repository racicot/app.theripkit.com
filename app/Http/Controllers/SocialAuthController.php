<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Providers\SocialAccountService;
use Socialite;

class SocialAuthController extends Controller
{
    public function facebookRedirect()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function facebookCallback()
    {
        $service = new SocialAccountService;
        $user = $service->createOrGetUser(Socialite::driver('facebook')->user(), 'facebook');
        auth()->login($user);

        return redirect()->to('/home');
    }

    public function googleRedirect()
    {
        return Socialite::driver('google')->redirect();
    }

    public function googleCallback()
    {
        $service = new SocialAccountService;
        $user = $service->createOrGetUser(Socialite::driver('google')->user(), 'google');

        if (!$user) {
            return redirect('roles')->with(['flashMessage' => 'The google account with which you are trying to log in or already logged in is not associated with any Build Impossible account. Please login with a valid google account',
                'class' => 'alert-success']);
        }

        auth()->login($user);

        return redirect()->to('/home');
    }
}