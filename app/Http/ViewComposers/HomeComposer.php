<?php

namespace App\Http\ViewComposers;

use App\Model\CompanyParent;
use App\Model\Rip;
use App\Model\Task;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;
use Illuminate\View\View;

class HomeComposer
{
    protected $router, $request;

    public function __construct(Router $router, Request $request)
    {
        $this->router = $router;
        $this->request = $request;
    }

    /**
     * Bind data to the view.
     *
     * @param  View $view
     * @return void
     */
    public function compose(View $view)
    {
        $route_name = Route::current()->getName();
        $users = User::whereHas('parent', function ($query) {
            $query->where('company_id', session('company_id'));
        })->get()->unique();

        if ($route_name == "home") {            
            $current_user = auth()->user()->id;
        } else {
            $current_user = $this->request->id;
        }

        $currentdate = Carbon::now()->format('Y/m/d');
        $rips = Rip::with(['category', 'category.goals'])->where('company_id', session('company_id'))->where('expired', '=', '0')->get();
        $todo_tasks = Task::with(['goal', 'goal.rip'])->whereHas('goal', function ($query) {
            $query->where('company_id', session('company_id'));
        })->whereHas('goal.rip', function ($query) {
            $query->where('expired', '0');
        })->get()->filter(function ($value) use ($current_user) {
            return (in_array($current_user, explode(',', $value->assign_to)));
        });
        $todo_tasks = $todo_tasks->filter(function ($value) {
            $start_date = Carbon::createFromFormat('Y-m-d', $value->goal->rip->start_date)->subDay();
            $end_date = Carbon::createFromFormat('Y-m-d', $value->goal->rip->end_date)->addDay();
            $today = Carbon::today();
            return ($today->between($start_date, $end_date));
        });
        $view->with([
            'current_id' => $current_user,
            'todo_tasks' => $todo_tasks,
            'users' => $users,
            'showAction' => false,
            'showAssignUser' => false,
            'userLimit' => config('magicNumbers.TODOUSERLIMIT'),
            'rips' => $rips,
            'showRipInfo' => true,
        ]);
    }
}
