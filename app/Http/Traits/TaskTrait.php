<?php
namespace App\Http\Traits;

use App\Model\Goal;
use App\Model\CompanyParent;
use App\Notifications\NewEvent;
use App\User;
use Illuminate\Http\Request;
use App\Events\TaskCreated;
use App\Events\TaskDeleted;
use App\Events\TaskUpdated;
use App\Events\TaskComplete;
use App\Events\TaskUndo;
use App\Events\AddTaskName;
use App\Model\Task;
use App\Http\Requests\Rip\AddTaskValidator;
use Illuminate\Support\Facades\View;
use App\Jobs\ProcessEvent;


trait TaskTrait
{

    /**
     * ripAddTask will add new task in a goal in RIP section
     * @param  Request $request [description]
     * @return           View
     */
    public function ripAddTask(AddTaskValidator $request)
    {
        $goal_id = base64_decode($request->id);
        $company_id = session('company_id');

        $goal = Goal::find($goal_id);
        \DB::beginTransaction();
        $task = Task::create([
            'goal_id' => $goal_id,
            'name' => strip_tags($request->task),
            'due_date' => date('Y-m-d',strtotime($goal->due_date)),
            'status' => 1,
            'assign_to' => '',
        ]);
        \DB::commit();
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');

        $newGoal = Goal::with('task')->find($goal_id);
        $count = $newGoal->task->count();
        $response['html'] = view('auth.rip.addTask')->with([
            'task' => $task,
            'goal' => $goal,
            'goalusers' => $goalUsers
        ])->render();
        $response['goalId'] = $goal_id;
        $response['count'] = $count;
        broadcast(new TaskCreated($task->id))->toOthers();
        return response()->json($response);
    }

    public function showTask(Request $request)
    {
        $task_id = $request->data;
        $task = Task::find($task_id);
        if(!$task){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $newGoal = Goal::with('task')->find($task->goal_id);
        $count = $newGoal->task->count();
        $response['html'] = view('auth.rip.addTask')->with([
            'task' => $task,
            'goal' => $newGoal,
            'goalusers' => $goalUsers
        ])->render();
        $response['goalId'] = $task->goal_id;
        $response['count'] = $count;
        $response['dataNull'] = false;
        return response()->json($response);
    }


    /** function return the user assign to the task
     * @param Request $request
     * @return html of the asssign user
     */
    public function taskUser(Request $request)
    {

        $task_id = $request->taskId;
        $taskUser = Task::where('id', $task_id)->first();
        $userArray = User::whereIn('id', explode(',', $taskUser->assign_to))->get();
        $html = '';
        foreach ($userArray as $user) {
            $html .= View::make('auth.rip.ajax.taskdropdown', ['user' => $user, 'task_id' => $task_id])->render();
        }
        return $html;
    }

    /** function  assign user to added task
     * @param Request $request
     * @return html of the asssign user
     */

    public function assignUserToAddedtask(Request $request)
    {
        $imageHtml = "";
        $taskId = $request->task_id;
        $task = Task::find($taskId);
        if(!$task)
        return response()->json($response['dataNull'] = true);

        $goal = Goal::with('task')->find($task->goal_id);
        $old_assigned_users = array_filter(explode(',', $task->assign_to));
        
        $user_ids = array();
        if($request->user){
            foreach ($request->user as $user) {
                if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
                    $user_ids[] = $this->createUser($user);
                } else {
                    $user_ids[] = $user;
                }
            }
            $task->update([
                'assign_to' => implode(',', $user_ids),
            ]);
        }
        else{
            $task->update([
                'assign_to' => '',
            ]);
        }    

        $userArray = User::whereIn('id', $user_ids)->get()->sortBy('name');
        $arrayCount = count($userArray);
        $imageHtml.= View::make('auth.rip.ajax.taskimage', ['users' => $userArray,'count' => $arrayCount])->render();        
        $response['imageHtml'] = $imageHtml;
        $response['dataNull'] = false;
        broadcast(new TaskUpdated($task->id))->toOthers();
        $removed_added = array_diff($old_assigned_users, $user_ids);
        
        if(!in_array($removed_added, $user_ids)) {
            // Mail Notification
            $mailMessage['msg_initial'] = config('setting.notification.removed_from_task');
            $mailMessage['msg_part1'] = config('setting.notification.under_rip');
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_start'] = $mailMessage['msg_initial']."'".$task->name."'".$mailMessage['msg_part1'];
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');
            $mailMessage['task_name'] = $task->name;
            
            $action = config('magicNumbers.MEETINGMAILACTION');
            $subject = 'RIPKIT: '.config('setting.notification.removed_user');
            User::whereIn('id', $removed_added)->each(function($user) use($mailMessage, $action, $subject){
                $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
            });
            
            // In-app Notification
            $message = config('setting.notification.removed_from_task')."'".$task->name."'".config('setting.notification.under_rip')."'".$goal->goal."'".config('setting.notification.related_company')."'".session('company_name')."'.";
            $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'), $subject);          
            
        } 
        
        // For Mail Notification
        $mailMessage['msg_initial'] = config('setting.notification.assigned_to_task');
        $mailMessage['msg_part1'] = config('setting.notification.under_rip');
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_start'] = $mailMessage['msg_initial']."'".$task->name."'".$mailMessage['msg_part1'];
        $mailMessage['msg_end'] = '.';
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $mailMessage['task_name'] = $task->name;
        $subject = 'RIPKIT: '.config('setting.notification.assign_task_to_user');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $task->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });
        // for in-app notification
        $message = config('setting.notification.assigned_to_task')."'".$task->name."'".config('setting.notification.under_rip')."'".$goal->goal."'".config('setting.notification.related_company')."'".session('company_name')."'";
        $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'), $subject);
        
        return response()->json($response);
    }

    /** function to broadcast the updated task content on other connections
     * @param Request $request
     * @return json response
     */
    public function updateTask(Request $request)
    {
        $task_id = $request->taskId;
        $task = Task::find($task_id);
        if(!$task)
        return response()->json($response['dataNull'] = true);
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $newGoal = Goal::with('task')->find($task->goal_id);
        $response['html'] = view('auth.rip.addTask')->with([
            'task' => $task,
            'goal' => $newGoal,
            'goalusers' => $goalUsers,
            'current_page'=> $request->current_page
        ])->render();


        $response['taskId'] = $task->id;
        $response['dataNull'] = false;
        return response()->json($response);
    }

    /** function delete the assign user from added task
     * @param Request $request
     * @return html of the asssign user
     */

    public function taskUserDelete(Request $request)
    {
        $imageHtml = "";
        $dropdownHtml = "";
        $id = $request->id;
        $taskId = $request->taskId;
        $task = Task::find($taskId);
        $goal = Goal::with('task')->find($task->goal_id);
        $assignUser = explode(',', $task->assign_to);
        $does_key = array_search($id, $assignUser);
        if (isset($does_key)) {
            unset($assignUser[$does_key]);
        }
        $task->update([
            'assign_to' => implode(',', $assignUser),
        ]);
        $userArray = User::whereIn('id', $assignUser)->get();
        $html = '';
        foreach ($userArray as $user) {
            $html .= View::make('auth.rip.ajax.taskdropdown', ['user' => $user, 'task_id' => $taskId])->render();
            $imageHtml .= View::make('auth.rip.ajax.taskimage', ['user' => $user])->render();
        }
        $response['dropdownHtml'] = $html;
        $response['imageHtml'] = $imageHtml;
        $response['task_id'] = $taskId;
        broadcast(new TaskUpdated($task->id))->toOthers();
        
        // For Mail Notification
        $message['msg_initial'] = config('setting.notification.removed_from_task');
        $message['msg_part1'] = config('setting.notification.under_rip');
        $message['msg_start'] = $message['msg_start']."'".$task->name."'".$message['msg_part1'];
        $message['msg_part'] = config('setting.notification.related_company');
        $message['msg_end'] = '.';
        $message['rip_name'] = $goal->goal;
        $message['company_name'] = session('company_name');
        $message['task_name'] = $task->name;
        $subject = 'RIPKIT: '.config('setting.notification.removed_user');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($message, $action, $subject){
            $user->notify(new NewEvent($message, $action, $user->name, $subject));
        });
        
        // For In-App Notification
        $message = config('setting.notification.removed_from_task')."'".$task->name."'".config('setting.notification.under_rip')."'".$goal->goal."'.";
        $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'));
        
        return response()->json($response);
    }

    /** function mask incomplete task as complete
     * @param Request $request
     * @return string
     */
    public function taskComplete(Request $request)
    {  

        $task = Task::find($request->taskId);  
        $goal = Goal::with('task')->find($task->goal_id);      
        $task->update([
            'percentage_complete' => 100
        ]);
        broadcast(new TaskComplete($task->id))->toOthers();
        $company_id = session('company_id');

        $current_id = $request->current_id;
        $todo_tasks = Task::with(['goal'])->whereHas('goal', function ($query) {
            $query->where('company_id', session('company_id'));
        })->get();
        $filtered_tasks = $todo_tasks->filter(function ($value, $key) use ($current_id) {
            return (in_array($current_id, explode(',', $value->assign_to)));
        });
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        
        // For Mail Notification       
        $message['msg_initial'] = config('setting.notification.assigned_task');
        $message['msg_part1'] = config('setting.notification.under_rip');
        $message['msg_start'] = $message['msg_initial']."'".$task->name."'".$message['msg_part1'];
        $message['msg_part'] = config('setting.notification.related_company');
        $message['msg_end'] = config('setting.notification.mark_completed');
        $message['rip_name'] = $goal->goal;
        $message['company_name'] = session('company_name');
        $message['task_name'] = $task->name;
        $subject = 'RIPKIT: '.config('setting.notification.task_completed');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $task->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($message, $action, $subject){
            $user->notify(new NewEvent($message, $action, $user->name, $subject));
        });
        
        $message = config('setting.notification.assigned_task').'"'.$task->name.'"'.config('setting.notification.under_rip').$goal->goal.config('setting.notification.related_company').'"'.session('company_name').'"'.config('setting.notification.mark_completed');
        $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'),$subject);
        
        $response['count'] = $filtered_tasks->count();
        $response['taskHtml'] = view('auth.rip.addTask')->with(['task' => $task, 'goalusers' => $goalUsers, 'current_page'=> $request->current_page])->render();
        $response['taskId'] = $task->id;
        $response['html'] = '<div class="card-list-item">
                      <div class="card-list-item__left">
                          <h3 class="card-list__heading">No active Tasks for this user</h3>
                      </div><!-- /.card-list-item__left -->
                  </div>';
        return response()->json($response);
    }

    /** function broadcast "mark incomplete task as complete" data on other connection
     * @param Request $request
     * @return string
     */
    public function showCompletedTask(Request $request)
    {   
        $task = Task::find($request->taskId);
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');        
        $response['taskHtml'] = view('auth.rip.addTask')->with(['task' => $task, 'goalusers' => $goalUsers, 'current_page'=> $request->current_page])->render();
        $response['taskId'] = $task->id;
        return response()->json($response);
    }

    /** Mark complete task as incomplete
     * @param Request $request
     * @return string
     */
    public function taskUndo(Request $request)
    {
        $task = Task::find($request->taskId);
        $goal = Goal::with('task')->find($task->goal_id);
        $task->update([
            'percentage_complete' => 0
        ]);
        broadcast(new TaskUndo($task->id))->toOthers();
        $subject = "RIPKIT: ".config('setting.notification.undo_task_status');
        $message = "The status for the task '".$task->name."' under the RIP ".$goal->goal." for the company '".session('company_name')."', has been changed to Incomplete.";
        $this->notifyUser($message, $task, config('setting.notification.undo_task_status'),$subject);
        
        // For Mail Notification       
        $mailMessage['msg_initial'] = config('setting.notification.task_status');
        $mailMessage['msg_part1'] = config('setting.notification.under_rip');
        $mailMessage['msg_start'] = $mailMessage['msg_initial']."'".$task->name."'".$mailMessage['msg_part1'];
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.mark_incomplete');
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $mailMessage['task_name'] = $task->name;
        //$subject = 'RIPKIT: '.config('setting.notification.task_completed');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $task->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });        
        
        $company_id = session('company_id');
        $current_id = $request->current_id;
        $todo_tasks = Task::with(['goal'])->whereHas('goal', function ($query) {
            $query->where('company_id', session('company_id'));
        })->get();
        $filtered_tasks = $todo_tasks->filter(function ($value, $key) use ($current_id) {
            return (in_array($current_id, explode(',', $value->assign_to)));
        });
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');

        $response['count'] = $filtered_tasks->count();
        $response['taskHtml'] = view('auth.rip.addTask')->with([
            'task' => $task,
            'goalusers' => $goalUsers,
            'current_page'=> $request->current_page
        ])->render();
        $response['taskId'] = $task->id;
        $response['html'] = '<div class="card-list-item">
                      <div class="card-list-item__left">
                          <h3 class="card-list__heading">No active Tasks for this user</h3>
                      </div><!-- /.card-list-item__left -->
                  </div>';
        return response()->json($response);


    }

    /** Function broadcast "Mark complete task as incomplete" data on other connections
     * @param Request $request
     * @return string
     */
    public function showUndoTask(Request $request)
    {   
        $task = Task::find($request->taskId);
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['taskHtml'] = view('auth.rip.addTask')->with([
            'task' => $task,
            'goalusers' => $goalUsers,
            'current_page'=> $request->current_page
        ])->render();
        $response['taskId'] = $task->id;
        return response()->json($response);


    }

    /** function return all the task mark as incomplete for the current goal
     * @param Request $request
     * @return string
     */
    public function incompleteTask(Request $request)
    {
        $tasks = Task::where(['goal_id' => $request->id])->get();
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['html'] = view('auth.rip.ajax.incompleteTask')->with([
            'tasks' => $tasks,
            'goalusers' => $goalUsers
        ])->render();
        $response['goalId'] = $request->id;
        return response()->json($response);
    }

    /** function return the view for all task marked as complete
     * @param Request $request
     * @return string
     */
    public function completeTask(Request $request)
    {
        $tasks = Task::where(['goal_id' => $request->id])->get();
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['html'] = view('auth.rip.ajax.completeTask')->with([
            'tasks' => $tasks,
            'goalusers' => $goalUsers
        ])->render();
        $response['goalId'] = $request->id;
        return response()->json($response);
    }

    /** function delete the task through ajax
     * @param Request $request
     * @return string
     */
    public function deleteTask(Request $request)
    {
        $id = $request->id;
        $task = Task::find($id);
        $task->delete();
        $goal = Goal::with('task')->find($task->goal_id);
        $count = $goal->task->count();
        $response['count'] = $count;
        $response['goalId'] = $task->goal_id;
        $response['taskId'] = $id;
        
        $subject = "RIPKIT:".config('setting.notification.delete_task');
        $message = "The task ".$task->name.", for the RIP '".$goal->goal."' of the company '".session('company_name')."', has been deleted.";
        $this->notifyUser($message, $task, config('setting.notification.delete_task'), $subject);
        
        // For Mail Notification       
        $mailMessage['msg_initial'] = "The task ";
        $mailMessage['msg_part1'] = config('setting.notification.under_rip');
        $mailMessage['msg_start'] = $mailMessage['msg_initial']."'".$task->name."'".$mailMessage['msg_part1'];
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.rip_deleted');
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $mailMessage['task_name'] = $task->name;
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $task->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });   
        
        broadcast(new TaskDeleted($response))->toOthers();
        return response()->json($response);
    }

    /** function update the task name on rip
     * @param Request $request
     * @return json response
     */

    public function taskName(Request $request)
    {
        
        $task = Task::find($request->task_id);
        $task_name = $task->name;
        $goal = Goal::with('task')->find($task->goal_id);
        $task->update([
            'name' => strip_tags($request->name)
        ]);
        $subject = "RIPKIT:".config('setting.notification.update_task_name');
        $message = "The task name of ".$task->name.", for the RIP '".$goal->goal."' of the company '".session('company_name')."', has been updated to ".strip_tags($request->name).".";
        $this->notifyUser($message, $task, config('setting.notification.update_task_name'), $subject);
        
        // For Mail Notification       
        $mailMessage['msg_initial'] = "The task ";
        $mailMessage['msg_part1'] = config('setting.notification.under_rip');
        $mailMessage['msg_start'] = $mailMessage['msg_initial']."'".$task_name."'".$mailMessage['msg_part1'];
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.updated_to')." '".$task->name."'.";
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $mailMessage['task_name'] = $task->name;
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $task->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });   
        
        
        broadcast(new AddTaskName($task->id))->toOthers();
        $response['name'] = $request->name;
        $response['id'] = $request->task_id;
        return response()->json($response);
    }


    /** function to broacast the updated task name on other connections
     * @param Request $request
     * @return json response
     */

    public function showTaskName(Request $request)
    {
        $task = Task::find($request->data);
        if(!$task){
            $return['dataNull'] = true;
            return response()->json($return);
        }
        $return['name'] = $task->name;
        $return['dataNull'] = false;
        $return['taskId'] = $task->id;

        return response()->json($return);
    }

    /** function show all the task in a goal
     * @param Request $request
     * @return json response
     */

    Public function allTasks(Request $request)
    {
        $tasks = Task::where(['goal_id' => $request->id])->get();
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['html'] = view('auth.rip.ajax.allTask')->with([
            'tasks' => $tasks,
            'goalusers' => $goalUsers
        ])->render();
        $response['goalId'] = $request->id;
        return response()->json($response);
    }

    /**
     * Function to get User List for Task
     * @param Request $request
     * @return type html
     */
    public function taskUserList(Request $request) {
        $task_id = $request->taskId;
        $company_id = !$request->has('companyId') ? session('company_id') : $request->companyId;
        $companyUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->sortBy('user.name')->unique('parent_id');
        
        $taskUser = Task::where('id', $task_id)->first();        
        $assignedUserArray = array();
 
        if($taskUser){
            if($taskUser->assign_to != ''){
                $assignedUserArray = explode(',',$taskUser->assign_to);
            }
        }        
        $assignedUsers = User::select('id')->whereIn('id',$assignedUserArray)->get();
        $new_assign_array = array();
        if(count($assignedUsers) > 0){
            foreach($assignedUsers as $key=>$val){
                $new_assign_array[] = $val->id;
            }
        }
        
        return view('auth.rip.ajax.addUserToTask')->with(['companyUsers' => $companyUsers, 'task_id' => $task_id, 'selectedUsers' => $new_assign_array]);
    }

     /**
     * Function to get User List for Rips
     * @param Request $request
     * @return type html
     */
    public function showAssignedTaskUsers(Request $request) {
        $task_id = $request->task_id;
        $company_id = session('company_id');
        $taskUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $taskUser = Task::where('id', $task_id)->first();
        $assignedUserArray = explode(',',$taskUser->assign_to);
        $assignedUsers = User::whereIn('id',$assignedUserArray)->orderBy('name')->get();         
        return view('auth.rip.ajax.showAddedUserintask')->with(['task_id' => $task_id, 'selectedUsers' => $assignedUsers]);
    }

    /**
     * Function to call Edit Task form
     * @param Request $request
     * @return int
     */
 
     public function editTask(Request $request) {
        $task_id = $request->taskId;
        $task = Task::find($task_id);
        return view('auth.rip.pusher.editTask')->with(['tasks' => $task]);  
    }
}

?>