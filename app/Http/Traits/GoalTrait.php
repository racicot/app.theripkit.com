<?php
namespace App\Http\Traits;

use App\Model\Category;
use App\Model\Goal;
use App\Model\Rip;
use App\Model\CompanyParent;
use App\Notifications\NewEvent;
use App\User;
use App\Http\Requests\Rip\AddCategoryValidator;
use Illuminate\Http\Request;
use App\Events\GoalCreated;
use App\Events\GoalDeleted;
use App\Events\AssignUser;
use App\Events\AssignUserDelete;
use App\Events\DueDate;
use App\Events\PercentageUpdate;
use App\Events\AddName;
use App\Model\Task;
use Illuminate\Support\Facades\View;
use Carbon\Carbon;
use App\Jobs\ProcessEvent;
use Illuminate\Notifications\Notifiable;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;


trait GoalTrait
{
    use Notifiable;


    /**
     * [addRipGoal function ass the  goal from the RIP section]
     * @param Request $request [description]
     */
    public function addRipGoal(AddCategoryValidator $request)
    {
        \DB::beginTransaction();
        $user_ids = array();
        $company_id = session('company_id');
        $rip = Rip::find($request->goal_type_id);
        if(!$rip){
            $response['dataNull'] = true;
            return json_encode($response);
        }
        $goal = Goal::create([
            'company_id' => $company_id,
            'category_id' => $request->category,
            'goal_type_id' => $request->goal_type_id,
            'goal' => strip_tags($request->goal),
            'status' => 1,
            'assign_to' => implode(',', $user_ids),
            'due_date' => $rip->end_date,
        ]);
        \DB::commit();
        $count = Goal::where(['category_id' => $request->category, 'goal_type_id' => $request->goal_type_id])->count();
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $user_ids[]= explode(',', $goal->assign_to);
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');        
        $response['html'] = view('auth.rip.ajax.addGoal')->with(['goal' => $goal, 'goalusers' => $goalUsers, 'users' => $user_ids,'currentRip'=>$rip])->render();
        $response['catId'] = $request->category;

        $response['goalId'] = $goal->id;
        $response['count'] = $count;
        
        $response['categoryAvgPer'] = $categoryAvgPer;
        $response['ripAvgPer'] = round(Goal::where(['goal_type_id' => $goal->goal_type_id])->avg('percentage_complete'));
        $response['ripId'] = $goal->goal_type_id;
        $response['dueDate'] = Carbon::parse($rip->end_date)->format('M d, Y');
        $response['dataNull'] = false;
        broadcast(new GoalCreated($goal->id))->toOthers();
        return json_encode($response);
    }

    /**
     * [goalUser function will return the goal user with html
     * @param  Request $request [description]
     * @return [type]  html    Return the html of the dropdown options
     */
    public function goalUser(Request $request)
    {
        $goal_id = $request->goalId;
        $goalUser = Goal::where('id', $goal_id)->first();
        $userArray = User::whereIn('id', explode(',', $goalUser->assign_to))->get();
        
        $html = View::make('auth.rip.ajax.shorttermgoaldropdown',
            ['users' => $userArray, 'goal_id' => $goal_id])->render();
        return $html;
    }

    /** function assign user to added goal
     * @param Request $request
     * @return html of the assign user
     */

    public function assignUserToAddedGoal(Request $request)
    {
        $goalId = $request->goal_id;
        $goal = Goal::with('task')->find($goalId);
        if(!$goal)
        return response()->json($response['dataNull'] = true);

        $taskNewUser = '';
        $count = count(array_filter(explode(',', $goal->assign_to)));
        $old_assigned_users = array_filter(explode(',', $goal->assign_to));
        
        $userStr ='';      
        $user_ids = array();
        if(!isset($request->user)){
            $goal->update([
                'assign_to' => '',
            ]);
        }
        else if(count($request->user) > 0){
            foreach ($request->user as $user) {
                if (filter_var($user, FILTER_VALIDATE_EMAIL)) {
                    $user_ids[] = $this->createUser($user);
                } else {
                    $user_ids[] = $user;
                }
            }
            $goal->update([
                'assign_to' => implode(',', $user_ids),
            ]);
        }
        else{
            $goal->update([
                'assign_to' => '',
            ]);
        }    
       
        $removed_added = array_diff($old_assigned_users, $user_ids);
        if(!in_array($removed_added, $user_ids)) {
            // Mail Notification
            $mailMessage['msg_start'] = config('setting.notification.removed_from_rip');
            $mailMessage['msg_part'] = config('setting.notification.related_company');
            $mailMessage['msg_end'] = '.';
            $mailMessage['rip_name'] = $goal->goal;
            $mailMessage['company_name'] = session('company_name');
            
            $action = config('magicNumbers.MEETINGMAILACTION');
            $subject = 'RIPKIT: '.config('setting.notification.removed_user_rip');
            $objUser = new User();
            $user_removed = $objUser->getNotifyUserOthenThenLogedIn($removed_added);
            User::whereIn('id', $user_removed)->each(function($user) use($mailMessage, $action, $subject){
                $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
            });
            
            // In-app Notification
            $message = config('setting.notification.removed_from_rip')."'".$goal->goal.config('setting.notification.related_company')."'".session('company_name')."'.";
            $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'), $subject);          
            
        } 
        // For Email Notification
        $mailMessage['msg_start'] = config('setting.notification.assigned_to_rip');
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = '.';
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $subject = 'RIPKIT: '.config('setting.notification.assign_rip_to_user');
        $action = \Config::get('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });
        
        // For In-App Notification
        $message = config('setting.notification.assigned_to_rip')."'".$goal->goal."'".config('setting.notification.related_company')."'".session('company_name')."'";
        $this->notifyUser($message, $goal, config('setting.notification.assign_rip_to_user'), $subject);            
        
        $userArray = User::whereIn('id', $user_ids)->get();
        $taskUserHtml = '';
        $response['taskUserId'] = 0;
        $eventResponse['taskUserId'] = 0;
        $arrayCount = count($userArray);
        $html = View::make('auth.rip.ajax.goalimage', ['users' => $userArray])->render();
        $tabHtml = View::make('auth.rip.ajax.goaltabimage', ['users' => $userArray, 'goal_id' => $goalId, 'count' => $arrayCount])->render();
        /*if (!empty($taskNewUser)) {
            $taskUserHtml .= View::make('auth.rip.ajax.goaltaskimage',
                ['userArray' => $userArray, 'taskNewUser' => $taskNewUser, 'count'=> $arrayCount])->render();
            $response['taskUserId'] = $userArray->find($taskNewUser)->id;
            $eventResponse['taskUserId'] = $userArray->find($taskNewUser)->id;
        }*/

        $response['tabHtml'] = $tabHtml;
        $response['html'] = $html;
        //$response['taskNewUser'] = $taskUserHtml;
        $response['count'] = $count;
        $eventResponse['goalId'] = $goal->id;
        $eventResponse['dataNull'] = false;
        $response['dataNull'] = false;
        broadcast(new AssignUser($eventResponse))->toOthers();
        return response()->json($response);
    }

    /** function to broadcast the assign user to added goal content on other connections
     * @param Request $request
     * @return json response
     */

    Public function assignUser(Request $request)
    {
        $goalId = $request->data['goalId'];
        $goal = Goal::with('task')->find($goalId);
        if(!$goal)
        return response()->json($response['dataNull'] = true);

        $userArray = User::whereIn('id', explode(',', $goal->assign_to))->get();
        $taskNewUser = $request->data['taskUserId'];
        $taskUserHtml = '';
        $dropdownHtml = View::make('auth.rip.ajax.shorttermgoaldropdown',
            ['users' => $userArray, 'goal_id' => $goalId])->render();
        $html = View::make('auth.rip.ajax.goalimage', ['users' => $userArray])->render();
        $tabHtml = View::make('auth.rip.ajax.goaltabimage', ['count' => count($userArray), 'users' => $userArray, 'goal_id' => $goalId])->render();
        if ($taskNewUser != 0) {
            $taskUserHtml .= View::make('auth.rip.ajax.goaltaskimage',
                ['userArray' => $userArray, 'taskNewUser' => $taskNewUser])->render();
            $response['taskUserId'] = $userArray->find($taskNewUser)->id;
        }
        $arrayCount = count($userArray);
        $response['dropdownHtml'] = $dropdownHtml;
        $response['tabHtml'] = $tabHtml;
        $response['html'] = $html;
        $response['taskNewUser'] = $taskUserHtml;
        $response['goalId'] = $goalId;
        $response['dataNull'] = false;
        $response['count'] = isset($arrayCount) ? $arrayCount : 0;

        return response()->json($response);
    }

    /** function delete the assign user to added goal
     * @param Request $request
     * @return html of the asssign user
     */
    public function goalUserDelete(Request $request)
    {
        $id = $request->id;
        $goalId = $request->goalId;
        $goal = Goal::find($goalId);
        $assignUser = explode(',', $goal->assign_to);
        $does_key = array_search($id, $assignUser);
        if ($does_key) {
            unset($assignUser[$does_key]);
        }
        $goal->update([
            'assign_to' => implode(',', $assignUser),
        ]);

        foreach ($goal->task as $task) {
            $taskAssignUser = explode(',', $task->assign_to);
            $does_key = array_search($id, $taskAssignUser);
            if ($does_key) {
                unset($taskAssignUser[$does_key]);
            }
            $task->update([
                'assign_to' => implode(',', $taskAssignUser),
            ]);
        }
        $userArray = User::whereIn('id', $assignUser)->get();
        $html = '';
        $dropdownHtml = '';
        $dropdownHtml = View::make('auth.rip.ajax.shorttermgoaldropdown',
            ['users' => $userArray, 'goal_id' => $goalId])->render();
        $tabHtml = View::make('auth.rip.ajax.goaltabimage', ['users' => $userArray, 'goal_id' => $goalId])->render();
        $html = View::make('auth.rip.ajax.goalimage', ['users' => $userArray])->render();
        $response['dropdownHtml'] = $dropdownHtml;
        $response['tabHtml'] = $tabHtml;
        $response['html'] = $html;
        $response['userId'] = $id;
        $eventResponse['userId'] = $id;
        $eventResponse['goalId'] = $goal->id;
        broadcast(new AssignUserDelete($eventResponse))->toOthers();
        return response()->json($response);
    }

    /** function to broacast removed user to added goal content on other connections
     * @param Request $request
     * @return json response
     */
    public function removeUser(Request $request)
    {
        $goalId = $request->data['goalId'];
        $goal = Goal::with('task')->find($goalId);
        $userArray = User::whereIn('id', explode(',', $goal->assign_to))->get();
        $taskNewUser = $request->data['userId'];
        $dropdownHtml = View::make('auth.rip.ajax.shorttermgoaldropdown',
            ['users' => $userArray, 'goal_id' => $goalId])->render();
        $tabHtml = View::make('auth.rip.ajax.goaltabimage', ['users' => $userArray, 'goal_id' => $goalId])->render();
        $html = View::make('auth.rip.ajax.goalimage', ['users' => $userArray])->render();
        $response['dropdownHtml'] = $dropdownHtml;
        $response['tabHtml'] = $tabHtml;
        $response['html'] = $html;
        $response['userId'] = $taskNewUser;
        $response['goalId'] = $goalId;
        return response()->json($response);
    }

    /** function to broacast the deleted goal on other connections
     * @param Request $request
     * @return json response
     */
    public function deleteGoal(Request $request)
    {
        $id = $request->id;
        \DB::beginTransaction();
        $goal = Goal::with(['category', 'comment', 'comment.attachment'])->find($id);
        Task::where('goal_id', $goal->id)->delete();
        foreach ($goal->comment as $comment) {
            foreach ($comment->attachment as $attachment) {
                $attachment->delete();
            }
            $comment->delete();
        }
        $goal->delete();
        \DB::commit();
        $count = Goal::where(['category_id' => $goal->category_id, 'goal_type_id' => $goal->goal_type_id])->count();
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $response['count'] = $count;
        $response['catId'] = $goal->category_id;
        $response['categoryAvgPer'] = $categoryAvgPer;
        $response['ripAvgPer'] = round(Goal::where(['goal_type_id' => $goal->goal_type_id])->avg('percentage_complete'));
        $response['ripId'] = $goal->goal_type_id;
        $response['goalId'] = $goal->id;
        $response['company_id'] = $goal->company_id;
        
        $mailMessage['msg_start'] = config('setting.notification.rip');
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.rip_deleted');
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
        $subject = 'RIPKIT: '.config('setting.notification.delete_a_rip');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });
        
        // For In-App Notification
        $message = "The RIP '".$goal->goal."' of the company '".session('company_name')."' has been deleted.";
        $this->notifyUser($message, $goal, config('setting.notification.delete_a_rip'), $subject);   
        
        broadcast(new GoalDeleted($response))->toOthers();
        return json_encode($response);
    }

    /*
     * function update the percentage of goal
     */
    public function goalPercentageUpdate(Request $request)
    {
        Goal::find($request->goalId)->update(['percentage_complete' => $request->percentage]);
        $goal = Goal::with('task')->find(round($request->goalId));
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $response['percentage'] = round($request->percentage);
        $response['goalId'] = $request->goalId;
        $response['category_id'] = $goal->category_id;
        $response['categoryAvgPer'] = $categoryAvgPer;
        $response['ripAvgPer'] = round(Goal::where(['goal_type_id' => $goal->goal_type_id])->avg('percentage_complete'));
        $response['ripId'] = $goal->goal_type_id;
        
        // For Mail Notification
        $mailMessage['msg_start'] = config('setting.notification.rip_progress');
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.updated_to')."'".round($request->percentage)."%'.";
        $mailMessage['rip_name'] = $goal->goal;
        $mailMessage['company_name'] = session('company_name');
       
        $subject = 'RIPKIT: '.config('setting.notification.rip_percentage_update');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });
        // For In-App notification
        $message = "The progress for the RIP '".$goal->goal."' of the company '".session('company_name')."' has been updated to ".round($request->percentage)."%.";
        $this->notifyUser($message, $goal, config('setting.notification.rip_percentage_update'), $subject);
        $notify = 0;
        if(auth()->user()->unreadNotifications->count()){
            $notify = 1;
        }
        $response['notify'] = $notify;
        broadcast(new PercentageUpdate($goal->id))->toOthers();
        return response()->json($response);
    }

    /** function to broadcast the updated percentage on other connections
     * @param Request $request
     * @return json response
     */
    public function showPercentage(Request $request)
    {
        $company_id = session('company_id');
        $goal_id = $request->data;
        $goal = Goal::find($goal_id);
        if(!$goal)
        return response()->json($response['dataNull'] = true);

        $count = Goal::where(['category_id' => $goal->category_id, 'goal_type_id' => $goal->goal_type_id])->count();
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $response['catId'] = $goal->category_id;
        $response['percentage'] = $goal->percentage_complete;
        $response['goalId'] = $goal->id;
        $response['count'] = $count;
        $response['dataNull'] = false;
        $response['categoryAvgPer'] = $categoryAvgPer;
        $response['ripAvgPer'] = round(Goal::where(['goal_type_id' => $goal->goal_type_id])->avg('percentage_complete'));
        $response['ripId'] = $goal->goal_type_id;
        return response()->json($response);
    }

    /** function to update the due date of goal
     * @param Request $request
     * @return json response
     */
    public function goalDueDate(Request $request)
    {   
        $goal = Goal::find($request->goal_id);
        if(!$goal){
            return $notify = 0;
        }
        $oldDate = date('M d, Y',strtotime($goal->due_date));
        $goal->update([
            'due_date' => date('Y-m-d',strtotime($request->due_date))
        ]);
        
        $message['msg_start'] = config('setting.notification.rip_due_date');
        $message['msg_part'] = config('setting.notification.related_company');
        $message['msg_end'] = config('setting.notification.updated_from')."'".$oldDate."' to '".$request->due_date."'.";
        $message['rip_name'] = $goal->goal;
        $message['company_name'] = session('company_name');
        $subject = 'RIPKIT: '.config('setting.notification.rip_due_date_update');
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($message, $action, $subject){
            $user->notify(new NewEvent($message, $action, $user->name, $subject));
        });
        
        // For In-App notification
        $message = "The due date of RIP ".$goal->goal." of the company ".session('company_name')." has been updated from ".$oldDate." to ".$request->due_date;
        $this->notifyUser($message, $goal, config('setting.notification.rip_due_date_update'), $subject);
        event(new DueDate($goal->id));
        $notify = 0;
        if(auth()->user()->unreadNotifications->count()){
            $notify = 1;
        }
        return $notify;
    }

    /** function to broacast the updated due date of goal on other connections
     * @param Request $request
     * @return json response
     */

    public function showDueDate(Request $request)
    {
        $goal = Goal::find($request->data);
        if(!$goal){
            $return['dataNull'] = true;
            return response()->json($return);
        }
        $return['duedate'] = Carbon::parse($goal->due_date)->format('M d, Y');
        $return['goalId'] = $goal->id;
        $rip = Rip::select('start_date','end_date')->where('id',$goal->goal_type_id)->get();
        $return['display_msg']  = '';
        if($goal->due_date < $rip[0]->start_date || $goal->due_date > $rip[0]->end_date){

            $return['display_msg']  = 'Due date is past/ahead of the rip date.';

        }
        return response()->json($return);
    }

    /** function to goal name of rip
     * @param Request $request
     * @return json response
     */

    public function goalName(Request $request)
    {
        $goal = Goal::find($request->goal_id);
        if(!$goal){
            $response['dataNull'] = true;
            $response['name'] = $request->name;
            $response['id'] = $request->goal_id;
            $response['notify'] = 0;
            return response()->json($response);
        }
        $oldName = $goal->goal;
        $goal->update([
            'goal' => strip_tags($request->name)
        ]);
        
        //For Mail Notification
        $mailMessage['msg_start'] = config('setting.notification.rip_name');
        $mailMessage['msg_part'] = config('setting.notification.related_company');
        $mailMessage['msg_end'] = config('setting.notification.updated_from')."'".$oldName."' to '".strip_tags($request->name)."'.";
        $mailMessage['rip_name'] = $oldName;
        $mailMessage['company_name'] = session('company_name');

        $subject = 'RIPKIT: '.config('setting.notification.rip_name_update'); 
        $action = config('magicNumbers.MEETINGMAILACTION');
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($mailMessage, $action, $subject){
            $user->notify(new NewEvent($mailMessage, $action, $user->name, $subject));
        });
        
        // For In-App Notifications
        $message = "The RIP name for ".$goal->goal." of the company ".session('company_name')." has been updated from ".$oldName." to ".strip_tags($request->name);
        $this->notifyUser($message, $goal, config('setting.notification.rip_name_update'), $subject);
        broadcast(new AddName($goal->id))->toOthers();
        $notify = 0;
        if(auth()->user()->unreadNotifications->count()){
            $notify = 1;
        }
        $response['name'] = $request->name;
        $response['id'] = $request->goal_id;
        $response['notify'] = $notify;
        $response['dataNull'] = false;
        return response()->json($response);
    }

    /** function to broacast the updated goal name  on other connections
     * @param Request $request
     * @return json response
     */

    public function showName(Request $request)
    {
        $goal = Goal::find($request->data);
        if(!$goal){
            $return['dataNull'] = true;
            return response()->json($return);
        }
        $return['name'] = $goal->goal;
        $return['dataNull'] = false;
        $return['goalId'] = $goal->id;
        return response()->json($return);
    }

    /** function to broacast newly added goal content on other connections
     * @param Request $request
     * @return json response
     */

    public function showGoal(Request $request)
    {
        $company_id = session('company_id');
        $goal_id = $request->data;
        $goal = Goal::find($goal_id);
        if(!$goal){
            $response['dataNull'] = true;
            return response()->json($response);
        }
        $count = Goal::where(['category_id' => $goal->category_id, 'goal_type_id' => $goal->goal_type_id])->count();
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['html'] = view('auth.rip.ajax.addGoal')->with([
            'goal' => $goal,
            'goalusers' => $goalUsers,
            'currentRip' => Rip::find($goal->goal_type_id),
        ])->render();
        $response['catId'] = $goal->category_id;
        $response['goalId'] = $goal->id;
        $response['dataNull'] = false;
        $response['count'] = $count;        
        $response['categoryAvgPer'] = $categoryAvgPer;
        return response()->json($response);
    }

    /** function to broacast the updated goal content on other connections
     * @param Request $request
     * @return json response
     */

    public function updateGoal(Request $request)
    {
        $company_id = session('company_id');
        $goal_id = $request->data;
        $goal = Goal::find($goal_id);
        $count = Goal::where(['category_id' => $goal->category_id, 'goal_type_id' => $goal->goal_type_id])->count();
        $categoryAvgPer = round(Goal::where([
            'category_id' => $goal->category_id,
            'goal_type_id' => $goal->goal_type_id
        ])->avg('percentage_complete'));
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->unique('parent_id');
        $response['html'] = view('auth.rip.ajax.addGoal')->with([
            'goal' => $goal,
            'goalusers' => $goalUsers,
            'currentRip' => Rip::find($goal->goal_type_id),
        ])->render();
        $response['catId'] = $goal->category_id;
        $response['goalId'] = $goal->id;
        $response['count'] = $count;
        $response['categoryAvgPer'] = $categoryAvgPer;
        return response()->json($response);
    }

    private function notifyUser($message, $goal, $action, $subject)
    {
        $userobj = new User();
        $assing_to = $userobj->getNotifyUserOthenThenLogedIn(explode(',', $goal->assign_to));
        User::whereIn('id', $assing_to)->each(function($user) use($message, $action, $subject){
             $user->notify(new NewEvent($message, $user->getNotificationAction($action), $user->name, $subject));
         });
    }

    /*
        Show goal percentage.. 
    */
    public function editGoal(Request $request)
    {   
        $goal = Goal::find($request->goalId);
        return view('auth.rip.ajax.editProgress')->with([ 'goal' => $goal]);
    }    

    /**
     * Function to get User List for Rips
     * @param Request $request
     * @return type html
     */
    public function goalUserList(Request $request) {
        $goal_id = $request->goalId;

        if($request->company_id != ''){$company_id = $request->company_id ;} else{$company_id = session('company_id');}

        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->sortBy('user.name')->unique('parent_id');
        $goalUser = Goal::where('id', $goal_id)->first();
        $assignedUserArray = explode(',',$goalUser->assign_to);
        $assignedUsers = User::select('id')->whereIn('id',$assignedUserArray)->get();
       
        $new_assign_array = array();
        if(count($assignedUsers) > 0){
            foreach($assignedUsers as $key=>$val){
                $new_assign_array[] = $val->id;
            }
        }
        $count = count($new_assign_array);        
        return view('auth.rip.ajax.addUserToRip')->with(['goalUsers' => $goalUsers, 'goal_id' => $goal_id, 'selectedUsers' => $new_assign_array, 'count' => $count]);
    }    
    
    /**
     * Function to show User List for Rips
     * @param Request $request
     * @return type html
     */
    public function showAssignedUsers(Request $request) {
        $goal_id = $request->goalId;
        $company_id = session('company_id');
        $goalUsers = CompanyParent::with('user')->where('company_id', $company_id)->get()->sortBy('user.name')->unique('parent_id');
        $goalUser = Goal::where('id', $goal_id)->first();
        $assignedUserArray = explode(',',$goalUser->assign_to);
        $assignedUsers = User::find($assignedUserArray);
        
        return view('auth.rip.ajax.showAddedUserinRip')->with(['goal_id' => $goal_id, 'selectedUsers' => $assignedUsers]);
    }


    

    /**
     * Function to get Rip overiew
     * @param Request $request
     * @return int
     */ 
    public function ripOverview(Request $request) {
        $route_name = Route::current()->getName();
        $users = Task::getCompanyUsers();

        $current_user = User::getUserExceptAdmin($request->id, $users);

        $company_ids =  Task::getUserAccociatedCompany($current_user);
        $todo_rips = Goal::with(['rip'])-> 
        whereIn('company_id',  $company_ids->company)
        ->whereHas('rip', function ($query) {
            $query->where('expired', '0');
        })->get()->filter(function ($value) use ($current_user) {
            return (in_array($current_user, explode(',', $value->assign_to)));
        });
        $todo_rips = $todo_rips->groupBy('company_info.name');

        $html = view('auth.overview.ripOverview')->with(['current_id' => $current_user, 'todo_rips' => $todo_rips, 'users' => $users])->render();
        return $html;
    }

}

?>