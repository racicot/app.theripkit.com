<?php

namespace App;

use App\Model\NotificationType;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use App\Model\Company;
use App\Model\CompanyParent;
use App\Model\Notification;
use App\Model\Goal;
use App\Model\Task;
use App\Model\SocialAccount;
use DB;
use App\Notifications\ResetPasswordNotification;

class User extends Authenticatable
{
    use Notifiable, HasRoles;

    protected $appends = ['user_role'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'company_id',
        'email_token',
        'status'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The company function returns the companies associated with user.
     *
     * @return model
     */
    public function company()
    {
        return $this->hasOne(Company::class);

    }

    public function getNameAttribute($value)
    {
        return ucfirst($value);
    }

    public function getCreatedAtAttribute($val)
    {    
        return date("d-m-Y",strtotime($val));

    }

    public function getUserRoleAttribute()
    {
        $role = ModelHasRole::where('model_id', $this->id)->first();
        if($role){
        return $role->role_id;
        }
        return 0;
    }

    /**
     * The userImage function returns the path of user's profile image.
     *
     * @return string
     */
    public function userImage()
    {
        $storageCheck = \Storage::disk('system_photos')->exists($this->image);
        if ($this->image == 'default.jpg' || !$storageCheck) {
            return asset('images/default.jpg');
        }
        return \Storage::url('system_photos' . '/' . $this->image);
    }

    /**
     * The getCareAbout function returns the exploded array of care_about field in users table.
     *
     * @return array
     */
    public function getCareAbout()
    {
        return explode(',', $this->care_about);
    }

    public function parent()
    {
        return $this->hasMany(CompanyParent::class, 'parent_id');
    }

    public function hasTask($id)
    {
        $task = Task::find($id);
        if ($this->hasAnyRole(['Super Admin'])) {
            return true;
        } elseif (($this->can('Add As Company Parent') || $this->can('Add As Company Owner'))) {
            return true;
        } elseif (in_array($this->id, explode(',', $task->assign_to))) {
            return true;
        } else {
            return false;
        }
    }

    public function delete()
    {
        $companyArray = CompanyParent::where('parent_id', $this->id)->get()->pluck('company_id')->toArray();
        $companies = Company::whereIn('id', $companyArray)->get();
        $companies->each(function ($company) {
            Goal::where('company_id', $company->id)->get()->each(function ($goal) {
                $goal->unassignUser($this->id);
            });
        });
        Company::where('user_id', $this->id)->get()->each(function ($company) {
            Goal::where('company_id', $company->id)->get()->each(function ($goal) {
                $goal->unassignUser($this->id);
            });
            $company->user_id = null;
            $company->save();
        });
        $this->syncPermissions([]);
        $this->syncRoles([]);
        SocialAccount::where('user_id', $this->id)->delete();
        \Storage::disk('system_photos')->delete($this->image);
        return parent::delete();
    }

    /**
     * Returns the companies assigned to a user
     * @return \Illuminate\Database\Eloquent\Collection|\Illuminate\Support\Collection|static[]
     */
    public function getCompanies()
    {
        if ($this->hasAnyRole(['Super Admin'])) {
            return Company::all();
        } else {
            $companies = Company::whereHas('parent', function ($query) {
                $query->where('parent_id', $this->id);
            })->get();
            return $companies;
        }
    }

    /**
     * Checks if a user is parent to a company or not
     * @param $company_id
     * @return bool
     */
    public function isCompanyAssigned($company_id)
    {
        if ($this->hasAnyRole(['Super Admin'])) { 
            return true;
        } else {
            $company = CompanyParent::where(['parent_id' => $this->id, 'company_id' => $company_id])->get();
            
            if ($company->isEmpty()) {
                return false;
            } else {
                return true;
            }
        }
    }

    /** Function return the type of notification used in this event('Email', 'System', 'Both')
     * @param $name
     * @return mixed
     */
    public function getNotificationAction($name)
    {
        $notificationType = NotificationType::where('name', $name)->first();
        return $notificationType->action;
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordNotification($token));
    }

    /**
     * get all user to send notification other then login user.
     *
     * @param  array  $user
     * @return $user
     */
    public function getNotifyUserOthenThenLogedIn($user)
    {
        if (($key = array_search(\Auth::user()->id, $user)) !== false) {
            unset($user[$key]);
        }
        return $user;
    }

    /**
     * get user id except admin.
     *
     * @param  array  $user_id,$user
     * @return $current_user
     */

    public static function getUserExceptAdmin($user_id, $users)
    {
        if($user_id> 0){
            $current_user = $user_id;
        }
        else{ 
            $userRole = User::find(auth()->user()->id)->user_role;
            if($userRole == config('setting.roles.superadmin')){
                $current_user = isset($users->first()->id) ? $users->first()->id : auth()->user()->id;
            }
            else{
                $current_user = auth()->user()->id;
            }            
        }
        return $current_user;
    }

    /**
     * scope to join roles table to users
     *
     * @param  object  $$query
     * @return $query
     */
    public  function scopeRoleJoin($query)
    {
        return $query->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
        ->leftJoin('roles', 'model_has_roles.role_id', '=', 'roles.id');
    }


    /**
     * scope to join roles and company table to users
     *
     * @param  object  $$query
     * @return $query
     */
    public  function scopeCompanyJoin($query)
    {
        return $query->select('users.*', 'roles.id as roles_id','roles.name as roles_name',(DB::raw(('(select c.name from
         companies as c join company_parents cp on c.id=cp.company_id where cp.parent_id=  `users`.`id` limit 0,1) as company_name'))))
         ->distinct('users.id');

    }

    /**
     * scope to join company_parents table to users
     *
     * @param  object  $$query
     * @return $query
     */
    public function scopeCompanyParent($query)
    {
        return $query->leftJoin('company_parents', 'users.id', '=', 'company_parents.parent_id')
        ->leftJoin('companies', 'company_parents.company_id', '=', 'companies.id')
        ->where('company_parents.company_id', session('company_id'));
    }
    
    /**
     * Function to check Active company
     * @param type $company_id
     */
    public function isCompanyActive($company_id) {
        if ($this->hasAnyRole(['Super Admin'])) { 
            return true;
        } else {
            $company_status = Company::where('id', $company_id)->pluck('status')->first();
            
            if ($company_status == '' || $company_status == 0) {
                return false;
            } else {
                return true;
            }
        }
    }
}
