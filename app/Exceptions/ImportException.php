<?php

namespace App\Exceptions;

class ImportException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}