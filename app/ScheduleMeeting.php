<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Zoom\Zoom;
use App\Notifications\NewEvent;
use App\Model\CompanyParent;
use App\ModelHasRole;
use Carbon\Carbon;
use App\Model\Company;
use App\Model\Rip;
use App\Mail\ZoomEmail;
use Illuminate\Support\Facades\Auth;
use Spatie\CalendarLinks\Link;

class ScheduleMeeting extends Model
{
    protected $appends = ['show_users', 'remaining_user_count', 'meeting_date_time', 'modified_meeting_date', 'modified_meeting_time'];

    public function getShowUsersAttribute()
    {
        $participants = explode(',', $this->participants);
        return \App\User::whereIn('id', $participants)->limit(\Config::get('magicNumbers.MEETINGLISTUSERLIMIT'))->get();
    }

    public function getRemainingUserCountAttribute()
    {
        $participants = explode(',', $this->participants);
        $limit = \Config::get('magicNumbers.MEETINGLISTUSERLIMIT');
        $remainingUser = count($participants) - $limit;
        return $remainingUser > 0 ? $remainingUser : 0;
    }

    public function getMeetingDateTimeAttribute()
    {
         $date = $this->meeting_date.' '.$this->meeting_time;
         $meetingTime = new Carbon($date);
         $meetingTime->addMinutes($this->duration);
         $currentDate = Carbon::now();
         return $meetingTime->lessThan($currentDate);
    }

    public function getModifiedMeetingDateAttribute()
    {
        return  date('M d, Y', strtotime($this->meeting_date));
    }


    public function getModifiedMeetingTimeAttribute()
    {
        return  date('G:i', strtotime($this->meeting_time));
    }

    /** Get Rips meeting list
     * @param int $ripId
     * @return json
     */
    public static function getMeetings($ripId)
    {
        try{
             $currentDate = Carbon::now();
             $currentDate->subMonths(\Config::get('magicNumbers.SUBONEMONTH'));
             $oneMonth = $currentDate->format('Y-m-d');
             $meetings = self::where('rips_id', $ripId)->whereDate('meeting_date', '>=', $oneMonth)->orderBy('id', 'desc')->get();
             $response['html'] = view('auth.scheduleMeeting.meetingList')->with([
                'meetings' => $meetings,
                'count' => $meetings->count(),
            ])->render();
            return response()->json($response, 200);
        }catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }
    /** Create Meeting
     * @return json
     */
    public static function createMeeting()
    {
        try{
            $input = request();
            $meeting = Zoom::createZoomMeeting($input); //Zoom Api for registering meeting
            $insert = new self;
            $insert->agenda = $meeting->topic;
            $insert->meeting_date = $input->meeting_date;
            $insert->meeting_time = $input->meeting_time;
            $insert->rips_id = $input->rip_id;
            $insert->companies_id = session('company_id');
            $insert->duration = $input->duration;
            $insert->participants = implode(',', $input->participants);
            $insert->meeting_uuid = $meeting->uuid;
            $insert->meeting_id = $meeting->id;
            $insert->start_url = $meeting->start_url;
            $insert->join_url = $meeting->join_url;
            $insert->host_id = $meeting->host_id;
            $insert->created_by = Auth::user()->id;
            $insert->save();
            self::sendMeetingMail($insert->id);
            return response()->json($insert, 200);
        }catch (\Exception $e) {
            return response()->json($e->getMessage().$e->getFile(), 500);
        }

    }

    /** Get all company users
     * @return json
     */
    public static function getCompanyUserList()
    {
        try{
            $ids = [];
            $userType = '';
            $excludeUser = '';
            $checkedUsers = [];
            $userIds = CompanyParent::where('company_id', session('company_id'))->get();
            $companyOwner = Company::find(session('company_id'));
            $superAdminId = self::getSuperAdminIds();
            if($companyOwner->user_id){
                $userType = '(Company Owner)';
                $checkedUsers = \App\User::where('id', $companyOwner->user_id)->get();
                $excludeUser = $companyOwner->user_id;
            }else {
               $userType = '(Super Admin)';
               $checkedUsers =  \App\User::find($superAdminId);
            }

            foreach ($userIds as $key => $value) {
               if($value->parent_id != Auth::user()->id && !in_array($value->parent_id, $superAdminId) && $value->parent_id != $excludeUser)
               array_push($ids, $value->parent_id);
            }
            $users = \App\User::find($ids)->sortBy('name');
            $response['html'] = view('auth.scheduleMeeting.addUser')->with([
               'users' => $users,
               'checkedUsers' => $checkedUsers,
               'userType' => $userType,
               'count' => $users->count(),
            ])->render();
            return response()->json($response, 200);
        }catch (\Exception $e) {
            return response()->json($e->getMessage().$e->getFile().$e->getLine(), 500);
        }
    }

    /** Get all super admin ids
     * @return array
     */
    public static function getSuperAdminIds()
    {
        try{
            $userIds = [];
            $modelHasRole = ModelHasRole::where('role_id', \Config::get('magicNumbers.ONE'))->get();
            foreach ($modelHasRole as $key => $value) {
                array_push($userIds, $value->model_id);
            }
            return $userIds;
        }catch (\Exception $e) {
            return [];
        }
    }

    /** Get all participants
     * @return json
     */
    public static function getParticipants($meetingId)
    {
        try{
            $meetings = self::find($meetingId);
            $participants = explode(',', $meetings->participants);
            $users = \App\User::find($participants);
            $response['html'] = view('auth.scheduleMeeting.showParticipants')->with([
                'users' => $users,
                'count' => $users->count(),
            ])->render();
            return response()->json($response, 200);
        }catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Delete Meeting
     * @param int $meetingId
     * @return json
     */
    public static function deleteMeeting($meetingId)
    {
        try{
            $meeting = self::find($meetingId);
            $ripId = $meeting->rips_id;
            $deleteZoomMeeting = Zoom::deleteZoomMeeting($meeting->meeting_id, $meeting->host_id); //Zoom Api for delete meeting
            self::sendMeetingMail($meetingId, false);
            $meeting->delete();
            $meetings = self::where('rips_id', $ripId)->orderBy('id', 'desc')->get();
             $response['html'] = view('auth.scheduleMeeting.meetingList')->with([
                'meetings' => $meetings,
                'count' => $meetings->count(),
            ])->render();
            $response['count'] = $meetings->count();
            return response()->json($response, 200);
        }catch (\Exception $e) {
            return response()->json($e->getMessage(), 500);
        }
    }

    /** Send/Cancel Meeting mail
     * @param int $meetingId
     * @param boolean $is_schedule
     * @return json
     */
    private static function sendMeetingMail($meetingId, $is_schedule = true)
    {
        $meeting = self::find($meetingId);
        $action = \Config::get('magicNumbers.MEETINGMAILACTION');
        $creater = User::find($meeting->created_by);
        $rip = Rip::find($meeting->rips_id);
        $msg = $is_schedule ? \Config::get('setting.notification.meeting_schedule') : \Config::get('setting.notification.meeting_cancel') ;
        $subject = $is_schedule ? \Config::get('setting.notification.meeting_schedule_subject') : \Config::get('setting.notification.meeting_cancel_subject') ;
        $company = Company::find($meeting->companies_id);
        $userObj = new User();
        $participants = $userObj->getNotifyUserOthenThenLogedIn(explode(',', $meeting->participants));
        $date_time = $meeting->meeting_date.' '.$meeting->meeting_time;
        $meeting_start_time = new Carbon($date_time);   // Meeting Start time
        
        $meeting_time = new Carbon($date_time);
        $end_time = $meeting_time->addMinutes($meeting->duration);  // Meeting End time
        
        // Meeting description for calendar
        $meeting_desc = self::meetingDescription($creater, $meeting->join_url, $rip, $date_time);
        
        // Create link for add to calendar with meeting details
        $link = Link::create($meeting->agenda, $meeting_start_time, $end_time)
        ->description($meeting_desc);

        // Generate a link to create an event on Google calendar
        $google_url = $link->google();
        $meeting['google_calendar_url'] = $google_url;

        // Generate a data uri for an ics file (for iCal & Outlook)
        $ical_url = $link->ics();
        $meeting['ical_calendar_url'] = $ical_url;
        
        User::whereIn('id', $participants)->each(function($user) use($creater, $meeting, $rip, $company, $msg, $subject){
            $mailInfo  = ['creater' => $creater, 'meeting' => $meeting, 'rip'=>$rip, 'company' => $company, 'user' => $user, 'msg' => $msg];
            \Mail::to($user->email)->send(new ZoomEmail($mailInfo, $subject));
        });
    }
    
    /**
     * Function to get meeting description
     * @param type $url
     * @param type $agenda
     * @param type $start_time
     * @param type $end_time
     * @return string
     */
    public static function meetingDescription($creator, $url, $rip, $date_time) {
        $meeting_desc = '';
        $meeting_desc .= $creator->name.config('setting.notification.scheduled_meeting');
        $meeting_desc .= '<br><br><br>'.config('setting.notification.topic').$rip->name;
        $meeting_desc .= '<br>'.config('setting.notification.time').date("M d, Y g:i a", strtotime($date_time));
        $meeting_desc .= '<br><br><br>'.config('setting.notification.join_meeting').'<br>'.$url;
        return $meeting_desc;
    }
}
