<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnnouncementMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * The object instance.
     *
     * @var 
     */
    public $mailInfo;
    public $subject;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($mailInfo, $subject)
    {
        $this->mailInfo = $mailInfo;
        $this->subject = $subject;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject($this->subject)
        ->view('email.announcementEmail')
        ->with($this->mailInfo);
    }
}
