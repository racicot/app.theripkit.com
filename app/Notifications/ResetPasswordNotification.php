<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class ResetPasswordNotification extends Notification
{
    use Queueable;


    public $token;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $user = User::where('email', $notifiable->email)->first();
        $userArray = $user->toArray();
        $name = isset($userArray['name']) ? $userArray['name'] : '';
        
        return (new MailMessage)
            ->view('email.resetPasswordEmail', ['data' =>  $this->token, 'email' => encrypt($notifiable->email), 'name' => $name])
            ->subject('RIPKIT: Reset Password');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->token;
    }
}
