<?php

namespace App\Providers;

use App\Model\Company;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class CustomValidationProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('parent', function ($attributes, $value, $parameters, $validator) {
            if (auth()->user()->can('Update Company Parents')) {
                if ($value == null) {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });

        Validator::extendImplicit('owner', function ($attributes, $value, $parameters, $validator) {
            if (auth()->user()->can('Update Company Parents')) {
                if ($value == '') {
                    return false;
                } else {
                    return true;
                }
            } else {
                return true;
            }
        });

        Validator::extend('duplicate', function ($attributes, $value, $parameters, $validator) {
            $path = request()->file('csv')->getRealPath();
            Excel::load($path, function ($reader) use ($value, $validator) {
                $rows = $reader->get();
                $duplicateKeys = $rows->where('email', $value)->keys()->toArray();
                if (count($duplicateKeys) > 1 && (session('key') == ($duplicateKeys[0] + 1))) {
                    $duplicateKeys = array_map(function ($arr) {
                        return ($arr + 1);
                    }, $duplicateKeys);
                    $message = 'Error: At line ' . session('key') . '. Duplicate email \'' . $value . '\' in line(s) - ' . implode(', ',
                            $duplicateKeys);
                    $validator->errors()->add($value, $message);
                }
            });
            return true;
        });

        Validator::extend('multiple', function ($attributes, $value, $parameters, $validator) {
            $msg = array();
            $companyArray = Company::all()->pluck('name')->toArray();
            foreach (explode(config('setting.csv_actions.csv_deliminator'), $value) as $item) {
                if (!in_array($item, $companyArray)) {
                    $msg[] = $item;
                }
            }
            if (count($msg)) {
                $message = 'Error: At line ' . session('key') . '. The company/companies - \'' . implode(',',
                        $msg) . '\' does not exist.';
                $validator->errors()->add($value, $message);
                return false;
            } else {
                return true;
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {

    }
}
