<?php

namespace App\Providers;

use App\Model\SocialAccount;
use Illuminate\Support\Facades\Storage;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Model\Company;
use App\User;

class SocialAccountService
{
    public function createOrGetUser(ProviderUser $providerUser, $provider)
    {
        $account = \App\Model\SocialAccount::whereProvider($provider)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            $this->setUserImage($providerUser);
            return $account->user;
        } else {
            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $provider
            ]);
            $user = \App\User::whereEmail($providerUser->getEmail())->first();
            if (!$user) {
                return 0;
            }
            $this->setUserImage($providerUser);
            $account->user()->associate($user);
            $account->save();
            return $user;
        }

    }

    /**
     * @param ProviderUser $providerUser
     */
    private function setUserImage(ProviderUser $providerUser)
    {
        $user = \App\User::whereEmail($providerUser->getEmail())->first();
        $image_url = $providerUser->avatar_original;
        $image_content = file_get_contents($image_url);
        $image_name = str_random(3) . '.jpg';
        $check = \Storage::disk('system_photos')->exists($user->image);
        if ($check) {
            \Storage::disk('system_photos')->delete($user->image);
        }
        \Storage::disk('system_photos')->put($image_name, $image_content);
        $user->image = $image_name;
        $user->save();
    }
}